import { useSelector } from 'react-redux';
import get from 'lodash/get';

function useUser() {
  const token = useSelector((state) => state.auth.token);
  const isAuthenticated = useSelector((state) => state.auth.token !== null);
  const user = useSelector((state) => state.auth.user);

  const isAdministrador = () => {
    return get(user, 'is_administrador', false);
  };

  const isPlanPremium = () => {
    return get(user, 'suscripcion.is_premium', false);
  };

  const bePlanPremium = () => {
    return get(user, 'suscripcion.be_premium', false);
  };

  const isSuscripcionActiva = () => {
    return get(user, 'suscripcion.is_activo', false);
  };

  const isSuscripcionPagada = () => {
    return get(user, 'suscripcion.is_pagado', false);
  };

  const canWrite = () => {
    return get(user, 'can_write', false);
  };

  return {
    ...user,
    token,
    isAuthenticated,
    isAdministrador,
    isPlanPremium,
    bePlanPremium,
    isSuscripcionActiva,
    isSuscripcionPagada,
    canWrite
  };
}

export default useUser;
