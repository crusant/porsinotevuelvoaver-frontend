import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Title from '../Title/Title';
import User from '../User/User';
import IconReturn from '../UI/Icons/IconReturn/IconReturn';
import styles from './Header.module.scss';

const Header = (props) => (
  <header className={styles.Header}>
    <div className={styles.BackContainer}>
      <Link className={styles.Back} to="/">
        <IconReturn className={styles.Icon} height="16px" /> Regresar al menú principal
      </Link>
    </div>
    <div className={styles.TitleContainer}>
      <Title>{props.title}</Title>
    </div>
    <div className={styles.UserContainer}>
      <User />
    </div>
  </header>
);

Header.propTypes = {
  title: PropTypes.string.isRequired
};

export default Header;
