import React from 'react';
import PropTypes from 'prop-types';
import Paragraph from '../UI/Paragraph/Paragraph';
import Button from '../UI/Button/Button';
import IconSoundWaves from '../UI/Icons/IconSoundWaves/IconSoundWaves';
import IconClose from '../UI/Icons/IconClose/IconClose';
import * as helpers from '../../helpers';
import styles from './SelectedAudio.module.scss';

const SelectedAudio = (props) => (
  <div className={styles.SelectedAudio}>
    {helpers.isFile(props.audio) ? (
      <div className={styles.Info}>
        <IconSoundWaves className={styles.Icon} width="30px" />
        <div className={styles.Text}>{helpers.truncate(props.audio.name, 12)}</div>
      </div>
    ) : helpers.isBlob(props.audio) ? (
      <audio className={styles.Audio} controls preload="auto">
        <source src={URL.createObjectURL(props.audio)} />
        Lo sentimos su navegador no puede reproducir audio, le sugerimos cambiar a la última versión de Chrome o Firefox.
      </audio>
    ) : <Paragraph>Audio sin nombre.</Paragraph>}
    <Button
      variant="danger"
      align={helpers.isFile(props.audio) ? 'right' : helpers.isBlob(props.audio) ? 'center' : 'right'}
      className={styles.Button}
      small
      onClick={props.onRemove}
    >
      <IconClose height="14px" /> Quitar audio
    </Button>
  </div>
);

SelectedAudio.propTypes = {
  audio: PropTypes.oneOfType([
    PropTypes.instanceOf(File),
    PropTypes.instanceOf(Blob)
  ]).isRequired,
  onRemove: PropTypes.func.isRequired
};

export default SelectedAudio;
