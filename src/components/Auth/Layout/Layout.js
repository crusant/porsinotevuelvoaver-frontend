import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import classNames from 'classnames';
import logotipo from '../../../assets/images/logotipo.png';
import * as actions from '../../../actions';
import styles from './Layout.module.scss';

const Layout = (props) => {
  const avisoPrivacidad = useSelector((state) => state.avisoPrivacidad.avisoPrivacidad);
  const terminosCondiciones = useSelector((state) => state.terminosCondiciones.terminosCondiciones);
  const dispatch = useDispatch();

  React.useEffect(() => {
    (async () => {
      await dispatch(actions.getAvisoPrivacidad());
      await dispatch(actions.getTerminosCondiciones());
    })();

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      <div className={styles.Register}>
        <div className={styles.Left}>
          <Link className={styles.Logo} to="/">
            <img src={logotipo} className={styles.Image} alt="Logotipo de Por si no te vuelvo a ver" />
          </Link>
          <div className={styles.Content}>
            {props.children(styles)}
          </div>
          <div className={styles.Copyright}>&copy;{new Date().getFullYear()} Por si no te vuelvo a ver. Todos los derechos reservados. <a href={avisoPrivacidad.documento_digital} rel="noopener noreferrer" target="_blank">{avisoPrivacidad.nombre}</a>. <a href={terminosCondiciones.documento_digital} rel="noopener noreferrer" target="_blank">{terminosCondiciones.nombre}</a>.</div>
        </div>
        <div className={styles.Right}>
          <div className={styles.Content}>
            {{
              'login': (
                <React.Fragment>
                  <h1 className={classNames(styles.Title, styles.Center)}>Bienvenido</h1>
                  <p className={styles.Paragraph}>En <strong>Por si no te vuelvo a ver</strong> te sugerimos mantener siempre actualizada la información de contacto, tanto de tu perfil, como de los custodios y la red familiar. De esto depende el éxito y buen desempeño de la <strong>Prueba de Vida</strong>.</p>
                  <p className={styles.Paragraph}>Si cuentas con el <strong>Servicio Premium</strong>, podrás llevar a cabo los cambios que consideres necesarios en tu perfil y cuestionarios, para mantenerlos siempre actualizados.</p>
                  <p className={styles.Paragraph}>Te sugerimos la elaboración y registro ante notario de un testamento, el cual dará certeza jurídica a tus deseos y voluntad.</p>
                  <p className={styles.Paragraph}>Para cualquier duda o aclaración ponte en contacto enviando un correo electrónico a <a href="mailto:contacto@porsinotevuelvoaver.com" className={styles.Link}>contacto@porsinotevuelvoaver.com</a>.</p>
                </React.Fragment>
              ),
              'register': (
                <React.Fragment>
                  <h1 className={styles.Title}>Crea tu cuenta en 7 pasos</h1>
                  <ol className={styles.List}>
                    <li className={styles.Item}>Ingresa tu nombre, número de celular y correo electrónico.</li>
                    <li className={styles.Item}>Crea una contraseña.</li>
                    <li className={styles.Item}>Acepta la política de privacidad, así como los términos y condiciones.</li>
                    <li className={styles.Item}>Selecciona la casilla <strong>Yo no soy un robot</strong> en recaptcha y registra los datos.</li>
                    <li className={styles.Item}>Si no existe ningún problema, recibirás un mensaje en tu correo electrónico con un enlace para continuar con tu registro.</li>
                    <li className={styles.Item}>Al dar click en el enlace, este te dirigirá a una página donde podrás seleccionar el tipo de <strong>Plan de Servicio</strong> que se adapte a tus necesidades, <strong>Premium</strong> o <strong>Gratuito</strong>.</li>
                    <li className={styles.Item}>Si escoges el <strong>Servicio Premium</strong> serás dirigido a las diferentes alternativas de pago, en caso de escoger el <strong>Servicio Gratuito</strong>, habrás concluido con el proceso.</li>
                  </ol>
                </React.Fragment>
              )
            }[props.page]}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

Layout.propTypes = {
  page: PropTypes.string
};

export default Layout;
