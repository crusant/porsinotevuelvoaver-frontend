import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import classNames from 'classnames';
import _ from 'lodash';
import scriptLoader from 'react-async-script-loader';
import * as actions from '../../../actions';
import IconPayPal from '../../UI/Icons/IconPayPal/IconPayPal';
import styles from './PayPal.module.scss';

const PayPal = (props) => {
  const history = useHistory();
  const [isLoadingButton, setIsLoadingButton] = React.useState(true);
  const paypalRef = React.useRef();
  const isAuthenticated = useSelector((state) => state.auth.token !== null);
  const isPayingWithPayPal = useSelector((state) => state.auth.isPayingWithPayPal);
  const dispatch = useDispatch();

  React.useEffect(() => {
    const { isScriptLoaded, isScriptLoadSucceed } = props;

    if (isScriptLoaded && isScriptLoadSucceed) {
      window.paypal.Buttons({
        createOrder: async () => {
          const response = await dispatch(actions.paypalCreateOrder(props.plan));

          return _.get(response, 'data.result.id', null);
        },
        onApprove: async ({ orderID }) => {
          const data = {
            order_id: orderID,
            plan_id: props.plan
          };

          if (isAuthenticated && !await dispatch(actions.payWithPayPalAndRenew(data))) {
            return;
          }

          if (!isAuthenticated && !await dispatch(actions.payWithPayPalAndRegister(props.url, data))) {
            return;
          }

          return history.push('/');
        },
        onError: () => {}
      }).render(paypalRef.current)
        .then(() => setIsLoadingButton(false));
    }

    // eslint-disable-next-line
  }, [props.isScriptLoaded, props.isScriptLoadSucceed]);

  return (
    <>
      <div className={classNames(styles.Loading, { [styles.Show]: isLoadingButton })}>Cargando botón de <IconPayPal height="24px" /></div>
      <div className={classNames(styles.PayPal, { [styles.Hide]: isPayingWithPayPal || isLoadingButton })} ref={paypalRef}></div>
      <div className={classNames(styles.Loading, { [styles.Show]: isPayingWithPayPal })}>Registrándote…</div>
    </>
  );
};

PayPal.propTypes = {
  plan: PropTypes.number,
  url: PropTypes.any
};

export default scriptLoader(`https://www.paypal.com/sdk/js?client-id=AddCUw-GvS7cTeXbY4I2gp6OmHDfu8MNoez70yHO529ix36eIaQ_a0IJfWeXqThfUPfmuXeZ1s84gRtM&disable-funding=credit,card&currency=MXN`)(PayPal);
