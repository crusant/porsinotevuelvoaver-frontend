import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import scriptLoader from 'react-async-script-loader';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import Paragraph from '../../UI/Paragraph/Paragraph';
import Button from '../../UI/Button/Button';
import IconOxxo from '../../UI/Icons/IconOxxo/IconOxxo';
import * as actions from '../../../actions';
import * as helpers from '../../../helpers';
import styles from './Oxxo.module.scss';

const Oxxo = (props) => {
  const history = useHistory();
  const mercadoPago = React.useRef(null);
  const isAuthenticated = useSelector((state) => state.auth.token !== null);
  const ticket = useSelector((state) => state.auth.ticket);
  const isPayingWithOxxo = useSelector((state) => state.auth.isPayingWithOxxo);
  const isPaidWithOxxo = useSelector((state) => state.auth.isPaidWithOxxo);
  const dispatch = useDispatch();

  const onPay = (event) => {
    event.preventDefault();

    mercadoPago.current.createToken(event.target, async (status, response) => {
      if (status !== 200 && status !== 201) {
        console.log('verify filled data');
      } else {
        const data = {
          plan_id: props.plan
        };

        if (isAuthenticated) {
          return dispatch(actions.payWithOxxoAndRenew(data));
        }

        return dispatch(actions.payWithOxxoAndRegister(props.url, data));
      }
    });
  };

  React.useEffect(() => {
    if (isPaidWithOxxo && !isAuthenticated) {
      history.push('/registrarse');
    }

    if (isPaidWithOxxo && isAuthenticated) {
      history.push('/');
    }

    // eslint-disable-next-line
  }, [isPaidWithOxxo]);

  React.useEffect(() => {
    if (!helpers.isEmptyString(ticket)) {
      window.open(ticket, '_blank');
    }
  }, [ticket]);

  React.useEffect(() => {
    const { isScriptLoaded, isScriptLoadSucceed } = props;

    if (isScriptLoaded && isScriptLoadSucceed) {
      mercadoPago.current = window.Mercadopago;
      mercadoPago.current.setPublishableKey('APP_USR-1b5258e0-eb05-4221-bcd1-d397a87b3e21');
    }

    // eslint-disable-next-line
  }, [props.isScriptLoaded, props.isScriptLoadSucceed]);

  return (
    <React.Fragment>
      <Form className={styles.Form} onSubmit={onPay}>
        <input type="hidden" name="paymentMethodId" value="oxxo" />
        <Button
          type="submit"
          variant="secondary"
          className={styles.Button}
          disabled={isPayingWithOxxo}
        >
          {isPayingWithOxxo ? 'Generando referencia de pago…' : (<>Pagar en <IconOxxo viewBox="0 0 956.69287 519.80316" className={styles.Icon} height="34" /></>)}
        </Button>
      </Form>
      <Alert variant="warning">
        <Paragraph>Si selecciona pagar con OXXO, su pago será acreditado de 1 a 2 días hábiles.</Paragraph>
      </Alert>
    </React.Fragment>
  );
};

Oxxo.propTypes = {
  plan: PropTypes.number,
  url: PropTypes.any
};

export default scriptLoader('https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js')(Oxxo);
