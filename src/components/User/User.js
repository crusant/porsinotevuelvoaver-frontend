import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import NotificationsNuevosUsuarios from '../Notifications/NotificationsNuevosUsuarios/NotificationsNuevosUsuarios';
import NotificationsLoader from '../Notifications/NotificationsLoader/NotificationsLoader';
import NotificationPruebaVida from '../Notifications/NotificationPruebaVida/NotificationPruebaVida';
import NotificationsEmpty from '../Notifications/NotificationsEmpty/NotificationsEmpty';
import IconBell from '../UI/Icons/IconBell/IconBell';
import IconMale from '../UI/Icons/IconMale/IconMale';
import useUser from '../../hooks/useUser';
import * as actions from '../../actions';
import styles from './User.module.scss';

const User = () => {
  const { isAdministrador, ...user } = useUser();
  const { pruebaVida, countUsers } = useSelector((state) => state.notificaciones.notificaciones);
  const isGetting = useSelector((state) => state.notificaciones.isGetting);
  const dispatch = useDispatch();

  const isNotificationPruebaVida = () => {
    return (!_.isEmpty(pruebaVida.fecha_limite) && !pruebaVida.fecha_respuesta && !pruebaVida.is_expired);
  };

  const onLogout = (event) => {
    event.preventDefault();

    if (window.confirm('¿Está seguro(a) que desea cerrar sesión?')) {
      dispatch(actions.logout());
    }
  };

  React.useEffect(() => {
    dispatch(actions.getNotificaciones());

    // eslint-disable-next-line
  }, []);

  return (
    <div className={styles.User}>
      <div className={styles.Notifications}>
        <IconBell height="25px" className={styles.Icon} />
        <div className={styles.Menu}>
          <div className={styles.Content}>
            {isGetting ? (
              <NotificationsLoader />
            ) : (
              (!isAdministrador() && isNotificationPruebaVida()) ? (
                <NotificationPruebaVida pruebaVida={pruebaVida} />
              ) : (isAdministrador() && countUsers > 0) ? (
                <NotificationsNuevosUsuarios count={countUsers} />
              ) : (
                <NotificationsEmpty />
              )
            )}
          </div>
        </div>
      </div>
      <div className={styles.Info}>
        <div className={styles.Circle}>
          <IconMale height="30px" className={styles.Icon} />
        </div>
        <div className={styles.Menu}>
          <div className={styles.Content}>
            <div className={styles.FullName}>{user.perfil.nombre_completo}</div>
            {!isAdministrador() && (
              <React.Fragment>
                <div className={styles.Email}>{user.email}</div>
                <div className={styles.Plan}>{user.suscripcion.plan.nombre}</div>
              </React.Fragment>
            )}
            <div className={styles.Separator}></div>
            <Link to="/cambiar-contrasena" className={styles.Action}>Cambiar contraseña</Link>
            {!isAdministrador() && (
              <a href="!#" className={styles.Action}>Primeros pasos</a>
            )}
            <a href="!#" className={styles.Action} onClick={onLogout}>Cerrar sesión</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default User;
