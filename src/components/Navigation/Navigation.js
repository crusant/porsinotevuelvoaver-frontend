import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import classNames from 'classnames';
import Logo from '../Logo/Logo';
import IconCRM from '../UI/Icons/IconCRM/IconCRM';
import IconMen from '../UI/Icons/IconMen/IconMen';
import IconExpiratedDate from '../UI/Icons/IconExpiratedDate/IconExpiratedDate';
import IconShield from '../UI/Icons/IconShield/IconShield';
import IconPayment from '../UI/Icons/IconPayment/IconPayment';
import IconWork from '../UI/Icons/IconWork/IconWork';
import IconWishList from '../UI/Icons/IconWishList/IconWishList';
import IconPet from '../UI/Icons/IconPet/IconPet';
import IconLetter from '../UI/Icons/IconLetter/IconLetter';
import IconSocialMedia from '../UI/Icons/IconSocialMedia/IconSocialMedia';
import IconTime from '../UI/Icons/IconTime/IconTime';
import IconHistory from '../UI/Icons/IconHistory/IconHistory';
import IconGabinet from '../UI/Icons/IconGabinet/IconGabinet';
import IconHealthy from '../../components/UI/Icons/IconHealthy/IconHealthy';
import IconMessage from '../../components/UI/Icons/IconMessage/IconMenssage';
import IconHandHeart from '../../components/UI/Icons/IconHandHeart/IconHandHeart';
import IconLocked from '../UI/Icons/IconLocked/IconLocked';
import IconContract from '../UI/Icons/IconContract/IconContract';
import IconArrowLeft from '../UI/Icons/IconArrowLeft/IconArrowLeft';
import IconArrowRight from '../UI/Icons/IconArrowRight/IconArrowRight';
import IconDownload from '../UI/Icons/IconDownload/IconDownload';
import useUser from '../../hooks/useUser';
import { API_URL } from '../../constants';
import styles from './Navigation.module.scss';

const Navigation = () => {
  const {
    token,
    isAdministrador,
    isPlanPremium,
    isSuscripcionActiva
  } = useUser();

  const [isToggle, setIsToogle] = React.useState(false);

  const onToggle = () => setIsToogle(!isToggle);

  return (
    <nav className={classNames(styles.Navigation, {[styles.isShow]: isToggle})}>
      <div className={styles.Container}>
        <Link to="/">
          <Logo className={styles.Logo} />
        </Link>
        <div className={styles.Menu}>
          {isAdministrador() ? (
            <React.Fragment>
              <NavLink className={styles.Item} exact activeClassName={styles.isActive} to="/configuracion-de-envio-de-pruebas-de-vida">
                <IconHealthy className={styles.Icon} width="30px" /> Configuración de envío de pruebas de vida
              </NavLink>
              <NavLink className={styles.Item} exact activeClassName={styles.isActive} to="/configuracion-de-tiempo-de-respuesta-a-pruebas-de-vida">
                <IconMessage className={styles.Icon} width="30px" /> Configuración de tiempo de respuesta a pruebas de vida
              </NavLink>
              <NavLink className={styles.Item} exact activeClassName={styles.isActive} to="/configuracion-de-tiempo-de-respuesta-de-pruebas-de-vida-a-custodios">
                <IconHandHeart className={styles.Icon} width="30px" /> Configuración de tiempo de respuesta de pruebas de vida a custodios
              </NavLink>
              <NavLink className={styles.Item} exact activeClassName={styles.isActive} to="/usuarios-registrados">
                <IconMen className={styles.Icon} width="30px" /> Usuarios registrados
              </NavLink>
              <NavLink className={styles.Item} exact activeClassName={styles.isActive} to="/enlaces-expirados">
                <IconExpiratedDate className={styles.Icon} width="30px" /> Enlaces expirados
              </NavLink>
              <NavLink className={styles.Item} exact activeClassName={styles.isActive} to="/actualizar-aviso-de-privacidad">
                <IconLocked className={styles.Icon} width="30px" /> Actualizar aviso de privacidad
              </NavLink>
              <NavLink className={styles.Item} exact activeClassName={styles.isActive} to="/actualizar-terminos-y-condiciones">
                <IconContract className={styles.Icon} width="30px" /> Actualizar términos y condiciones
              </NavLink>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <NavLink className={styles.Item} exact activeClassName={styles.isActive} to="/datos-personales-custodios-y-red-familiar">
                <IconCRM className={styles.Icon} width="30px" /> Datos personales, custodios y red familiar
              </NavLink>
              <NavLink className={styles.Item} exact activeClassName={styles.isActive} to="/asesores-y-contactos">
                <IconMen className={styles.Icon} width="30px" /> Asesores y contactos
              </NavLink>
              <NavLink className={classNames(styles.Item, { [styles.isDisabled]: !isPlanPremium() || !isSuscripcionActiva() })} exact activeClassName={styles.isActive} to="/polizas-de-seguros">
                <IconShield className={styles.Icon} width="30px" /> Pólizas de seguros
              </NavLink>
              <NavLink className={classNames(styles.Item, { [styles.isDisabled]: !isPlanPremium() || !isSuscripcionActiva() })} exact activeClassName={styles.isActive} to="/patrimonio">
                <IconPayment className={styles.Icon} width="30px" /> Patrimonio
              </NavLink>
              <NavLink className={classNames(styles.Item, { [styles.isDisabled]: !isPlanPremium() || !isSuscripcionActiva() })} exact activeClassName={styles.isActive} to="/empleo-y-pension">
                <IconWork className={styles.Icon} width="30px" /> Empleo y pensión
              </NavLink>
              <NavLink className={classNames(styles.Item, { [styles.isDisabled]: !isPlanPremium() || !isSuscripcionActiva() })} exact activeClassName={styles.isActive} to="/deseos-postumos">
                <IconWishList className={styles.Icon} width="30px" /> Deseos póstumos
              </NavLink>
              <NavLink className={classNames(styles.Item, { [styles.isDisabled]: !isPlanPremium() || !isSuscripcionActiva() })} exact activeClassName={styles.isActive} to="/mascotas">
                <IconPet className={styles.Icon} width="30px" /> Mascotas
              </NavLink>
              <NavLink className={styles.Item} exact activeClassName={styles.isActive} to="/instrucciones-particulares">
                <IconLetter className={styles.Icon} width="30px" /> Instrucciones a custodios
              </NavLink>
              <NavLink className={classNames(styles.Item, { [styles.isDisabled]: !isPlanPremium() || !isSuscripcionActiva() })} exact activeClassName={styles.isActive} to="/correo-y-redes-sociales">
                <IconSocialMedia className={styles.Icon} width="30px" /> Correo y redes sociales
              </NavLink>
              <NavLink className={classNames(styles.Item, { [styles.isDisabled]: !isPlanPremium() || !isSuscripcionActiva() })} exact activeClassName={styles.isActive} to="/mensajes-postumos">
                <IconTime className={styles.Icon} width="30px" /> Mensajes póstumos
              </NavLink>
              <NavLink className={classNames(styles.Item, { [styles.isDisabled]: !isPlanPremium() || !isSuscripcionActiva() })} exact activeClassName={styles.isActive} to="/mi-historia-y-legado">
                <IconHistory className={styles.Icon} width="30px" /> Mi historia y legado
              </NavLink>
              <NavLink className={classNames(styles.Item, { [styles.isDisabled]: !isPlanPremium() || !isSuscripcionActiva() })} exact activeClassName={styles.isActive} to="/documentos-digitales-y-ubicacion-fisica">
                <IconGabinet className={styles.Icon} width="30px" /> Documentos digitales y ubicación física
              </NavLink>
              <a
                href={isPlanPremium() ? `${API_URL}aplicacion/formularios?token=${token}` : '!#'}
                className={classNames(styles.Item, { [styles.isDisabled]: !isPlanPremium() || !isSuscripcionActiva() })}
                target="_blank"
                rel="noopener noreferrer"
              >
                <IconDownload className={styles.Icon} width="30px" /> Descargar formularios
              </a>
            </React.Fragment>
          )}
        </div>
      </div>
      <button className={styles.Toggle} onClick={onToggle}>
        {isToggle ? (
          <IconArrowLeft className={styles.Icon} height="14px" />
        ) : (
          <IconArrowRight className={styles.Icon} height="14px" />
        )}
      </button>
    </nav>
  );
};

export default Navigation;
