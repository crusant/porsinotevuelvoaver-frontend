import React from 'react';
import classNames from 'classnames';
import _ from 'lodash';
import IconLove from '../../UI/Icons/IconLove/IconLove';
import styles from './EstatusPruebaVidaUsuarioAndCustodio.module.scss';

const EstatusPruebaVidaUsuarioAndCustodio = ({ pruebaVidaUsuario }) => {
  const pruebaVidaCustodio = _.get(pruebaVidaUsuario, 'prueba_vida_custodio', {});
  const hasPruebaVidaUsuarioFechaRespuesta = !_.isNull(_.get(pruebaVidaUsuario, 'fecha_respuesta', null));
  const isExpiredPruebaVidaUsuario = _.get(pruebaVidaUsuario, 'is_expired', true);
  const hasPruebaVidaCustodioFechaRespuesta = !_.isNull(_.get(pruebaVidaCustodio, 'fecha_respuesta', null));
  const isExpiredPruebaVidaCustodio = _.get(pruebaVidaCustodio, 'is_expired', true);

  if (_.isNull(pruebaVidaUsuario)) {
    return null;
  }

  if (hasPruebaVidaUsuarioFechaRespuesta || hasPruebaVidaCustodioFechaRespuesta) {
    return <IconLove className={classNames(styles.EstatusPruebaVidaUsuarioAndCustodio, styles.Success)} />;
  }

  if ((isExpiredPruebaVidaUsuario && !hasPruebaVidaUsuarioFechaRespuesta)
   && (isExpiredPruebaVidaCustodio && !hasPruebaVidaCustodioFechaRespuesta)
  ) {
    return <IconLove className={classNames(styles.EstatusPruebaVidaUsuarioAndCustodio, styles.Danger)} />;
  }

  return <IconLove className={classNames(styles.EstatusPruebaVidaUsuarioAndCustodio, styles.Warning)} />
};

export default EstatusPruebaVidaUsuarioAndCustodio;
