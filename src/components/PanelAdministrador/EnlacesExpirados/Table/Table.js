import React from 'react';
import { useTable, usePagination } from 'react-table';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import IconArrowLeft from '../../../UI/Icons/IconArrowLeft/IconArrowLeft';
import IconArrowRight from '../../../UI/Icons/IconArrowRight/IconArrowRight';
import styles from './Table.module.scss';

const Table = ({ columns, data, isLoading }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    state: {
      pageIndex
    },
  } = useTable({
    columns,
    data,
    initialState: {
      pageIndex: 0
    }
  }, usePagination);

  return (
    <div className={styles.Table}>
      <FormTable {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render('Header')}</th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row) => {
            prepareRow(row);

            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                })}
              </tr>
            )
          })}
          {isLoading && (
            <tr>
              <td colSpan={10000}>Cargando...</td>
            </tr>
          )}
        </tbody>
      </FormTable>
      <div className={styles.Footer}>
        <div className={styles.Info}>
          Página <strong>{pageIndex + 1} de {pageOptions.length}</strong>
        </div>
        <div className={styles.Pagination}>
          <button
            type="button"
            className={styles.Button}
            disabled={!canPreviousPage}
            onClick={() => gotoPage(0)}
          >
            <IconArrowLeft className={styles.Icon} />
            <IconArrowLeft className={styles.Icon} />
          </button>
          <button
            type="button"
            className={styles.Button}
            disabled={!canPreviousPage}
            onClick={() => previousPage()}
          >
            <IconArrowLeft className={styles.Icon} />
          </button>
          <button
            type="button"
            className={styles.Button}
            disabled={!canNextPage}
            onClick={() => nextPage()}
          >
            <IconArrowRight className={styles.Icon} />
          </button>
          <button
            type="button"
            className={styles.Button}
            disabled={!canNextPage}
            onClick={() => gotoPage(pageCount - 1)}
          >
            <IconArrowRight className={styles.Icon} />
            <IconArrowRight className={styles.Icon} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Table;
