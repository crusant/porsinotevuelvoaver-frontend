import React from 'react';
import classNames from 'classnames';
import moment from 'moment';
import _ from 'lodash';
import IconLove from '../../UI/Icons/IconLove/IconLove';
import styles from './EstatusPruebaVidaCustodio.module.scss';

const EstatusPruebaVidaCustodio = (props) => {
  if (!!_.isNull(props.pruebaVida)) {
    return null;
  }

  const { fecha_envio, fecha_respuesta, is_expired } = props.pruebaVida;
  const hasFechaRespuesta = !_.isNull(fecha_respuesta);
  const fechaEnvio =  moment(fecha_envio).format('LL');
  const fechaRespuesta = moment(fecha_respuesta).format('LL [a las] HH:mm');

  if (is_expired && !hasFechaRespuesta) {
    return (
      <span className={classNames(styles.EstatusPruebaVidaCustodio, styles.Danger)}>
        <IconLove className={styles.Icon} /> El custodio no respondió a la prueba de vida
      </span>
    );
  }

  if (!hasFechaRespuesta) {
    return (
      <span className={classNames(styles.EstatusPruebaVidaCustodio, styles.Warning)}>
        <IconLove className={styles.Icon} /> La prueba de vida se envió al custodio el {fechaEnvio}
      </span>
    );
  }

  return (
    <span className={classNames(styles.EstatusPruebaVidaCustodio, styles.Success)}>
      <IconLove className={styles.Icon} /> La prueba de vida se envió al custodio el {fechaEnvio} y se recibió el {fechaRespuesta} hrs
    </span>
  );
};

export default EstatusPruebaVidaCustodio;
