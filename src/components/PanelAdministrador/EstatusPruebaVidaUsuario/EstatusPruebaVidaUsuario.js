import React from 'react';
import classNames from 'classnames';
import moment from 'moment';
import _ from 'lodash';
import IconLove from '../../UI/Icons/IconLove/IconLove';
import styles from './EstatusPruebaVidaUsuario.module.scss';

const EstatusPruebaVidaUsuario = (props) => {
  if (_.isNull(props.pruebaVida)) {
    return null;
  }

  const { fecha_envio, fecha_respuesta, is_expired } = props.pruebaVida;
  const hasFechaRespuesta = !_.isNull(fecha_respuesta);
  const fechaEnvio =  moment(fecha_envio).format('LL');
  const fechaRespuesta = moment(fecha_respuesta).format('LL [a las] HH:mm');


  if (is_expired && !hasFechaRespuesta) {
    return (
      <span className={classNames(styles.EstatusPruebaVidaUsuario, styles.Danger)}>
        <IconLove className={styles.Icon} /> El usuario no respondió a la prueba de vida
      </span>
    );
  }

  if (!hasFechaRespuesta) {
    return (
      <span className={classNames(styles.EstatusPruebaVidaUsuario, styles.Warning)}>
        <IconLove className={styles.Icon} /> La prueba de vida se envió el {fechaEnvio}
      </span>
    );
  }

  return (
    <span className={classNames(styles.EstatusPruebaVidaUsuario, styles.Success)}>
      <IconLove className={styles.Icon} /> La prueba de vida se envío el {fechaEnvio} y se recibió el {fechaRespuesta} hrs
    </span>
  );
};

export default EstatusPruebaVidaUsuario;
