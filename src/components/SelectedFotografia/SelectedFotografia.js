import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Paragraph from '../UI/Paragraph/Paragraph';
import Button from '../UI/Button/Button';
import IconPicture from '../UI/Icons/IconPicture/IconPicture';
import IconClose from '../UI/Icons/IconClose/IconClose';
import * as helpers from '../../helpers';
import styles from './SelectedFotografia.module.scss';

const SelectedFotografia = (props) => (
  <div className={classNames(styles.SelectedFotografia, { [styles.IsFile]: (helpers.isFile(props.fotografia) || !helpers.isBlob(props.fotografia)) })}>
    {helpers.isFile(props.fotografia) ? (
      <div className={styles.Info}>
        <IconPicture className={styles.Icon} width="30px" />
        <div className={styles.Text}>{helpers.truncate(props.fotografia.name, 12)}</div>
      </div>
    ) : helpers.isBlob(props.fotografia) ? (
      <img src={URL.createObjectURL(props.fotografia)} className={styles.Image} alt="Fotografía tomada" />
    ) : <Paragraph>Fotografía sin nombre.</Paragraph>}
    <Button
      variant="danger"
      align={helpers.isFile(props.fotografia) ? 'right' : helpers.isBlob(props.fotografia) ? 'center' : 'right'}
      className={styles.Button}
      small
      onClick={props.onRemove}
    >
      <IconClose height="14px" /> Quitar fotografía
    </Button>
  </div>
);

SelectedFotografia.propTypes = {
  fotografia: PropTypes.oneOfType([
    PropTypes.instanceOf(File),
    PropTypes.instanceOf(Blob)
  ]).isRequired,
  onRemove: PropTypes.func.isRequired
};

export default SelectedFotografia;
