import React from 'react';
import classNames from 'classnames';
import styles from './Title.module.scss';

const Title = ({ className, ...props }) => (
  <h1 className={classNames(styles.Title, className)} {...props}>
    {props.children}
  </h1>
);

export default Title;
