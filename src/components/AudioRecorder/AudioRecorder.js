import React from 'react';
import PropTypes from 'prop-types';
import RecordRTC from 'recordrtc';
import Alert from 'react-bootstrap/Alert';
import Paragraph from '../UI/Paragraph/Paragraph';
import Button from '../UI/Button/Button';
import IconPlay from '../UI/Icons/IconPlay/IconPlay';
import IconRecord from '../UI/Icons/IconRecord/IconRecord';
import IconMute from '../UI/Icons/IconMute/IconMute';
import IconStop from '../UI/Icons/IconStop/IconStop';
import * as helpers from '../../helpers';
import styles from './AudioRecorder.module.scss';

const hasGetUserMedia = !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
const isBrowserEdge = navigator.userAgent.indexOf('Edge') !== -1 && (!!navigator.msSaveOrOpenBlob || !!navigator.msSaveBlob);
const isBrowserSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
const isWindowsPlatform = navigator.platform && navigator.platform.toString().toLowerCase().indexOf('win') === -1;

const AudioRecorder = (props) => {
  const recorder = React.useRef(null);
  const stream = React.useRef(null);
  const startedTime = React.useRef(null);
  const timeout = React.useRef(null);
  const sizeRef = React.useState(null);
  const [isCompatible, setIsCompatible] = React.useState(true);
  const [isMicrophoneError, setIsMicrofoneError] = React.useState(false);
  const [duration, setDuration] = React.useState('00:00:00');
  const [size, setSize] = React.useState(0);
  const [isRecording, setIsRecording] = React.useState(false);

  const captureStream = (callback) => {
    if (stream.current) {
      callback(stream.current);
      return;
    }

    navigator.mediaDevices.getUserMedia({
      audio: isBrowserEdge ? true : { echoCancellation: true }
    }).then((value) => {
      callback(value);
    }).catch(() => {
      setIsMicrofoneError(true);
    });
  };

  const onStart = () => {
    setIsRecording(true);

    if (!stream.current) {
      captureStream((value) => {
        stream.current = value;

        const options = {
          type: 'audio',
          numberOfAudioChannels: isBrowserEdge ? 1 : 2,
          checkForInactiveTracks: true,
          bufferSize: 16384,
          timeSlice: 1000
        };

        if (isBrowserEdge || isBrowserSafari) {
          options.recorderType = RecordRTC.StereoAudioRecorder;
        }

        if (isWindowsPlatform) {
          options.sampleRate = 48000;
        }

        if (isBrowserSafari) {
          options.sampleRate = 44100;
          options.bufferSize = 4096;
          options.numberOfAudioChannels = 2;
        }

        recorder.current = RecordRTC(stream.current, options);
        recorder.current.startRecording();
        startedTime.current = new Date().getTime();

        (function looper() {
          if (!recorder.current) {
              return;
          }
          const currentTime = new Date().getTime();
          const duration = helpers.calculateTime(Math.floor((currentTime - startedTime.current) / 1000));
          const internal = recorder.current.getInternalRecorder();

          if (internal && internal.getArrayOfBlobs) {
            const blob = new Blob(internal.getArrayOfBlobs(), {
              type: 'video/webm'
            });

            sizeRef.current = blob.size;
            setSize(blob.size);
          }

          setDuration(duration);

          if (sizeRef.current >= 8000000) {
              onStop();
              return;
          }

          timeout.current = setTimeout(looper, 1000);
        })();
      });
    }
  };

  const onStop = () => {
    if (!recorder.current) {
      return;
    }

    recorder.current.stopRecording(() => {
      setIsRecording(false);
      setDuration('00:00:00');

      props.onRecorded(recorder.current.blob);

      startedTime.current = null;
      clearTimeout(timeout.current);

      if (stream.current) {
        stream.current.stop();
        stream.current = null;
      }

      recorder.current.destroy();
      recorder.current = null;
    });
  };

  React.useEffect(() => {
    setIsCompatible(hasGetUserMedia);
  }, []);

  if (!isCompatible) {
    return (
      <Alert variant="danger">
        <Paragraph>Lo sentimos su navegador no puede grabar audio, le sugerimos cambiar a la última versión de Chrome o Firefox.</Paragraph>
      </Alert>
    );
  }

  return (
    <div className={styles.AudioRecorder}>
      {isMicrophoneError ? (
        <div className={styles.Error}>
          <IconMute className={styles.Icon} /> Lo sentimos no podemos acceder a su micrófono.
        </div>
      ) : (
        <div className={styles.Info}>
          {isRecording && <IconRecord className={styles.Icon} />} {duration} | {RecordRTC.bytesToSize(size)}
        </div>
      )}
      {(isRecording && !isMicrophoneError) ? (
        <Button
          variant="primary"
          className={styles.Button}
          small
          onClick={onStop}
        >
          <IconStop height="14px" /> Terminar grabación
        </Button>
      ) : (
        <Button
          variant="primary"
          className={styles.Button}
          small
          disabled={isMicrophoneError}
          onClick={onStart}
        >
          <IconPlay height="14px" /> Iniciar grabación
        </Button>
      )}
    </div>
  );
};

AudioRecorder.propTypes = {
  onRecorded: PropTypes.func.isRequired
};

export default AudioRecorder;
