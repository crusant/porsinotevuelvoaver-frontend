import React from 'react';
import { useHistory } from 'react-router-dom';
import YouTube from 'react-youtube';
import Modal from 'react-bootstrap/Modal';
import IconClose from '../../UI/Icons/IconClose/IconClose';
import LoadingVideo from '../LoadingVideo/LoadingVideo';
import poster from '../../../assets/images/video-custodio.jpg';
import styles from './VideoCustodio.module.scss';

const VideoCustodio = () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [isShown, setIsShown] = React.useState(true);
  const history = useHistory();

  const hasCustodioHash = (history.location.hash === '#custodio');

  const onHide = () => setIsShown(false);

  const onReady = () => setIsLoading(false);

  return (
    <Modal
      show={hasCustodioHash && isShown}
      size="lg"
      centered
      dialogClassName={styles.Modal}
      onHide={onHide}
    >
      <Modal.Body className={styles.Body}>
        {isLoading && (
          <LoadingVideo
            poster={poster}
            alt="¿Qué es un custodio?"
          />
        )}
        <YouTube
          videoId="6x9tbFXN6kY"
          className={styles.YouTube}
          onReady={onReady}
        />
        <button
          className={styles.Close}
          onClick={onHide}
        >
          <IconClose
            className={styles.Icon}
            height={12}
          />
        </button>
      </Modal.Body>
    </Modal>
  );
};

export default VideoCustodio;
