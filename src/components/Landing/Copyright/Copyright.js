import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import IconFacebookSquare from '../../UI/Icons/IconFacebookSquare/IconFacebookSquare';
import IconInstagramSquare from '../../UI/Icons/IconInstagramSquare/IconInstagramSquare';
import IconYouTubeSquare from '../../UI/Icons/IconYouTubeSquare/IconYouTubeSquare';
import * as actions from '../../../actions';
import styles from './Copyright.module.scss';

const Copyright = (props) => {
  const avisoPrivacidad = useSelector((state) => state.avisoPrivacidad.avisoPrivacidad);
  const terminosCondiciones = useSelector((state) => state.terminosCondiciones.terminosCondiciones);
  const dispatch = useDispatch();

  React.useEffect(() => {
    (async () => {
      await dispatch(actions.getAvisoPrivacidad());
      await dispatch(actions.getTerminosCondiciones());
    })();

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      <Container fluid className={classNames(styles.Container, props.className)}>
        <Row>
          <Col>
            <Container>
              <Row>
                <Col>
                  <div className={styles.Copyright}>
                    <div className={styles.SocialNetwork}>
                      <a
                        href="https://www.facebook.com/porsinotevuelvoavermx"
                        className={styles.Link}
                        rel="noopener noreferrer"
                        target="_blank"
                      >
                        <IconFacebookSquare className={styles.Icon} width="22px" />
                        Facebook
                      </a>
                      <a
                        href="https://www.instagram.com/porsinotevuelvoavermx/"
                        className={styles.Link}
                        rel="noopener noreferrer"
                        target="_blank"
                      >
                        <IconInstagramSquare className={styles.Icon} width="22px" />
                        Instagram
                      </a>
                      <a
                        href="https://www.youtube.com/channel/UCRgSGhyiKlK9OIv3MeZu_FA/featured"
                        className={styles.Link}
                        rel="noopener noreferrer"
                        target="_blank"
                      >
                        <IconYouTubeSquare className={styles.Icon} width="22px" />
                        YouTube
                      </a>
                    </div>
                    &copy;{moment().year()} Por si no te vuelvo a ver. Todos los derechos reservados. <a href={avisoPrivacidad.documento_digital} className={styles.Link} rel="noopener noreferrer" target="_blank">{avisoPrivacidad.nombre}</a>. <a href={terminosCondiciones.documento_digital} className={styles.Link} rel="noopener noreferrer" target="_blank">{terminosCondiciones.nombre}</a>.
                  </div>
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
      </Container>
    </React.Fragment>
  );
};

Copyright.propTypes = {
  className: PropTypes.string
};

export default Copyright;
