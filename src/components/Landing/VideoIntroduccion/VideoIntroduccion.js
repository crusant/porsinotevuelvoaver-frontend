import React from 'react';
import PropTypes from 'prop-types';
import YouTube from 'react-youtube';
import LoadingVideo from '../LoadingVideo/LoadingVideo';
import poster from '../../../assets/images/video.jpg';
import styles from './VideoIntroduccion.module.scss';

const VideoIntroduccion = (props) => {
  const [isLoading, setIsLoading] = React.useState(true);

  const onReady = () => {
    setIsLoading(false);
  };

  return (
    <React.Fragment>
      {isLoading && (
        <LoadingVideo
          poster={poster}
          alt="Descubre cómo funciona Por si no te vuelvo a ver"
        />
      )}
      <YouTube
        videoId="-8NgsSbUUTo"
        className={styles.YouTube}
        onReady={onReady}
        onPlay={props.onPlay}
        onPause={props.onPause}
      />
    </React.Fragment>
  );
};

VideoIntroduccion.propTypes = {
  onPlay: PropTypes.func.isRequired,
  onPause: PropTypes.func.isRequired
};

export default VideoIntroduccion;
