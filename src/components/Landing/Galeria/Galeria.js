import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import VideoIntroduccion from '../VideoIntroduccion/VideoIntroduccion';
import useWindowSize from '../../../hooks/useWindowSize';
import galeria01 from '../../../assets/images/galeria-01.jpg';
import galeria02 from '../../../assets/images/galeria-02.jpg';
import galeria03 from '../../../assets/images/galeria-03.jpg';
import '../../../../node_modules/slick-carousel/slick/slick.css';
import '../../../../node_modules/slick-carousel/slick/slick-theme.css';
import styles from './Galeria.module.scss';

const Galeria = (props) => {
  const sliderRef = React.useRef(null);
  const [, windowHeight] = useWindowSize();
  const navbarHeight = props.navbarHeight;
  const minimumNabvarHeight = 100;
  const maximumWindowHeight = 620;
  const minimumWindowHeight = 580;
  const currentNabvarHeight = (navbarHeight > 0) ? navbarHeight : minimumNabvarHeight;
  const currentWindowHeight = (windowHeight - currentNabvarHeight);

  const galeriaStyles = {
    marginTop: `-${currentNabvarHeight}px`,
    paddingTop: `${currentNabvarHeight}px`
  };

  let sliderHeight = 0;

  if ((currentWindowHeight > minimumWindowHeight) && (currentWindowHeight < maximumWindowHeight)) {
    sliderHeight = currentWindowHeight;
  } else if (currentWindowHeight < minimumWindowHeight) {
    sliderHeight = minimumWindowHeight;
  } else if (currentWindowHeight > maximumWindowHeight) {
    sliderHeight = maximumWindowHeight;
  }

  const settings = {
    arrows: false,
    autoplay: true,
    autoplaySpeed: 60000,
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  const sliderStyles = {
    height: `${sliderHeight}px`
  };

  const onPlaying = () => {
    sliderRef.current.slickPause();
  };

  const onPause = () => {
    sliderRef.current.slickPlay();
  };

  return (
    <div style={galeriaStyles}>
      <Slider ref={sliderRef} {...settings} className={styles.Galeria} style={sliderStyles}>
        <div className={styles.Item}>
            <div className={styles.Container} style={{backgroundImage: `url(${galeria01})`}}>
              <div className={styles.Body}>
                <h2 className={styles.Title}>Por si no te vuelvo a ver</h2>
                <div className={styles.Content}>
                  <div className={styles.Description}>
                    <p className={styles.Paragraph}>En nuestro país la cultura de la previsión es escasa, solo el 4% de la población lleva a cabo arreglos con respecto a sus bienes y deseos póstumos. El no registrar tus deseos oportunamente hereda el problema a la siguiente generación, lo cual puede llegar a generar conflictos que dañan y rompen los vínculos familiares.</p>
                    <p className={styles.Paragraph}>En Por si no te vuelvo a ver te ofrecemos dejar a nuestro resguardo y envío póstumo toda la información digital que a tus seres queridos desees compartir, tal como su biografía, pensamientos, relación de bienes, así como documentos y recuerdos importantes para ti de manera segura y confiable.</p>
                  </div>
                  <div className={styles.Video}>
                    <VideoIntroduccion
                      onPlay={onPlaying}
                      onPause={onPause}
                    />
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div className={styles.Item}>
          <div className={styles.Container} style={{backgroundImage: `url(${galeria02})`}}>
            <div className={styles.Body}>
              <h2 className={styles.Title}>Por si no te vuelvo a ver</h2>
              <div className={styles.Content}>
                <div className={styles.Description}>
                  <p className={styles.Paragraph}>En nuestro país la cultura de la previsión es escasa, solo el 4% de la población lleva a cabo arreglos con respecto a sus bienes y deseos póstumos. El no registrar tus deseos oportunamente hereda el problema a la siguiente generación, lo cual puede llegar a generar conflictos que dañan y rompen los vínculos familiares.</p>
                  <p className={styles.Paragraph}>En Por si no te vuelvo a ver te ofrecemos dejar a nuestro resguardo y envío póstumo toda la información digital que a tus seres queridos desees compartir, tal como su biografía, pensamientos, relación de bienes, así como documentos y recuerdos importantes para ti de manera segura y confiable.</p>
                </div>
                <div className={styles.Video}>
                  <VideoIntroduccion
                    onPlay={onPlaying}
                    onPause={onPause}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.Item}>
          <div className={styles.Container} style={{backgroundImage: `url(${galeria03})`}}>
            <div className={styles.Body}>
              <h2 className={styles.Title}>Por si no te vuelvo a ver</h2>
              <div className={styles.Content}>
                <div className={styles.Description}>
                  <p className={styles.Paragraph}>En nuestro país la cultura de la previsión es escasa, solo el 4% de la población lleva a cabo arreglos con respecto a sus bienes y deseos póstumos. El no registrar tus deseos oportunamente hereda el problema a la siguiente generación, lo cual puede llegar a generar conflictos que dañan y rompen los vínculos familiares.</p>
                  <p className={styles.Paragraph}>En Por si no te vuelvo a ver te ofrecemos dejar a nuestro resguardo y envío póstumo toda la información digital que a tus seres queridos desees compartir, tal como su biografía, pensamientos, relación de bienes, así como documentos y recuerdos importantes para ti de manera segura y confiable.</p>
                </div>
                <div className={styles.Video}>
                  <VideoIntroduccion
                    onPlay={onPlaying}
                    onPause={onPause}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Slider>
    </div>
  );
};

Galeria.propTypes = {
  navbarHeight: PropTypes.number.isRequired
};

export default Galeria;
