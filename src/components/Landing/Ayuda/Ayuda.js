import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { scroller } from 'react-scroll';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import classNames from 'classnames';
import Recaptcha from 'react-recaptcha';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Alert from 'react-bootstrap/Alert';
import Form from 'react-bootstrap/Form';
import Image from 'react-bootstrap/Image';
import Title from '../Title/Title';
import Background from '../../UI/Landing/Backgroud/Background';
import Paragraph from '../../UI/Paragraph/Paragraph';
import Button from '../../UI/Button/Button';
import styles from './Ayuda.module.scss';
import * as helpers from '../../../helpers';
import * as actions from '../../../actions';

const Ayuda = () => {
  const recaptchaRef = React.useRef(null);
  const [isSent, setIsSent] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const [isVerified, setIsVerified] = React.useState(false);
  const isSending = useSelector((state) => state.contacto.isSending);
  const errors = useSelector((state) => state.contacto.errors);
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      nombre: '',
      correo_electronico: '',
      telefono: '',
      mensaje: '',
      recaptcha: false
    },
    validationSchema: Yup.object({
      nombre: Yup.string().required('El campo "Nombre" es obligatorio.'),
      correo_electronico: Yup.string()
        .email('El campo "Correo electrónico" debe ser una dirección de correo válida.')
        .required('El campo "Correo electrónico" es obligatorio.'),
      telefono: Yup.number()
        .typeError('El campo "Teléfono" debe ser un número.')
        .required('El campo "Teléfono" es obligatorio.'),
      mensaje: Yup.string().required('El campo "Mensaje" es obligatorio.'),
      recaptcha: Yup.bool().oneOf([true], 'Debe confirmar que no es un robot.')
    }),
    onSubmit: async (values)  => {
      setIsSent(false);

      if (!isVerified) {
        return false;
      }

      if (await dispatch(actions.enviarMensajeContacto(values))) {
        formik.resetForm();
        setIsSent(true);
        scroller.scrollTo('ayuda');
      }

      setIsVerified(false);
      recaptchaRef.current.reset();
      formik.setSubmitting(false);
    }
  });

  const onRecaptchaLoaded = () => {
    setIsLoading(false);
  };

  const onVerifyRecaptcha = (response) => {
    if (response) {
      setIsVerified(true);
      formik.setFieldValue('recaptcha', true);
    } else {
      formik.setFieldValue('recaptcha', false);
    }
  };

  React.useEffect(() => {
    if (helpers.isObject(errors)) {
      formik.setErrors(errors);
      formik.setSubmitting(false);
    }

    // eslint-disable-next-line
  }, [errors]);

  return (
    <Background color>
      <Container>
        <Row>
          <Col>
            <Title quote="Por si no te vuelvo a ver" text="Ayuda" />
          </Col>
        </Row>
        <Row>
          <Col lg={6}>
            <Accordion className={styles.Accordion} defaultActiveKey="0">
              <Card className={styles.Card}>
                <Accordion.Collapse className={styles.Body} eventKey="0">
                  <Card.Body>
                    La vida está llena de incertidumbre y situaciones fuera de tu control. <i>Por si no te vuelvo a ver</i> te brinda la oportunidad de notificar automáticamente a un beneficiario tu voluntad, instrucciones y deseos en caso de presentarse una enfermedad o ausencia. El sistema te permite, desde dejar un mensaje póstumo, hasta apoyarte con una serie de formularios para documentar y resguardar información que desees transmitir a tus seres queridos, tales como última voluntad, mensajes póstumos, seguros vigentes, bienes materiales, instrucciones con respecto a mascotas, etc. <br /><br /> El valor de la información que entregas y almacenas digitalmente es invaluable, ya que permites a tus seres queridos afrontar la difícil situación en la que podrían llegar a encontrarse en caso de tu ausencia, así como mantener un contacto vivo a través de medios digitales con tu persona, con quien eres, con tus deseos y voluntad.
                  </Card.Body>
                </Accordion.Collapse>
                <Accordion.Toggle as={Card.Header} className={styles.Header} eventKey="0">
                  ¿Por qué debería usar Por si no te vuelvo a ver?
                </Accordion.Toggle>
              </Card>
              <Card className={styles.Card}>
                <Accordion.Collapse className={styles.Body} eventKey="1">
                  <Card.Body>
                    <i>Por si no te vuelvo a ver es seguro</i>, ya que tu información personal y financiera se mantiene privada y no se comparte con nadie. Toda la información de nuestro sitio web está encriptada, por lo que los datos viajan protegidos por Internet con un Certificado SSL.
                  </Card.Body>
                </Accordion.Collapse>
                <Accordion.Toggle as={Card.Header} className={styles.Header} eventKey="1">
                  ¿Es seguro?
                </Accordion.Toggle>
              </Card>
              <Card className={styles.Card}>
                <Accordion.Collapse className={styles.Body} eventKey="3">
                  <Card.Body>
                    Puedes pagar con tu cuenta de PayPal, con tarjeta de crédito, con tarjeta de débito o pagar en tiendas OXXO mediante MercadoPago.
                  </Card.Body>
                </Accordion.Collapse>
                <Accordion.Toggle as={Card.Header} className={styles.Header} eventKey="3">
                  ¿Cuáles son los métodos de pago?
                </Accordion.Toggle>
              </Card>
              <Card className={styles.Card}>
                <Accordion.Collapse className={styles.Body} eventKey="4">
                  <Card.Body>
                    En nuestro <strong>Plan Gratis</strong> únicamente puedes capturar tus datos personales, los datos de un custodio, los datos de un asesor o contacto y escribir instrucciones limitadas para tu custodio.<br /><br />Mientras que en nuestro <strong>Plan Premium</strong> puedes capturar los datos de tus custodios, todos tus asesores y contactos, pólizas de seguro, tu patrimonio, los datos de tu empleo y pensión, tus últimos deseos, los datos de tus mascotas, información de tus cuentas de correo y redes sociales, todo lo anterior de forma ilimitada. Además, podrás redactar o grabar un audio o video con tus últimos deseos, tu historia y legado, así como las instrucciones para tus custodios.
                  </Card.Body>
                </Accordion.Collapse>
                <Accordion.Toggle as={Card.Header} className={styles.Header} eventKey="4">
                  ¿Cuáles son las limitaciones del Plan Gratis?
                </Accordion.Toggle>
              </Card>
              <Card className={styles.Card}>
                <Accordion.Collapse className={styles.Body} eventKey="5">
                  <Card.Body>
                    Al ingresar a tu cuenta, deberás hacer clic en el botón <i>ACTUALIZAR MI PLAN</i> y seleccionar el método de pago correspondiente. Una vez realizado el pago, tu cuenta gratuita se actualizará a una cuenta premium.
                  </Card.Body>
                </Accordion.Collapse>
                <Accordion.Toggle as={Card.Header} className={styles.Header} eventKey="5">
                  ¿Cómo puedo actualizar mi plan gratis a un plan premium?
                </Accordion.Toggle>
              </Card>
              <Card className={styles.Card}>
                <Accordion.Collapse className={styles.Body} eventKey="6">
                  <Card.Body>
                    Si requieres una atención personalizada deberás rellenar nuestro formulario de contacto.
                  </Card.Body>
                </Accordion.Collapse>
                <Accordion.Toggle as={Card.Header} className={styles.Header} eventKey="6">
                  ¿A dónde acudo si tengo dudas o necesito ayuda?
                </Accordion.Toggle>
              </Card>
              <Card className={styles.Card}>
                <Accordion.Collapse className={styles.Body} eventKey="7">
                  <Card.Body>
                    Te recordamos que tu usuario es el correo electrónico con el que te registraste.<br /><br />Si olvidaste tu contraseña, sólo debes hacer clic en este enlace <Link to="/recuperar-contrasenia">https://www.porsinotevuelvoaver.com/recuperar-contrasenia</Link>, deberás capturar tu usuario y dar clic en el botón <i>ENVIAR</i>, posteriormente te llegará un enlace a tu correo electrónico para realizar el cambio de tu contraseña.
                  </Card.Body>
                </Accordion.Collapse>
                <Accordion.Toggle as={Card.Header} className={styles.Header} eventKey="7">
                  ¿Qué pasa si olvido mi usuario o contraseña?
                </Accordion.Toggle>
              </Card>
            </Accordion>
          </Col>
          <Col lg={6}>
            {isSent && (
              <Alert
                variant="success"
                onClose={() => setIsSent(false)}
                dismissible
              >
                Gracias por contactarnos en breve nos comunicaremos contigo.
              </Alert>
            )}
            <Form className={styles.Form} noValidate onSubmit={formik.handleSubmit}>
              <h2 className={styles.Title}>Formulario de contacto</h2>
              <p className={styles.Quote}>Si requieres una atención personalizada, ponte en contacto con nosotros a través del siguiente formulario.</p>
              <Form.Group controlId="nombre" className={styles.Group}>
                <Form.Label className={styles.Label}>Nombre</Form.Label>
                <Form.Control
                  type="text"
                  name="nombre"
                  className={styles.Control}
                  maxLength="100"
                  isInvalid={formik.errors.nombre && formik.touched.nombre}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.nombre}
                />
                <Form.Control.Feedback type="invalid">
                  {formik.errors.nombre}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="correo_electronico" className={styles.Group}>
                <Form.Label className={styles.Label}>Correo electrónico</Form.Label>
                <Form.Control
                  type="email"
                  name="correo_electronico"
                  className={styles.Control}
                  maxLength="320"
                  isInvalid={formik.errors.correo_electronico && formik.touched.correo_electronico}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.correo_electronico}
                />
                <Form.Control.Feedback type="invalid">
                  {formik.errors.correo_electronico}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="telefono" className={styles.Group}>
                <Form.Label className={styles.Label}>Teléfono</Form.Label>
                <Form.Control
                  type="tel"
                  name="telefono"
                  className={styles.Control}
                  maxLength="10"
                  isInvalid={formik.errors.telefono && formik.touched.telefono}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.telefono}
                />
                <Form.Control.Feedback type="invalid">
                  {formik.errors.telefono}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="mensaje" className={styles.Group}>
                <Form.Label className={styles.Label}>Mensaje</Form.Label>
                <Form.Control
                  as="textarea"
                  name="mensaje"
                  className={styles.Control}
                  maxLength="500"
                  rows="4"
                  isInvalid={formik.errors.mensaje && formik.touched.mensaje}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.mensaje}
                />
                <Form.Control.Feedback type="invalid">
                  {formik.errors.mensaje}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group>
                {isLoading && <Paragraph>Cargando captcha...</Paragraph>}
                <Recaptcha
                  ref={recaptchaRef}
                  sitekey="6LdhmKsZAAAAAKyKuj9NRlhu0tTejyb6AmNT2x3E"
                  render="explicit"
                  hl="es-419"
                  verifyCallback={onVerifyRecaptcha}
                  onloadCallback={onRecaptchaLoaded}
                />
                <Form.Control.Feedback
                  type="invalid"
                  className={classNames({
                    'd-block': formik.errors.recaptcha && formik.touched.recaptcha
                  })}
                >
                  {formik.errors.recaptcha}
                </Form.Control.Feedback>
              </Form.Group>
              <Button
                type="submit"
                variant="primary"
                align="right"
                disabled={isSending}
              >
                {isSending ? 'Enviando...' : 'Enviar'}
              </Button>
            </Form>
            <h2 className={styles.Title}>¡Descarga nuestra app!</h2>
            <div className={styles.Badges}>
              <a
                href="https://play.google.com/store/apps/details?id=com.porsinotevuelvoaver.app&hl=es_MX"
                className={styles.Badge}
                target="_blank"
                rel="noopener noreferrer"
                title="Por si no te vuelvo a ver"
              >
                <Image src={require('../../../assets/images/google-play-badge.png')} />
              </a>
              <a
                href="https://apps.apple.com/mx/app/por-si-no-te-vuelvo-a-ver/id1582535376"
                className={styles.Badge}
                target="_blank"
                rel="noopener noreferrer"
                title="Por si no te vuelvo a ver"
              >
                <Image src={require('../../../assets/images/play-store-badge.png')} />
              </a>
            </div>
          </Col>
        </Row>
      </Container>
    </Background>
  );
};

export default Ayuda;
