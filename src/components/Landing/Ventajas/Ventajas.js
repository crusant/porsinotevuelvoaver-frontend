import React from 'react';
import classNames from 'classnames';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Title from '../Title/Title';
import Background from '../../UI/Landing/Backgroud/Background';
import image from '../../../assets/images/ventajas.png';
import styles from './Ventajas.module.scss';

const Ventajas = () => (
  <Background color>
    <Container>
      <Row className={styles.Ventajas}>
        <Col>
          <Row>
            <Col>
              <Title center quote="Por si no te vuelvo a ver" text="Ventajas" />
            </Col>
          </Row>
          <Row>
            <Col className={styles.Graphic}>
              <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                  <img src={image} className={styles.Image} alt="Ventajas" />
                </Col>
              </Row>
              <Row className={styles.List}>
                <Col className={styles.Items}>
                  <Row className={styles.Item}>
                    <Col lg={4}>
                      <div className={classNames(styles.Ventaja, styles.Right, styles.Last)}>
                        <h5 className={`${styles.Title}`}>4. Orden</h5>
                        <p className={styles.Text}>Mediante una serie de formularios podrás organizar tus bienes y deseos.</p>
                      </div>
                    </Col>
                  </Row>
                  <Row className={styles.Item}>
                    <Col lg={{ span: 6, offset: 6 }}>
                      <div className={`${styles.Ventaja} ${styles.Left}`}>
                        <h5 className={`${styles.Title}`}>1. Resguardo</h5>
                        <p className={styles.Text}>Almacenamiento y resguardo de tu información mediante cifrado y autenticación, solo accesible a las personas que tu elijas (Custodios) una vez que resulte negativa la prueba de vida.</p>
                      </div>
                    </Col>
                  </Row>
                  <Row className={styles.Item}>
                    <Col lg={{ span: 4, offset: 8 }}>
                      <div className={`${styles.Ventaja} ${styles.Left}`}>
                        <h5 className={`${styles.Title}`}>2. Tranquilidad</h5>
                        <p className={styles.Text}>Cuenta con la seguridad de que tu información resguardada será entregada a tus custodios.</p>
                      </div>
                    </Col>
                  </Row>
                  <Row className={styles.Item}>
                    <Col lg={6}>
                      <div className={classNames(styles.Ventaja, styles.Right)}>
                        <h5 className={`${styles.Title}`}>3. Legado y trascendencia</h5>
                        <p className={styles.Text}>Permite que generaciones futuras se beneficien con tus experiencias, consejos y vivencias.</p>
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  </Background>
);

export default Ventajas;
