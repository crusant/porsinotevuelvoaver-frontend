import React from 'react';
import Plan from '../../../UI/Landing/Plan/Plan';
import BreakLine from '../../../UI/BreakLine/BreakLine';

const PlanAnual = () => {
  return (
    <Plan active>
      <Plan.Price was>$ 1198</Plan.Price>
      <Plan.Type>Servicio Premium<BreakLine /> Oferta</Plan.Type>
      <Plan.Discount>-30%</Plan.Discount>
      <Plan.Price>$ 899 <Plan.Recurrence>Anual</Plan.Recurrence></Plan.Price>
      <Plan.Description>
        <Plan.Paragraph>Descuento del 30%.</Plan.Paragraph>
        <Plan.Paragraph>Registro ilimitado de custodios.</Plan.Paragraph>
        <Plan.Paragraph>Modificación ilimitada de tu información.</Plan.Paragraph>
        <Plan.Paragraph>Prueba de Vida mediante Correo Electrónico y SMS.</Plan.Paragraph>
        <Plan.Paragraph>Grabar o subir mensajes póstumos en video y audio.</Plan.Paragraph>
        <Plan.Paragraph>Acceso a todos los formularios, tales como: Biografía, Mascotas, Asesores, Directorio, Bienes, Documentos personales, etc.</Plan.Paragraph>
      </Plan.Description>
    </Plan>
  );
};

export default PlanAnual;
