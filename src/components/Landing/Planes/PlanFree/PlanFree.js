import React from 'react';
import Plan from '../../../UI/Landing/Plan/Plan';
import BreakLine from '../../../UI/BreakLine/BreakLine';

const PlanFree = () => {
  return (
    <Plan>
      <Plan.Type>Servicio Regular<BreakLine /> Sin Costo</Plan.Type>
      <Plan.Price>Gratis <Plan.Recurrence>&nbsp;</Plan.Recurrence></Plan.Price>
      <Plan.Description>
        <Plan.Paragraph>Visualizar el menú de Formularios.</Plan.Paragraph>
        <Plan.Paragraph>Registro de Datos Generales.</Plan.Paragraph>
        <Plan.Paragraph>Selección de 2 custodios.</Plan.Paragraph>
        <Plan.Paragraph>Registro de Información de Contactos Relevantes.</Plan.Paragraph>
        <Plan.Paragraph>Instrucciones o mensajes escritos póstumos limitados.</Plan.Paragraph>
        <Plan.Paragraph>Prueba de vida únicamente por correo electrónico.</Plan.Paragraph>
      </Plan.Description>
    </Plan>
  );
};

export default PlanFree;
