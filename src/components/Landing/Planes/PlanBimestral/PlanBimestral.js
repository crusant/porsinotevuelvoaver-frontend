import React from 'react';
import Plan from '../../../UI/Landing/Plan/Plan';
import BreakLine from '../../../UI/BreakLine/BreakLine';

const PlanBimestral = () => {
  return (
    <Plan>
      <Plan.Type>Servicio Premium<BreakLine /> Regular</Plan.Type>
      <Plan.Price>$ 599 <Plan.Recurrence>Semestral</Plan.Recurrence></Plan.Price>
      <Plan.Description>
        <Plan.Paragraph>Registro ilimitado de custodios.</Plan.Paragraph>
        <Plan.Paragraph>Modificación ilimitada de tu información.</Plan.Paragraph>
        <Plan.Paragraph>Prueba de Vida mediante Correo Electrónico y SMS.</Plan.Paragraph>
        <Plan.Paragraph>Grabar o subir mensajes póstumos en video y audio.</Plan.Paragraph>
        <Plan.Paragraph>Acceso a todos los formularios, tales como: Biografía, Mascotas, Asesores, Directorio, Bienes, Documentos personales, etc.</Plan.Paragraph>
      </Plan.Description>
    </Plan>
  );
};

export default PlanBimestral;
