import React from 'react';
import PropTypes from 'prop-types';
import Spinner from 'react-bootstrap/Spinner';
import styles from './LoadingVideo.module.scss';

const LoadingVideo = (props) => (
  <div className={styles.Loading}>
    <img
      src={props.poster}
      className={styles.Poster}
      alt={props.alt}
    />
    <Spinner
      animation="border"
      className={styles.Spinner}
    />
  </div>
);

LoadingVideo.propTypes = {
  poster: PropTypes.string.isRequired,
  alt: PropTypes.string
};

export default LoadingVideo;
