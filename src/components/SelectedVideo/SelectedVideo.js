import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Paragraph from '../UI/Paragraph/Paragraph';
import Button from '../UI/Button/Button';
import IconFilmStrip from '../UI/Icons/IconFilmStrip/IconFilmStrip';
import IconClose from '../UI/Icons/IconClose/IconClose';
import * as helpers from '../../helpers';
import styles from './SelectedVideo.module.scss';

const SelectedVideo = (props) => (
  <div className={classNames(styles.SelectedVideo, { [styles.IsFile]: (helpers.isFile(props.video) || !helpers.isBlob(props.video)) })}>
    {helpers.isFile(props.video) ? (
      <div className={styles.Info}>
        <IconFilmStrip className={styles.Icon} width="30px" />
        <div className={styles.Text}>{helpers.truncate(props.video.name, 12)}</div>
      </div>
    )  : helpers.isBlob(props.video) ? (
      <video className={styles.Video} controls preload="auto">
        <source src={URL.createObjectURL(props.video)} />
        Lo sentimos su navegador no puede reproducir video, le sugerimos cambiar a la última versión de Chrome o Firefox.
      </video>
    ) : (
      <React.Fragment>
        <IconFilmStrip className={styles.Icon} width="30px" />
        <Paragraph>Video sin nombre.</Paragraph>
      </React.Fragment>
    )}
    <Button
      variant="danger"
      align={helpers.isFile(props.video) ? 'right' : helpers.isBlob(props.video) ? 'center' : 'right'}
      className={styles.Button}
      small
      onClick={props.onRemove}
    >
      <IconClose height="14px" /> Quitar video
    </Button>
  </div>
);

SelectedVideo.propTypes = {
  video: PropTypes.oneOfType([
    PropTypes.instanceOf(File),
    PropTypes.instanceOf(Blob)
  ]).isRequired,
  onRemove: PropTypes.func.isRequired
};

export default SelectedVideo;
