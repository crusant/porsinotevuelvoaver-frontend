import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import Button from '../UI/Button/Button';
import IconDownload from '../UI/Icons/IconDownload/IconDownload';
import IconClose from '../UI/Icons/IconClose/IconClose';
import styles from './SavedAudio.module.scss';

const SavedAudio = (props) => {
  const user = useSelector((state) => state.auth.user);

  const canWrite = () => {
    return _.get(user, 'can_write', false);
  };

  return (
    <div className={styles.SavedAudio}>
      <audio src={props.audio} className={styles.Audio} controls preload="auto">
        Lo sentimos su navegador no puede reproducir audio, le sugerimos cambiar a la última versión de Chrome o Firefox.
      </audio>
      <Button
        variant="secondary"
        className={styles.Button}
        small
        disabled={props.isDownloading}
        onClick={props.onDownload}
      >
        <IconDownload height="14px" /> {props.isDownloading ? 'Descargando audio...' : 'Descargar audio'}
      </Button>
      {canWrite() && (
        <Button
          variant="danger"
          className={styles.Button}
          small
          onClick={props.onDelete}
        >
          <IconClose height="14px" /> Quitar audio
        </Button>
      )}
    </div>
  );
};

SavedAudio.defaultProps = {
  onDelete: () => {}
};

SavedAudio.propTypes = {
  audio: PropTypes.string.isRequired,
  onDownload: PropTypes.func.isRequired,
  isDownloading: PropTypes.bool.isRequired,
  onDelete: PropTypes.func
};

export default SavedAudio;
