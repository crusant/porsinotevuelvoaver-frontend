import React from 'react';
import PropTypes from 'prop-types';
import RecordRTC from 'recordrtc';
import Alert from 'react-bootstrap/Alert';
import Paragraph from '../UI/Paragraph/Paragraph';
import Button from '../UI/Button/Button';
import IconPlay from '../UI/Icons/IconPlay/IconPlay';
import IconStop from '../UI/Icons/IconStop/IconStop';
import IconNoVideo from '../UI/Icons/IconNoVideo/IconNoVideo';
import * as helpers from '../../helpers';
import styles from './VideoRecorder.module.scss';

const hasGetUserMedia = !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);

const VideoRecorder = (props) => {
  const recorder = React.useRef(null);
  const stream = React.useRef(null);
  const startedTime = React.useRef(null);
  const timeout = React.useRef(null);
  const video = React.useRef(null);
  const sizeRef = React.useState(null);
  const [isCompatible, setIsCompatible] = React.useState(true);
  const [isMicrophoneError, setIsMicrofoneError] = React.useState(false);
  const [duration, setDuration] = React.useState('00:00:00');
  const [size, setSize] = React.useState(0);
  const [isRecording, setIsRecording] = React.useState(false);

  const captureStream = (callback) => {
    if (stream.current) {
      callback(stream.current);
      return;
    }

    navigator.mediaDevices.getUserMedia({
      audio: true,
      video: true
    }).then((value) => {
      callback(value);
    }).catch(() => {
      setIsMicrofoneError(true);
    });
  };

  const onStart = () => {
    setIsRecording(true);

    if (!stream.current) {
      captureStream((value) => {
        stream.current = value;
        video.current.muted = true;
        video.current.valume = 0;
        video.current.srcObject = value;

        recorder.current = RecordRTC(stream.current, {
          recorderType: RecordRTC.MediaStreamRecorder,
          mimeType: 'video/webm',
          timeSlice: 1000
        });

        recorder.current.startRecording();
        recorder.current.camera = value;
        startedTime.current = new Date().getTime();

        (function looper() {
          if (!recorder.current) {
              return;
          }

          const currentTime = new Date().getTime();
          const duration = helpers.calculateTime(Math.floor((currentTime - startedTime.current) / 1000));
          const internal = recorder.current.getInternalRecorder();

          if (internal && internal.getArrayOfBlobs) {
            const blob = new Blob(internal.getArrayOfBlobs(), {
              type: 'video/webm'
            });

            sizeRef.current = blob.size;
            setSize(blob.size);
          }

          setDuration(duration);

          if (sizeRef.current >= 64000000) {
              onStop();
              return;
          }

          timeout.current = setTimeout(looper, 1000);
        })();
      });
    }
  };

  const onStop = () => {
    if (!recorder.current) {
      return;
    }

    recorder.current.stopRecording(() => {
      setIsRecording(false);
      setDuration('00:00:00');
      clearTimeout(timeout.current);

      props.onRecorded(recorder.current.blob);

      if (stream.current) {
        stream.current.stop();
        stream.current = null;
      }

      recorder.current.camera.stop();
      recorder.current.destroy();
      recorder.current = null;
    });
  };

  React.useEffect(() => {
    setIsCompatible(hasGetUserMedia);
  }, []);

  if (!isCompatible) {
    return (
      <Alert variant="danger">
        <Paragraph>Lo sentimos su navegador no puede grabar video, le sugerimos cambiar a la última versión de Chrome o Firefox.</Paragraph>
      </Alert>
    );
  }

  return (
    <div className={styles.VideoRecorder}>
      {isMicrophoneError ? (
        <div className={styles.Error}>
          <IconNoVideo className={styles.Icon} /> Lo sentimos no podemos acceder a su videocámara.
        </div>
      ) : (
        <React.Fragment>
          <video ref={video} className={styles.Video} controls autoPlay playsInline preload="auto">
            <source src={video} />
            Lo sentimos su navegador no puede reproducir video, le sugerimos cambiar a la última versión de Chrome o Firefox.
          </video>
          <Paragraph className={styles.Info}>{duration} | {RecordRTC.bytesToSize(size)}</Paragraph>
        </React.Fragment>
      )}
      {(isRecording && !isMicrophoneError) ? (
        <Button
          variant="primary"
          align="center"
          className={styles.Button}
          small
          onClick={onStop}
        >
          <IconStop height="14px" /> Terminar grabación
        </Button>
      ) : (
        <Button
          variant="primary"
          align="center"
          className={styles.Button}
          small
          disabled={isMicrophoneError}
          onClick={onStart}
        >
          <IconPlay height="14px" /> Iniciar grabación
        </Button>
      )}
    </div>
  );
};

VideoRecorder.propTypes = {
  onRecorded: PropTypes.func
};

export default VideoRecorder;
