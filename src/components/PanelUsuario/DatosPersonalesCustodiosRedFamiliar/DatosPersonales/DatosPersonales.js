import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Paragraph from '../../../UI/Paragraph/Paragraph';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormFile from '../../../UI/Form/FormFile/FormFile';
import File from '../../../UI/File/File';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../../UI/Form/FormTitle/FormTitle';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/datos-personales-custodios-red-familiar/datos-personales';
import * as helpers from '../../../../helpers';

const DatosPersonales = (props) => {
  const user = useSelector((state) => state.auth.user);
  const perfil = useSelector((state) => state.datosPersonales.perfil);
  const domicilioFiscal = useSelector((state) => state.datosPersonales.domicilioFiscal);
  const madre = useSelector((state) => state.datosPersonales.madre);
  const padre = useSelector((state) => state.datosPersonales.padre);
  const parejaConyuge = useSelector((state) => state.datosPersonales.parejaConyuge);
  const isLoading = useSelector((state) => state.datosPersonales.isLoading);
  const isSaving = useSelector((state) => state.datosPersonales.isSaving);
  const isDownloadingActaNacimiento = useSelector((state) => state.datosPersonales.isDownloadingActaNacimiento);
  const isDownloadingActaMatrimonio = useSelector((state) => state.datosPersonales.isDownloadingActaMatrimonio);
  const dispatch = useDispatch();

  const canWrite = () =>
    user?.can_write;

  const onChangeDatosPersonalesInput = (event) => {
    const input = event.target;
    const [entity, attribute] = input.name.split('.');
    const value = input.type === 'checkbox' ? input.checked : input.value;

    dispatch(actions.changeDatosPersonalesInput(entity, attribute, value));
  };

  const onChangeActaNacimientoDigitalInput = (value) => {
    dispatch(actions.changeDatosPersonalesInput('perfil', 'acta_nacimiento_digital', value));
  };

  const onRemoveActaNacimientoDigitalInput = () => {
    dispatch(actions.changeDatosPersonalesInput('perfil', 'acta_nacimiento_digital', ''));
  };

  const onChangeActaMatrimonioDigitalInput = (value) => {
    dispatch(actions.changeDatosPersonalesInput('parejaConyuge', 'acta_matrimonio_digital', value));
  };

  const onRemoveActaMatrimonioDigitalInput = () => {
    dispatch(actions.changeDatosPersonalesInput('parejaConyuge', 'acta_matrimonio_digital', ''));
  };

  const onChangeHasParejaConyugeInput = (event) => {
    dispatch(actions.changeDatosPersonalesInput('perfil', 'has_pareja_conyuge', event.target.checked));
    dispatch(actions.changeDatosPersonalesInput('parejaConyuge', 'is_vivo', event.target.checked));
    dispatch(actions.changeDatosPersonalesInput('parejaConyuge', 'has_mismo_domicilio', false));
  };

  const onChangeIsVivoInput = (event) => {
    dispatch(actions.changeDatosPersonalesInput('parejaConyuge', 'is_vivo', event.target.checked));
    dispatch(actions.changeDatosPersonalesInput('parejaConyuge', 'has_mismo_domicilio', false));
  };

  const onSaveDatosPersonales = async (event) => {
    event.preventDefault();

    const data = {
      perfil,
      domicilioFiscal,
      madre,
      padre,
      parejaConyuge
    };

    const formData = new FormData();
    helpers.buildFormData(formData, data);

    props.setShowToast(false);
    props.setToastMessage('');

    if (await dispatch(actions.saveDatosPersonales(formData))) {
      props.onSelectKey('1');
      props.setToastMessage('Los datos personales se guardaron con éxito.');
      props.setShowToast(true);
    }

    window.scrollTo(0, 0);
  };

  const onDownloadActaNacimiento = (actaNacimiento) => {
    const extension = String(actaNacimiento).split('.').pop();

    dispatch(actions.downloadDatosPersonalesActaNacimiento(extension));
  };

  const onDownloadActaMatrimonio = (actaMatrimonio) => {
    const extension = String(actaMatrimonio).split('.').pop();

    dispatch(actions.downloadDatosPersonalesActaMatrimonio(extension));
  };

  React.useEffect(() => {
    if (!isLoading) {
      props.setUpdatedAt(perfil?.updated_at);
    }

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isLoading]);

  React.useEffect(() => {
    dispatch(actions.getDatosPersonales());

    return () => {
      dispatch(actions.resetDatosPersonalesErrors());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <form onSubmit={onSaveDatosPersonales}>
      <FormRow right>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          </FormColumn>
        )}
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
      <Paragraph title>El propósito de este apartado es el registro de información personal y de contacto.<br /><br /> Puedes llevar a cabo el resguardo de documentos digitalizados en formato PDF, Word, Excel, JPG o PNG. En caso de requerir anexar documentos adicionales, puedes hacerlo en la sección "Documentos digitales y ubicación física".</Paragraph>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.nombre_completo">Nombre completo*</FormLabel>
              <FormInput
                type="text"
                name="perfil.nombre_completo"
                id="perfil.nombre_completo"
                required
                maxLength={100}
                onChange={onChangeDatosPersonalesInput}
                value={perfil.nombre_completo}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Nombre completo</FormLabel>
              <Paragraph>{perfil.nombre_completo}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn left>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.fecha_nacimiento">Fecha de nacimiento*</FormLabel>
              <FormHelpText>&nbsp;</FormHelpText>
              <FormInput
                type="date"
                name="perfil.fecha_nacimiento"
                id="perfil.fecha_nacimiento"
                required
                max="9999-12-31"
                onChange={onChangeDatosPersonalesInput}
                value={perfil.fecha_nacimiento}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Fecha de nacimiento</FormLabel>
              <Paragraph>
                {!!perfil.fecha_nacimiento && moment(perfil.fecha_nacimiento).format('LL')}
              </Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.lugar_nacimiento">Lugar de nacimiento*</FormLabel>
              <FormHelpText>Capture el municipio y el estado. Por ejemplo: Mérida, Yucatán</FormHelpText>
              <FormInput
                type="text"
                name="perfil.lugar_nacimiento"
                id="perfil.lugar_nacimiento"
                required
                maxLength={150}
                onChange={onChangeDatosPersonalesInput}
                value={perfil.lugar_nacimiento}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Lugar de nacimiento</FormLabel>
              <Paragraph>{perfil.lugar_nacimiento}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.acta_nacimiento_digital">Acta de nacimiento digital <FormText>(Opcional)</FormText></FormLabel>
              <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF, Word, PowerPoint, Excel, .jpg o .png.</FormHelpText>
              {helpers.isEmptyString(perfil.acta_nacimiento_digital) ? (
                <FormFile accept=".pdf,.doc,.docx,.ppt,.pptx,.xsl,.xlsx,.jpg,.jpeg,.png" onChange={onChangeActaNacimientoDigitalInput} />
              ) : (
                <File
                  file={perfil.acta_nacimiento_digital}
                  canDownload
                  isDownloading={isDownloadingActaNacimiento}
                  onDownload={onDownloadActaNacimiento}
                  onRemove={onRemoveActaNacimientoDigitalInput}
                />
              )}
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Acta de nacimiento digital</FormLabel>
              {!!perfil.acta_nacimiento_digital ? (
                <File
                  file={perfil.acta_nacimiento_digital}
                  canDownload
                  isDownloading={isDownloadingActaNacimiento}
                  onDownload={onDownloadActaNacimiento}
                  onRemove={onRemoveActaNacimientoDigitalInput}
                />
              ) : (
                <Paragraph>No adjuntó archivo digital</Paragraph>
              )}
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.acta_nacimiento_fisica">Donde encontrar el acta de nacimiento <FormText>(Opcional)</FormText></FormLabel>
              <FormInput
                type="text"
                name="perfil.acta_nacimiento_fisica"
                id="perfil.acta_nacimiento_fisica"
                maxLength={500}
                onChange={onChangeDatosPersonalesInput}
                value={perfil.acta_nacimiento_fisica}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Donde encontrar el acta de nacimiento</FormLabel>
              <Paragraph>{perfil.acta_nacimiento_fisica}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn left>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.curp">CURP*</FormLabel>
              <FormInput
                type="text"
                name="perfil.curp"
                id="perfil.curp"
                required
                minLength={18}
                maxLength={18}
                onChange={onChangeDatosPersonalesInput}
                value={perfil.curp}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>CURP</FormLabel>
              <Paragraph>{perfil.curp}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.rfc">RFC <FormText>(Opcional)</FormText></FormLabel>
              <FormInput
                type="text"
                name="perfil.rfc"
                id="perfil.rfc"
                maxLength={13}
                onChange={onChangeDatosPersonalesInput}
                value={perfil.rfc}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>RFC</FormLabel>
              <Paragraph>{perfil.rfc}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormSeparator />
      <FormTitle>Domicilio particular</FormTitle>
      <FormRow>
        <FormColumn left>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.calle_numero">Calle y número*</FormLabel>
              <FormInput
                type="text"
                name="perfil.calle_numero"
                id="perfil.calle_numero"
                required
                maxLength={100}
                onChange={onChangeDatosPersonalesInput}
                value={perfil.calle_numero}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Calle y número</FormLabel>
              <Paragraph>{perfil.calle_numero}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.colonia">Colonia*</FormLabel>
              <FormInput
                type="text"
                name="perfil.colonia"
                id="perfil.colonia"
                required
                maxLength={100}
                onChange={onChangeDatosPersonalesInput}
                value={perfil.colonia}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Colonia</FormLabel>
              <Paragraph>{perfil.colonia}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn left>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.codigo_postal">C.P. <FormText>(Opcional)</FormText></FormLabel>
              <FormInput
                type="number"
                name="perfil.codigo_postal"
                id="perfil.codigo_postal"
                digits={5}
                min={0}
                max={99999}
                step={1}
                onChange={onChangeDatosPersonalesInput}
                value={perfil.codigo_postal}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>C.P.</FormLabel>
              <Paragraph>{perfil.codigo_postal}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn center>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.municipio">Municipio*</FormLabel>
              <FormInput
                type="text"
                name="perfil.municipio"
                id="perfil.municipio"
                required
                maxLength={100}
                onChange={onChangeDatosPersonalesInput}
                value={perfil.municipio}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Municipio</FormLabel>
              <Paragraph>{perfil.municipio}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="perfil.estado">Estado*</FormLabel>
              <FormInput
                type="text"
                name="perfil.estado"
                id="perfil.estado"
                required
                maxLength={40}
                onChange={onChangeDatosPersonalesInput}
                value={perfil.estado}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Estado</FormLabel>
              <Paragraph>{perfil.estado}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormSeparator />
      <FormTitle>Domicilio fiscal</FormTitle>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <FormInput
              as="checkbox"
              name="domicilioFiscal.is_mismo_domicilio_particular"
              id="domicilioFiscal.is_mismo_domicilio_particular"
              checked={domicilioFiscal.is_mismo_domicilio_particular}
              onChange={onChangeDatosPersonalesInput}
              label="Es el mismo que el domicilio particular"
            />
          ) : (
            domicilioFiscal.is_mismo_domicilio_particular && (
              <FormLabel>Es el mismo que el domicilio particular</FormLabel>
            )
          )}
        </FormColumn>
      </FormRow>
      {!domicilioFiscal.is_mismo_domicilio_particular && (
        <React.Fragment>
          <FormRow>
            <FormColumn left>
              <FormLabel htmlFor="domicilioFiscal.calle_numero">Calle y número*</FormLabel>
              {canWrite() ? (
                <FormInput
                  type="text"
                  name="domicilioFiscal.calle_numero"
                  id="domicilioFiscal.calle_numero"
                  required
                  maxLength={100}
                  onChange={onChangeDatosPersonalesInput}
                  value={domicilioFiscal.calle_numero}
                />
              ) : (
                <Paragraph>{domicilioFiscal.calle_numero}</Paragraph>
              )}
            </FormColumn>
            <FormColumn right>
              <FormLabel htmlFor="domicilioFiscal.colonia">Colonia*</FormLabel>
              {canWrite() ? (
                <FormInput
                  type="text"
                  name="domicilioFiscal.colonia"
                  id="domicilioFiscal.colonia"
                  required
                  maxLength={100}
                  onChange={onChangeDatosPersonalesInput}
                  value={domicilioFiscal.colonia}
                />
              ) : (
                <Paragraph>{domicilioFiscal.colonia}</Paragraph>
              )}
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn left>
              <FormLabel htmlFor="domicilioFiscal.codigo_postal">C.P. <FormText>(Opcional)</FormText></FormLabel>
              {canWrite() ? (
                <FormInput
                  type="number"
                  name="domicilioFiscal.codigo_postal"
                  id="domicilioFiscal.codigo_postal"
                  digits={5}
                  min={0}
                  max={99999}
                  step={1}
                  onChange={onChangeDatosPersonalesInput}
                  value={domicilioFiscal.codigo_postal}
                />
              ) : (
                <Paragraph>{domicilioFiscal.codigo_postal}</Paragraph>
              )}
            </FormColumn>
            <FormColumn center>
              <FormLabel htmlFor="domicilioFiscal.municipio">Municipio*</FormLabel>
              {canWrite() ? (
                <FormInput
                  type="text"
                  name="domicilioFiscal.municipio"
                  id="domicilioFiscal.municipio"
                  required
                  maxLength={100}
                  onChange={onChangeDatosPersonalesInput}
                  value={domicilioFiscal.municipio}
                />
              ) : (
                <Paragraph>{domicilioFiscal.municipio}</Paragraph>
              )}
            </FormColumn>
            <FormColumn right>
              <FormLabel htmlFor="domicilioFiscal.estado">Estado*</FormLabel>
              {canWrite() ? (
                <FormInput
                  type="text"
                  name="domicilioFiscal.estado"
                  id="domicilioFiscal.estado"
                  required
                  maxLength={40}
                  onChange={onChangeDatosPersonalesInput}
                  value={domicilioFiscal.estado}
                />
              ) : (
                <Paragraph>{domicilioFiscal.estado}</Paragraph>
              )}
            </FormColumn>
          </FormRow>
        </React.Fragment>
      )}
      <FormSeparator />
      <FormTitle>Datos de la madre</FormTitle>
      {canWrite() ? (
        <FormRow>
          <FormColumn>
            <FormLabel htmlFor="madre.is_vivo">Vive*</FormLabel>
            <FormInput
              as="switch"
              name="madre.is_vivo"
              id="madre.is_vivo"
              checked={madre.is_vivo}
              falseText="No"
              trueText="Sí"
              onChange={onChangeDatosPersonalesInput}
            />
          </FormColumn>
        </FormRow>
      ) : (
        !madre.is_vivo && (
          <FormRow>
            <FormColumn>
              <FormLabel>Vive</FormLabel>
              <Paragraph>No</Paragraph>
            </FormColumn>
          </FormRow>
        )
      )}
      {madre.is_vivo && (
        <React.Fragment>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="madre.nombre_completo">Nombre completo*</FormLabel>
                  <FormInput
                    type="text"
                    name="madre.nombre_completo"
                    id="madre.nombre_completo"
                    required
                    maxLength={100}
                    onChange={onChangeDatosPersonalesInput}
                    value={madre.nombre_completo}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Nombre completo</FormLabel>
                  <Paragraph>{madre.nombre_completo}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="madre.telefono">Teléfono*</FormLabel>
                  <FormInput
                    type="tel"
                    name="madre.telefono"
                    id="madre.telefono"
                    required
                    minLength={10}
                    maxLength={10}
                    onChange={onChangeDatosPersonalesInput}
                    value={madre.telefono}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Teléfono</FormLabel>
                  <Paragraph>{madre.telefono}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="madre.correo_electronico">Correo electrónico <FormText>(Opcional)</FormText></FormLabel>
                  <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                  <FormInput
                    type="text"
                    name="madre.correo_electronico"
                    id="madre.correo_electronico"
                    maxLength={320}
                    pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                    title="Por favor ingrese un correo electrónico valido."
                    onChange={onChangeDatosPersonalesInput}
                    value={madre.correo_electronico}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Correo electrónico</FormLabel>
                  <Paragraph>{madre.correo_electronico}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="madre.nacionalidad">Nacionalidad*</FormLabel>
                  <FormHelpText>&nbsp;</FormHelpText>
                  <FormInput
                    type="text"
                    name="madre.nacionalidad"
                    id="madre.nacionalidad"
                    required
                    maxLength={50}
                    onChange={onChangeDatosPersonalesInput}
                    value={madre.nacionalidad}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Nacionalidad</FormLabel>
                  <Paragraph>{madre.nacionalidad}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
        </React.Fragment>
      )}
      <FormSeparator />
      <FormTitle>Datos del padre</FormTitle>
      {canWrite() ? (
        <FormRow>
          <FormColumn left>
            <FormLabel htmlFor="padre.is_vivo">Vive*</FormLabel>
            <FormInput
              as="switch"
              name="padre.is_vivo"
              id="padre.is_vivo"
              checked={padre.is_vivo}
              falseText="No"
              trueText="Sí"
              onChange={onChangeDatosPersonalesInput}
            />
          </FormColumn>
        </FormRow>
      ) : (
        !padre.is_vivo && (
          <FormRow>
            <FormColumn>
              <FormLabel>Vive</FormLabel>
              <Paragraph>No</Paragraph>
            </FormColumn>
          </FormRow>
        )
      )}
      {padre.is_vivo && (
        <React.Fragment>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="padre.nombre_completo">Nombre completo*</FormLabel>
                  <FormInput
                    type="text"
                    name="padre.nombre_completo"
                    id="padre.nombre_completo"
                    required
                    maxLength={100}
                    onChange={onChangeDatosPersonalesInput}
                    value={padre.nombre_completo}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Nombre completo</FormLabel>
                  <Paragraph>{padre.nombre_completo}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="padre.telefono">Teléfono*</FormLabel>
                  <FormInput
                    type="tel"
                    name="padre.telefono"
                    id="padre.telefono"
                    required
                    minLength={10}
                    maxLength={10}
                    onChange={onChangeDatosPersonalesInput}
                    value={padre.telefono}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Teléfono</FormLabel>
                  <Paragraph>{padre.telefono}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="padre.correo_electronico">Correo electrónico <FormText>(Opcional)</FormText></FormLabel>
                  <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                  <FormInput
                    type="text"
                    name="padre.correo_electronico"
                    id="padre.correo_electronico"
                    maxLength={320}
                    pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                    title="Por favor ingrese un correo electrónico valido."
                    onChange={onChangeDatosPersonalesInput}
                    value={padre.correo_electronico}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Correo electrónico</FormLabel>
                  <Paragraph>{padre.correo_electronico}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="padre.nacionalidad">Nacionalidad*</FormLabel>
                  <FormHelpText>&nbsp;</FormHelpText>
                  <FormInput
                    type="text"
                    name="padre.nacionalidad"
                    id="padre.nacionalidad"
                    required
                    maxLength={50}
                    onChange={onChangeDatosPersonalesInput}
                    value={padre.nacionalidad}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Nacionalidad</FormLabel>
                  <Paragraph>{padre.nacionalidad}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
        </React.Fragment>
      )}
      <FormSeparator />
      <FormTitle>Datos de la pareja o cónyuge</FormTitle>
      {canWrite() ? (
        <FormRow>
          <FormColumn left>
            <FormLabel htmlFor="perfil.has_pareja_conyuge">¿Tiene pareja o cónyuge?*</FormLabel>
            <FormInput
              as="switch"
              name="perfil.has_pareja_conyuge"
              id="perfil.has_pareja_conyuge"
              checked={perfil.has_pareja_conyuge}
              falseText="No"
              trueText="Sí"
              onChange={onChangeHasParejaConyugeInput}
            />
          </FormColumn>
          {perfil.has_pareja_conyuge && (
            <FormColumn right>
              <FormLabel htmlFor="parejaConyuge.is_vivo">Vive*</FormLabel>
              <FormInput
                as="switch"
                name="parejaConyuge.is_vivo"
                id="parejaConyuge.is_vivo"
                checked={parejaConyuge.is_vivo}
                falseText="No"
                trueText="Sí"
                onChange={onChangeIsVivoInput}
              />
            </FormColumn>
          )}
        </FormRow>
      ) : (
        !perfil.has_pareja_conyuge ? (
          <FormRow>
            <FormColumn left>
              <FormLabel>¿Tiene pareja o cónyuge?*</FormLabel>
              <Paragraph>No</Paragraph>
            </FormColumn>
          </FormRow>
        ) : (perfil.has_pareja_conyuge && !parejaConyuge.is_vivo) && (
          <FormRow>
            <FormColumn left>
              <FormLabel>¿Tiene pareja o cónyuge?*</FormLabel>
              <Paragraph>Sí</Paragraph>
            </FormColumn>
            <FormColumn right>
              <FormLabel>Vive</FormLabel>
              <Paragraph>No</Paragraph>
            </FormColumn>
          </FormRow>
        )
      )}
      {(perfil.has_pareja_conyuge && parejaConyuge.is_vivo) && (
        <React.Fragment>
          <FormRow>
            <FormColumn>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="parejaConyuge.nombre_completo">Nombre completo*</FormLabel>
                  <FormInput
                    type="text"
                    name="parejaConyuge.nombre_completo"
                    id="parejaConyuge.nombre_completo"
                    required
                    maxLength={100}
                    onChange={onChangeDatosPersonalesInput}
                    value={parejaConyuge.nombre_completo}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Nombre completo</FormLabel>
                  <Paragraph>{parejaConyuge.nombre_completo}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="parejaConyuge.telefono">Teléfono*</FormLabel>
                  <FormHelpText>&nbsp;</FormHelpText>
                  <FormInput
                    type="tel"
                    name="parejaConyuge.telefono"
                    id="parejaConyuge.telefono"
                    required
                    minLength={10}
                    maxLength={10}
                    onChange={onChangeDatosPersonalesInput}
                    value={parejaConyuge.telefono}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Teléfono</FormLabel>
                  <Paragraph>{parejaConyuge.telefono}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="parejaConyuge.correo_electronico">Correo electrónico <FormText>(Opcional)</FormText></FormLabel>
                  <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                  <FormInput
                    type="text"
                    name="parejaConyuge.correo_electronico"
                    id="parejaConyuge.correo_electronico"
                    maxLength={320}
                    pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                    title="Por favor ingrese un correo electrónico valido."
                    onChange={onChangeDatosPersonalesInput}
                    value={parejaConyuge.correo_electronico}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Correo electrónico</FormLabel>
                  <Paragraph>{parejaConyuge.correo_electronico}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          {canWrite() ? (
            <FormRow>
              <FormColumn>
                <FormInput
                  as="checkbox"
                  name="parejaConyuge.has_mismo_domicilio"
                  id="parejaConyuge.has_mismo_domicilio"
                  checked={parejaConyuge.has_mismo_domicilio}
                  onChange={onChangeDatosPersonalesInput}
                  label="Tiene el mismo domicilio"
                />
              </FormColumn>
            </FormRow>
          ) : (
            parejaConyuge.has_mismo_domicilio && (
              <FormRow>
                <FormColumn>
                  <FormLabel>Tiene el mismo domicilio</FormLabel>
                </FormColumn>
              </FormRow>
            )
          )}
          {!parejaConyuge.has_mismo_domicilio && (
            <React.Fragment>
              <FormRow>
                <FormColumn left>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="parejaConyuge.calle_numero">Calle y número*</FormLabel>
                      <FormInput
                        type="text"
                        name="parejaConyuge.calle_numero"
                        id="parejaConyuge.calle_numero"
                        required
                        maxLength={100}
                        onChange={onChangeDatosPersonalesInput}
                        value={parejaConyuge.calle_numero}
                      />
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>Calle y número</FormLabel>
                      <Paragraph>{parejaConyuge.calle_numero}</Paragraph>
                    </React.Fragment>
                  )}
                </FormColumn>
                <FormColumn right>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="parejaConyuge.colonia">Colonia*</FormLabel>
                      <FormInput
                        type="text"
                        name="parejaConyuge.colonia"
                        id="parejaConyuge.colonia"
                        required
                        maxLength={100}
                        onChange={onChangeDatosPersonalesInput}
                        value={parejaConyuge.colonia}
                      />
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>Colonia</FormLabel>
                      <Paragraph>{parejaConyuge.colonia}</Paragraph>
                    </React.Fragment>
                  )}
                </FormColumn>
              </FormRow>
              <FormRow>
                <FormColumn left>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="parejaConyuge.codigo_postal">C.P. <FormText>(Opcional)</FormText></FormLabel>
                      <FormInput
                        type="number"
                        name="parejaConyuge.codigo_postal"
                        id="parejaConyuge.codigo_postal"
                        digits={5}
                        min={0}
                        max={99999}
                        step={1}
                        onChange={onChangeDatosPersonalesInput}
                        value={parejaConyuge.codigo_postal}
                      />
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>C.P.</FormLabel>
                      <Paragraph>{parejaConyuge.codigo_postal}</Paragraph>
                    </React.Fragment>
                  )}
                </FormColumn>
                <FormColumn center>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="parejaConyuge.municipio">Municipio*</FormLabel>
                      <FormInput
                        type="text"
                        name="parejaConyuge.municipio"
                        id="parejaConyuge.municipio"
                        required
                        maxLength={100}
                        onChange={onChangeDatosPersonalesInput}
                        value={parejaConyuge.municipio}
                      />
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>Municipio</FormLabel>
                      <Paragraph>{parejaConyuge.municipio}</Paragraph>
                    </React.Fragment>
                  )}
                </FormColumn>
                <FormColumn right>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="parejaConyuge.estado">Estado*</FormLabel>
                      <FormInput
                        type="text"
                        name="parejaConyuge.estado"
                        id="parejaConyuge.estado"
                        required
                        maxLength={40}
                        onChange={onChangeDatosPersonalesInput}
                        value={parejaConyuge.estado}
                      />
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>Estado</FormLabel>
                      <Paragraph>{parejaConyuge.estado}</Paragraph>
                    </React.Fragment>
                  )}
                </FormColumn>
              </FormRow>
            </React.Fragment>
          )}
          <FormRow>
            <FormColumn>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="parejaConyuge.acta_matrimonio_digital">Acta de matrimonio digital <FormText>(Opcional)</FormText></FormLabel>
                  <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF, Word, PowerPoint, Excel, .jpg o .png.</FormHelpText>
                  {helpers.isEmptyString(parejaConyuge.acta_matrimonio_digital) ? (
                    <FormFile accept=".pdf,.doc,.docx,.ppt,.pptx,.xsl,.xlsx,.jpg,.jpeg,.png" onChange={onChangeActaMatrimonioDigitalInput} />
                  ) : (
                    <File
                      file={parejaConyuge.acta_matrimonio_digital}
                      canDownload
                      isDownloading={isDownloadingActaMatrimonio}
                      onDownload={onDownloadActaMatrimonio}
                      onRemove={onRemoveActaMatrimonioDigitalInput}
                    />
                  )}
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Acta de matrimonio digital</FormLabel>
                  {!!parejaConyuge.acta_matrimonio_digital ? (
                    <File
                      file={parejaConyuge.acta_matrimonio_digital}
                      canDownload
                      isDownloading={isDownloadingActaMatrimonio}
                      onDownload={onDownloadActaMatrimonio}
                      onRemove={onRemoveActaMatrimonioDigitalInput}
                    />
                  ) : (
                    <Paragraph>No adjuntó archivo digital</Paragraph>
                  )}
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="parejaConyuge.acta_matrimonio_fisica">Donde encontrar el acta de matrimonio <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="parejaConyuge.acta_matrimonio_fisica"
                    id="parejaConyuge.acta_matrimonio_fisica"
                    maxLength={500}
                    onChange={onChangeDatosPersonalesInput}
                    value={parejaConyuge.acta_matrimonio_fisica}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Donde encontrar el acta de matrimonio</FormLabel>
                  <Paragraph>{parejaConyuge.acta_matrimonio_fisica}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
        </React.Fragment>
      )}
      <FormSeparator />
      <FormRow right>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          </FormColumn>
        )}
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
    </form>
  );
};

DatosPersonales.propTypes = {
  onSelectKey: PropTypes.func.isRequired,
  setUpdatedAt: PropTypes.func
};

export default DatosPersonales;
