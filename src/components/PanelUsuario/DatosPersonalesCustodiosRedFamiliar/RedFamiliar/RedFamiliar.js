import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import Paragraph from '../../../UI/Paragraph/Paragraph';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/datos-personales-custodios-red-familiar/red-familiar';

const RedFamiliar = (props) => {
  const user = useSelector((state) => state.auth.user);
  const familiares = useSelector((state) => state.redFamiliar.familiares);
  const familiar = useSelector((state) => state.redFamiliar.familiar);
  const isGetting = useSelector((state) => state.redFamiliar.isGetting);
  const isSaving = useSelector((state) => state.redFamiliar.isSaving);
  const isUpdating = useSelector((state) => state.redFamiliar.isUpdating);
  const isDeleting = useSelector((state) => state.redFamiliar.isDeleting);
  const dispatch = useDispatch();

  const hasFamiliares = (familiares.length > 0);

  const canWrite = () =>
    user?.can_write;

  const setUpdatedAt = () => {
    const [familiar] = familiares.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    props.setUpdatedAt(familiar?.updated_at);
  };

  const onChangeFamiliarInput = (event) => {
    const [, attribute] = event.target.name.split('.');

    dispatch(actions.changeFamiliarInput(attribute, event.target.value));
  };

  const onResetFamiliarInputs = () => {
    dispatch(actions.resetFamiliarInputs());
  };

  const onEditFamiliar = (familiar) => {
    window.scrollTo(0, 0);
    dispatch(actions.editFamiliar(familiar));
  };

  const onSaveOrUpdateFamiliar = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!familiar.id && await dispatch(actions.saveFamiliar(familiar))) {
      props.setToastMessage('Los datos del familiar se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (familiar.id && await dispatch(actions.updateFamiliar(familiar))) {
      props.setToastMessage('Los datos del familiar se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const onDeleteFamiliar = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este familiar?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteFamiliar(id))) {
      props.setToastMessage('Los datos del familiar se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    setUpdatedAt();

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [
      isGetting,
      isSaving,
      isUpdating,
      isDeleting
  ]);

  React.useEffect(() => {
    dispatch(actions.getFamiliares());

    return () => {
      onResetFamiliarInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      <FormRow>
        <FormColumn>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Regresar
          </Button>
        </FormColumn>
      </FormRow>
      {canWrite() && (
        <React.Fragment>
          <Paragraph title>La Red Familiar está conformada por aquellas personas con quienes deseas compartir tus experiencias, historias y legado, para lo cual, en caso de no validarse tu supervivencia, recibirán un vínculo de acceso a la sección "Mi historia y Legado".<br /><br /> Es importante señalar que no tendrán acceso al resto de los apartados del menú principal.</Paragraph>
          <form onSubmit={onSaveOrUpdateFamiliar}>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="familiar.nombre_completo">Nombre completo*</FormLabel>
                <FormInput
                  type="text"
                  name="familiar.nombre_completo"
                  id="familiar.nombre_completo"
                  required
                  maxLength={100}
                  onChange={onChangeFamiliarInput}
                  value={familiar.nombre_completo}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left strict>
                <FormLabel htmlFor="familiar.correo_electronico">Correo electrónico*</FormLabel>
                <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                <FormInput
                  type="text"
                  name="familiar.correo_electronico"
                  id="familiar.correo_electronico"
                  required
                  maxLength={320}
                  pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                  title="Por favor ingrese un correo electrónico valido."
                  onChange={onChangeFamiliarInput}
                  value={familiar.correo_electronico}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetFamiliarInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!familiar.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Nombre completo</th>
            <th>Correo electrónico</th>
            {canWrite() && (<th></th>)}
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan={3}>Cargando...</td>
            </tr>
          ) : hasFamiliares ? familiares.map((familiar) => (
            <tr key={familiar.id}>
              <td>{familiar.nombre_completo}</td>
              <td>{familiar.correo_electronico}</td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="secondary"
                      small
                      onClick={() => onEditFamiliar(familiar)}
                    >
                      Editar
                    </Button>
                    <Button
                      variant="danger"
                      small
                      onClick={() => onDeleteFamiliar(familiar.id)}
                    >
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={3}>
                {canWrite() ? 'No has agregado ningún familiar.' : 'No se agregó ningún familiar.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
      <FormSeparator />
      <FormRow>
        <FormColumn>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Regresar
          </Button>
        </FormColumn>
      </FormRow>
    </React.Fragment>
  );
};

RedFamiliar.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default RedFamiliar;
