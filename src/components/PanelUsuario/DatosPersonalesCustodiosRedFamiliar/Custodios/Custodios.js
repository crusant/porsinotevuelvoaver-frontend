import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import Paragraph from '../../../UI/Paragraph/Paragraph';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/datos-personales-custodios-red-familiar/custodios';

const Custodios = (props) => {
  const user = useSelector((state) => state.auth.user);
  const custodios = useSelector((state) => state.custodios.custodios);
  const custodio = useSelector((state) => state.custodios.custodio);
  const parentescos = useSelector((state) => state.custodios.parentescos);
  const isLoading = useSelector((state) => state.custodios.isLoading);
  const isSaving = useSelector((state) => state.custodios.isSaving);
  const isUpdating = useSelector((state) => state.custodios.isUpdating);
  const isDeleting = useSelector((state) => state.custodios.isDeleting);
  const dispatch = useDispatch();

  const hasCustodios = (custodios.length > 0);
  const hasParentescos = (parentescos.length > 0);

  const canWrite = () =>
    user?.can_write;

  const isPlanPremium = () =>
    user?.suscripcion?.is_premium;

  const isSuscripcionActiva = () =>
    user?.suscripcion?.is_activo;

  const isDisabledForm = () => {
    return !isPlanPremium() && (custodios.length >= 2) && (custodio && !custodio.id);
  };

  const setUpdatedAt = () => {
    const [custodio] = custodios.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    props.setUpdatedAt(custodio?.updated_at);
  };

  const onChangeCustodioInput = (event) => {
    const input = event.target;
    const [, attribute] = input.name.split('.');

    dispatch(actions.changeCustodioInput(attribute, input.value));
  };

  const onResetCustodioInputs = () => {
    dispatch(actions.resetCustodioInputs());
  };

  const onEditCustodio = (custodio) => {
    window.scrollTo(0, 0);
    dispatch(actions.editCustodio(custodio));
  };

  const onSaveOrUpdateCustodio = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!custodio.id && await dispatch(actions.saveCustodio(custodio))) {
      props.setToastMessage('Los datos del custodio se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (custodio.id && await dispatch(actions.updateCustodio(custodio))) {
      props.setToastMessage('Los datos del custodio se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const getParentescoNombre = (id) => {
    const parentesco = parentescos.find((parentesco) => parentesco.id === parseInt(id, 10));

    if (parentesco) {
      return parentesco.nombre;
    }

    return null;
  };

  const onDeleteCustodio = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este custodio?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteCustodio(id))) {
      props.setToastMessage('Los datos del custodio se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    setUpdatedAt();

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [
    isLoading,
    isSaving,
    isUpdating,
    isDeleting
  ]);

  React.useEffect(() => {
    dispatch(actions.getCustodios());

    return () => {
      onResetCustodioInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('0')}
          >
            Regresar
          </Button>
        </FormColumn>
        {(isPlanPremium() && isSuscripcionActiva()) && (
          <FormColumn auto>
            <Button
              variant="primary"
              disabled={isSaving}
              onClick={() => props.onSelectKey('2')}
            >
              Siguiente
            </Button>
          </FormColumn>
        )}
      </FormRow>
      {canWrite() && (
        <React.Fragment>
          {(!isPlanPremium() || !isSuscripcionActiva()) && (
            <Alert variant="warning">
              <Paragraph>Para agregar más custodios, debes actualizar tu plan gratuito a un plan de pago.</Paragraph>
            </Alert>
          )}
          <Paragraph title>Los custodios deben ser personas responsables y de tu entera confianza, son quienes validarán tu supervivencia en caso no contar con tu respuesta a la prueba de vida y, en caso de desaparición o fallecimiento, tendrán acceso a toda información registrada en los apartados.<br /><br /> Su misión será apoyarte a cumplir con tu voluntad y deseos. Para que puedan llevar a cabo sus funciones es muy importante contar con el registro de su correo electrónico y número de celular actualizado.</Paragraph>
          <form onSubmit={onSaveOrUpdateCustodio}>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="custodio.nombre_completo">Nombre completo*</FormLabel>
                <FormInput
                  type="text"
                  name="custodio.nombre_completo"
                  id="custodio.nombre_completo"
                  disabled={isDisabledForm()}
                  required
                  maxLength={100}
                  onChange={onChangeCustodioInput}
                  value={custodio.nombre_completo}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="custodio.celular">Número de celular*</FormLabel>
                <FormHelpText>&nbsp;</FormHelpText>
                <FormInput
                  type="tel"
                  name="custodio.celular"
                  id="custodio.celular"
                  disabled={isDisabledForm()}
                  required
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeCustodioInput}
                  value={custodio.celular}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="custodio.correo_electronico">Correo electrónico*</FormLabel>
                <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                <FormInput
                  type="text"
                  name="custodio.correo_electronico"
                  id="custodio.correo_electronico"
                  disabled={isDisabledForm()}
                  required
                  maxLength={320}
                  pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                  title="Por favor ingrese un correo electrónico valido."
                  onChange={onChangeCustodioInput}
                  value={custodio.correo_electronico}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="custodio.motivos">¿Porqué te escogí a ti como custodio? <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  as="textarea"
                  name="custodio.motivos"
                  id="custodio.motivos"
                  rows={3}
                  disabled={isDisabledForm()}
                  maxLength={500}
                  onChange={onChangeCustodioInput}
                  value={custodio.motivos}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left strict>
                <FormLabel htmlFor="custodio.parentesco_id">Parentesco*</FormLabel>
                <FormInput
                  as="select"
                  name="custodio.parentesco_id"
                  id="custodio.parentesco_id"
                  disabled={!hasParentescos || isDisabledForm()}
                  required
                  onChange={onChangeCustodioInput}
                  value={custodio.parentesco_id}
                >
                  <option value="">Seleccione...</option>
                  {parentescos.map(({ id, nombre }) => (
                    <option key={id} value={id}>
                      {nombre}
                    </option>
                  ))}
                </FormInput>
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating || isDisabledForm()}
                  onClick={onResetCustodioInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!custodio.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving || isDisabledForm()}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Nombre completo</th>
            <th>Número de celular</th>
            <th>Correo electrónico</th>
            <th>Parentesco</th>
            {canWrite() && (<th></th>)}
          </tr>
        </thead>
        <tbody>
          {isLoading ? (
            <tr>
              <td colSpan={5}>Cargando...</td>
            </tr>
          ) : hasCustodios ? custodios.map((custodio, index) => (
            <tr key={custodio.id}>
              <td>{custodio.nombre_completo}</td>
              <td>{custodio.celular}</td>
              <td>{custodio.correo_electronico}</td>
              <td>{getParentescoNombre(custodio.parentesco_id)}</td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="secondary"
                      small
                      onClick={() => onEditCustodio(custodio)}
                    >
                      Editar
                    </Button>
                    <Button
                      variant="danger"
                      small
                      onClick={() => onDeleteCustodio(custodio.id)}
                    >
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={5}>
                {canWrite() ? 'No has agregado ningún custodio.' : 'No se agregó ningún custodio.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
      <FormSeparator />
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('0')}
          >
            Regresar
          </Button>
        </FormColumn>
        {(isPlanPremium() && isSuscripcionActiva()) && (
          <FormColumn auto>
            <Button
              variant="primary"
              disabled={isSaving}
              onClick={() => props.onSelectKey('2')}
            >
              Siguiente
            </Button>
          </FormColumn>
        )}
      </FormRow>
    </React.Fragment>
  );
};

Custodios.propTypes = {
  onSelectKey: PropTypes.func.isRequired,
  setUpdatedAt: PropTypes.func
};

export default Custodios;
