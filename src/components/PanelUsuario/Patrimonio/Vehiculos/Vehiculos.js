import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/patrimonio/vehiculos';

const Vehiculo = (props) => {
  const user = useSelector((state) => state.auth.user);
  const vehiculo = useSelector((state) => state.vehiculos.vehiculo);
  const vehiculos = useSelector((state) => state.vehiculos.vehiculos);
  const isGetting = useSelector((state) => state.vehiculos.isGetting);
  const isSaving = useSelector((state) => state.vehiculos.isSaving);
  const isUpdating = useSelector((state) => state.vehiculos.isUpdating);
  const isDeleting = useSelector((state) => state.vehiculos.isDeleting);
  const dispatch = useDispatch();

  const hasVehiculos = (vehiculos.length > 0);

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [vehiculo] = vehiculos.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    return vehiculo?.updated_at;
  };

  const onChangeInput = (event) => {
    const input = event.target;
    const [entity, attribute] = input.name.split('.');
    const value = input.value;

    dispatch(actions.changeVehiculoInputs(entity, attribute, value));
  };

  const onResetVehiculoInputs = () => {
    dispatch(actions.resetVehiculoInputs());
  };

  const onSaveOrUpdateVehiculo = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!vehiculo.id && await dispatch(actions.saveVehiculo(vehiculo))) {
      props.setToastMessage('Los datos del vehículo se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (vehiculo.id && await dispatch(actions.updateVehiculo(vehiculo))) {
      props.setToastMessage('Los datos del vehículo se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const onLoadInputs = (vehiculo) => {
    window.scrollTo(0, 0);
    dispatch(actions.loadVehiculoInputs(vehiculo));
  };

  const onDeleteVehiculo = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este vehículo?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteVehiculo(id))) {
      props.setToastMessage('Los datos del vehículo se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    props.setUpdatedAt(getUpdatedAt());

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [
    isGetting,
    isSaving,
    isUpdating,
    isDeleting
  ]);

  React.useEffect(() => {
    dispatch(actions.getVehiculos());

    return () => {
      onResetVehiculoInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateVehiculo}>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="vehiculo.marca_tipo">Marca y tipo*</FormLabel>
                <FormInput
                  type="text"
                  name="vehiculo.marca_tipo"
                  id="vehiculo.marca_tipo"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={vehiculo.marca_tipo}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="vehiculo.modelo">Modelo*</FormLabel>
                <FormInput
                  type="number"
                  name="vehiculo.modelo"
                  id="vehiculo.modelo"
                  required
                  digits={4}
                  min={0}
                  max={9999}
                  step={1}
                  onChange={onChangeInput}
                  value={vehiculo.modelo}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="vehiculo.serie">Número de serie*</FormLabel>
                <FormInput
                  type="text"
                  name="vehiculo.serie"
                  id="vehiculo.serie"
                  required
                  maxLength={50}
                  onChange={onChangeInput}
                  value={vehiculo.serie}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="vehiculo.lugar_registro">Lugar de registro <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="vehiculo.lugar_registro"
                  id="vehiculo.lugar_registro"
                  maxLength={100}
                  onChange={onChangeInput}
                  value={vehiculo.lugar_registro}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="vehiculo.documento_propiedad_fisico">Donde encontrar el documento de propiedad <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="vehiculo.documento_propiedad_fisico"
                  id="vehiculo.documento_propiedad_fisico"
                  maxLength={500}
                  onChange={onChangeInput}
                  value={vehiculo.documento_propiedad_fisico}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="vehiculo.otros_documentos_fisico">Donde encontrar otros documentos (tenencia, control vehícular, etc) <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="vehiculo.otros_documentos_fisico"
                  id="vehiculo.otros_documentos_fisico"
                  maxLength={500}
                  onChange={onChangeInput}
                  value={vehiculo.otros_documentos_fisico}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="vehiculo.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="vehiculo.indicaciones"
                  id="vehiculo.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeInput}
                  value={vehiculo.indicaciones}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetVehiculoInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!vehiculo.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Marca y tipo</th>
            <th>Modelo</th>
            <th>Número de serie</th>
            <th>Lugar de registro</th>
            <th>Donde encontrar el documento de propiedad</th>
            <th>Donde encontrar otros documentos (tenencia, control vehícular, etc)</th>
            <th>Instrucciones</th>
            {canWrite() && (<th></th>)}
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan={8}>Cargando...</td>
            </tr>
          ) : hasVehiculos ? vehiculos.map((vehiculo) => (
            <tr key={vehiculo.id}>
              <td>{vehiculo.marca_tipo}</td>
              <td>{vehiculo.modelo}</td>
              <td>{vehiculo.serie}</td>
              <td>{vehiculo.lugar_registro}</td>
              <td>{vehiculo.documento_propiedad_fisico}</td>
              <td>{vehiculo.otros_documentos_fisico}</td>
              <td>{vehiculo.indicaciones}</td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="secondary"
                      small
                      onClick={() => onLoadInputs(vehiculo)}
                    >
                      Editar
                    </Button>
                    <Button
                      variant="danger"
                      small
                      onClick={() => onDeleteVehiculo(vehiculo.id)}
                    >
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={8}>
                {canWrite() ? 'No has agregado ningún vehículo.' : 'No se agregó ningún vehículo.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </React.Fragment>
  );
};

export default Vehiculo;
