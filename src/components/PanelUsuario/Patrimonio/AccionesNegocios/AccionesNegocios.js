import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormControl from '../../../UI/Form/FormControl/FormControl';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../../UI/Form/FormTitle/FormTitle';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/patrimonio/acciones-negocios';
import * as helpers from '../../../../helpers';

const AccionesNegocios = (props) => {
  const [rowKey, setRowKey] = React.useState(-1);
  const user = useSelector((state) => state.auth.user);
  const accionNegocio = useSelector((state) => state.accionesNegocios.accionNegocio);
  const socio = useSelector((state) => state.accionesNegocios.socio);
  const socios = useSelector((state) => state.accionesNegocios.accionNegocio.socios);
  const accionesNegocios = useSelector((state) => state.accionesNegocios.accionesNegocios);
  const isGetting = useSelector((state) => state.accionesNegocios.isGetting);
  const isSaving = useSelector((state) => state.accionesNegocios.isSaving);
  const isUpdating = useSelector((state) => state.accionesNegocios.isUpdating);
  const isDeleting = useSelector((state) => state.accionesNegocios.isDeleting);
  const dispatch = useDispatch();

  const hasSocios = (socios.length > 0);
  const hasAccionesNegocios = (accionesNegocios.length > 0);

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [accionNegocio] = accionesNegocios.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    return accionNegocio?.updated_at;
  };

  const onChangeInput = (event) => {
    const input = event.target;
    const [entity, attribute] = input.name.split('.');
    const value = input.value;

    dispatch(actions.changeAccionNegocioInputs(entity, attribute, value));
  };

  const onAddSocio = () => {
    const nombreCompleto = socio.nombre_completo;

    if (helpers.isEmptyString(nombreCompleto)) {
      return;
    }

    dispatch(actions.addSocio(socio));
    dispatch(actions.resetSocio());
  };

  const onRemoveSocio = (index) => {
    dispatch(actions.removeSocio(index));
  };

  const onResetAccionNegocioInputs = () => {
    dispatch(actions.resetAccionNegocioInputs());
  };

  const onSaveOrUpdateAccionNegocio = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!accionNegocio.id && await dispatch(actions.saveAccionNegocio(accionNegocio))) {
      props.setToastMessage('Los datos de la acción o negocio se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (accionNegocio.id && await dispatch(actions.updateAccionNegocio(accionNegocio))) {
      props.setToastMessage('Los datos de la acción o negocio se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const onToggleRow = (key) => {
    if (rowKey !== key) {
      setRowKey(key);
    } else {
      setRowKey(-1);
    }
  };

  const onLoadInputs = (accionNegocio) => {
    window.scrollTo(0, 0);
    dispatch(actions.loadAccionNegocioInputs(accionNegocio));
  };

  const onDeleteAccionNegocio = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar esta acción o negocio?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteAccionNegocio(id))) {
      props.setToastMessage('Los datos de la acción o negocio se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    props.setUpdatedAt(getUpdatedAt());

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [
    isGetting,
    isSaving,
    isUpdating,
    isDeleting
  ]);

  React.useEffect(() => {
    dispatch(actions.getAccionesNegocios());

    return () => {
      onResetAccionNegocioInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateAccionNegocio}>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="accionNegocio.nombre">Nombre*</FormLabel>
                <FormInput
                  type="text"
                  name="accionNegocio.nombre"
                  id="accionNegocio.nombre"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={accionNegocio.nombre}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="accionNegocio.tipo_negocio">Tipo de negocio*</FormLabel>
                <FormInput
                  type="text"
                  name="accionNegocio.tipo_negocio"
                  id="accionNegocio.tipo_negocio"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={accionNegocio.tipo_negocio}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="accionNegocio.porcentaje_participacion">Porcentaje de participación*</FormLabel>
                <FormInput
                  type="number"
                  name="accionNegocio.porcentaje_participacion"
                  id="accionNegocio.porcentaje_participacion"
                  min={1}
                  max={100}
                  step={1}
                  onChange={onChangeInput}
                  value={accionNegocio.porcentaje_participacion}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="accionNegocio.acuerdos_compra_venta">Acuerdos de compra-venta <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="accionNegocio.acuerdos_compra_venta"
                  id="accionNegocio.acuerdos_compra_venta"
                  maxLength={100}
                  onChange={onChangeInput}
                  value={accionNegocio.acuerdos_compra_venta}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="accionNegocio.telefono">Teléfono*</FormLabel>
                <FormHelpText>&nbsp;</FormHelpText>
                <FormInput
                  type="tel"
                  name="accionNegocio.telefono"
                  id="accionNegocio.telefono"
                  required
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeInput}
                  value={accionNegocio.telefono}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="accionNegocio.correo_electronico">Correo electrónico <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                <FormInput
                  type="text"
                  name="accionNegocio.correo_electronico"
                  id="accionNegocio.correo_electronico"
                  maxLength={320}
                  pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                  title="Por favor ingrese un correo electrónico valido."
                  onChange={onChangeInput}
                  value={accionNegocio.correo_electronico}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormTitle>Domicilio</FormTitle>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="accionNegocio.colonia">Colonia <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="accionNegocio.colonia"
                  id="accionNegocio.colonia"
                  maxLength={100}
                  onChange={onChangeInput}
                  value={accionNegocio.colonia}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="accionNegocio.ciudad">Ciudad <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="accionNegocio.ciudad"
                  id="accionNegocio.ciudad"
                  maxLength={100}
                  onChange={onChangeInput}
                  value={accionNegocio.ciudad}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="accionNegocio.codigo_postal">C.P. <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="number"
                  name="accionNegocio.codigo_postal"
                  id="accionNegocio.codigo_postal"
                  digits={5}
                  min={0}
                  max={99999}
                  step={1}
                  onChange={onChangeInput}
                  value={accionNegocio.codigo_postal}
                />
              </FormColumn>
              <FormColumn center>
                <FormLabel htmlFor="accionNegocio.municipio">Municipio <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="accionNegocio.municipio"
                  id="accionNegocio.municipio"
                  maxLength={100}
                  onChange={onChangeInput}
                  value={accionNegocio.municipio}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="accionNegocio.estado">Estado <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="accionNegocio.estado"
                  id="accionNegocio.estado"
                  maxLength={40}
                  onChange={onChangeInput}
                  value={accionNegocio.estado}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormTitle>Socios <FormText>(Opcional)</FormText></FormTitle>
            <FormTable>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre completo</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td>
                    <FormControl
                      type="text"
                      name="socio.nombre_completo"
                      id="socio.nombre_completo"
                      maxLength={100}
                      onChange={onChangeInput}
                      value={socio.nombre_completo}
                    />
                  </td>
                  <td>
                    <Button
                      variant="primary"
                      small
                      onClick={onAddSocio}
                    >
                      Agregar
                    </Button>
                  </td>
                </tr>
                {hasSocios ? socios.map((socio, index) => (
                  <tr key={index}>
                    <td>{(index + 1)}</td>
                    <td>{socio.nombre_completo}</td>
                    <td>
                      <Button
                        variant="danger"
                        small
                        onClick={() => onRemoveSocio(index)}
                      >
                        Quitar
                      </Button>
                    </td>
                  </tr>
                )) : (
                  <tr>
                    <td colSpan={3}>No has agregado ningún socio.</td>
                  </tr>
                )}
              </tbody>
            </FormTable>
            <FormSeparator />
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="accionNegocio.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="accionNegocio.indicaciones"
                  id="accionNegocio.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeInput}
                  value={accionNegocio.indicaciones}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetAccionNegocioInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!accionNegocio.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th rowSpan={2}>Nombre</th>
            <th rowSpan={2}>Tipo de negocio</th>
            <th rowSpan={2}>Porcentaje de participación</th>
            <th rowSpan={2}>Acuerdos de compra-venta</th>
            <th rowSpan={2}>Teléfono</th>
            <th rowSpan={2}>Correo electrónico</th>
            <th colSpan={5}>Domicilio</th>
            <th rowSpan={2}>Instrucciones</th>
            <th rowSpan={2}></th>
          </tr>
          <tr>
            <th>Colonia</th>
            <th>Ciudad</th>
            <th>C.P.</th>
            <th>Municipio</th>
            <th>Estado</th>
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan={13}>Cargando...</td>
            </tr>
          ) : hasAccionesNegocios ? accionesNegocios.map((accionNegocio, index) => (
            <React.Fragment key={accionNegocio.id}>
              <tr>
                <td>{accionNegocio.nombre}</td>
                <td>{accionNegocio.tipo_negocio}</td>
                <td>{accionNegocio.porcentaje_participacion} %</td>
                <td>{accionNegocio.acuerdos_compra_venta}</td>
                <td>{accionNegocio.telefono}</td>
                <td>{accionNegocio.correo_electronico}</td>
                <td>{accionNegocio.colonia}</td>
                <td>{accionNegocio.ciudad}</td>
                <td>{accionNegocio.codigo_postal}</td>
                <td>{accionNegocio.municipio}</td>
                <td>{accionNegocio.estado}</td>
                <td>{accionNegocio.indicaciones}</td>
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="light"
                      small
                      onClick={() => onToggleRow(index)}
                    >
                      Ver más
                    </Button>
                    {canWrite() && (
                      <React.Fragment>
                        <Button
                          variant="secondary"
                          small
                          onClick={() => onLoadInputs(accionNegocio)}
                        >
                          Editar
                        </Button>
                        <Button
                          variant="danger"
                          small
                          onClick={() => onDeleteAccionNegocio(accionNegocio.id)}
                        >
                          Eliminar
                        </Button>
                      </React.Fragment>
                    )}
                  </FormButtonGroup>
                </td>
              </tr>
              {(rowKey === index) && (
                <tr>
                  <td colSpan={13}>
                    <FormTable sub>
                      <thead>
                        <tr>
                          <th>Socios</th>
                        </tr>
                        <tr>
                          <th>Nombre completo</th>
                        </tr>
                      </thead>
                      <tbody>
                        {(accionNegocio.socios.length > 0) ? accionNegocio.socios.map((socio, index) => (
                          <tr key={index}>
                            <td>{socio.nombre_completo}</td>
                          </tr>
                        )) : (
                          <tr>
                            <td>
                              {canWrite() ? 'No agregaste ningún asesor.' : 'No se agregó ningún asesor.'}
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </FormTable>
                  </td>
                </tr>
              )}
            </React.Fragment>
          )) : (
            <tr>
              <td colSpan={13}>
                {canWrite() ? 'No has guardado ninguna acción o negocio.' : 'No se agregó ninguna acción o negocio.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </React.Fragment>
  );
};

export default AccionesNegocios;
