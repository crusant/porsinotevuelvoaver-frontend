import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../../UI/Form/FormTitle/FormTitle';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/patrimonio/otros-patrimonios';

const OtrosPatrimonios = (props) => {
  const user = useSelector((state) => state.auth.user);
  const otroPatrimonio = useSelector((state) => state.otrosPatrimonios.otroPatrimonio);
  const otrosPatrimonios = useSelector((state) => state.otrosPatrimonios.otrosPatrimonios);
  const isGetting = useSelector((state) => state.otrosPatrimonios.isGetting);
  const isSaving = useSelector((state) => state.otrosPatrimonios.isSaving);
  const isUpdating = useSelector((state) => state.otrosPatrimonios.isUpdating);
  const isDeleting = useSelector((state) => state.otrosPatrimonios.isDeleting);
  const dispatch = useDispatch();

  const hasOtrosPatrimonios = (otrosPatrimonios.length > 0);

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [otroPatrimonio] = otrosPatrimonios.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    return otroPatrimonio?.updated_at;
  };

  const onChangeInput = (event) => {
    const input = event.target;
    const [entity, attribute] = input.name.split('.');
    const value = input.value;

    dispatch(actions.changeOtroPatrimonioInputs(entity, attribute, value));
  };

  const onResetOtroPatrimonioInputs = () => {
    dispatch(actions.resetOtroPatrimonioInputs());
  };

  const onSaveOrUpdateOtroPatrimonio = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!otroPatrimonio.id && await dispatch(actions.saveOtroPatrimonio(otroPatrimonio))) {
      props.setToastMessage('Los datos del patrimonio se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (otroPatrimonio.id && await dispatch(actions.updateOtroPatrimonio(otroPatrimonio))) {
      props.setToastMessage('Los datos del patrimonio se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const onLoadInputs = (otroPatrimonio) => {
    window.scrollTo(0, 0);
    dispatch(actions.loadOtroPatrimonioInputs(otroPatrimonio));
  };

  const onDeleteOtroPatrimonio = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este patrimonio?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteOtroPatrimonio(id))) {
      props.setToastMessage('Los datos del patrimonio se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    props.setUpdatedAt(getUpdatedAt());

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [
    isGetting,
    isSaving,
    isUpdating,
    isDeleting
  ]);

  React.useEffect(() => {
    dispatch(actions.getOtrosPatrimonios());

    return () => {
      onResetOtroPatrimonioInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateOtroPatrimonio}>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="otroPatrimonio.tipo_valor">Tipo de valor*</FormLabel>
                <FormInput
                  type="text"
                  name="otroPatrimonio.tipo_valor"
                  id="otroPatrimonio.tipo_valor"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={otroPatrimonio.tipo_valor}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="otroPatrimonio.titulo_propiedad">Título de propiedad <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="otroPatrimonio.titulo_propiedad"
                  id="otroPatrimonio.titulo_propiedad"
                  maxLength={100}
                  onChange={onChangeInput}
                  value={otroPatrimonio.titulo_propiedad}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="otroPatrimonio.contacto">Nombre completo de la persona de contacto*</FormLabel>
                <FormInput
                  type="text"
                  name="otroPatrimonio.contacto"
                  id="otroPatrimonio.contacto"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={otroPatrimonio.contacto}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="otroPatrimonio.telefono">Teléfono*</FormLabel>
                <FormHelpText>&nbsp;</FormHelpText>
                <FormInput
                  type="tel"
                  name="otroPatrimonio.telefono"
                  id="otroPatrimonio.telefono"
                  required
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeInput}
                  value={otroPatrimonio.telefono}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="otroPatrimonio.correo_electronico">Correo electrónico <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                <FormInput
                  type="text"
                  name="otroPatrimonio.correo_electronico"
                  id="otroPatrimonio.correo_electronico"
                  maxLength={320}
                  pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                  title="Por favor ingrese un correo electrónico valido."
                  onChange={onChangeInput}
                  value={otroPatrimonio.correo_electronico}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormTitle>Domicilio</FormTitle>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="otroPatrimonio.colonia">Colonia <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="otroPatrimonio.colonia"
                  id="otroPatrimonio.colonia"
                  maxLength={100}
                  onChange={onChangeInput}
                  value={otroPatrimonio.colonia}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="otroPatrimonio.ciudad">Ciudad <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="otroPatrimonio.ciudad"
                  id="otroPatrimonio.ciudad"
                  maxLength={100}
                  onChange={onChangeInput}
                  value={otroPatrimonio.ciudad}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="otroPatrimonio.codigo_postal">C.P. <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="number"
                  name="otroPatrimonio.codigo_postal"
                  id="otroPatrimonio.codigo_postal"
                  digits={5}
                  min={0}
                  max={99999}
                  step={1}
                  onChange={onChangeInput}
                  value={otroPatrimonio.codigo_postal}
                />
              </FormColumn>
              <FormColumn center>
                <FormLabel htmlFor="otroPatrimonio.municipio">Municipio <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="otroPatrimonio.municipio"
                  id="otroPatrimonio.municipio"
                  maxLength={100}
                  onChange={onChangeInput}
                  value={otroPatrimonio.municipio}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="otroPatrimonio.estado">Estado <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="otroPatrimonio.estado"
                  id="otroPatrimonio.estado"
                  maxLength={40}
                  onChange={onChangeInput}
                  value={otroPatrimonio.estado}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="otroPatrimonio.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="otroPatrimonio.indicaciones"
                  id="otroPatrimonio.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeInput}
                  value={otroPatrimonio.indicaciones}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetOtroPatrimonioInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!otroPatrimonio.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th rowSpan={2}>Tipo de valor*</th>
            <th rowSpan={2}>Título de propiedad</th>
            <th rowSpan={2}>Nombre completo de la persona de contacto</th>
            <th rowSpan={2}>Teléfono</th>
            <th rowSpan={2}>Correo electrónico</th>
            <th colSpan={5}>Domicilio</th>
            <th rowSpan={2}>Instrucciones*</th>
            {canWrite() && (<th rowSpan={2}></th>)}
          </tr>
          <tr>
            <th>Colonia</th>
            <th>Ciudad</th>
            <th>C.P.</th>
            <th>Municipio</th>
            <th>Estado</th>
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan={12}>Cargando...</td>
            </tr>
          ) : hasOtrosPatrimonios ? otrosPatrimonios.map((otroPatrimonio) => (
            <tr key={otroPatrimonio.id}>
              <td>{otroPatrimonio.tipo_valor}</td>
              <td>{otroPatrimonio.titulo_propiedad}</td>
              <td>{otroPatrimonio.contacto}</td>
              <td>{otroPatrimonio.telefono}</td>
              <td>{otroPatrimonio.correo_electronico}</td>
              <td>{otroPatrimonio.colonia}</td>
              <td>{otroPatrimonio.ciudad}</td>
              <td>{otroPatrimonio.codigo_postal}</td>
              <td>{otroPatrimonio.municipio}</td>
              <td>{otroPatrimonio.estado}</td>
              <td>{otroPatrimonio.indicaciones}</td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="secondary"
                      small
                      onClick={() => onLoadInputs(otroPatrimonio)}
                    >
                      Editar
                    </Button>
                    <Button
                      variant="danger"
                      small
                      onClick={() => onDeleteOtroPatrimonio(otroPatrimonio.id)}
                    >
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={12}>
                {canWrite() ? 'No has agregado ningún otro patrimonio.' : 'No se agregó ningún otro patrimonio.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </React.Fragment>
  );
};

export default OtrosPatrimonios;
