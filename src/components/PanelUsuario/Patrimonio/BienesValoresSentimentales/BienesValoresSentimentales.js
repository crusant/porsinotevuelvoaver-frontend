import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/patrimonio/bienes-valores-sentimentales';

const BienesValoresSentimentales = (props) => {
  const user = useSelector((state) => state.auth.user);
  const bienValorSentimental = useSelector((state) => state.bienesValoresSentimentales.bienValorSentimental);
  const bienesValoresSentimentales = useSelector((state) => state.bienesValoresSentimentales.bienesValoresSentimentales);
  const isGetting = useSelector((state) => state.bienesValoresSentimentales.isGetting);
  const isSaving = useSelector((state) => state.bienesValoresSentimentales.isSaving);
  const isUpdating = useSelector((state) => state.bienesValoresSentimentales.isUpdating);
  const isDeleting = useSelector((state) => state.bienesValoresSentimentales.isDeleting);
  const dispatch = useDispatch();

  const hasBienesValoresSentimentales = (bienesValoresSentimentales.length > 0);

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [bienValorSentimental] = bienesValoresSentimentales.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    return bienValorSentimental?.updated_at;
  };

  const onChangeInput = (event) => {
    const input = event.target;
    const [entity, attribute] = input.name.split('.');
    const value = input.value;

    dispatch(actions.changeBienValorSentimentalInputs(entity, attribute, value));
  };

  const onResetBienValorSentimentalInputs = () => {
    dispatch(actions.resetBienValorSentimentalInputs());
  };

  const onSaveOrUpdateBienValorSentimental = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!bienValorSentimental.id && await dispatch(actions.saveBienValorSentimental(bienValorSentimental))) {
      props.setToastMessage('Los datos del bien con valor sentimental se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (bienValorSentimental.id && await dispatch(actions.updateBienValorSentimental(bienValorSentimental))) {
      props.setToastMessage('Los datos del bien con valor sentimental se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const onLoadInputs = (bienValorSentimental) => {
    window.scrollTo(0, 0);
    dispatch(actions.loadBienValorSentimentalInputs(bienValorSentimental));
  };

  const onDeleteBienValorSentimental =async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este bien con valor sentimental?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteBienValorSentimental(id))) {
      props.setToastMessage('Los datos del bien con valor sentimental se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    props.setUpdatedAt(getUpdatedAt());

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [
    isGetting,
    isSaving,
    isUpdating,
    isDeleting
  ]);

  React.useEffect(() => {
    dispatch(actions.getBienesValoresSentimentales());

    return () => {
      onResetBienValorSentimentalInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateBienValorSentimental}>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="bienValorSentimental.objeto">Objeto*</FormLabel>
                <FormInput
                  type="text"
                  name="bienValorSentimental.objeto"
                  id="bienValorSentimental.objeto"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={bienValorSentimental.objeto}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="bienValorSentimental.ubicacion_fisica">Donde encontrar el objeto <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="bienValorSentimental.ubicacion_fisica"
                  id="bienValorSentimental.ubicacion_fisica"
                  maxLength={500}
                  onChange={onChangeInput}
                  value={bienValorSentimental.ubicacion_fisica}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="bienValorSentimental.destino_final">Destino final deseado del bien*</FormLabel>
                <FormInput
                  type="text"
                  name="bienValorSentimental.destino_final"
                  id="bienValorSentimental.destino_final"
                  required
                  maxLength={500}
                  onChange={onChangeInput}
                  value={bienValorSentimental.destino_final}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetBienValorSentimentalInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!bienValorSentimental.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Objeto</th>
            <th>Donde encontrar el objeto</th>
            <th>Destino final deseado del bien</th>
            {canWrite() && (<th></th>)}
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan={4}>Cargando...</td>
            </tr>
          ) : hasBienesValoresSentimentales ? bienesValoresSentimentales.map((bienValorSentimental) => (
            <tr key={bienValorSentimental.id}>
              <td>{bienValorSentimental.objeto}</td>
              <td>{bienValorSentimental.ubicacion_fisica}</td>
              <td>{bienValorSentimental.destino_final}</td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="secondary"
                      small
                      onClick={() => onLoadInputs(bienValorSentimental)}
                    >
                      Editar
                    </Button>
                    <Button
                      variant="danger"
                      small
                      onClick={() => onDeleteBienValorSentimental(bienValorSentimental.id)}
                    >
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={4}>
                {canWrite() ? 'No has agregado ningún bien con valor sentimental.' : 'No se agregó ningún bien con valor sentimental.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </React.Fragment>
  );
};

export default BienesValoresSentimentales;
