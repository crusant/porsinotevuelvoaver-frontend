import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/patrimonio/adeudos';
import * as helpers from '../../../../helpers';

const Adeudos = (props) => {
  const user = useSelector((state) => state.auth.user);
  const adeudo = useSelector((state) => state.adeudos.adeudo);
  const adeudos = useSelector((state) => state.adeudos.adeudos);
  const isGetting = useSelector((state) => state.adeudos.isGetting);
  const isSaving = useSelector((state) => state.adeudos.isSaving);
  const isUpdating = useSelector((state) => state.adeudos.isUpdating);
  const isDeleting = useSelector((state) => state.adeudos.isDeleting);
  const dispatch = useDispatch();

  const hasAdeudos = () =>
    adeudos.length > 0;

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [adeudo] = adeudos.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    return adeudo?.updated_at;
  };

  const getMontoOriginal = (montoOriginal) =>
    !helpers.isEmptyString(montoOriginal)
      ? helpers.money(montoOriginal)
      : '';

  const onChangeInput = (event) => {
    const input = event.target;
    const [entity, attribute] = input.name.split('.');
    const value = input.type === 'checkbox' ? input.checked : input.value;

    dispatch(actions.changeAdeudoInputs(entity, attribute, value));
  };

  const onResetAdeudoInputs = () => {
    dispatch(actions.resetAdeudoInputs());
  };

  const onSaveOrUpdateAdeudo = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!adeudo.id && await dispatch(actions.saveAdeudo(adeudo))) {
      props.setToastMessage('Los datos del adeudo se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (adeudo.id && await dispatch(actions.updateAdeudo(adeudo))) {
      props.setToastMessage('Los datos del adeudo se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const onLoadInputs = (adeudo) => {
    window.scrollTo(0, 0);
    dispatch(actions.loadAdeudoInputs(adeudo));
  };

  const onDeleteAdeudo = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este adeudo?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteAdeudo(id))) {
      props.setToastMessage('Los datos del adeudo se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    props.setUpdatedAt(getUpdatedAt());

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [
    isGetting,
    isSaving,
    isUpdating,
    isDeleting
  ]);

  React.useEffect(() => {
    dispatch(actions.getAdeudos());

    return () => {
      onResetAdeudoInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateAdeudo}>
            <FormRow>
              <FormColumn left strict>
                <FormLabel htmlFor="adeudo.tipo">Tipo de adeudo</FormLabel>
                <FormInput
                  as="switch"
                  name="adeudo.tipo"
                  id="adeudo.tipo"
                  checked={adeudo.tipo}
                  falseText="Préstamo personal"
                  trueText="Tarjeta de crédito"
                  onChange={onChangeInput}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="adeudo.cuenta_contrato">Cuenta o contrato (últimos 4 dígitos)*</FormLabel>
                <FormInput
                  type="number"
                  name="adeudo.cuenta_contrato"
                  id="adeudo.cuenta_contrato"
                  required
                  digits={4}
                  min={0}
                  max={9999}
                  step={1}
                  onChange={onChangeInput}
                  value={adeudo.cuenta_contrato}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="adeudo.institucion_persona">{adeudo.tipo ? 'Institución' : 'Institución o persona'}*</FormLabel>
                <FormInput
                  type="text"
                  name="adeudo.institucion_persona"
                  id="adeudo.institucion_persona"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={adeudo.institucion_persona}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="adeudo.fecha_otorgamiento">Fecha de otorgamiento*</FormLabel>
                <FormInput
                  type="date"
                  name="adeudo.fecha_otorgamiento"
                  id="adeudo.fecha_otorgamiento"
                  required
                  onChange={onChangeInput}
                  value={adeudo.fecha_otorgamiento}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="adeudo.monto_original">Monto original {adeudo.tipo ? <FormText>(Opcional)</FormText> : '*'}</FormLabel>
                <FormInput
                  type="number"
                  name="adeudo.monto_original"
                  id="adeudo.monto_original"
                  decimal
                  required={!adeudo.tipo}
                  min={0.01}
                  step={0.01}
                  onChange={onChangeInput}
                  value={adeudo.monto_original}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left strict>
                <FormLabel htmlFor="adeudo.plazo">Plazo {adeudo.tipo ? <FormText>(Opcional)</FormText> : '*'}</FormLabel>
                <FormInput
                  type="text"
                  name="adeudo.plazo"
                  id="adeudo.plazo"
                  required={!adeudo.tipo}
                  maxLength={100}
                  onChange={onChangeInput}
                  value={adeudo.plazo}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="adeudo.documento_fisico">Donde encontrar el documento <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="adeudo.documento_fisico"
                  id="adeudo.documento_fisico"
                  maxLength={500}
                  onChange={onChangeInput}
                  value={adeudo.documento_fisico}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="adeudo.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="adeudo.indicaciones"
                  id="adeudo.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeInput}
                  value={adeudo.indicaciones}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetAdeudoInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!adeudo.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Tipo de adeudo</th>
            <th>Cuenta o contrato</th>
            <th>Institución o persona</th>
            <th>Fecha de otorgamiento</th>
            <th>Monto original</th>
            <th>Plazo</th>
            <th>Donde encontrar el documento</th>
            <th>Instrucciones</th>
            {canWrite() && (<th></th>)}
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan={9}>Cargando...</td>
            </tr>
          ) : hasAdeudos() ? adeudos.map((adeudo) => (
            <tr key={adeudo.id}>
              <td>{adeudo.tipo ? 'Tarjeta de crédito' : 'Préstamo personal'}</td>
              <td>{adeudo.cuenta_contrato}</td>
              <td>{adeudo.institucion_persona}</td>
              <td>{moment(adeudo.fecha_otorgamiento).format('D MMM YYYY')}</td>
              <td>{getMontoOriginal(adeudo.monto_original)}</td>
              <td>{adeudo.plazo}</td>
              <td>{adeudo.documento_fisico}</td>
              <td>{adeudo.indicaciones}</td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="secondary"
                      small
                      onClick={() => onLoadInputs(adeudo)}
                    >
                      Editar
                    </Button>
                    <Button
                      variant="danger"
                      small
                      onClick={() => onDeleteAdeudo(adeudo.id)}
                    >
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={9}>
                {canWrite() ? 'No has agregado ningún adeudo.' : 'No se agregó ningún adeudo.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </React.Fragment>
  );
};

export default Adeudos;
