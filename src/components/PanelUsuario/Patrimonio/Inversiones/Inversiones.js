import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import Paragraph from '../../../UI/Paragraph/Paragraph';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormControl from '../../../UI/Form/FormControl/FormControl';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../../UI/Form/FormTitle/FormTitle';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/patrimonio/inversiones';
import * as helpers from '../../../../helpers';

const Inversiones = (props) => {
  const [rowKey, setRowKey] = React.useState(-1);
  const [showBeneficiariosErrors, setShowBeneficiariosErrors] = React.useState(false);
  const user = useSelector((state) => state.auth.user);
  const inversion = useSelector((state) => state.inversiones.inversion);
  const beneficiario = useSelector((state) => state.inversiones.beneficiario);
  const beneficiarios = useSelector((state) => state.inversiones.inversion.beneficiarios);
  const inversiones = useSelector((state) => state.inversiones.inversiones);
  const isGetting = useSelector((state) => state.inversiones.isGetting);
  const isSaving = useSelector((state) => state.inversiones.isSaving);
  const isUpdating = useSelector((state) => state.inversiones.isUpdating);
  const isDeleting = useSelector((state) => state.inversiones.isDeleting);
  const dispatch = useDispatch();

  const hasBeneficiarios = (beneficiarios.length > 0);
  const hasInversiones = (inversiones.length > 0);

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [inversion] = inversiones.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    return inversion?.updated_at;
  };

  const onChangeInput = (event) => {
    const input = event.target;
    const [entity, attribute] = input.name.split('.');
    const value = input.value;

    dispatch(actions.changeInversionInputs(entity, attribute, value));
  };

  const sumPorcentajesBeneficiarios = (porcentaje, beneficiario) => {
    return porcentaje + parseInt(beneficiario.porcentaje, 10);
  };

  const getPorcentajeTotal = (initialPorcentaje = 0) => {
    return inversion.beneficiarios.reduce(sumPorcentajesBeneficiarios, initialPorcentaje);
  };

  const isProcentajeEqualTo = (porcentaje) => {
    return getPorcentajeTotal() === porcentaje;
  };

  const onAddBeneficiario = () => {
    const nombreCompleto = beneficiario.nombre_completo;
    const porcentaje = parseInt(beneficiario.porcentaje, 10);
    const porcentajes = getPorcentajeTotal(porcentaje);
    const isGreaterThan100 = (porcentajes > 100);

    setShowBeneficiariosErrors(false);

    if (isGreaterThan100) {
      setShowBeneficiariosErrors(true);
    }

    if (helpers.isEmptyString(nombreCompleto)|| !helpers.isInteger(porcentaje) || porcentaje <= 0 || isGreaterThan100) {
      return;
    }

    dispatch(actions.addBeneficiario(beneficiario));
    dispatch(actions.resetBeneficiario());
  };

  const onHideBeneficiariosErrors = () => {
    setShowBeneficiariosErrors(false);
  };

  const onRemoveBeneficiario = (index) => {
    dispatch(actions.removeBeneficiario(index));
  };

  const onResetInversionInputs = () => {
    dispatch(actions.resetInversionInputs());
  };

  const onSaveOrUpdateInversion = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!inversion.id && await dispatch(actions.saveInversion(inversion))) {
      props.setToastMessage('Los datos de la inversión se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (inversion.id && await dispatch(actions.updateInversion(inversion))) {
      props.setToastMessage('Los datos de la inversión se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const onToggleRow = (key) => {
    if (rowKey !== key) {
      setRowKey(key);
    } else {
      setRowKey(-1);
    }
  };

  const onLoadInputs = (inversion) => {
    window.scrollTo(0, 0);
    dispatch(actions.loadInversionInputs(inversion));
  };

  const onDeleteInversion = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar esta inversión?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteInversion(id))) {
      props.setToastMessage('Los datos de la inversión se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    props.setUpdatedAt(getUpdatedAt());

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [
    isGetting,
    isSaving,
    isUpdating,
    isDeleting
  ]);

  React.useEffect(() => {
    dispatch(actions.getInversiones());

    return () => {
      onResetInversionInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateInversion}>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="inversion.tipo">Tipo de inversión o cuenta bancaria*</FormLabel>
                <FormInput
                  type="text"
                  name="inversion.tipo"
                  id="inversion.tipo"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={inversion.tipo}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="inversion.cuenta_contrato">Cuenta o contrato (últimos 4 dígitos)*</FormLabel>
                <FormInput
                  type="number"
                  name="inversion.cuenta_contrato"
                  id="inversion.cuenta_contrato"
                  required
                  digits={4}
                  min={0}
                  max={9999}
                  step={1}
                  onChange={onChangeInput}
                  value={inversion.cuenta_contrato}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="inversion.institucion">Institución*</FormLabel>
                <FormInput
                  type="text"
                  name="inversion.institucion"
                  id="inversion.institucion"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={inversion.institucion}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="inversion.titular">Titular*</FormLabel>
                <FormInput
                  type="text"
                  name="inversion.titular"
                  id="inversion.titular"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={inversion.titular}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormTitle>Beneficiarios*</FormTitle>
            <FormHelpText underTitle>El porcentaje total debe ser del 100%.</FormHelpText>
            {showBeneficiariosErrors && (
              <Alert variant="danger" onClose={onHideBeneficiariosErrors} dismissible>
                <Paragraph>No se puede agregar al beneficiario, porque el porcentaje total no puede ser mayor al 100%.</Paragraph>
              </Alert>
            )}
            {isProcentajeEqualTo(100) && (
              <Alert variant="success">
                <Paragraph>Haz alcanzado el porcentaje total del 100%, por lo que ya no puedes agregar mas beneficiarios.</Paragraph>
              </Alert>
            )}
            <FormTable>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre completo</th>
                  <th>Porcentaje</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td>
                    <FormControl
                      type="text"
                      name="beneficiario.nombre_completo"
                      id="beneficiario.nombre_completo"
                      disabled={isProcentajeEqualTo(100)}
                      maxLength={100}
                      onChange={onChangeInput}
                      value={beneficiario.nombre_completo}
                    />
                  </td>
                  <td>
                    <FormControl
                      type="number"
                      name="beneficiario.porcentaje"
                      id="beneficiario.porcentaje"
                      disabled={isProcentajeEqualTo(100)}
                      min={1}
                      max={100}
                      step={1}
                      onChange={onChangeInput}
                      value={beneficiario.porcentaje}
                    />
                  </td>
                  <td>
                    <Button
                      variant="primary"
                      small
                      disabled={isProcentajeEqualTo(100)}
                      onClick={onAddBeneficiario}
                    >
                      Agregar
                    </Button>
                  </td>
                </tr>
                {hasBeneficiarios ? beneficiarios.map((beneficiario, index) => (
                  <tr key={index}>
                    <td>{(index + 1)}</td>
                    <td>{beneficiario.nombre_completo}</td>
                    <td>{beneficiario.porcentaje} %</td>
                    <td>
                      <Button
                        variant="danger"
                        small
                        onClick={() => onRemoveBeneficiario(index)}
                      >
                        Quitar
                      </Button>
                    </td>
                  </tr>
                )) : (
                  <tr>
                    <td colSpan={4}>No haz agregado ningún beneficiario.</td>
                  </tr>
                )}
                <tr>
                  <td colSpan={2}><strong>Porcentaje total</strong></td>
                  <td><strong>{getPorcentajeTotal()} %</strong></td>
                  <td></td>
                </tr>
              </tbody>
            </FormTable>
            <FormSeparator />
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="inversion.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="inversion.indicaciones"
                  id="inversion.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeInput}
                  value={inversion.indicaciones}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetInversionInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!inversion.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Tipo de inversión</th>
            <th>Cuenta o contrato</th>
            <th>Institución</th>
            <th>Titular</th>
            <th>Instrucciones</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan={6}>Cargando...</td>
            </tr>
          ) : hasInversiones ? inversiones.map((inversion, index) => (
            <React.Fragment key={inversion.id}>
              <tr>
                <td>{inversion.tipo}</td>
                <td>{inversion.cuenta_contrato}</td>
                <td>{inversion.institucion}</td>
                <td>{inversion.titular}</td>
                <td>{inversion.indicaciones}</td>
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="light"
                      small
                      onClick={() => onToggleRow(index)}
                    >
                      Ver más
                    </Button>
                    {canWrite() && (
                      <React.Fragment>
                        <Button
                          variant="secondary"
                          small
                          onClick={() => onLoadInputs(inversion)}
                        >
                          Editar
                        </Button>
                        <Button
                          variant="danger"
                          small
                          onClick={() => onDeleteInversion(inversion.id)}
                        >
                          Eliminar
                        </Button>
                      </React.Fragment>
                    )}
                  </FormButtonGroup>
                </td>
              </tr>
              {(rowKey === index) && (
                <tr>
                  <td colSpan={6}>
                    <FormTable sub>
                      <thead>
                        <tr>
                          <th colSpan={2}>Beneficiarios</th>
                        </tr>
                        <tr>
                          <th>Nombre completo</th>
                          <th>Porcentaje</th>
                        </tr>
                      </thead>
                      <tbody>
                        {inversion.beneficiarios.map((beneficiario, index) => (
                          <tr key={index}>
                            <td>{beneficiario.nombre_completo}</td>
                            <td>{beneficiario.porcentaje}</td>
                          </tr>
                        ))}
                      </tbody>
                    </FormTable>
                  </td>
                </tr>
              )}
            </React.Fragment>
          )) : (
            <tr>
              <td colSpan={6}>
                {canWrite() ? 'No has agregado ninguna inversión.' : 'No se agregó ninguna inversión.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </React.Fragment>
  );
};

export default Inversiones;
