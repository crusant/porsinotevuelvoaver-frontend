import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as bienesInmueblesActions from '../../../../actions/patrimonio/bienes-inmuebles';
import * as monedasActions from '../../../../actions/monedas';
import * as helpers from '../../../../helpers';

const BienesInmuebles = (props) => {
  const user = useSelector((state) => state.auth.user);
  const bienInmueble = useSelector((state) => state.bienesInmuebles.bienInmueble);
  const bienesInmuebles = useSelector((state) => state.bienesInmuebles.bienesInmuebles);
  const isGetting = useSelector((state) => state.bienesInmuebles.isGetting);
  const isSaving = useSelector((state) => state.bienesInmuebles.isSaving);
  const isUpdating = useSelector((state) => state.bienesInmuebles.isUpdating);
  const isDeleting = useSelector((state) => state.bienesInmuebles.isDeleting);
  const monedas = useSelector((state) => state.monedas.monedas);
  const dispatch = useDispatch();

  const hasMonedas = (monedas.length > 0);
  const hasBienesInmuebles = (bienesInmuebles.length > 0);

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [bienInmueble] = bienesInmuebles.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    return bienInmueble?.updated_at;
  };

  const onChangeInput = (event) => {
    const input = event.target;
    const [entity, attribute] = input.name.split('.');
    let value = input.type === 'checkbox' ? input.checked : input.value;

    if (attribute === 'moneda_id') {
      value = parseInt(value, 10);
    }

    dispatch(bienesInmueblesActions.changeBienInmuebleInputs(entity, attribute, value));
  };

  const onResetBienInmuebleInputs = () => {
    dispatch(bienesInmueblesActions.resetBienInmuebleInputs());
  };

  const onSaveOrUpdateBienInmueble = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!bienInmueble.id && await dispatch(bienesInmueblesActions.saveBienInmueble(bienInmueble))) {
      props.setToastMessage('Los datos del bien inmueble se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (bienInmueble.id && await dispatch(bienesInmueblesActions.updateBienInmueble(bienInmueble))) {
      props.setToastMessage('Los datos del bien inmueble se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const getMonedaNombre = (id, especificada) => {
    const moneda = monedas.find((moneda) => moneda.id === id);

    if (!moneda) {
      return '';
    }

    return (moneda.id !== 5) ? moneda.nombre : especificada;
  };

  const getMonto = (bienInmueble) => {
    const { monto, moneda, moneda_id } = bienInmueble;

    if (helpers.isEmptyString(monto)) {
      return '';
    }

    return `${helpers.money(monto)} ${getMonedaNombre(moneda_id, moneda)}`;
  };

  const onLoadInputs = (bienInmueble) => {
    window.scrollTo(0, 0);
    dispatch(bienesInmueblesActions.loadBienInmuebleInputs(bienInmueble));
  };

  const onDeleteBienInmueble = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este bien inmueble?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(bienesInmueblesActions.deleteBienInmueble(id))) {
      props.setToastMessage('Los datos del bien inmueble se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    props.setUpdatedAt(getUpdatedAt());

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [
    isGetting,
    isSaving,
    isUpdating,
    isDeleting
  ]);

  React.useEffect(() => {
    (async () => {
      await dispatch(bienesInmueblesActions.getBienesInmuebles());
      await dispatch(monedasActions.getMonedas());
    })();

    return () => {
      onResetBienInmuebleInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
     <React.Fragment>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateBienInmueble}>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="bienInmueble.localizacion">Localización*</FormLabel>
                <FormInput
                  type="text"
                  name="bienInmueble.localizacion"
                  id="bienInmueble.localizacion"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={bienInmueble.localizacion}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="bienInmueble.superficie_terreno">Superficie del terreno en &#13217; <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="number"
                  name="bienInmueble.superficie_terreno"
                  id="bienInmueble.superficie_terreno"
                  decimal
                  min={0.01}
                  step={0.01}
                  onChange={onChangeInput}
                  value={bienInmueble.superficie_terreno}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="bienInmueble.superficie_construccion">Superificie de la construcción en &#13217; <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="number"
                  name="bienInmueble.superficie_construccion"
                  id="bienInmueble.superficie_construccion"
                  decimal
                  min={0.01}
                  step={0.01}
                  onChange={onChangeInput}
                  value={bienInmueble.superficie_construccion}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="bienInmueble.registro_publico_propiedad">Datos del registro público de la propiedad <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="bienInmueble.registro_publico_propiedad"
                  id="bienInmueble.registro_publico_propiedad"
                  maxLength={500}
                  onChange={onChangeInput}
                  value={bienInmueble.registro_publico_propiedad}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormRow>
              <FormColumn left strict>
                <FormLabel htmlFor="bienInmueble.has_credito_hipotecario">Crédito hipotecario</FormLabel>
                <FormInput
                  as="switch"
                  name="bienInmueble.has_credito_hipotecario"
                  id="bienInmueble.has_credito_hipotecario"
                  checked={bienInmueble.has_credito_hipotecario}
                  falseText="No"
                  trueText="Sí"
                  onChange={onChangeInput}
                />
              </FormColumn>
            </FormRow>
            {bienInmueble.has_credito_hipotecario && (
              <React.Fragment>
                <FormRow>
                  <FormColumn left>
                    <FormLabel htmlFor="bienInmueble.contrato">Contrato (últimos 4 dígitos)*</FormLabel>
                    <FormInput
                      type="number"
                      name="bienInmueble.contrato"
                      id="bienInmueble.contrato"
                      required
                      digits={4}
                      min={0}
                      max={9999}
                      step={1}
                      onChange={onChangeInput}
                      value={bienInmueble.contrato}
                    />
                  </FormColumn>
                  <FormColumn right>
                    <FormLabel htmlFor="bienInmueble.institucion">Institución*</FormLabel>
                    <FormInput
                      type="text"
                      name="bienInmueble.institucion"
                      id="bienInmueble.institucion"
                      required
                      maxLength={100}
                      onChange={onChangeInput}
                      value={bienInmueble.institucion}
                    />
                  </FormColumn>
                </FormRow>
                <FormRow>
                  <FormColumn left>
                    <FormLabel htmlFor="bienInmueble.monto">Monto total del crédito*</FormLabel>
                    <FormInput
                      type="number"
                      name="bienInmueble.monto"
                      id="bienInmueble.monto"
                      required
                      decimal
                      min={0.01}
                      step={0.01}
                      onChange={onChangeInput}
                      value={bienInmueble.monto}
                    />
                  </FormColumn>
                  <FormColumn right>
                    <FormLabel htmlFor="bienInmueble.moneda_id">Moneda*</FormLabel>
                    <FormInput
                      as="select"
                      name="bienInmueble.moneda_id"
                      id="bienInmueble.moneda_id"
                      disabled={!hasMonedas}
                      required
                      onChange={onChangeInput}
                      value={bienInmueble.moneda_id}
                    >
                      <option value="">Seleccione...</option>
                      {monedas.map(({ id, nombre }) => (
                        <option key={id} value={id}>
                          {nombre}
                        </option>
                      ))}
                    </FormInput>
                  </FormColumn>
                </FormRow>
                {(bienInmueble.moneda_id === 5) && (
                  <FormRow>
                    <FormColumn left strict>
                      <FormLabel htmlFor="bienInmueble.moneda">Especifique*</FormLabel>
                      <FormInput
                        type="text"
                        name="bienInmueble.moneda"
                        id="bienInmueble.moneda"
                        required
                        maxLength={42}
                        onChange={onChangeInput}
                        value={bienInmueble.moneda}
                      />
                    </FormColumn>
                  </FormRow>
                )}
              </React.Fragment>
            )}
            <FormSeparator />
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="bienInmueble.escrituras_fisica">Donde encontrar la escritura o el título de la propiedad <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="bienInmueble.escrituras_fisica"
                  id="bienInmueble.escrituras_fisica"
                  maxLength={500}
                  onChange={onChangeInput}
                  value={bienInmueble.escrituras_fisica}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="bienInmueble.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="bienInmueble.indicaciones"
                  id="bienInmueble.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeInput}
                  value={bienInmueble.indicaciones}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetBienInmuebleInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!bienInmueble.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th rowSpan={2}>Localización</th>
            <th rowSpan={2}>Superficie del terreno en &#13217;</th>
            <th rowSpan={2}>Superificie de la construcción en &#13217;</th>
            <th rowSpan={2}>Datos del registro público de la propiedad</th>
            <th colSpan={3}>Crédito hipotecario</th>
            <th rowSpan={2}>Donde encontrar la escritura o el título de la propiedad</th>
            <th rowSpan={2}>Instrucciones</th>
            {canWrite() && (<th rowSpan={2}></th>)}
          </tr>
          <tr>
            <th>Contrato</th>
            <th>Institución</th>
            <th>Monto</th>
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan={10}>Cargando...</td>
            </tr>
          ) : hasBienesInmuebles ? bienesInmuebles.map((bienInmueble) => (
            <tr key={bienInmueble.id}>
              <td>{bienInmueble.localizacion}</td>
              <td>
                {!helpers.isEmptyString(bienInmueble.superficie_terreno) && (
                  `${bienInmueble.superficie_terreno} \u33A1`
                )}
              </td>
              <td>
                {!helpers.isEmptyString(bienInmueble.superficie_construccion) && (
                  `${bienInmueble.superficie_construccion} \u33A1`
                )}
              </td>
              <td>{bienInmueble.registro_publico_propiedad}</td>
              <td>{bienInmueble.contrato}</td>
              <td>{bienInmueble.institucion}</td>
              <td>{getMonto(bienInmueble)}</td>
              <td>{bienInmueble.escrituras_fisica}</td>
              <td>{bienInmueble.indicaciones}</td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="secondary"
                      small
                      onClick={() => onLoadInputs(bienInmueble)}
                    >
                      Editar
                    </Button>
                    <Button
                      variant="danger"
                      small
                      onClick={() => onDeleteBienInmueble(bienInmueble.id)}
                    >
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={10}>
                {canWrite() ? 'No has agregado ningún bien inmueble.' : 'No se agregó ningún bien inmueble.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
     </React.Fragment>
  );
};

export default BienesInmuebles;
