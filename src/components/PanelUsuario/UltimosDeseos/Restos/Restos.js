import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormFile from '../../../UI/Form/FormFile/FormFile';
import File from '../../../UI/File/File';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../../UI/Form/FormTitle/FormTitle';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/ultimos-deseos/restos';
import * as helpers from '../../../../helpers';
import Paragraph from '../../../UI/Paragraph/Paragraph';

const Restos = (props) => {
  const user = useSelector((state) => state.auth.user);
  const resto = useSelector((state) => state.restos.resto);
  const isLoading = useSelector((state) => state.restos.isLoading);
  const isSaving = useSelector((state) => state.restos.isSaving);
  const isDownloading = useSelector((state) => state.restos.isDownloading);
  const dispatch = useDispatch();

  const canWrite = () =>
    user?.can_write;

  const onChangeRestoInput = (event) => {
    const input = event.target;
    const [, attribute] = input.name.split('.');
    const value = input.type === 'checkbox' ? input.checked : input.value;

    dispatch(actions.changeRestoInput(attribute, value));
  };

  const onChangeSepultadoInput = (event) => {
    const input = event.target;
    const [, attribute] = input.name.split('.');
    const value = input.type === 'checkbox' ? input.checked : input.value;

    dispatch(actions.changeSepultadoInput(attribute, value));
  };

  const onChangeContratoDigitalInput = (value) => {
    dispatch(actions.changeSepultadoInput('contrato_digital', value));
  };

  const onRemoveContratoDigitalInput = () => {
    dispatch(actions.changeSepultadoInput('contrato_digital', ''));
  };

  const onChangeCremadoInput = (event) => {
    const input = event.target;
    const [, attribute] = input.name.split('.');
    const value = input.type === 'checkbox' ? input.checked : input.value;

    dispatch(actions.changeCremadoInput(attribute, value));
  };

  const onChangeDocumentoDigitalInput = (value) => {
    dispatch(actions.changeCremadoInput('documento_digital', value));
  };

  const onRemoveDocumentoDigitalInput = () => {
    dispatch(actions.changeCremadoInput('documento_digital', ''));
  };

  const onSaveRestos = async (event) => {
    event.preventDefault();
    props.setShowToast(false);
    props.setToastMessage('');

    const formData = new FormData();
    helpers.buildFormData(formData, resto);

    if (await dispatch(actions.saveRestos(formData))) {
      props.setToastMessage('Los datos de restos se guardaron con éxito.');
      props.setShowToast(true);
    }

    window.scrollTo(0, 0);
  };

  const onDownloadDocumentoDigital = (documento) => {
    const extension = String(documento).split('.').pop();

    dispatch(actions.downloadRestosDocumento(1, extension));
  };

  const onDownloadContratoDigital = (contrato) => {
    const extension = String(contrato).split('.').pop();

    dispatch(actions.downloadRestosDocumento(2, extension));
  };

  React.useEffect(() => {
    if (!isLoading || !isSaving) {
      props.setUpdatedAt(resto?.updated_at);
    }

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isLoading, isSaving]);

  React.useEffect(() => {
    dispatch(actions.getRestos());

    return () => {
      dispatch(actions.resetRestosErrors());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <form onSubmit={onSaveRestos}>
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('3')}
          >
            Regresar
          </Button>
        </FormColumn>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar'}
            </Button>
          </FormColumn>
        )}
      </FormRow>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="resto.is_cremado">Los restos serán</FormLabel>
              <FormInput
                as="switch"
                name="resto.is_cremado"
                id="resto.is_cremado"
                checked={resto.is_cremado}
                falseText="Sepultados"
                trueText="Cremados"
                onChange={onChangeRestoInput}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Los restos serán</FormLabel>
              <Paragraph>{resto.is_cremado ? 'Cremados' : 'Sepultados'}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      {resto.is_cremado ? (
        <React.Fragment>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="cremado.responsable">Responsable de la cremación <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="cremado.responsable"
                    id="cremado.responsable"
                    maxLength={100}
                    onChange={onChangeCremadoInput}
                    value={resto.cremado.responsable}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Responsable de la cremación</FormLabel>
                  <Paragraph>{resto.cremado.responsable}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="cremado.telefono">Teléfono <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="tel"
                    name="cremado.telefono"
                    id="cremado.telefono"
                    minLength={10}
                    maxLength={10}
                    onChange={onChangeCremadoInput}
                    value={resto.cremado.telefono}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Teléfono</FormLabel>
                  <Paragraph>{resto.cremado.telefono}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormSeparator />
          <FormTitle>Domicilio</FormTitle>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="cremado.colonia">Colonia <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="cremado.colonia"
                    id="cremado.colonia"
                    maxLength={100}
                    onChange={onChangeCremadoInput}
                    value={resto.cremado.colonia}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Colonia</FormLabel>
                  <Paragraph>{resto.cremado.colonia}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="cremado.ciudad">Ciudad <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="cremado.ciudad"
                    id="cremado.ciudad"
                    maxLength={100}
                    onChange={onChangeCremadoInput}
                    value={resto.cremado.ciudad}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Ciudad</FormLabel>
                  <Paragraph>{resto.cremado.ciudad}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="cremado.codigo_postal">C.P. <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="number"
                    name="cremado.codigo_postal"
                    id="cremado.codigo_postal"
                    digits={5}
                    min={0}
                    max={99999}
                    step={1}
                    onChange={onChangeCremadoInput}
                    value={resto.cremado.codigo_postal}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>C.P.</FormLabel>
                  <Paragraph>{resto.cremado.codigo_postal}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn center>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="cremado.municipio">Municipio <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="cremado.municipio"
                    id="cremado.municipio"
                    maxLength={100}
                    onChange={onChangeCremadoInput}
                    value={resto.cremado.municipio}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Municipio</FormLabel>
                  <Paragraph>{resto.cremado.municipio}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="cremado.estado">Estado <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="cremado.estado"
                    id="cremado.estado"
                    maxLength={40}
                    onChange={onChangeCremadoInput}
                    value={resto.cremado.estado}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Estado</FormLabel>
                  <Paragraph>{resto.cremado.estado}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormSeparator />
          <FormRow>
            <FormColumn>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="cremado.has_nicho">Se cuenta con un nicho para resguardar las cenizas</FormLabel>
                  <FormInput
                    as="switch"
                    name="cremado.has_nicho"
                    id="cremado.has_nicho"
                    checked={resto.cremado.has_nicho}
                    falseText="No"
                    trueText="Sí"
                    onChange={onChangeCremadoInput}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Se cuenta con un nicho para resguardar las cenizas</FormLabel>
                  <Paragraph>{resto.cremado.has_nicho ? 'Sí' : 'No'}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          {resto.cremado.has_nicho && (
            <React.Fragment>
              <FormRow>
                <FormColumn>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="cremado.documento_digital">Documento digital <FormText>(Opcional)</FormText></FormLabel>
                      <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF, Word, PowerPoint, Excel, .jpg o .png.</FormHelpText>
                      {helpers.isEmptyString(resto.cremado.documento_digital) ? (
                        <FormFile accept=".pdf,.doc,.docx,.ppt,.pptx,.xsl,.xlsx,.jpg,.jpeg,.png" onChange={onChangeDocumentoDigitalInput} />
                      ) : (
                        <File
                          file={resto.cremado.documento_digital}
                          canDownload
                          isDownloading={isDownloading}
                          onDownload={onDownloadDocumentoDigital}
                          onRemove={onRemoveDocumentoDigitalInput}
                        />
                      )}
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>Documento digital</FormLabel>
                      <File
                        file={resto.cremado.documento_digital}
                        canDownload
                        isDownloading={isDownloading}
                        onDownload={onDownloadDocumentoDigital}
                        onRemove={onRemoveDocumentoDigitalInput}
                      />
                    </React.Fragment>
                  )}
                </FormColumn>
              </FormRow>
              <FormRow>
                <FormColumn>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="cremado.documento_fisico">Donde encontrar el documento <FormText>(Opcional)</FormText></FormLabel>
                      <FormInput
                        type="text"
                        name="cremado.documento_fisico"
                        id="cremado.documento_fisico"
                        maxLength={500}
                        onChange={onChangeCremadoInput}
                        value={resto.cremado.documento_fisico}
                      />
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>Donde encontrar el documento</FormLabel>
                      <Paragraph>{resto.cremado.documento_fisico}</Paragraph>
                    </React.Fragment>
                  )}
                </FormColumn>
              </FormRow>
              <FormRow>
                <FormColumn>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="cremado.lugar_numero">Lugar y número de nicho <FormText>(Opcional)</FormText></FormLabel>
                      <FormInput
                        type="text"
                        name="cremado.lugar_numero"
                        id="cremado.lugar_numero"
                        maxLength={100}
                        onChange={onChangeCremadoInput}
                        value={resto.cremado.lugar_numero}
                      />
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>Lugar y número de nicho</FormLabel>
                      <Paragraph>{resto.cremado.lugar_numero}</Paragraph>
                    </React.Fragment>
                  )}
                </FormColumn>
              </FormRow>
            </React.Fragment>
          )}
          <FormSeparator />
          <FormRow>
            <FormColumn>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="cremado.indicaciones">Instrucciones con respecto a las cenizas <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    as="textarea"
                    name="cremado.indicaciones"
                    id="cremado.indicaciones"
                    rows={3}
                    maxLength={500}
                    onChange={onChangeCremadoInput}
                    value={resto.cremado.indicaciones}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Instrucciones con respecto a las cenizas</FormLabel>
                  <Paragraph>{resto.cremado.indicaciones}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="sepultado.nombre_nicho">Nombre del cementerio o Iglesia (nicho) <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="sepultado.nombre_nicho"
                    id="sepultado.nombre_nicho"
                    maxLength={100}
                    onChange={onChangeSepultadoInput}
                    value={resto.sepultado.nombre_nicho}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Nombre del cementerio o Iglesia (nicho)</FormLabel>
                  <Paragraph>{resto.sepultado.nombre_nicho}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="sepultado.telefono">Teléfono <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="tel"
                    name="sepultado.telefono"
                    id="sepultado.telefono"
                    minLength={10}
                    maxLength={10}
                    onChange={onChangeSepultadoInput}
                    value={resto.sepultado.telefono}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Teléfono</FormLabel>
                  <Paragraph>{resto.sepultado.telefono}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormSeparator />
          <FormTitle>Domicilio</FormTitle>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="sepultado.colonia">Colonia <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="sepultado.colonia"
                    id="sepultado.colonia"
                    maxLength={100}
                    onChange={onChangeSepultadoInput}
                    value={resto.sepultado.colonia}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Colonia</FormLabel>
                  <Paragraph>{resto.sepultado.colonia}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="sepultado.ciudad">Ciudad <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="sepultado.ciudad"
                    id="sepultado.ciudad"
                    maxLength={100}
                    onChange={onChangeSepultadoInput}
                    value={resto.sepultado.ciudad}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Ciudad</FormLabel>
                  <Paragraph>{resto.sepultado.ciudad}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn left>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="sepultado.codigo_postal">C.P. <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="number"
                    name="sepultado.codigo_postal"
                    id="sepultado.codigo_postal"
                    digits={5}
                    min={0}
                    max={99999}
                    step={1}
                    onChange={onChangeSepultadoInput}
                    value={resto.sepultado.codigo_postal}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>C.P.</FormLabel>
                  <Paragraph>{resto.sepultado.codigo_postal}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn center>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="sepultado.municipio">Municipio <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="sepultado.municipio"
                    id="sepultado.municipio"
                    maxLength={100}
                    onChange={onChangeSepultadoInput}
                    value={resto.sepultado.municipio}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Municipio</FormLabel>
                  <Paragraph>{resto.sepultado.municipio}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
            <FormColumn right>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="sepultado.estado">Estado <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    type="text"
                    name="sepultado.estado"
                    id="sepultado.estado"
                    maxLength={40}
                    onChange={onChangeSepultadoInput}
                    value={resto.sepultado.estado}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Estado</FormLabel>
                  <Paragraph>{resto.sepultado.estado}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormSeparator />
          <FormRow>
            <FormColumn>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="sepultado.has_contrato">Existe contrato con el cementerio</FormLabel>
                  <FormInput
                    as="switch"
                    name="sepultado.has_contrato"
                    id="sepultado.has_contrato"
                    checked={resto.sepultado.has_contrato}
                    falseText="No"
                    trueText="Sí"
                    onChange={onChangeSepultadoInput}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Existe contrato con el cementerio</FormLabel>
                  <Paragraph>{resto.sepultado.has_contrato ? 'Sí' : 'No'}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          {resto.sepultado.has_contrato && (
            <React.Fragment>
              <FormRow>
                <FormColumn>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="sepultado.contrato_digital">Contrato digital <FormText>(Opcional)</FormText></FormLabel>
                      <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF, Word, PowerPoint, Excel, .jpg o .png.</FormHelpText>
                      {helpers.isEmptyString(resto.sepultado.contrato_digital) ? (
                        <FormFile accept=".pdf,.doc,.docx,.ppt,.pptx,.xsl,.xlsx,.jpg,.jpeg,.png" onChange={onChangeContratoDigitalInput} />
                      ) : (
                        <File
                          file={resto.sepultado.contrato_digital}
                          canDownload
                          isDownloading={isDownloading}
                          onDownload={onDownloadContratoDigital}
                          onRemove={onRemoveContratoDigitalInput}
                        />
                      )}
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>Contrato digital</FormLabel>
                      <File
                        file={resto.sepultado.contrato_digital}
                        canDownload
                        isDownloading={isDownloading}
                        onDownload={onDownloadContratoDigital}
                        onRemove={onRemoveContratoDigitalInput}
                      />
                    </React.Fragment>
                  )}
                </FormColumn>
              </FormRow>
              <FormRow>
                <FormColumn>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="sepultado.contrato_fisico">Donde encontrar el contrato <FormText>(Opcional)</FormText></FormLabel>
                      <FormInput
                        type="text"
                        name="sepultado.contrato_fisico"
                        id="sepultado.contrato_fisico"
                        maxLength={500}
                        onChange={onChangeSepultadoInput}
                        value={resto.sepultado.contrato_fisico}
                      />
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>Donde encontrar el contrato</FormLabel>
                      <Paragraph>{resto.sepultado.contrato_fisico}</Paragraph>
                    </React.Fragment>
                  )}
                </FormColumn>
              </FormRow>
              <FormRow>
                <FormColumn>
                  {canWrite() ? (
                    <React.Fragment>
                      <FormLabel htmlFor="sepultado.seccion_lote_bloque">Sección, lote y bloque <FormText>(Opcional)</FormText></FormLabel>
                      <FormInput
                        type="text"
                        name="sepultado.seccion_lote_bloque"
                        id="sepultado.seccion_lote_bloque"
                        maxLength={100}
                        onChange={onChangeSepultadoInput}
                        value={resto.sepultado.seccion_lote_bloque}
                      />
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <FormLabel>Sección, lote y bloque</FormLabel>
                      <Paragraph>{resto.sepultado.seccion_lote_bloque}</Paragraph>
                    </React.Fragment>
                  )}
                </FormColumn>
              </FormRow>
            </React.Fragment>
          )}
          <FormRow>
            <FormColumn>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="sepultado.apitafio">Epitafio <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    as="textarea"
                    name="sepultado.apitafio"
                    id="sepultado.apitafio"
                    rows={3}
                    maxLength={500}
                    onChange={onChangeSepultadoInput}
                    value={resto.sepultado.apitafio}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Epitafio</FormLabel>
                  <Paragraph>{resto.sepultado.apitafio}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn>
              {canWrite() ? (
                <React.Fragment>
                  <FormLabel htmlFor="sepultado.observaciones">Otras observaciones <FormText>(Opcional)</FormText></FormLabel>
                  <FormInput
                    as="textarea"
                    name="sepultado.observaciones"
                    id="sepultado.observaciones"
                    rows={3}
                    maxLength={500}
                    onChange={onChangeSepultadoInput}
                    value={resto.sepultado.observaciones}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <FormLabel>Otras observaciones</FormLabel>
                  <Paragraph>{resto.sepultado.observaciones}</Paragraph>
                </React.Fragment>
              )}
            </FormColumn>
          </FormRow>
        </React.Fragment>
      )}
      <FormSeparator />
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('3')}
          >
            Regresar
          </Button>
        </FormColumn>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar'}
            </Button>
          </FormColumn>
        )}
      </FormRow>
    </form>
  );
};

Restos.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default Restos;
