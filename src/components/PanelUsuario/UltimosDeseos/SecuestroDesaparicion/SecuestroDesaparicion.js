import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/ultimos-deseos/secuestro-desaparicion';
import Paragraph from '../../../UI/Paragraph/Paragraph';

const SecuestroDesaparicion = (props) => {
  const user = useSelector((state) => state.auth.user);
  const secuestroDesaparicion = useSelector((state) => state.secuestroDesaparicion.secuestroDesaparicion);
  const isLoading = useSelector((state) => state.secuestroDesaparicion.isLoading);
  const isSaving = useSelector((state) => state.secuestroDesaparicion.isSaving);
  const dispatch = useDispatch();

  const canWrite = () =>
    user?.can_write;

  const onChangeSecuestroDesaparicionInput = (event) => {
    const [, attribute] = event.target.name.split('.');

    dispatch(actions.changeSecuestroDesaparicionInput(attribute, event.target.value));
  };

  const onSaveSecuestroDesaparicion = async (event) => {
    event.preventDefault();
    props.setShowToast(false);
    props.setToastMessage('');

    if (await dispatch(actions.saveSecuestroDesaparicion(secuestroDesaparicion))) {
      props.onSelectKey('4');
      props.setToastMessage('Los datos de secuestro o desaparición se guardaron con éxito.');
      props.setShowToast(true);
    }

    window.scrollTo(0, 0);
  };

  React.useEffect(() => {
    if (!isLoading) {
      props.setUpdatedAt(secuestroDesaparicion?.updated_at);
    }

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isLoading]);

  React.useEffect(() => {
    dispatch(actions.getSecuestroDesaparicion());

    return () => {
      dispatch(actions.resetErrors());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <form onSubmit={onSaveSecuestroDesaparicion}>
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('2')}
          >
            Regresar
          </Button>
        </FormColumn>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          </FormColumn>
        )}
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('4')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="secuestroDesaparicion.indicaciones">Instrucciones*</FormLabel>
              <FormInput
                as="textarea"
                name="secuestroDesaparicion.indicaciones"
                id="secuestroDesaparicion.indicaciones"
                rows={3}
                required
                maxLength={500}
                onChange={onChangeSecuestroDesaparicionInput}
                value={secuestroDesaparicion.indicaciones}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Instrucciones</FormLabel>
              <Paragraph>
                {secuestroDesaparicion.indicaciones || 'El usuario no respondió esta pregunta.'}
              </Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormSeparator />
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('2')}
          >
            Regresar
          </Button>
        </FormColumn>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          </FormColumn>
        )}
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('4')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
    </form>
  );
};

SecuestroDesaparicion.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default SecuestroDesaparicion;
