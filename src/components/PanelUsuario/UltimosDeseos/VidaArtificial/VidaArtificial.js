import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/ultimos-deseos/vida-artificial';
import Paragraph from '../../../UI/Paragraph/Paragraph';

const VidaArtificial = (props) => {
  const user = useSelector((state) => state.auth.user);
  const vidaArtificial = useSelector((state) => state.vidaArtificial.vidaArtificial);
  const isLoading = useSelector((state) => state.vidaArtificial.isLoading);
  const isSaving = useSelector((state) => state.vidaArtificial.isSaving);
  const dispatch = useDispatch();

  const canWrite = () =>
    user?.can_write;

  const onChangeVidaArtificialInput = (event) => {
    const [, attribute] = event.target.name.split('.');
    const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

    dispatch(actions.changeVidaArtificialInput(attribute, value));
  };

  const onSaveVidaArtificial = async (event) => {
    event.preventDefault();
    props.setShowToast(false);
    props.setToastMessage('');

    if (await dispatch(actions.saveVidaArtificial(vidaArtificial))) {
      props.onSelectKey('3');
      props.setToastMessage('Los datos de vida artificial se guardaron con éxito.');
      props.setShowToast(true);
    }

    window.scrollTo(0, 0);
  };

  React.useEffect(() => {
    if (!isLoading) {
      props.setUpdatedAt(vidaArtificial?.updated_at);
    }

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isLoading]);

  React.useEffect(() => {
    dispatch(actions.getVidaArtificial());

    return () => {
      dispatch(actions.resetErrors());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <form onSubmit={onSaveVidaArtificial}>
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Regresar
          </Button>
        </FormColumn>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          </FormColumn>
        )}
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('3')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn left strict>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="vidaArtificial.is_vivir">Deseo vida artificial en un hospital cuando ya no exista remedio para una enfermedad*</FormLabel>
              <FormInput
                as="switch"
                name="vidaArtificial.is_vivir"
                id="vidaArtificial.is_vivir"
                checked={vidaArtificial.is_vivir}
                falseText="No"
                trueText="Sí"
                onChange={onChangeVidaArtificialInput}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Deseo vida artificial en un hospital cuando ya no exista remedio para una enfermedad</FormLabel>
              <Paragraph>{vidaArtificial.is_vivir ? 'Sí' : 'No'}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      {vidaArtificial.is_vivir && (
        <FormRow>
          <FormColumn>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="vidaArtificial.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="vidaArtificial.indicaciones"
                  id="vidaArtificial.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeVidaArtificialInput}
                  value={vidaArtificial.indicaciones}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Instrucciones</FormLabel>
                <Paragraph>{vidaArtificial.indicaciones}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
      )}
      <FormSeparator />
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Regresar
          </Button>
        </FormColumn>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          </FormColumn>
        )}
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('3')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
    </form>
  );
};

VidaArtificial.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default VidaArtificial;
