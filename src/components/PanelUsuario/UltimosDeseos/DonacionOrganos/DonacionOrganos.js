import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/ultimos-deseos/donacion-organos';
import Paragraph from '../../../UI/Paragraph/Paragraph';

const DonacionOrganos = (props) => {
  const user = useSelector((state) => state.auth.user);
  const donacionOrgano = useSelector((state) => state.donacionOrganos.donacionOrgano);
  const isLoading = useSelector((state) => state.donacionOrganos.isLoading);
  const isSaving = useSelector((state) => state.donacionOrganos.isSaving);
  const dispatch = useDispatch();

  const canWrite = () =>
    user?.can_write;

  const onChangeDonacionOrganosInput = (event) => {
    const [, attribute] = event.target.name.split('.');
    const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

    dispatch(actions.changeDonacionOrganosInput(attribute, value));
  };

  const onSaveDonacionOrganos = async (event) => {
    event.preventDefault();
    props.setShowToast(false);
    props.setToastMessage('');

    if (await dispatch(actions.saveDonacionOrganos(donacionOrgano))) {
      props.onSelectKey('2');
      props.setToastMessage('Los datos de donación de órganos se guardaron con éxito.');
      props.setShowToast(true);
    }

    window.scrollTo(0, 0);
  };

  React.useEffect(() => {
    if (!isLoading) {
      props.setUpdatedAt(donacionOrgano?.updated_at);
    }

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isLoading]);

  React.useEffect(() => {
    dispatch(actions.getDonacionOrganos());

    return () => {
      dispatch(actions.resetErrors());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <form onSubmit={onSaveDonacionOrganos}>
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('0')}
          >
            Regresar
          </Button>
        </FormColumn>
        <FormColumn auto>
          {canWrite() && (
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          )}
        </FormColumn>
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('2')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn left strict>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="donacionOrgano.is_donar">Deseo donar algún órgano cuando sea útil para otra persona*</FormLabel>
              <FormInput
                as="switch"
                name="donacionOrgano.is_donar"
                id="donacionOrgano.is_donar"
                checked={donacionOrgano.is_donar}
                falseText="No"
                trueText="Sí"
                onChange={onChangeDonacionOrganosInput}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Deseo donar algún órgano cuando sea útil para otra persona</FormLabel>
              <Paragraph>{donacionOrgano.is_donar ? 'Sí' : 'No'}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      {donacionOrgano.is_donar && (
        <FormRow>
          <FormColumn>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="donacionOrgano.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="donacionOrgano.indicaciones"
                  id="donacionOrgano.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeDonacionOrganosInput}
                  value={donacionOrgano.indicaciones}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Instrucciones</FormLabel>
                <Paragraph>{donacionOrgano.indicaciones}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
      )}
      <FormSeparator />
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('0')}
          >
            Regresar
          </Button>
        </FormColumn>
        <FormColumn auto>
          {canWrite() && (
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          )}
        </FormColumn>
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('2')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
    </form>
  );
};

DonacionOrganos.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default DonacionOrganos;
