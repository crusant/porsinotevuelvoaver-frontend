import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Paragraph from '../../../UI/Paragraph/Paragraph';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormFile from '../../../UI/Form/FormFile/FormFile';
import File from '../../../UI/File/File';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../../UI/Form/FormTitle/FormTitle';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/ultimos-deseos/datos-testamento';
import * as helpers from '../../../../helpers';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';

const DatosTestamento = (props) => {
  const user = useSelector((state) => state.auth.user);
  const testamento = useSelector((state) => state.datosTestamento.testamento);
  const isLoading = useSelector((state) => state.datosTestamento.isLoading);
  const isSaving = useSelector((state) => state.datosTestamento.isSaving);
  const isDownloading = useSelector((state) => state.datosTestamento.isDownloading);
  const dispatch = useDispatch();

  const canWrite = () =>
    user?.can_write;

  const onChangeDatosTestamentoInput = (event) => {
    const [, attribute] = event.target.name.split('.');

    dispatch(actions.changeDatosTestamentoInput(attribute, event.target.value));
  };

  const onChangeDocumentoDigitalInput = (value) => {
    dispatch(actions.changeDatosTestamentoInput('documento_digital', value));
  };

  const onRemoveDocumentoDigitalInput = () => {
    dispatch(actions.changeDatosTestamentoInput('documento_digital', ''));
  };

  const onSaveDatosTestamento = async (event) => {
    event.preventDefault();
    props.setShowToast(false);
    props.setToastMessage('');

    const formData = new FormData();
    helpers.buildFormData(formData, testamento);

    if (await dispatch(actions.saveDatosTestamento(formData))) {
      props.onSelectKey('1');
      props.setToastMessage('Los datos del testamento se guardaron con éxito.');
      props.setShowToast(true);
    }

    window.scrollTo(0, 0);
  };

  const onDownloadDocumentoDigital = (documento) => {
    const extension = String(documento).split('.').pop();

    dispatch(actions.downloadDatosTestamentoDocumento(extension));
  };

  React.useEffect(() => {
    if (!isLoading) {
      props.setUpdatedAt(testamento?.updated_at);
    }

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isLoading]);

  React.useEffect(() => {
    dispatch(actions.getDatosTestamento());

    return () => {
      dispatch(actions.resetDatosTestamentoErrors());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      <form onSubmit={onSaveDatosTestamento}>
        <FormRow right>
          {canWrite() && (
            <FormColumn auto>
              <Button
                type="submit"
                variant="secondary"
                marginRight
                disabled={isSaving}
              >
                {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
              </Button>
            </FormColumn>
          )}
          <FormColumn auto>
            <Button
              variant="primary"
              disabled={isSaving}
              onClick={() => props.onSelectKey('1')}
            >
              Siguiente
            </Button>
          </FormColumn>
        </FormRow>
        <FormRow className="mb-0">
          <FormColumn>
            <Paragraph title>Para darle legalidad a tus deseos y voluntades te invitamos a que formalices un testamento con un Notario Público.<br /> Un testamento garantiza que se dispondrá de tus bienes con la precisión que hayas señalado, con ello se evitan conflictos familiares que dañan y rompen vínculos familiares, gastos innecesarios y trámites desgastantes.</Paragraph>
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.notario">Nombre del notario*</FormLabel>
                <FormInput
                  type="text"
                  name="testamento.notario"
                  id="testamento.notario"
                  required
                  maxLength={100}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.notario}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Nombre del notario</FormLabel>
                <Paragraph>{testamento.notario}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn left>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.numero_notaria">Número de la notaría*</FormLabel>
                <FormInput
                  type="text"
                  name="testamento.numero_notaria"
                  id="testamento.numero_notaria"
                  required
                  maxLength={20}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.numero_notaria}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Número de la notaría</FormLabel>
                <Paragraph>{testamento.numero_notaria}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
          <FormColumn right>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.telefono">Teléfono*</FormLabel>
                <FormInput
                  type="tel"
                  name="testamento.telefono"
                  id="testamento.telefono"
                  required
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.telefono}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Teléfono</FormLabel>
                <Paragraph>{testamento.telefono}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn left>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.fecha_extencion">Extendido el día*</FormLabel>
                <FormInput
                  type="date"
                  name="testamento.fecha_extencion"
                  id="testamento.fecha_extencion"
                  required
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.fecha_extencion}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Extendido el día</FormLabel>
                <Paragraph>{testamento.fecha_extencion && moment(testamento.fecha_extencion).format('LL')}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
          <FormColumn right>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.numero_escritura">Número de escritura, volumen o tomo <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="testamento.numero_escritura"
                  id="testamento.numero_escritura"
                  maxLength={20}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.numero_escritura}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Número de escritura, volumen o tomo</FormLabel>
                <Paragraph>{testamento.numero_escritura}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
        <FormSeparator />
        <FormTitle>Domicilio</FormTitle>
        <FormRow>
          <FormColumn left>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.calle_numero">Calle y número*</FormLabel>
                <FormInput
                  type="text"
                  name="testamento.calle_numero"
                  id="testamento.calle_numero"
                  required
                  maxLength={100}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.calle_numero}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Calle y número</FormLabel>
                <Paragraph>{testamento.calle_numero}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
          <FormColumn right>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.colonia">Colonia*</FormLabel>
                <FormInput
                  type="text"
                  name="testamento.colonia"
                  id="testamento.colonia"
                  required
                  maxLength={100}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.colonia}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Colonia</FormLabel>
                <Paragraph>{testamento.colonia}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn left>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.codigo_postal">C.P. <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="number"
                  name="testamento.codigo_postal"
                  id="testamento.codigo_postal"
                  digits={5}
                  min={0}
                  max={99999}
                  step={1}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.codigo_postal}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>C.P.</FormLabel>
                <Paragraph>{testamento.codigo_postal}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
          <FormColumn center>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.municipio">Municipio*</FormLabel>
                <FormInput
                  type="text"
                  name="testamento.municipio"
                  id="testamento.municipio"
                  required
                  maxLength={100}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.municipio}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Municipio</FormLabel>
                <Paragraph>{testamento.municipio}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
          <FormColumn right>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.estado">Estado*</FormLabel>
                <FormInput
                  type="text"
                  name="testamento.estado"
                  id="testamento.estado"
                  required
                  maxLength={40}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.estado}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Estado</FormLabel>
                <Paragraph>{testamento.estado}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
        <FormSeparator />
        <FormRow>
          <FormColumn>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.documento_digital">Testamento digital <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF, Word, PowerPoint, Excel, .jpg o .png.</FormHelpText>
                {helpers.isEmptyString(testamento.documento_digital) ? (
                  <FormFile accept=".pdf,.doc,.docx,.ppt,.pptx,.xsl,.xlsx,.jpg,.jpeg,.png" onChange={onChangeDocumentoDigitalInput} />
                ) : (
                  <File
                    file={testamento.documento_digital}
                    canDownload
                    isDownloading={isDownloading}
                    onDownload={onDownloadDocumentoDigital}
                    onRemove={onRemoveDocumentoDigitalInput}
                  />
                )}
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Testamento digital</FormLabel>
                {!helpers.isEmptyString(testamento.documento_digital) && (
                  <File
                    file={testamento.documento_digital}
                    canDownload
                    isDownloading={isDownloading}
                    onDownload={onDownloadDocumentoDigital}
                    onRemove={onRemoveDocumentoDigitalInput}
                  />
                )}
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.documento_fisico">Donde encontrar el testamento <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="testamento.documento_fisico"
                  id="testamento.documento_fisico"
                  maxLength={500}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.documento_fisico}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Donde encontrar el testamento</FormLabel>
                <Paragraph>{testamento.documento_fisico}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="testamento.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="testamento.indicaciones"
                  id="testamento.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeDatosTestamentoInput}
                  value={testamento.indicaciones}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Instrucciones</FormLabel>
                <Paragraph>{testamento.indicaciones}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
        <FormSeparator />
        <FormRow right>
          {canWrite() && (
            <FormColumn auto>
              <Button
                type="submit"
                variant="secondary"
                marginRight
                disabled={isSaving}
              >
                {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
              </Button>
            </FormColumn>
          )}
          <FormColumn auto>
            <Button
              variant="primary"
              disabled={isSaving}
              onClick={() => props.onSelectKey('1')}
            >
              Siguiente
            </Button>
          </FormColumn>
        </FormRow>
      </form>
    </React.Fragment>
  );
};

DatosTestamento.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default DatosTestamento;
