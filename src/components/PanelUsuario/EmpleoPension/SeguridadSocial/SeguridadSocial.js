import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/empleo-pension/seguridad-social';

const SeguridadSocial = (props) => {
  const user = useSelector((state) => state.auth.user);
  const seguridadesSociales = useSelector((state) => state.seguridadSocial.seguridadesSociales);
  const seguridadSocial = useSelector((state) => state.seguridadSocial.seguridadSocial);
  const isLoading = useSelector((state) => state.seguridadSocial.isLoading);
  const isSaving = useSelector((state) => state.seguridadSocial.isSaving);
  const isUpdating = useSelector((state) => state.seguridadSocial.isUpdating);
  const isDeleting = useSelector((state) => state.seguridadSocial.isDeleting);
  const dispatch = useDispatch();

  const hasSeguridadesSociales = (seguridadesSociales.length > 0);

  const canWrite = () =>
    user?.can_write;

  const setUpdatedAt = () => {
    const [seguridadSocial] = seguridadesSociales.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    props.setUpdatedAt(seguridadSocial?.updated_at);
  };

  const onChangeSeguridadSocialInput = (event) => {
    const [, attribute] = event.target.name.split('.');

    dispatch(actions.changeSeguridadSocialInput(attribute, event.target.value));
  };

  const onResetSeguridadSocialInputs = () => {
    dispatch(actions.resetSeguridadSocialInputs());
  };

  const onEditSeguridadSocial = (seguridadSocial) => {
    window.scrollTo(0, 0);
    dispatch(actions.editSeguridadSocial(seguridadSocial));
  };

  const onSaveOrUpdateSeguridadSocial = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!seguridadSocial.id && await dispatch(actions.saveSeguridadSocial(seguridadSocial))) {
      props.setToastMessage('Los datos de la seguridad social se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (seguridadSocial.id && await dispatch(actions.updateSeguridadSocial(seguridadSocial))) {
      props.setToastMessage('Los datos de la seguridad social se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const onDeleteSeguridadSocial = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar esta seguridad social?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteSeguridadSocial(id))) {
      props.setToastMessage('Los datos de la seguridad social se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    if (!isLoading || !isSaving || !isUpdating || !isDeleting) {
      setUpdatedAt();
    }

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isLoading, isSaving, isUpdating, isDeleting]);

  React.useEffect(() => {
    dispatch(actions.getSeguridadesSociales());

    return () => {
      onResetSeguridadSocialInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('0')}
          >
            Regresar
          </Button>
        </FormColumn>
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('2')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateSeguridadSocial}>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="seguridadSocial.nombre">Nombre*</FormLabel>
                <FormHelpText>Capture el nombre de su seguridad social. Por ejemplo: IMSS</FormHelpText>
                <FormInput
                  type="text"
                  name="seguridadSocial.nombre"
                  id="seguridadSocial.nombre"
                  required
                  maxLength={100}
                  onChange={onChangeSeguridadSocialInput}
                  value={seguridadSocial.nombre}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="seguridadSocial.numero">Número de seguridad social*</FormLabel>
                <FormInput
                  type="text"
                  name="seguridadSocial.numero"
                  id="seguridadSocial.numero"
                  required
                  maxLength={20}
                  onChange={onChangeSeguridadSocialInput}
                  value={seguridadSocial.numero}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="seguridadSocial.clinica">Clínica*</FormLabel>
                <FormInput
                  type="text"
                  name="seguridadSocial.clinica"
                  id="seguridadSocial.clinica"
                  maxLength={100}
                  onChange={onChangeSeguridadSocialInput}
                  value={seguridadSocial.clinica}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="seguridadSocial.telefono">Teléfono <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="tel"
                  name="seguridadSocial.telefono"
                  id="seguridadSocial.telefono"
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeSeguridadSocialInput}
                  value={seguridadSocial.telefono}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetSeguridadSocialInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!seguridadSocial.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Número de seguridad social</th>
            <th>Clínica</th>
            <th>Teléfono</th>
            {canWrite() && (<th></th>)}
          </tr>
        </thead>
        <tbody>
          {isLoading ? (
            <tr>
              <td colSpan={5}>Cargando...</td>
            </tr>
          ) : hasSeguridadesSociales ? seguridadesSociales.map((seguridadSocial, index) => (
            <tr key={index}>
              <td>{seguridadSocial.nombre}</td>
              <td>{seguridadSocial.numero}</td>
              <td>{seguridadSocial.clinica}</td>
              <td>{seguridadSocial.telefono}</td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button variant="secondary" small onClick={() => onEditSeguridadSocial(seguridadSocial)}>
                      Editar
                    </Button>
                    <Button variant="danger" small onClick={() => onDeleteSeguridadSocial(seguridadSocial.id)}>
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={5}>
                {canWrite() ? 'No has agregado ninguna seguridad social.' : 'No se agregó ninguna seguridad social.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
      <FormSeparator />
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('0')}
          >
            Regresar
          </Button>
        </FormColumn>
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('2')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
    </React.Fragment>
  );
};

SeguridadSocial.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default SeguridadSocial;
