import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormFile from '../../../UI/Form/FormFile/FormFile';
import File from '../../../UI/File/File';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../../UI/Form/FormTitle/FormTitle';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/empleo-pension/datos-empleo';
import * as helpers from '../../../../helpers';
import Paragraph from '../../../UI/Paragraph/Paragraph';

const DatosEmpleo = (props) => {
  const user = useSelector((state) => state.auth.user);
  const empleo = useSelector((state) => state.datosEmpleo.empleo);
  const prestaciones = useSelector((state) => state.datosEmpleo.prestaciones);
  const isLoading = useSelector((state) => state.datosEmpleo.isLoading);
  const isSaving = useSelector((state) => state.datosEmpleo.isSaving);
  const isDownloadingContrato = useSelector((state) => state.datosEmpleo.isDownloadingContrato);
  const isDownloadingReciboPago = useSelector((state) => state.datosEmpleo.isDownloadingReciboPago);
  const dispatch = useDispatch();

  const hasPrestaciones = (prestaciones.length > 0);

  const canWrite = () =>
    user?.can_write;

  const onChangeDatosEmpleoInput = (event) => {
    const [, attribute] = event.target.name.split('.');

    dispatch(actions.changeDatosEmpleoInput(attribute, event.target.value));
  };

  const onChangePrestacionInput = (prestacion) => {
    dispatch(actions.changePrestacionInput(prestacion));
  };

  const onChangeDescripcionInput = (event) => {
    dispatch(actions.changeDescripcionInput(event.target.value));
  };

  const onChangeContratoDigitalInput = (value) => {
    dispatch(actions.changeDatosEmpleoInput('contrato_digital', value));
  };

  const onRemoveContratoDigitalInput = () => {
    dispatch(actions.changeDatosEmpleoInput('contrato_digital', ''));
  };

  const onChangeReciboPagoDigitalInput = (value) => {
    dispatch(actions.changeDatosEmpleoInput('recibo_pago_digital', value));
  };

  const onRemoveReciboPagoDigitalInput = () => {
    dispatch(actions.changeDatosEmpleoInput('recibo_pago_digital', ''));
  };

  const isPrestacionById = (id) => {
    return (prestacion) => {
      return prestacion.id === id;
    };
  };

  const hasPrestacionById = (id) => {
    return empleo.prestaciones.some(isPrestacionById(id));
  };

  const getDescripcionByPrestacionId = (prestacionId) => {
    const prestacion = empleo.prestaciones.find(isPrestacionById(prestacionId));

    if (!prestacion) {
      return '';
    }

    return prestacion.descripcion;
  };

  const onSaveDatosEmpleo = async (event) => {
    event.preventDefault();

    props.setShowToast(false);
    props.setToastMessage('');

    const formData = new FormData();
    helpers.buildFormData(formData, empleo);

    if (await dispatch(actions.saveDatosEmpleo(formData))) {
      props.onSelectKey('1');
      props.setToastMessage('Los datos del empleo se guardaron con éxito.');
      props.setShowToast(true);
    }

    window.scrollTo(0, 0);
  };

  const onDownloadContratoDigital = (contrato) => {
    const extension = String(contrato).split('.').pop();

    dispatch(actions.downloadDatosEmpleoContrato(extension));
  };

  const onDownloadReciboPagoDigital = (reciboPago) => {
    const extension = String(reciboPago).split('.').pop();

    dispatch(actions.downloadDatosEmpleoReciboPago(extension));
  };

  React.useEffect(() => {
    if (!isLoading) {
      props.setUpdatedAt(empleo?.updated_at);
    }

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isLoading]);

  React.useEffect(() => {
    dispatch(actions.getDatosEmpleo());

    return () => {
      dispatch(actions.resetDatosEmpleoErrors());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <form onSubmit={onSaveDatosEmpleo}>
      <FormRow right>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          </FormColumn>
        )}
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
      <Paragraph title>En este apartado podrás registrar toda la información relacionada con tu empleo, seguridad social y pensión.<br /> En caso de requerir anexar documentos adicionales, puedes hacerlo en la sección "Documentos digitales y ubicación física".</Paragraph>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.empresa">Nombre de la empresa*</FormLabel>
              <FormInput
                type="text"
                name="empleo.empresa"
                id="empleo.empresa"
                required
                maxLength={100}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.empresa}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Nombre de la empresa</FormLabel>
              <Paragraph>{empleo.empresa}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn left>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.puesto">Puesto*</FormLabel>
              <FormInput
                type="text"
                name="empleo.puesto"
                id="empleo.puesto"
                required
                maxLength={100}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.puesto}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Puesto</FormLabel>
              <Paragraph>{empleo.puesto}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.numero_empleado">Número de empleado o clave <FormText>(Opcional)</FormText></FormLabel>
              <FormInput
                type="text"
                name="empleo.numero_empleado"
                id="empleo.numero_empleado"
                maxLength={10}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.numero_empleado}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Número de empleado o clave</FormLabel>
              <Paragraph>{empleo.numero_empleado}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn left>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.jefe_directo">Nombre del jefe directo*</FormLabel>
              <FormInput
                type="text"
                name="empleo.jefe_directo"
                id="empleo.jefe_directo"
                required
                maxLength={100}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.jefe_directo}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Nombre del jefe directo</FormLabel>
              <Paragraph>{empleo.jefe_directo}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.dependencia_area">Dependencia o área*</FormLabel>
              <FormInput
                type="text"
                name="empleo.dependencia_area"
                id="empleo.dependencia_area"
                required
                maxLength={100}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.dependencia_area}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Dependencia o área</FormLabel>
              <Paragraph>{empleo.dependencia_area}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn left>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.telefono">Teléfono*</FormLabel>
              <FormInput
                type="tel"
                name="empleo.telefono"
                id="empleo.telefono"
                required
                minLength={10}
                maxLength={10}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.telefono}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Teléfono</FormLabel>
              <Paragraph>{empleo.telefono}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.enlace_rh">Contacto o Enlace con Recursos Humanos de la compañía <FormText>(Opcional)</FormText></FormLabel>
              <FormInput
                type="text"
                name="empleo.enlace_rh"
                id="empleo.enlace_rh"
                maxLength={100}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.enlace_rh}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Contacto o Enlace con Recursos Humanos de la compañía</FormLabel>
              <Paragraph>{empleo.enlace_rh}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormSeparator />
      <FormTitle>Domicilio</FormTitle>
      <FormRow>
        <FormColumn left>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.calle_numero">Calle y número*</FormLabel>
              <FormInput
                type="text"
                name="empleo.calle_numero"
                id="empleo.calle_numero"
                required
                maxLength={100}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.calle_numero}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Calle y número</FormLabel>
              <Paragraph>{empleo.calle_numero}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.colonia">Colonia*</FormLabel>
              <FormInput
                type="text"
                name="empleo.colonia"
                id="empleo.colonia"
                required
                maxLength={100}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.colonia}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Colonia</FormLabel>
              <Paragraph>{empleo.colonia}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn left>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.codigo_postal">C.P. <FormText>(Opcional)</FormText></FormLabel>
              <FormInput
                type="number"
                name="empleo.codigo_postal"
                id="empleo.codigo_postal"
                digits={5}
                min={0}
                max={99999}
                step={1}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.codigo_postal}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>C.P.</FormLabel>
              <Paragraph>{empleo.codigo_postal}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn center>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.municipio">Municipio*</FormLabel>
              <FormInput
                type="text"
                name="empleo.municipio"
                id="empleo.municipio"
                required
                maxLength={100}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.municipio}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Municipio</FormLabel>
              <Paragraph>{empleo.municipio}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.estado">Estado*</FormLabel>
              <FormInput
                type="text"
                name="empleo.estado"
                id="empleo.estado"
                required
                maxLength={40}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.estado}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Estado</FormLabel>
              <Paragraph>{empleo.estado}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormSeparator />
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="prestaciones">Prestaciones</FormLabel>
              {hasPrestaciones ? prestaciones.map((prestacion) => (
                <FormInput
                  key={prestacion.id}
                  as="checkbox"
                  name="prestaciones"
                  id={`prestacion_${prestacion.id}`}
                  inline
                  checked={hasPrestacionById(prestacion.id)}
                  onChange={() => onChangePrestacionInput(prestacion)}
                  label={prestacion.nombre}
                />
              )) : 'Cargando...'}
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Prestaciones</FormLabel>
              {prestaciones.map((prestacion, index) => (
                hasPrestacionById(prestacion.id) && <Paragraph key={prestacion.id}>{prestacion.nombre}</Paragraph>
              ))}
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
          {hasPrestacionById(7) && (
            <FormRow>
              <FormColumn>
                {canWrite() ? (
                  <React.Fragment>
                    <FormLabel htmlFor="prestacion.descripcion">Especifique*</FormLabel>
                    <FormInput
                      type="text"
                      name="prestacion.descripcion"
                      id="prestacion.descripcion"
                      required
                      maxLength={500}
                      onChange={onChangeDescripcionInput}
                      value={getDescripcionByPrestacionId(7)}
                    />
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <FormLabel>Especifique</FormLabel>
                    <Paragraph>{getDescripcionByPrestacionId(7)}</Paragraph>
                  </React.Fragment>
                )}
              </FormColumn>
            </FormRow>
          )}
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.contrato_digital">Contrato vigente digital <FormText>(Opcional)</FormText></FormLabel>
              <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF, Word, PowerPoint, Excel, .jpg o .png.</FormHelpText>
              {helpers.isEmptyString(empleo.contrato_digital) ? (
                <FormFile accept=".pdf,.doc,.docx,.ppt,.pptx,.xsl,.xlsx,.jpg,.jpeg,.png" onChange={onChangeContratoDigitalInput} />
              ) : (
                <File
                  file={empleo.contrato_digital}
                  canDownload
                  isDownloading={isDownloadingContrato}
                  onDownload={onDownloadContratoDigital}
                  onRemove={onRemoveContratoDigitalInput}
                />
              )}
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Contrato vigente digital</FormLabel>
              {!helpers.isEmptyString(empleo.contrato_digital) && (
                <File
                  file={empleo.contrato_digital}
                  canDownload
                  isDownloading={isDownloadingContrato}
                  onDownload={onDownloadContratoDigital}
                  onRemove={onRemoveContratoDigitalInput}
                />
              )}
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.contrato_fisico">Donde encontrar el contrato vigente <FormText>(Opcional)</FormText></FormLabel>
              <FormInput
                type="text"
                name="empleo.contrato_fisico"
                id="empleo.contrato_fisico"
                maxLength={500}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.contrato_fisico}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Donde encontrar el contrato vigente</FormLabel>
              <Paragraph>{empleo.contrato_fisico}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.recibo_pago_digital">Último recibo de pago digital <FormText>(Opcional)</FormText></FormLabel>
              <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF, Word, PowerPoint, Excel, .jpg o .png.</FormHelpText>
              {helpers.isEmptyString(empleo.recibo_pago_digital) ? (
                <FormFile accept=".pdf,.doc,.docx,.ppt,.pptx,.xsl,.xlsx,.jpg,.jpeg,.png" onChange={onChangeReciboPagoDigitalInput} />
              ) : (
                <File
                  file={empleo.recibo_pago_digital}
                  canDownload
                  isDownloading={isDownloadingReciboPago}
                  onDownload={onDownloadReciboPagoDigital}
                  onRemove={onRemoveReciboPagoDigitalInput}
                />
              )}
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Último recibo de pago digital</FormLabel>
              {!helpers.isEmptyString(empleo.recibo_pago_digital) && (
                <File
                  file={empleo.recibo_pago_digital}
                  canDownload
                  isDownloading={isDownloadingReciboPago}
                  onDownload={onDownloadReciboPagoDigital}
                  onRemove={onRemoveReciboPagoDigitalInput}
                />
              )}
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.recibo_pago_fisico">Donde encontrar los recibos de pago <FormText>(Opcional)</FormText></FormLabel>
              <FormInput
                type="text"
                name="empleo.recibo_pago_fisico"
                id="empleo.recibo_pago_fisico"
                maxLength={500}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.recibo_pago_fisico}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Donde encontrar los recibos de pago</FormLabel>
              <Paragraph>{empleo.recibo_pago_fisico}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="empleo.indicaciones">Instrucciones*</FormLabel>
              <FormInput
                as="textarea"
                name="empleo.indicaciones"
                id="empleo.indicaciones"
                rows={3}
                required
                maxLength={500}
                onChange={onChangeDatosEmpleoInput}
                value={empleo.indicaciones}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Instrucciones</FormLabel>
              <Paragraph>{empleo.indicaciones}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormSeparator />
      <FormRow right>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          </FormColumn>
        )}
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
    </form>
  );
};

DatosEmpleo.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default DatosEmpleo;
