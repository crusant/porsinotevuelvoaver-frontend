import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import Paragraph from '../../../UI/Paragraph/Paragraph';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormControl from '../../../UI/Form/FormControl/FormControl';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../../UI/Form/FormTitle/FormTitle';
import FormMargin from '../../../UI/Form/FormMargin/FormMargin';
import FormTable from '../../../UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../../UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../../UI/Button/Button';
import * as actions from '../../../../actions/empleo-pension/plan-pension';
import * as helpers from '../../../../helpers';

const PlanPension = (props) => {
  const [rowKey, setRowKey] = React.useState(-1);
  const [showBeneficiariosErrors, setShowBeneficiariosErrors] = React.useState(false);
  const user = useSelector((state) => state.auth.user);
  const planPension = useSelector((state) => state.planPension.planPension);
  const planesPensiones = useSelector((state) => state.planPension.planesPensiones);
  const beneficiario = useSelector((state) => state.planPension.beneficiario);
  const beneficiarios = useSelector((state) => state.planPension.planPension.beneficiarios);
  const asesor = useSelector((state) => state.planPension.planPension.asesor);
  const isLoading = useSelector((state) => state.planPension.isLoading);
  const isSaving = useSelector((state) => state.planPension.isSaving);
  const isUpdating = useSelector((state) => state.planPension.isUpdating);
  const isDeleting = useSelector((state) => state.planPension.isDeleting);
  const dispatch = useDispatch();

  const hasPlanesPensiones = (planesPensiones.length > 0);
  const hasBeneficiarios = (beneficiarios.length > 0);

  const canWrite = () =>
    user?.can_write;

    const setUpdatedAt = () => {
      const [planPension] = planesPensiones.sort((a, b) => {
        return a.updated_at > b.updated_at ? -1
          : a.updated_at < b.updated_at ? 1
          : 0;
      });

      props.setUpdatedAt(planPension?.updated_at);
    };

  const onChangePlanPensionInput = (event) => {
    const [, attribute] = event.target.name.split('.');

    dispatch(actions.changePlanPensionInput(attribute, event.target.value));
  };

  const onChangeBeneficiarioInput = (event) => {
    const [, attribute] = event.target.name.split('.');

    dispatch(actions.changeBeneficiarioInput(attribute, event.target.value));
  };

  const onChangeAsesorInput = (event) => {
    const [, attribute] = event.target.name.split('.');

    dispatch(actions.changeAsesorInput(attribute, event.target.value));
  };

  const sumPorcentajesBeneficiarios = (porcentaje, beneficiario) => {
    return porcentaje + parseInt(beneficiario.porcentaje, 10);
  };

  const getPorcentajeTotal = (initialPorcentaje = 0) => {
    return planPension.beneficiarios.reduce(sumPorcentajesBeneficiarios, initialPorcentaje);
  };

  const isProcentajeEqualTo = (porcentaje) => {
    return getPorcentajeTotal() === porcentaje;
  };

  const onAddBeneficiario = () => {
    const nombreCompleto = beneficiario.nombre_completo;
    const porcentaje = parseInt(beneficiario.porcentaje, 10);
    const porcentajes = getPorcentajeTotal(porcentaje);
    const isGreaterThan100 = (porcentajes > 100);

    setShowBeneficiariosErrors(false);

    if (isGreaterThan100) {
      setShowBeneficiariosErrors(true);
    }

    if (helpers.isEmptyString(nombreCompleto)|| !helpers.isInteger(porcentaje) || porcentaje <= 0 || isGreaterThan100) {
      return;
    }

    dispatch(actions.addBeneficiario(beneficiario));
    dispatch(actions.resetBeneficiario());
  };

  const onHideBeneficiariosErrors = () => {
    setShowBeneficiariosErrors(false);
  };

  const onRemoveBeneficiario = (index) => {
    dispatch(actions.removeBeneficiario(index));
  };

  const onResetPlanPensionInputs = () => {
    dispatch(actions.resetPlanPensionInputs());
  };

  const onEditPlanPension = (planPension) => {
    window.scrollTo(0, 0);
    dispatch(actions.editPlanPension(planPension));
  };

  const onSaveOrUpdatePlanPension = async (event) => {
    event.preventDefault();
    props.setShowToast(false);

    if (!planPension.id && await dispatch(actions.savePlanPension(planPension))) {
      props.setToastMessage('Los datos del plan de pensión se guardaron con éxito.');
      props.setShowToast(true);
    }

    if (planPension.id && await dispatch(actions.updatePlanPension(planPension))) {
      props.setToastMessage('Los datos del plan de pensión se actualizaron con éxito.');
      props.setShowToast(true);
    }
  };

  const onDeletePlanPension = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este plan de pensión?');
    props.setShowToast(false);

    if (isConfirmed && await dispatch(actions.deletePlanPension(id))) {
      props.setToastMessage('Los datos del plan de pensión se eliminaron con éxito.');
      props.setShowToast(true);
    }
  };

  const onToggleRow = (key) => {
    if (rowKey !== key) {
      setRowKey(key);
    } else {
      setRowKey(-1);
    }
  };

  React.useEffect(() => {
    if (!isLoading || !isSaving || !isUpdating || !isDeleting) {
      setUpdatedAt();
    }

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isLoading, isSaving, isUpdating, isDeleting]);

  React.useEffect(() => {
    dispatch(actions.getPlanesPensiones());

    return () => {
      onResetPlanPensionInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      <FormRow>
        <FormColumn>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Regresar
          </Button>
        </FormColumn>
      </FormRow>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdatePlanPension}>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="planPension.nombre">Nombre*</FormLabel>
                <FormHelpText>Capture el nombre de su plan de pensión. Por ejemplo: AFORE</FormHelpText>
                <FormInput
                  type="text"
                  name="planPension.nombre"
                  id="planPension.nombre"
                  required
                  maxLength={100}
                  onChange={onChangePlanPensionInput}
                  value={planPension.nombre}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="planPension.compania">Compañía*</FormLabel>
                <FormInput
                  type="text"
                  name="planPension.compania"
                  id="planPension.compania"
                  required
                  maxLength={100}
                  onChange={onChangePlanPensionInput}
                  value={planPension.compania}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="planPension.numero_contrato">Número de contrato o póliza*</FormLabel>
                <FormInput
                  type="text"
                  name="planPension.numero_contrato"
                  id="planPension.numero_contrato"
                  maxLength={20}
                  onChange={onChangePlanPensionInput}
                  value={planPension.numero_contrato}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="planPension.telefono">Teléfono <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="tel"
                  name="planPension.telefono"
                  id="planPension.telefono"
                  minLength={10}
                  maxLength={10}
                  onChange={onChangePlanPensionInput}
                  value={planPension.telefono}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="planPension.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="planPension.indicaciones"
                  id="planPension.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangePlanPensionInput}
                  value={planPension.indicaciones}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormTitle>Beneficiarios*</FormTitle>
            <FormHelpText underTitle>El porcentaje total debe ser del 100%.</FormHelpText>
            {showBeneficiariosErrors && (
              <Alert variant="danger" onClose={onHideBeneficiariosErrors} dismissible>
                <Paragraph>No se puede agregar al beneficiario, porque el porcentaje total no puede ser mayor al 100%.</Paragraph>
              </Alert>
            )}
            {isProcentajeEqualTo(100) && (
              <Alert variant="success">
                <Paragraph>Haz alcanzado el porcentaje total del 100%, por lo que ya no puedes agregar mas beneficiarios.</Paragraph>
              </Alert>
            )}
            <FormTable>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre completo</th>
                  <th>Porcentaje</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td>
                    <FormControl
                      type="text"
                      name="beneficiario.nombre_completo"
                      id="beneficiario.nombre_completo"
                      disabled={isProcentajeEqualTo(100)}
                      maxLength={100}
                      onChange={onChangeBeneficiarioInput}
                      value={beneficiario.nombre_completo}
                    />
                  </td>
                  <td>
                    <FormControl
                      type="number"
                      name="beneficiario.porcentaje"
                      id="beneficiario.porcentaje"
                      disabled={isProcentajeEqualTo(100)}
                      min={1}
                      max={100}
                      step={1}
                      onChange={onChangeBeneficiarioInput}
                      value={beneficiario.porcentaje}
                    />
                  </td>
                  <td>
                    <Button
                      variant="primary"
                      small
                      disabled={isProcentajeEqualTo(100)}
                      onClick={onAddBeneficiario}
                    >
                      Agregar
                    </Button>
                  </td>
                </tr>
                {hasBeneficiarios ? beneficiarios.map((beneficiario, index) => (
                  <tr key={beneficiario.nombre_completo}>
                    <td>{(index + 1)}</td>
                    <td>{beneficiario.nombre_completo}</td>
                    <td>{beneficiario.porcentaje} %</td>
                    <td>
                      <Button
                        variant="danger"
                        small
                        onClick={() => onRemoveBeneficiario(index)}
                      >
                        Quitar
                      </Button>
                    </td>
                  </tr>
                )) : (
                  <tr>
                    <td colSpan={4}>No haz agregado ningún beneficiario.</td>
                  </tr>
                )}
                <tr>
                  <td colSpan={2}><strong>Porcentaje total</strong></td>
                  <td><strong>{getPorcentajeTotal()} %</strong></td>
                  <td></td>
                </tr>
              </tbody>
            </FormTable>
            <FormSeparator />
            <FormTitle>Datos del asesor</FormTitle>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="asesor.nombre_completo">Nombre completo <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="asesor.nombre_completo"
                  id="asesor.nombre_completo"
                  maxLength={100}
                  onChange={onChangeAsesorInput}
                  value={asesor.nombre_completo}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="asesor.telefono_oficina">Teléfono de oficina <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="tel"
                  name="asesor.telefono_oficina"
                  id="asesor.telefono_oficina"
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeAsesorInput}
                  value={asesor.telefono_oficina}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="asesor.telefono_particular">Teléfono particular <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="tel"
                  name="asesor.telefono_particular"
                  id="asesor.telefono_particular"
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeAsesorInput}
                  value={asesor.telefono_particular}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left strict>
                <FormLabel htmlFor="asesor.correo_electronico">Correo electrónico <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                <FormInput
                  type="text"
                  name="asesor.correo_electronico"
                  id="asesor.correo_electronico"
                  maxLength={320}
                  pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                  title="Por favor ingrese un correo electrónico valido."
                  onChange={onChangeAsesorInput}
                  value={asesor.correo_electronico}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetPlanPensionInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!planPension.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Compañía</th>
            <th>Número de contrato o póliza</th>
            <th>Teléfono</th>
            <th>Instrucciones</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {isLoading ? (
            <tr>
              <td colSpan={6}>Cargando...</td>
            </tr>
          ) : hasPlanesPensiones ? planesPensiones.map((planPension, index) => (
            <React.Fragment key={planPension.id}>
              <tr>
                <td>{planPension.nombre}</td>
                <td>{planPension.compania}</td>
                <td>{planPension.numero_contrato}</td>
                <td>{planPension.telefono}</td>
                <td>{planPension.indicaciones}</td>
                <td>
                  <FormButtonGroup>
                    <Button variant="light" small onClick={() => onToggleRow(index)}>
                      Ver más
                    </Button>
                    {canWrite() && (
                      <React.Fragment>
                        <Button variant="secondary" small onClick={() => onEditPlanPension(planPension)}>
                          Editar
                        </Button>
                        <Button variant="danger" small onClick={() => onDeletePlanPension(planPension.id)}>
                          Eliminar
                        </Button>
                      </React.Fragment>
                    )}
                  </FormButtonGroup>
                </td>
              </tr>
              {(rowKey === index) && (
                <tr>
                  <td colSpan={6}>
                    <FormTable sub>
                      <thead>
                        <tr>
                          <th colSpan={2}>Beneficiarios</th>
                        </tr>
                        <tr>
                          <th>Nombre completo</th>
                          <th>Porcentaje</th>
                        </tr>
                      </thead>
                      <tbody>
                        {planPension.beneficiarios.map((beneficiario, index) => (
                          <tr key={index}>
                            <td>{beneficiario.nombre_completo}</td>
                            <td>{beneficiario.porcentaje}</td>
                          </tr>
                        ))}
                      </tbody>
                    </FormTable>
                    <h6>Datos del asesor</h6>
                    <ul>
                      <li><strong>Nombre completo</strong> {planPension.asesor.nombre_completo}</li>
                      <li><strong>Teléfono de oficina</strong> {planPension.asesor.telefono_oficina}</li>
                      <li><strong>Teléfono particular</strong> {planPension.asesor.telefono_particular}</li>
                      <li><strong>Correo electrónico</strong> {planPension.asesor.correo_electronico}</li>
                    </ul>
                  </td>
                </tr>
              )}
            </React.Fragment>
          )) : (
            <tr>
              <td colSpan={6}>
                {canWrite() ? 'No has agregado ningún plan de pensión.' : 'No se agregó ningún plan de pensión.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
      <FormSeparator />
      <FormRow>
        <FormColumn>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Regresar
          </Button>
        </FormColumn>
      </FormRow>
    </React.Fragment>
  );
};

PlanPension.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default PlanPension;
