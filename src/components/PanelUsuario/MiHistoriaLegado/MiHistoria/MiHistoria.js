import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import classNames from  'classnames';
import set from 'lodash/set';
import isEmpty from 'lodash/isEmpty';
import Paragraph from '../../../UI/Paragraph/Paragraph';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormCategories from '../../../UI/Form/FormCategories/FormCategories';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormFile from '../../../UI/Form/FormFile/FormFile';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import Button from '../../../UI/Button/Button';
import AudioRecorder from '../../../AudioRecorder/AudioRecorder';
import AudioPlayer from '../../../UI/AudioPlayer/AudioPlayer';
import AudioDownloader from '../../../UI/AudioDownloader/AudioDownloader';
import VideoRecorder from '../../../VideoRecorder/VideoRecorder';
import SelectedVideo from '../../../SelectedVideo/SelectedVideo';
import SavedVideo from '../../../SavedVideo/SavedVideo';
import IconQuestion from '../../../UI/Icons/IconQuestion/IconQuestion';
import useUser from '../../../../hooks/useUser';
import * as actions from '../../../../actions';
import * as helpers from '../../../../helpers';

const MiHistoria = (props) => {
  const [currentQuestion, setCurrentQuestion] = React.useState(1);
  const [audios, setAudios] = React.useState([]);
  const [videos, setVideos] = React.useState([]);
  const preguntas = useSelector((state) => state.miHistoria.preguntas);
  const isGetting = useSelector((state) => state.miHistoria.isGetting);
  const isSaving = useSelector((state) => state.miHistoria.isSaving);
  const isDownloadingAudio = useSelector((state) => state.miHistoria.isDownloadingAudio);
  const isDownloadingVideo = useSelector((state) => state.miHistoria.isDownloadingVideo);
  const dispatch = useDispatch();

  const { canWrite } = useUser();

  const questionIndex = (currentQuestion - 1);
  const pregunta = preguntas[questionIndex];
  const audio = audios[questionIndex];
  const video = videos[questionIndex];

  const hasPreguntas = () => {
    return (preguntas.length > 0);
  };

  const hasAudio = () => {
    return (audio || !helpers.isEmptyString(pregunta.respuesta.audio));
  };

  const hasVideo = () => {
    return (video || !helpers.isEmptyString(pregunta.respuesta.video));
  };

  const isFirstQuestion = () => {
    return (currentQuestion === 1);
  };

  const isLastQuestion = () => {
    return (currentQuestion === preguntas.length);
  };

  const getUpdatedAt = () => {
    const [pregunta] = preguntas.sort((a, b) => {
      return a.respuesta.updated_at > b.respuesta.updated_at ? -1
        : a.respuesta.updated_at < b.respuesta.updated_at ? 1
        : 0;
    });

    return pregunta?.respuesta?.updated_at;
  };

  const getSaveButtonText = () => {
    if (isSaving) {
      return 'Guardando...';
    }

    if (isLastQuestion()) {
      return 'Guardar';
    }

    return 'Guardar y siguiente';
  };

  const onChangeMiHistoriaInput = (event) => {
    const input = event.target;
    const attribute = input.name.split('.').pop();
    const value = input.type === 'checkbox' ? input.checked : input.value;

    dispatch(actions.changeMiHistoriaInput(questionIndex, attribute, value));
  };

  const onChangeMiHistoriaAudioInput = (value) => setAudios((prevState) => helpers.arrayReplace(prevState, questionIndex, value));

  const onRemoveMiHistoriaAudio = () => {
    setAudios((prevState) => helpers.arrayReplace(prevState, questionIndex, null));
    onDeleteMiHistoriaAudio();
  };

  const onDeleteMiHistoriaAudio = () => dispatch(actions.changeMiHistoriaInput(questionIndex, 'audio', ''));

  const onDownloadMiHistoriaAudioInMp3 = () => {
    const preguntaId = pregunta.id;

    dispatch(actions.downloadMiHistoriaAudio(preguntaId, 'mp3'));
  };

  const onDownloadMiHistoriaAudioInStandard = () => {
    const preguntaId = pregunta.id;
    const extension = pregunta.respuesta.audio.split('.').pop();

    dispatch(actions.downloadMiHistoriaAudio(preguntaId, extension));
  };

  const onChangeMiHistoriaVideoInput = (value) => setVideos((prevState) => helpers.arrayReplace(prevState, questionIndex, value));

  const onRemoveMiHistoriaVideo = () => {
    setVideos((prevState) => helpers.arrayReplace(prevState, questionIndex, null));
    onDeleteMiHistoriaVideo(questionIndex);
  };

  const onDeleteMiHistoriaVideo = () => dispatch(actions.changeMiHistoriaInput(questionIndex, 'video', ''));

  const onDownloadMiHistoriaVideo = () => {
    const preguntaId = pregunta.id;
    const extension = pregunta.respuesta.video.split('.').pop();

    dispatch(actions.downloadMiHistoriaVideo(preguntaId, extension));
  };

  const onSaveMiHistoria = async (event) => {
    event.preventDefault();

    const isAudio = (helpers.isFileOrBlob(audio));
    const isVideo = (helpers.isFileOrBlob(video));

    set(pregunta, 'respuesta.audio', isAudio ? audio : pregunta.respuesta.audio);
    set(pregunta, 'respuesta.video', isVideo ? video : pregunta.respuesta.video);

    props.setShowToast(false);
    props.setToastMessage('');

    if (await dispatch(actions.saveMiHistoria(pregunta))) {
      setAudios((prevState) => {
        prevState[questionIndex] = null;
        return [...prevState];
      });

      setVideos((prevState) => {
        prevState[questionIndex] = null;
        return [...prevState];
      });

      props.setToastMessage('Los datos de tu respuesta se guardaron con éxito.');
      props.setShowToast(true);

      onNextQuestionOrSection();
    };
  };

  const onPrevQuestionOrSection = () => {
    if (isFirstQuestion()) {
      props.onSelectKey('0');
    }

    if (!isFirstQuestion()) {
      setCurrentQuestion((prevState) => --prevState);
    }
  };

  const onNextQuestionOrSection = () => {
    if (!isLastQuestion()) {
      setCurrentQuestion((prevState) => prevState + 1);
    }
  };

  React.useEffect(() => {
    if (hasPreguntas() && audios.length <= 0) {
      setAudios(new Array(preguntas.length).fill(null));
    }

    if (hasPreguntas() && videos.length <= 0) {
      setVideos(new Array(preguntas.length).fill(null));
    }

    // eslint-disable-next-line
  }, [preguntas]);

  React.useEffect(() => {
    props.setUpdatedAt(getUpdatedAt());

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isGetting, isSaving]);

  React.useEffect(() => {
    window.scrollTo(0, 0);

    dispatch(actions.getMiHistoria());

    return () => {
      dispatch(actions.resetMiHistoria());
    };

    // eslint-disable-next-line
  }, []);

  if (!hasPreguntas()) {
    return <Paragraph>Cargando...</Paragraph>;
  }

  return (
    <form onSubmit={onSaveMiHistoria}>
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={onPrevQuestionOrSection}
          >
            Regresar
          </Button>
        </FormColumn>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {getSaveButtonText()}
            </Button>
          </FormColumn>
        )}
        {!isLastQuestion() && (
          <FormColumn auto>
            <Button
              variant="primary"
              disabled={isSaving}
              onClick={onNextQuestionOrSection}
            >
              Siguiente
            </Button>
          </FormColumn>
        )}
      </FormRow>
      <FormRow>
        <FormColumn>
          <Paragraph title center className="mb-0">Para guardar tus respuestas recuerda hacer clic en el botón GUARDAR Y SIGUIENTE.</Paragraph>
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          <FormCategories>
            {preguntas.map((_, index) => {
              const question = (index + 1);

              return (
                <FormCategories.Category
                  key={index}
                  name="preguntas"
                  id={`pregunta.${index}`}
                  checked={(question === currentQuestion)}
                  onChange={() => setCurrentQuestion(question)}
                  label={String(question)}
                  value={question}
                >
                  {(styles) => <IconQuestion className={styles.Icon} height="52px" />}
                </FormCategories.Category>
              );
            })}
          </FormCategories>
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="respuesta.texto">{pregunta.nombre} <FormText>(Opcional)</FormText></FormLabel>
              <FormInput
                as="textarea"
                name="respuesta.texto"
                id="respuesta.texto"
                rows={3}
                maxLength={6000}
                onChange={onChangeMiHistoriaInput}
                value={pregunta.respuesta.texto}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>{pregunta.nombre}</FormLabel>
              <Paragraph>
                {!isEmpty(pregunta.respuesta.texto) ? pregunta.respuesta.texto : 'El usuario no respondió esta pregunta'}
              </Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow className={classNames({'mb-0': !canWrite()})}>
        <FormColumn left>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="respuesta.is_grabar_audio">Audio <FormText>(Opcional)</FormText></FormLabel>
              <FormHelpText>El audio debe de pesar como máximo 8 MB y ser de tipo .mp3, .webm, .oga, .weba o .wav.</FormHelpText>
              <FormInput
                as="switch"
                name="respuesta.is_grabar_audio"
                id="respuesta.is_grabar_audio"
                checked={pregunta.respuesta.is_grabar_audio}
                falseText="Subir"
                trueText="Grabar"
                onChange={onChangeMiHistoriaInput}
              />
            </React.Fragment>
          ) : (
            <FormLabel>Audio</FormLabel>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="respuesta.is_grabar_video">Video <FormText>(Opcional)</FormText></FormLabel>
              <FormHelpText>El video debe de pesar como máximo 64 MB y ser de tipo .mp4, .mpeg, .avi, .mkv o .webm.</FormHelpText>
              <FormInput
                as="switch"
                name="respuesta.is_grabar_video"
                id="respuesta.is_grabar_video"
                checked={pregunta.respuesta.is_grabar_video}
                falseText="Subir"
                trueText="Grabar"
                onChange={onChangeMiHistoriaInput}
              />
            </React.Fragment>
          ) : (
            <FormLabel>Video</FormLabel>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn left>
          {canWrite() && (
            pregunta.respuesta.is_grabar_audio ? (
              !hasAudio() && <AudioRecorder onRecorded={onChangeMiHistoriaAudioInput} />
            ) : (
              !hasAudio() && <FormFile onChange={onChangeMiHistoriaAudioInput} />
            )
          )}
          {(canWrite() && helpers.isFileOrBlob(audio)) && (
            <AudioPlayer audio={audio} onRemove={onRemoveMiHistoriaAudio} />
          )}
          {helpers.isUrl(pregunta.respuesta.audio) ? (
            <AudioDownloader
              audio={pregunta.respuesta.audio}
              canRemove={canWrite()}
              isDownloading={isDownloadingAudio}
              onDownloadInMp3={onDownloadMiHistoriaAudioInMp3}
              onDownloadInStandard={onDownloadMiHistoriaAudioInStandard}
              onRemove={onDeleteMiHistoriaAudio}
            />
          ) : !canWrite() && (
            <Paragraph>El usuario no subió/grabo audio.</Paragraph>
          )}
        </FormColumn>
        <FormColumn right>
          {canWrite() && (
            pregunta.respuesta.is_grabar_video ? (
              !hasVideo() && <VideoRecorder onRecorded={onChangeMiHistoriaVideoInput} />
            ) : (
              !hasVideo() && <FormFile onChange={onChangeMiHistoriaVideoInput} />
            )
          )}
          {(canWrite() && helpers.isFileOrBlob(video)) && (
            <SelectedVideo video={video} onRemove={onRemoveMiHistoriaVideo} />
          )}
          {helpers.isUrl(pregunta.respuesta.video) ? (
            <SavedVideo
              video={pregunta.respuesta.video}
              onDownload={onDownloadMiHistoriaVideo}
              isDownloading={isDownloadingVideo}
              onDelete={onDeleteMiHistoriaVideo}
            />
          ) : !canWrite() && (
            <Paragraph>El usuario no subió/grabo video.</Paragraph>
          )}
        </FormColumn>
      </FormRow>
      <FormSeparator />
      <FormRow>
        <FormColumn left>
          <Button
            variant="light"
            disabled={isSaving}
            onClick={onPrevQuestionOrSection}
          >
            Regresar
          </Button>
        </FormColumn>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {getSaveButtonText()}
            </Button>
          </FormColumn>
        )}
        {!isLastQuestion() && (
          <FormColumn auto>
            <Button
              variant="primary"
              disabled={isSaving}
              onClick={onNextQuestionOrSection}
            >
              Siguiente
            </Button>
          </FormColumn>
        )}
      </FormRow>
    </form>
  );
};

MiHistoria.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default MiHistoria;
