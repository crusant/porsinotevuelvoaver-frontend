import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import get from 'lodash/get';
import nth from 'lodash/nth';
import isArray from 'lodash/isArray';
import FormRow from '../../../UI/Form/FormRow/FormRow';
import FormColumn from '../../../UI/Form/FormColumn/FormColumn';
import FormLabel from '../../../UI/Form/FormLabel/FormLabel';
import FormText from '../../../UI/Form/FormText/FormText';
import FormHelpText from '../../../UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../../UI/Form/FormInput/FormInput';
import FormSeparator from '../../../UI/Form/FormSeparator/FormSeparator';
import Paragraph from '../../../UI/Paragraph/Paragraph';
import Button from '../../../UI/Button/Button';
import ImageSelected from '../../../UI/ImageSelected/ImageSelected';
import ImageDownloader from '../../../UI/ImageDownloader/ImageDownloader';
import AudioRecorder from '../../../AudioRecorder/AudioRecorder';
import AudioPlayer from '../../../UI/AudioPlayer/AudioPlayer';
import AudioDownloader from '../../../UI/AudioDownloader/AudioDownloader';
import VideoRecorder from '../../../VideoRecorder/VideoRecorder';
import VideoPlayer from '../../../UI/VideoPlayer/VideoPlayer';
import VideoDownloader from '../../../UI/VideoDownloader/VideoDownloader';
import DragAndDrop from '../../../UI/DragAndDrop/DragAndDrop';
import * as actions from '../../../../actions';
import * as helpers from '../../../../helpers';

const EscrituraLibre = (props) => {
  const user = useSelector((state) => state.auth.user);
  const escrituraLibre = useSelector((state) => state.escrituraLibre.escrituraLibre);
  const isGetting = useSelector((state) => state.escrituraLibre.isGetting);
  const isSaving = useSelector((state) => state.escrituraLibre.isSaving);
  const isDownloadingAudio = useSelector((state) => state.escrituraLibre.isDownloadingAudio);
  const isDownloadingVideo = useSelector((state) => state.escrituraLibre.isDownloadingVideo);
  const dispatch = useDispatch();

  const canWrite = () => {
    return get(user, 'can_write', false);
  };

  const onChangeEscrituraLibreInput = (event) => {
    const input = event.target;
    const value = input.type === 'checkbox' ? input.checked : input.value;

    dispatch(actions.changeEscrituraLibreInput(input.name, value));
  };

  // Portada
  const onChangeEscrituraLibrePortadaInput = (files) => {
    dispatch(actions.changeEscrituraLibreInput('portada', nth(files, 0)));
  };

  const onRemoveEscrituraLibrePortadaInput = () => {
    dispatch(actions.changeEscrituraLibreInput('portada', ''));
  };

  // Audio
  const onChangeEscrituraLibreAudioInput = (files) => {
    if (isArray(files)) {
      return dispatch(actions.changeEscrituraLibreInput('audio', nth(files, 0)));
    }

    return dispatch(actions.changeEscrituraLibreInput('audio', files));
  };

  const onDownloadEscrituraLibreAudioInMp3 = () => {
    dispatch(actions.downloadEscrituraLibreAudio('mp3'));
  };

  const onDownloadEscrituraLibreAudioInStandard = () => {
    const extension = escrituraLibre.audio.split('.').pop();

    dispatch(actions.downloadEscrituraLibreAudio(extension));
  };

  const onRemoveEscrituraLibreAudio = () => {
    dispatch(actions.changeEscrituraLibreInput('audio', ''));
  };

  // Video
  const onChangeEscrituraLibreVideoInput = (files) => {
    if (isArray(files)) {
      return dispatch(actions.changeEscrituraLibreInput('video', nth(files, 0)));
    }

    return dispatch(actions.changeEscrituraLibreInput('video', files));
  };

  const onDownloadEscrituraLibreVideo = () => {
    const extension = escrituraLibre.video.split('.').pop();

    dispatch(actions.downloadEscrituraLibreVideo(extension));
  };

  const onRemoveEscrituraLibreVideo = () => {
    dispatch(actions.changeEscrituraLibreInput('video', ''));
  };

  const onSaveEscrituraLibre = async (event) => {
    event.preventDefault();

    props.setShowToast(false);
    props.setToastMessage('');

    if (await dispatch(actions.saveEscrituraLibre(escrituraLibre))) {
      props.onSelectKey('1');
      props.setToastMessage('Los datos de tu historia se guardaron con éxito.');
      props.setShowToast(true);
    }
  };

  React.useEffect(() => {
    if (!isGetting) {
      props.setUpdatedAt(escrituraLibre?.updated_at);
    }

    return () => {
      props.setUpdatedAt(null);
    };

    // eslint-disable-next-line
  }, [isGetting]);

  React.useEffect(() => {
    window.scrollTo(0, 0);

    dispatch(actions.getEscrituraLibre());

    return () => {
      dispatch(actions.resetEscrituraLibreErrors());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <form onSubmit={onSaveEscrituraLibre}>
      <FormRow right>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          </FormColumn>
        )}
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
      <Paragraph title>La sección Mi historia y legado te permite el registro de tus vivencias personales, experiencias, historias y anécdotas.<br /> Es una manera de dar a conocer a las futuras generaciones tus lecciones de vida, conocimientos y aprendizajes para su enriquecimiento y beneficio.</Paragraph>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="portada">Portada <FormText>(Opcional)</FormText></FormLabel>
              <FormHelpText>La portada debe de pesar como máximo 4 MB y ser de tipo .jpg, .jpeg, .bmp o .png.</FormHelpText>
              {helpers.isUrl(escrituraLibre.portada) ? (
                <ImageDownloader
                  url={escrituraLibre.portada}
                  route={escrituraLibre.route}
                  onRemove={onRemoveEscrituraLibrePortadaInput}
                />
              ) : helpers.isFile(escrituraLibre.portada) ? (
                <ImageSelected
                  image={escrituraLibre.portada}
                  onRemove={onRemoveEscrituraLibrePortadaInput}
                />
              ) : (
                <DragAndDrop
                  accept=".jpg, .jpeg, .bmp, .png"
                  maxSize={4194304}
                  multiple={false}
                  onDropAccepted={onChangeEscrituraLibrePortadaInput}
                />
              )}
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>Portada</FormLabel>
              {helpers.isUrl(escrituraLibre.portada) ? (
                <ImageDownloader
                  url={escrituraLibre.portada}
                  route={escrituraLibre.route}
                  canRemove={false}
                  onRemove={onRemoveEscrituraLibrePortadaInput}
                />
              ) : (
                <Paragraph>El usuario no subió una portada.</Paragraph>
              )}
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          {canWrite() ? (
            <React.Fragment>
              <FormLabel htmlFor="texto">En este espacio podrás plasmar todo lo relacionado a tu historia y legado para ser transmitido a tus seres queridos. Además, tienes la opción de grabar o subir audio o video con tu celular o PC*</FormLabel>
              <FormInput
                as="textarea"
                name="texto"
                id="texto"
                rows={3}
                required
                maxLength={6000}
                onChange={onChangeEscrituraLibreInput}
                value={escrituraLibre.texto}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <FormLabel>En este espacio podrás plasmar todo lo relacionado a tu historia y legado para ser transmitido a tus seres queridos. Además, tienes la opción de grabar o subir audio o video con tu celular o PC</FormLabel>
              <Paragraph>{escrituraLibre.texto}</Paragraph>
            </React.Fragment>
          )}
        </FormColumn>
      </FormRow>
      {canWrite() ? (
        <React.Fragment>
          <FormRow>
            <FormColumn>
              <FormLabel htmlFor="is_grabar_audio">Audio <FormText>(Opcional)</FormText></FormLabel>
              <FormHelpText>El audio debe de pesar como máximo 8 MB y ser de tipo .mp3, .webm, .oga, .weba o .wav.</FormHelpText>
              <FormInput
                as="switch"
                name="is_grabar_audio"
                id="is_grabar_audio"
                checked={escrituraLibre.is_grabar_audio}
                falseText="Subir"
                trueText="Grabar"
                onChange={onChangeEscrituraLibreInput}
              />
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn>
              {helpers.isUrl(escrituraLibre.audio) ? (
                <AudioDownloader
                  audio={escrituraLibre.audio}
                  isDownloading={isDownloadingAudio}
                  onDownloadInMp3={onDownloadEscrituraLibreAudioInMp3}
                  onDownloadInStandard={onDownloadEscrituraLibreAudioInStandard}
                  onRemove={onRemoveEscrituraLibreAudio}
                />
              ) : helpers.isFileOrBlob(escrituraLibre.audio) ? (
                <AudioPlayer
                  audio={escrituraLibre.audio}
                  onRemove={onRemoveEscrituraLibreAudio}
                />
              ) : escrituraLibre.is_grabar_audio ? (
                <AudioRecorder onRecorded={onChangeEscrituraLibreAudioInput} />
              ) : (
                <DragAndDrop
                  maxSize={8388608}
                  multiple={false}
                  onDropAccepted={onChangeEscrituraLibreAudioInput}
                />
              )}
            </FormColumn>
          </FormRow>
        </React.Fragment>
      ) : (
        <FormRow>
          <FormColumn>
            <FormLabel>Audio</FormLabel>
            {helpers.isUrl(escrituraLibre.audio) ? (
              <AudioDownloader
                audio={escrituraLibre.audio}
                isDownloading={isDownloadingAudio}
                onDownloadInMp3={onDownloadEscrituraLibreAudioInMp3}
                onDownloadInStandard={onDownloadEscrituraLibreAudioInStandard}
                onRemove={onRemoveEscrituraLibreAudio}
              />
            ) : (
              <Paragraph>El usuario no subió/grabo audio.</Paragraph>
            )}
          </FormColumn>
        </FormRow>
      )}
      {canWrite() ? (
        <React.Fragment>
          <FormRow>
            <FormColumn>
              <FormLabel htmlFor="is_grabar_video">Video <FormText>(Opcional)</FormText></FormLabel>
              <FormHelpText>El video debe de pesar como máximo 64 MB y ser de tipo .mp4, .mpeg, .avi, .mkv o .webm.</FormHelpText>
              <FormInput
                as="switch"
                name="is_grabar_video"
                id="is_grabar_video"
                checked={escrituraLibre.is_grabar_video}
                falseText="Subir"
                trueText="Grabar"
                onChange={onChangeEscrituraLibreInput}
              />
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn>
              {helpers.isUrl(escrituraLibre.video) ? (
                <VideoDownloader
                  video={escrituraLibre.video}
                  isDownloading={isDownloadingVideo}
                  onDownload={onDownloadEscrituraLibreVideo}
                  onRemove={onRemoveEscrituraLibreVideo}
                />
              ) : helpers.isFileOrBlob(escrituraLibre.video) ? (
                <VideoPlayer
                  video={escrituraLibre.video}
                  onRemove={onRemoveEscrituraLibreVideo}
                />
              ) : escrituraLibre.is_grabar_video ? (
                <VideoRecorder onRecorded={onChangeEscrituraLibreVideoInput} />
              ) : (
                <DragAndDrop
                  maxSize={67108864}
                  multiple={false}
                  onDropAccepted={onChangeEscrituraLibreVideoInput}
                />
              )}
            </FormColumn>
          </FormRow>
        </React.Fragment>
      ) : (
        <FormRow>
          <FormColumn>
            <FormLabel>Video</FormLabel>
            {helpers.isUrl(escrituraLibre.video) ? (
              <VideoDownloader
                video={escrituraLibre.video}
                isDownloading={isDownloadingVideo}
                onDownload={onDownloadEscrituraLibreVideo}
                onRemove={onRemoveEscrituraLibreVideo}
              />
            ) : (
              <Paragraph>El usuario no subió/grabo video.</Paragraph>
            )}
          </FormColumn>
        </FormRow>
      )}
      <FormSeparator />
      <FormRow right>
        {canWrite() && (
          <FormColumn auto>
            <Button
              type="submit"
              variant="secondary"
              marginRight
              disabled={isSaving}
            >
              {isSaving ? 'Guardando...' : 'Guardar y siguiente'}
            </Button>
          </FormColumn>
        )}
        <FormColumn auto>
          <Button
            variant="primary"
            disabled={isSaving}
            onClick={() => props.onSelectKey('1')}
          >
            Siguiente
          </Button>
        </FormColumn>
      </FormRow>
    </form>
  );
};

EscrituraLibre.propTypes = {
  onSelectKey: PropTypes.func.isRequired
};

export default EscrituraLibre;
