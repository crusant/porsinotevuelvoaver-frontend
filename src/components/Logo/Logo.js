import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import logoColor from '../../assets/images/logotipo.png';
import logoWhite from '../../assets/images/logotipo-blanco.png';
import styles from './Logo.module.scss';

const Logo = ({ white, className, ...props }) => (
  <img
    src={white ? logoWhite : logoColor}
    className={classNames(styles.Logo, className)}
    {...props}
    alt="Logotipo de Por si no te vuelvo a ver"
  />
);

Logo.defaultProps = {
  white: false
};

Logo.propTypes = {
  white: PropTypes.bool
};

export default Logo;
