import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import Button from '../UI/Button/Button';
import IconDownload from '../UI/Icons/IconDownload/IconDownload';
import IconClose from '../UI/Icons/IconClose/IconClose';
import styles from './SavedVideo.module.scss';

const SavedVideo = (props) => {
  const user = useSelector((state) => state.auth.user);

  const canWrite = () => {
    return _.get(user, 'can_write', false);
  };

  return (
    <div className={styles.SavedVideo}>
      <video src={props.video} className={styles.Video} preload="auto" controls>
        Lo sentimos su navegador no puede reproducir video, le sugerimos cambiar a la última versión de Chrome o Firefox.
      </video>
      <Button
        variant="secondary"
        className={styles.Button}
        small
        disabled={props.isDownloading}
        onClick={props.onDownload}
      >
        <IconDownload height="14px" /> {props.isDownloading ? 'Descargando video...' : 'Descargar video'}
      </Button>
      {canWrite() && (
        <Button
          variant="danger"
          className={styles.Button}
          small
          onClick={props.onDelete}
        >
          <IconClose height="14px" /> Quitar video
        </Button>
      )}
    </div>
  );
};

SavedVideo.defaultProps = {
  onDelete: () => {}
};

SavedVideo.propTypes = {
  video: PropTypes.string.isRequired,
  onDownload: PropTypes.func.isRequired,
  isDownloading: PropTypes.bool.isRequired,
  onDelete: PropTypes.func
};

export default SavedVideo;
