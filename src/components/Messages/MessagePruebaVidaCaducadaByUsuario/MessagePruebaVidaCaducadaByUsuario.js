import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import MessageLayout from '../MessageLayout/MessageLayout';

const MessagePruebaVidaCaducadaByUsuario = (props) => {
  const getUserPrimerNombre = () => {
    const nombreCompleto = _.get(props.pruebaVida, 'user.perfil.nombre_completo', '');
    return _.first(_.split(nombreCompleto, ' '));
  };

  const getPruevaVidaFechaLimite = () => {
    return moment(props.pruebaVida.fecha_limite).format('DD [de] MMMM [de] YYYY');
  };

  return (
    <MessageLayout title="¡Prueba de vida caducada!">
      {(styles) => (
        <React.Fragment>
          <p className={styles.Text}>Hola <strong>{getUserPrimerNombre()}</strong>, te informamos que el enlace para confirmar<br /> tu prueba de vida ha <strong>caducado</strong>.</p>
          <p className={styles.Text}><span className={styles.Uppercase}>Fecha límite de confirmación</span><br /> {getPruevaVidaFechaLimite()}</p>
        </React.Fragment>
      )}
    </MessageLayout>
  );
};

export default MessagePruebaVidaCaducadaByUsuario;
