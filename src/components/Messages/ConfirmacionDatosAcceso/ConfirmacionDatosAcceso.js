import React from 'react';
import PropTypes from 'prop-types';
import Layout from '../Layout/Layout';
import Logo from '../../Logo/Logo';
import styles from './ConfirmacionDatosAcceso.module.scss';

const ConfirmacionDatosAcceso = ({ primerNombre }) => (
  <Layout
    title="¡Datos de acceso entregados!"
    contentClass={styles.Layout}
  >
    <Logo className={styles.Logo} />
    <p className={styles.Text}>Los datos de acceso de la cuenta de <strong>{primerNombre}</strong>, han sido<br /> enviados a los correos electrónicos de sus custodios.</p>
  </Layout>
);

ConfirmacionDatosAcceso.propTypes = {
  primerNombre: PropTypes.string.isRequired
};

export default ConfirmacionDatosAcceso;
