import React from 'react';
import MessageLayout from '../MessageLayout/MessageLayout';

const MessagePruebaVidaInvalidaByUsuario = (props) => (
  <MessageLayout title="¡Prueba de vida inválida!">
    {(styles) => (
      <React.Fragment>
        <p className={styles.Text}>Hola, te informamos que el enlace para confirmar<br /> tu prueba de vida es <strong>inválido</strong>.</p>
      </React.Fragment>
    )}
  </MessageLayout>
);

export default MessagePruebaVidaInvalidaByUsuario;
