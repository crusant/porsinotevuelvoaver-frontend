import React from 'react';
import PropTypes from 'prop-types';
import Layout from '../Layout/Layout';
import Logo from '../../Logo/Logo';
import styles from './PruebaVidaExpirada.module.scss';

const PruebaVidaExpirada = ({ custodio, primerNombre, fechaLimite }) => (
  <Layout
    title="¡Prueba de vida caducada!"
    contentClass={styles.Layout}
  >
    <Logo className={styles.Logo} />
    {custodio ? (
      <p className={styles.Text}>Te informamos que el enlace para confirmar<br /> la prueba de vida de <strong>{primerNombre}</strong> ha <strong>caducado</strong>.</p>
    ) : (
      <p className={styles.Text}>Hola <strong>{primerNombre}</strong>, te informamos que el enlace para confirmar<br /> tu prueba de vida ha <strong>caducado</strong>.</p>
    )}
    <p className={styles.Text}><span className="text-uppercase">Fecha límite de confirmación</span><br /> {fechaLimite}</p>
  </Layout>
);

PruebaVidaExpirada.defaultProps = {
  custodio: false
};

PruebaVidaExpirada.propTypes = {
  custodio: PropTypes.bool,
  primerNombre: PropTypes.string.isRequired,
  fechaLimite: PropTypes.string.isRequired
};

export default PruebaVidaExpirada;
