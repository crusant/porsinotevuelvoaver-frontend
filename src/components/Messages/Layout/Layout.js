import React from  'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import Logo from '../../Logo/Logo';
import styles from './Layout.module.scss';

const Layout = ({ title, contentClass, children }) => (
  <div className={styles.Layout}>
    <div className={styles.Header}>
      <div className={styles.Container}>
        <Logo white className={styles.Logo} />
        <h1 className={styles.Title}>{title}</h1>
      </div>
    </div>
    <div className={classNames(styles.Content, contentClass)}>
      {children}
    </div>
    <div className={styles.Footer}>
      <div className={styles.Container}>
        <p className={styles.Text}>Recuerda que nunca estarás solo, nuestro equipo siempre te ayudará a cada paso.<br /> Si tienes dudas o consultas puedes contáctarnos cuando lo desees al correo<br /> <strong>contacto@porsinotevuelvoaver.com</strong></p>
        <p className={classNames(styles.Text, styles.Highlight)}>¡Siempre estamos listos para ayudarte!</p>
      </div>
      <div className={styles.Copyright}>&copy; {moment().year()} Por si no te vuelvo a ver. Todos los derechos reservados.</div>
    </div>
  </div>
);

Layout.propTypes = {
  title: PropTypes.string.isRequired,
  contentClass: PropTypes.string
};

export default Layout;
