import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import MessageLayout from '../MessageLayout/MessageLayout';

const MessagePruebaVidaConfirmadaByUsuario = (props) => {
  const getUserPrimerNombre = () => {
    const nombreCompleto = _.get(props.pruebaVida, 'user.perfil.nombre_completo', '');
    return _.first(_.split(nombreCompleto, ' '));
  };

  const getPruevaVidaFechaRespuesta = () => {
    return moment(props.pruebaVida.fecha_respuesta).format('DD [de] MMMM [de] YYYY [a las] HH:mm');
  };

  return (
    <MessageLayout title="¡Prueba de vida confirmada!">
      {(styles) => (
        <React.Fragment>
          <p className={styles.Text}>Hola <strong>{getUserPrimerNombre()}</strong>, te informamos que tu prueba de vida ya ha sido<br /> <strong>confirmada con éxito</strong>.</p>
          <p className={styles.Text}><span className={styles.Uppercase}>Fecha y hora de confirmación</span><br /> {getPruevaVidaFechaRespuesta()} hrs.</p>
        </React.Fragment>
      )}
    </MessageLayout>
  );
};

export default MessagePruebaVidaConfirmadaByUsuario;
