import React from 'react';
import PropTypes from 'prop-types';
import Layout from '../Layout/Layout';
import Logo from '../../Logo/Logo';
import styles from './PruebaVidaConfirmada.module.scss';

const PruebaVidaConfirmada = ({ custodio, primerNombre, fechaRespuesta }) => (
  <Layout
    title="¡Prueba de vida confirmada!"
    contentClass={styles.Layout}
  >
    <Logo className={styles.Logo} />
    {custodio ? (
      <p className={styles.Text}>Te informamos que la prueba de vida de <strong>{primerNombre}</strong> ya ha sido<br /> <strong>confirmada con éxito</strong>.</p>
    ) : (
      <p className={styles.Text}>Hola <strong>{primerNombre}</strong>, te informamos que tu prueba de vida ya ha sido<br /> <strong>confirmada con éxito</strong>.</p>
    )}
    <p className={styles.Text}><span className="text-uppercase">Fecha y hora de confirmación</span><br /> {fechaRespuesta} hrs.</p>
  </Layout>
);

PruebaVidaConfirmada.defaultProps = {
  custodio: false
};

PruebaVidaConfirmada.propTypes = {
  custodio: PropTypes.bool,
  primerNombre: PropTypes.string.isRequired,
  fechaRespuesta: PropTypes.string.isRequired
};

export default PruebaVidaConfirmada;
