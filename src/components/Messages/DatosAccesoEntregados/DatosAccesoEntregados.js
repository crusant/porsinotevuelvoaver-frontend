import React from 'react';
import PropTypes from 'prop-types';
import Layout from '../Layout/Layout';
import Logo from '../../Logo/Logo';
import styles from './DatosAccesoEntregados.module.scss';

const DatosAccesoEntregados = ({ primerNombre }) => (
  <Layout
    title="¡Datos de acceso entregados!"
    contentClass={styles.Layout}
  >
    <Logo className={styles.Logo} />
    <p className={styles.Text}>Te informamos que los datos de acceso de la cuenta de <strong>{primerNombre}</strong> ya han sido entregados.</p>
  </Layout>
);

DatosAccesoEntregados.propTypes = {
  primerNombre: PropTypes.string.isRequired
};

export default DatosAccesoEntregados;
