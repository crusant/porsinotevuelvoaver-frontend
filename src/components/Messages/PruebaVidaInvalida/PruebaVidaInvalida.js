import React from 'react';
import PropTypes from 'prop-types';
import Layout from '../Layout/Layout';
import Logo from '../../Logo/Logo';
import styles from './PruebaVidaInvalida.module.scss';

const PruebaVidaInvalida = ({ custodio }) => (
  <Layout
    title="¡Prueba de vida inválida!"
    contentClass={styles.Layout}
  >
    <Logo className={styles.Logo} />
    {custodio ? (
      <p className={styles.Text}>Te informamos que el enlace para confirmar<br /> la prueba de vida es <strong>inválido</strong>.</p>
    ) : (
      <p className={styles.Text}>Hola, te informamos que el enlace para confirmar<br /> tu prueba de vida es <strong>inválido</strong>.</p>
    )}
  </Layout>
);

PruebaVidaInvalida.defaultProps = {
  custodio: false
};

PruebaVidaInvalida.propTypes = {
  custodio: PropTypes.bool
};

export default PruebaVidaInvalida;
