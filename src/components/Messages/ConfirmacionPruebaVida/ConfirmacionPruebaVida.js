import React from 'react';
import PropTypes from 'prop-types';
import Layout from '../Layout/Layout';
import Logo from '../../Logo/Logo';
import styles from './ConfirmacionPruebaVida.module.scss';

const ConfirmacionPruebaVida = ({ custodio, primerNombre, fechaRespuesta }) => (
  <Layout
    title={custodio ? '¡Gracias por confirmar la prueba de vida!' : '¡Gracias por confirmar tu prueba de vida!'}
    contentClass={styles.Layout}
  >
    <Logo className={styles.Logo} />
    {custodio ? (
      <p className={styles.Text}>Te informamos que la prueba de vida de <strong>{primerNombre}</strong>, ha sido<br /> <strong>confirmada con éxito</strong>.</p>
    ) : (
      <p className={styles.Text}>Hola <strong>{primerNombre}</strong>, te informamos que tu prueba de vida ha sido<br /> <strong>confirmada con éxito</strong>.</p>
    )}
    <p className={styles.Text}><span className={styles.Uppercase}>Fecha y hora de confirmación</span><br /> {fechaRespuesta} hrs.</p>
  </Layout>
);

ConfirmacionPruebaVida.defaultProps = {
  custodio: false
};

ConfirmacionPruebaVida.propTypes = {
  custodio: PropTypes.bool,
  primerNombre: PropTypes.string.isRequired,
  fechaRespuesta: PropTypes.string.isRequired
};

export default ConfirmacionPruebaVida;
