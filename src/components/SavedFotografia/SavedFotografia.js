import React from 'react';
import Button from '../UI/Button/Button';
import IconDownload from '../UI/Icons/IconDownload/IconDownload';
import IconClose from '../UI/Icons/IconClose/IconClose';
import styles from './SavedFotografia.module.scss';

const SavedFotografia = (props) => (
  <div className={styles.SavedFotografia}>
    <img src={props.fotografia} className={styles.Image} alt="Fotografía tomada"/>
    <Button
      variant="secondary"
      className={styles.Button}
      small
      disabled={props.isDownloading}
      onClick={props.onDownload}
    >
      <IconDownload height="14px" /> {props.isDownloading ? 'Descargando fotografía...' : 'Descargar fotografía'}
    </Button>
    <Button
      variant="danger"
      className={styles.Button}
      small
      onClick={props.onDelete}
    >
      <IconClose height="14px" /> Quitar fotografía
    </Button>
  </div>
);

export default SavedFotografia;
