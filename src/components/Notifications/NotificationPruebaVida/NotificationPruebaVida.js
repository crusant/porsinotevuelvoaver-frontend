import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import moment from 'moment';
import Button from '../../UI/Button/Button';
import * as actions from '../../../actions/notificaciones';
import styles from './NotificationPruebaVida.module.scss';

const NotificationPruebaVida = (props) => {
  const user = useSelector((state) => state.auth.user);
  const dispatch = useDispatch();
  const { fecha_limite, url, isConfirming } = props.pruebaVida;

  const getUserPrimerNombre = () => {
    return _.first(_.split(user.perfil.nombre_completo, ' '));
  };

  const getPruevaVidaFechaLimite = () => {
    return moment(fecha_limite).format('DD [de] MMMM');
  };

  const onConfirmarPruebaVida = () => {
    dispatch(actions.confirmarNotificacionPruebaVida(url));
  };

  return (
    <div className={styles.NotificationPruebaVida}>
      <div className={styles.Title}>Prueba de vida</div>
      <div className={styles.Text}>
        Hola {getUserPrimerNombre()}, tienes hasta el <span className={styles.Highlight}>{getPruevaVidaFechaLimite()}</span> para confirmar tu prueba de vida
      </div>
      <Button
        variant="primary"
        small
        disabled={isConfirming}
        onClick={onConfirmarPruebaVida}
      >
        {isConfirming ? 'Confirmando...' : 'Confirmar'}
      </Button>
    </div>
  );
};

export default NotificationPruebaVida;
