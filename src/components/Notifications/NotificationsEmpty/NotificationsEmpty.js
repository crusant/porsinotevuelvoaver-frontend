import React from 'react';
import styles from './NotificationsEmpty.module.scss';

const NotificationsEmpty = () => (
  <div className={styles.NotificationsEmpty}>
    No hay notificaciones
  </div>
);

export default NotificationsEmpty;
