import React from 'react';
import styles from './NotificationsNuevosUsuarios.module.scss';

const NotificationsNuevosUsuarios = (props) => {
  const isPlural = () => {
    return props.count > 1;
  };

  return (
    <div className={styles.NotificationsNuevosUsuarios}>
      <div className={styles.Title}>{isPlural() ? 'Usuarios nuevos' : 'Usuario nuevo'}</div>
      <div className={styles.Text}>
        Hola Alberto, se {isPlural() ? 'han' : 'ha'} registrado <span className={styles.Highlight}>{props.count} {isPlural() ? 'usuarios nuevos' : 'usuario nuevo'}</span> en el sistema
      </div>
    </div>
  );
};

export default NotificationsNuevosUsuarios;
