import React from 'react';
import styles from './NotificationsLoader.module.scss';

const NotificationsLoader = () => (
  <div className={styles.NotificationsLoader}>
    Cargando...
  </div>
);

export default NotificationsLoader;
