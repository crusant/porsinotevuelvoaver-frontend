import React from 'react';

const Audio  = (props) => (
  <audio {...props} preload="auto">
    Lo sentimos su navegador no puede reproducir audio, le sugerimos cambiar a la última versión de Google Chrome o Firefox.
  </audio>
);

export default Audio;
