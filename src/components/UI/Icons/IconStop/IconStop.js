import React from 'react';

const IconStop = (props) => (
  <svg {...props} viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg">
    <rect width="256" height="256"/>
	</svg>
);

export default IconStop;
