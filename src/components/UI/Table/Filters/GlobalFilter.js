import React from 'react';
import { useAsyncDebounce } from 'react-table';
import IconSearch from '../../Icons/IconSearch/IconSearch';
import styles from './GlobalFilter.module.scss';

const GlobalFilter = ({
  globalFilter,
  setGlobalFilter
}) => {
  const [value, setValue] = React.useState(globalFilter);

  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 200);

  return (
    <div className={styles.GlobalFilter}>
      <input
        type="text"
        onChange={(event) => {
          const input = event.target;

          setValue(input.value);
          onChange(input.value);
        }}
        placeholder="Buscar..."
        value={value || ''}
      />
      <IconSearch className={styles.Icon} height="16px" />
    </div>
  );
};

export default GlobalFilter;
