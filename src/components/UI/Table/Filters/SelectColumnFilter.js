import React from 'react';
import _ from 'lodash';
import styles from './SelectColumnFilter.module.scss';

const SelectColumnFilter = ({
  data, accessor, label, onFilter
}) => {
  const [value, setValue] = React.useState('');

  const options = React.useMemo(() => {
    const options = new Set();

    data.forEach((datum, i) => {
      if (!_.isEmpty(_.get(datum, accessor, ''))) {
        options.add({ value: _.get(datum, accessor, ''), label: _.get(datum, accessor, '') });
      }
    });

    return _.uniqBy([...options.values()], (option) => option.value);

    // eslint-disable-next-line
  }, [data]);

  const onChange = (event) => {
    const input = event.target;

    onFilter(input.value, accessor);
    setValue(input.value);
  };

  return (
    <div className={styles.SelectColumnFilter}>
      {label}
      <select
        onChange={onChange}
        value={value}
      >
        <option value="">Todos</option>
        {options.map((option) => (
          <option
            key={option.value}
            value={option.value}
          >
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
}

export default SelectColumnFilter;
