import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useTable, useFilters, useGlobalFilter, useExpanded, usePagination } from 'react-table';
import _ from 'lodash';
import Button from 'react-bootstrap/Button';
import FormTable from '../Form/FormTable/FormTable';
import GlobalFilter from './Filters/GlobalFilter';
import SelectColumnFilter from './Filters/SelectColumnFilter';
import IconArrowRight from '../Icons/IconArrowRight/IconArrowRight';
import IconArrowLeft from '../Icons/IconArrowLeft/IconArrowLeft';
import IconDownload from '../Icons/IconDownload/IconDownload';
import { API_URL } from '../../../constants';
import styles from './Table.module.scss';

const Table = ({
  columns,
  hiddenColumns,
  data,
  loading,
  canFilteringByPlan,
  canFilteringByStatus,
  canDownloadInExcel,
  renderRowSubComponent
}) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    visibleColumns,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: {
      pageIndex,
      pageSize,
      ...state
    },
    setGlobalFilter,
    setFilter
  } = useTable({
      columns,
      data,
      initialState: {
        hiddenColumns: _.concat(['prueba_vida_usuario.status'], hiddenColumns),
        pageIndex: 0
      }
    },
    useFilters,
    useGlobalFilter,
    useExpanded,
    usePagination
  );

  const onFilter = (value, accessor) => {
    setFilter(accessor, value);
  };

  return (
    <div className={styles.Table}>
      <div className={styles.Header}>
        <div className={styles.PageSize}>
          Mostrar
          <select
            onChange={(event) => {
              setPageSize(Number(event.target.value))
            }}
            value={pageSize}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                {pageSize}
              </option>
            ))}
          </select>
          resultados
        </div>
        {canFilteringByPlan && (
          <SelectColumnFilter
            data={data}
            accessor="suscripcion.plan.nombre"
            label="Plan:"
            onFilter={onFilter}
          />
        )}
        {canFilteringByStatus && (
          <SelectColumnFilter
            data={data}
            accessor="prueba_vida_usuario.status"
            label="Estatus:"
            onFilter={onFilter}
          />
        )}
        <GlobalFilter
          globalFilter={state.globalFilter}
          setGlobalFilter={setGlobalFilter}
        />
      </div>
      {canDownloadInExcel && (
          <Button
            href={`${API_URL}usuarios-registrados/exportar?token=${localStorage.getItem('token')}`}
            className={classNames('c-button c-button--secondary c-button--small', styles.Download)}
            target="_blank"
          >
            <IconDownload className="c-button__icon" height="14px" /> Descargar en Excel
          </Button>
        )}
      <FormTable {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render('Header')}</th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row) => {
            prepareRow(row);

            return (
              <React.Fragment {..._.omit(row.getRowProps(), 'role')}>
                <tr>
                  {row.cells.map((cell) => (
                    <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                  ))}
                </tr>
                {row.isExpanded ? (
                  <tr>
                    <td colSpan={visibleColumns.length}>
                      {renderRowSubComponent({ row })}
                    </td>
                  </tr>
                ) : null}
              </React.Fragment>
            );
          })}
          {loading && (
            <tr>
              <td colSpan={10000}>Cargando...</td>
            </tr>
          )}
        </tbody>
      </FormTable>
      <div className={styles.Footer}>
        <div className={styles.Info}>
          Página <strong>{pageIndex + 1} de {pageOptions.length}</strong>
        </div>
        <div className={styles.Pagination}>
          <button
            type="button"
            className={styles.Button}
            disabled={!canPreviousPage}
            onClick={() => gotoPage(0)}
          >
            <IconArrowLeft className={styles.Icon} />
            <IconArrowLeft className={styles.Icon} />
          </button>
          <button
            type="button"
            className={styles.Button}
            disabled={!canPreviousPage}
            onClick={() => previousPage()}
          >
            <IconArrowLeft className={styles.Icon} />
          </button>
          <button
            type="button"
            className={styles.Button}
            disabled={!canNextPage}
            onClick={() => nextPage()}
          >
            <IconArrowRight className={styles.Icon} />
          </button>
          <button
            type="button"
            className={styles.Button}
            disabled={!canNextPage}
            onClick={() => gotoPage(pageCount - 1)}
          >
            <IconArrowRight className={styles.Icon} />
            <IconArrowRight className={styles.Icon} />
          </button>
        </div>
      </div>
    </div>
  );
};

PropTypes.defaultProps = {
  hiddenColumns: []
};

export default Table;
