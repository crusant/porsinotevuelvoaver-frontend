import React from 'react';

const Video = (props) => (
  <video {...props} preload="auto">
    Lo sentimos su navegador no puede reproducir video, le sugerimos cambiar a la última versión de Google Chrome o Firefox.
  </video>
);

export default Video;
