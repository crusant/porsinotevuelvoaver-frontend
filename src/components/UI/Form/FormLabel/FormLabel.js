import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './FormLabel.module.scss';

const FormLabel = (props) => {
  const classes = classNames(styles.FormLabel, props.className, {
    [styles.Center]: props.center
  });

  return (
  <label className={classes} htmlFor={props.htmlFor}>
    {props.children}
  </label>
  );
};

FormLabel.propTypes = {
  center: PropTypes.bool,
  htmlFor: PropTypes.string
};

export default FormLabel;
