import React from 'react';
import styles from './FormTitle.module.scss';

const FormTitle = (props) => (
  <h3 className={styles.FormTitle}>{props.children}</h3>
);

export default FormTitle;
