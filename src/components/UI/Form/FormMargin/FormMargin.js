import React from 'react';
import styles from './FormMargin.module.scss';

const FormMargin = () => (
  <div className={styles.FormMargin} />
);

export default FormMargin;
