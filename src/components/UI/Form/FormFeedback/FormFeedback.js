import React from 'react';
import PropTypes from 'prop-types';
import * as helpers from '../../../../helpers';
import styles from './FormFeedback.module.scss';

const FormFeedback = (props) => {
  if (!helpers.isObject(props.errors)) {
    return null;
  }

  return (
    <div className={styles.FormFeedback}>
      {props.errors[props.name]}
    </div>
  );
};

FormFeedback.propTypes = {
  errors: PropTypes.any,
  name: PropTypes.string
};

export default FormFeedback;
