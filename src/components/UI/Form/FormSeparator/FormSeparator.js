import React from 'react';
import styles from './FormSeparator.module.scss';

const FormSeparator = () => (
  <hr className={styles.FormSeparator} />
);

export default FormSeparator;
