import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './FormRow.module.scss';

const FormRow = (props) => {
  const classes = classNames(styles.FormRow, props.className, {
   'justify-content-end': props.right,
   'align-items-center': props.middle
  });

  return (
    <div className={classes}>
      {props.children}
    </div>
  );
};

FormRow.propTypes = {
  right: PropTypes.bool,
  middle: PropTypes.bool
};

export default FormRow;
