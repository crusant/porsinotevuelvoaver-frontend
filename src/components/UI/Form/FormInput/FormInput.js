import React from 'react';
import PropTypes from 'prop-types';
import FormCheckbox from '../FormCheckbox/FormCheckbox';
import FormSwitch from '../FormSwitch/FormSwitch';
import FormRadio from '../FormRadio/FormRadio';
import FormControl from '../FormControl/FormControl';

const FormInput = (props) => {
  switch (props.as) {
    case 'checkbox':
      return <FormCheckbox {...props} />;
    case 'switch':
      return <FormSwitch {...props} />
    case 'radio':
      return <FormRadio {...props} />
    default:
      return <FormControl {...props} />;
  }
};

FormInput.propTypes = {
  as: PropTypes.string
};

export default FormInput;
