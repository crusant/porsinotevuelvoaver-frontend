import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './FormCategories.module.scss';

const FormCategories = (props) => (
  <div className={classNames(styles.FormCategories, props.className)}>
    {props.children}
  </div>
);

const Category = (props) => (
  <div className={classNames(styles.Category, props.className)}>
    <input
      type="radio"
      name={props.name}
      id={props.id}
      disabled={props.disabled}
      checked={props.checked}
      onChange={props.onChange}
      value={props.value}
    />
    <label htmlFor={props.id}>
      <div className={styles.Circle}>
        {props.children(styles)}
      </div>
      <div className={styles.Label}>
        {props.label}
      </div>
    </label>
  </div>
);

Category.defaultProps = {
  onChange: () => {}
};

Category.propTypes = {
  name: PropTypes.string,
  id: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  label: PropTypes.string,
  value: PropTypes.any
};

FormCategories.Category = Category;

export default FormCategories;
