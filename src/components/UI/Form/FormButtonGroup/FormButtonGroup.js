import React from 'react';
import styles from './FormButtonGroup.module.scss';

const FormButtonGroup = (props) => (
  <div className={styles.FormButtonGroup}>
    {props.children}
  </div>
);

export default FormButtonGroup;
