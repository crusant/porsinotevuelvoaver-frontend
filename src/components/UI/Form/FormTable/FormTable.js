import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './FormTable.module.scss';

const FormTable = ({ sub, ...props }) => {
  const responsiveClasses = classNames(styles.Responsive, {
    [styles.Sub]: sub
  });

  const tableClasses = classNames(styles.FormTable, {
    [styles.Sub]: sub
  });

  return (
    <div className={responsiveClasses}>
      <table className={tableClasses} {...props}>
        {props.children}
      </table>
    </div>
  );
};

FormTable.propTypes = {
  sub: PropTypes.bool
};

export default FormTable;
