import React from 'react';
import classNames from 'classnames';
import styles from './FormText.module.scss';

const FormText = (props) => (
  <span className={classNames(styles.FormText, props.className)}>
    {props.children}
  </span>
);

export default FormText;
