import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import FormText from '../FormText/FormText';
import styles from './FormHelpText.module.scss';

const FormHelpText = (props) => {
  const classes = classNames(styles.FormHelpText, {
    [styles.UnderTitle]: props.underTitle
  });

  return (
    <FormText className={classes}>
      {props.children}
    </FormText>
  );
};

FormHelpText.propTypes = {
  underTitle: PropTypes.bool
};

export default FormHelpText;
