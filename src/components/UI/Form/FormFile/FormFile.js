import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './FormFile.module.scss';

const FormFile = (props) => {
  const fileRef = React.useRef(null);
  const inputRef = React.useRef(null);
  const [dragging, setDragging] = React.useState(false);

  const classes = classNames(styles.FormFile, {
    [styles.DragIn]: dragging,
    [styles.isDisabled]: props.disabled
  });

  const onSelect = () => {
    inputRef.current && inputRef.current.click();
  };

  const onSelected = (event) => {
    const multiple = props.multiple;
    const files = event.target.files;

    if (multiple && files && files.length > 0) {
      props.onChange(files);
    }

    if (!multiple && files && files.length === 1) {
      props.onChange(files[0]);
    }

    event.target.value = '';
  };

  React.useEffect(() => {
    const file = fileRef.current;

    const onDragIn = (event) => {
      event.preventDefault();
      event.stopPropagation();

      if (props.disabled) {
        return false;
      }

      const multiple = props.multiple;
      const items = event.dataTransfer.items;

      if ((multiple && items && items.length > 0) || (!multiple && items && items.length === 1)) {
        setDragging(true);
      }
    };

    const onDragOut = (event) => {
      event.preventDefault();
      event.stopPropagation();

      setDragging(false);
    };

    const onDrag = (event) => {
      event.preventDefault();
      event.stopPropagation();
    };

    const onDrop = (event) => {
      event.preventDefault();
      event.stopPropagation();

      if (props.disabled) {
        return false;
      }

      const multiple = props.multiple;
      const files = event.dataTransfer.files;

      setDragging(false);

      if (multiple && files && files.length > 0) {
        props.onChange(files);
      }

      if (!multiple && files && files.length === 1) {
        props.onChange(files[0]);
      }
    };

    file.addEventListener('dragenter', onDragIn);
    file.addEventListener('dragleave', onDragOut);
    file.addEventListener('dragover', onDrag);
    file.addEventListener('drop', onDrop);

    return () => {
      file.removeEventListener('dragenter', onDragIn);
      file.removeEventListener('dragleave', onDragOut);
      file.removeEventListener('dragover', onDrag);
      file.removeEventListener('drop', onDrop);
    };

    // eslint-disable-next-line
  }, []);

  return (
    <div ref={fileRef} className={classes} onClick={onSelect}>
      {dragging ? 'Suelta tu archivo aquí' : 'Haz clic o arrastra el archivo aquí'}
      <input
        type="file"
        ref={inputRef}
        className="d-none"
        accept={props.accept}
         multiple={props.multiple}
        disabled={props.disabled}
        onChange={onSelected}
      />
    </div>
  );
};

FormFile.defaultProps = {
  onChange: () => {}
};

FormFile.propTypes = {
  multiple: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func
};

export default FormFile;
