import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './FormColumn.module.scss';

const FormColumn = (props) => {
  const classes = classNames(styles.FormColumn, {
    [styles.Strict]: props.strict,
    [styles.Center]: props.center,
    [styles.Right]: props.right,
    [styles.Left]: props.left,
    [styles.Auto]: props.auto
  });

  return (
    <div className={classes}>
      {props.children}
    </div>
  );
};

FormColumn.propTypes = {
  strict: PropTypes.bool,
  center: PropTypes.bool,
  right: PropTypes.bool,
  left: PropTypes.bool,
  auto: PropTypes.bool
};

export default FormColumn;
