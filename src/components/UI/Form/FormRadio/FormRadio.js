import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './FormRadio.module.scss';

const FormRadio = (props) => {
  const classes = classNames(styles.FormCheckbox, props.className, {
    [styles.Inline]: props.inline
  });

  return (
    <div className={classes}>
      <input type="radio" name={props.name} id={props.id} checked={props.checked} onChange={props.onChange} value={props.value} />
      <label htmlFor={props.id}>{props.label}</label>
    </div>
  );
};

FormRadio.defaultProps = {
  onChange: () => {}
};

FormRadio.propTypes = {
  name: PropTypes.string,
  id: PropTypes.string.isRequired,
  inline: PropTypes.bool,
  label: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  value: PropTypes.any
};

export default FormRadio;
