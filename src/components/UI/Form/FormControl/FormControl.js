import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import * as helpers from '../../../../helpers';
import styles from './FormControl.module.scss';

const FormControl = React.forwardRef(({ integer, decimal, digits, ...props }, ref) => {
  const classes = classNames(styles.FormControl, props.className);

  const onChange = (event) => {
    const value = event.target.value;

    if (integer && !helpers.validateInteger(value)) {
      return;
    }

    if (props.max && !helpers.validateMax(value, props.max)) {
      return;
    }

    if (digits && !helpers.isEmptyString(value) && !helpers.validateDigits(value, digits)) {
      event.target.setCustomValidity(`El valor debe contener exactamente ${digits} digitos.`);
    } else {
      event.target.setCustomValidity('');
    }

    if (decimal && !helpers.validateDecimal(value)) {
      return;
    }

    props.onChange(event);
  };

  switch (props.as) {
    case 'textarea':
      return <textarea ref={ref} {...props} className={classes} />;
    case 'select':
      return (
        <select ref={ref} {...props} className={classes}>
          {props.children}
        </select>
      );
    default:
      return <input ref={ref} {...props} className={classes} onChange={onChange} />;
  }
});

FormControl.defaultProps = {
  onChange: () => {}
};

FormControl.propTypes = {
  as: PropTypes.oneOf(['textarea', 'select']),
  integer: PropTypes.bool,
  decimal: PropTypes.bool,
  onChange: PropTypes.func
};

export default FormControl;
