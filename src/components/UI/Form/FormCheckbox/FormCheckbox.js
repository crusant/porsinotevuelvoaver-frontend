import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './FormCheckbox.module.scss';

const FormCheckbox = (props) => {
  const classes = classNames(styles.FormCheckbox, props.className, {
    [styles.Inline]: props.inline
  });

  return (
    <div className={classes}>
      <input type="checkbox" name={props.name} id={props.id} onChange={props.onChange} checked={props.checked} />
      <label htmlFor={props.id}>{props.label}</label>
    </div>
  );
};

FormCheckbox.defaultProps = {
  onChange: () => {}
};

FormCheckbox.propTypes = {
  name: PropTypes.string,
  id: PropTypes.string.isRequired,
  inline: PropTypes.bool,
  label: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func
};

export default FormCheckbox;
