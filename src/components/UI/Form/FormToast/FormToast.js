import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Toast from 'react-bootstrap/Toast';
import styles from './FormToast.module.scss';

const FormToast = (props) => {
  const {
    variant,
    show,
    title,
    message,
    onClose
  } = props;

  return (
    <Toast
      onClose={onClose}
      show={show}
      delay={5000}
      autohide
      className={styles.Toast}
    >
      <Toast.Header
        className={classNames('text-light', {
          'bg-success': variant === 'success'
        })}
      >
        <strong className="mr-auto">{title}</strong>
      </Toast.Header>
      <Toast.Body className="bg-white">{message}</Toast.Body>
    </Toast>
  );
};

FormToast.propTypes = {
  variant: PropTypes.oneOf(['success']),
  show: PropTypes.bool,
  title: PropTypes.string,
  message: PropTypes.string,
  onClose: PropTypes.func
};

export default FormToast;
