import React from 'react';
import styles from './FormRange.module.scss';

const FormRange = (props) => {
  const range = React.useRef(null);
  const [newValue, setNewValue] = React.useState(0);
  const [newPosition, setNewPosition] = React.useState(0);

  const calculateThumbPosition = () => {
    if (range.current) {
      const input = range.current;
      const newValue = Number((input.value - input.min) * 100 / (input.max - input.min));
      const newPosition = 0 - (newValue * 0.25);

      setNewValue(newValue);
      setNewPosition(newPosition);
    }
  };

  React.useEffect(() => {
    calculateThumbPosition();
  }, [props.value]);

  return (
    <div className={styles.FormRange}>
      <span className={styles.Label} style={{ left: `calc(${newValue}% + (${newPosition}px))` }}>
        {props.value}
      </span>
      <input
        type="range"
        name={props.name}
        id={props.name}
        ref={range}
        className={styles.Range}
        min={props.min}
        max={props.max}
        onChange={props.onChange}
        value={props.value}
      />
    </div>
  );
};

export default FormRange;
