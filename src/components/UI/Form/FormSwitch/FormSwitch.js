import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './FormSwitch.module.scss';

const FormSwitch = (props) => (
  <div className={classNames(styles.FormSwitch, props.className)}>
    <span className={classNames(styles.Text, styles.Left)}>{props.falseText}</span>
    <input type="checkbox" name={props.name} id={props.id} disabled={props.disabled} onChange={props.onChange} checked={props.checked} />
    <label htmlFor={props.id}></label>
    <span className={classNames(styles.Text, styles.Right)}>{props.trueText}</span>
  </div>
);

FormSwitch.defaultProps = {
  onChange: () => {}
};

FormSwitch.propTypes = {
  name: PropTypes.string,
  id: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  falseText: PropTypes.string,
  trueText: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func
};

export default FormSwitch;
