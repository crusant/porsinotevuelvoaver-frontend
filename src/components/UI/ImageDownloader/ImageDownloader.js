import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from 'react-bootstrap/Button';
import IconPicture from '../Icons/IconPicture/IconPicture';
import IconDownload from '../Icons/IconDownload/IconDownload';
import IconClose from '../Icons/IconClose/IconClose';
import styles from './ImageDownloader.module.scss';

const ImageDownloader = ({ url, route, canRemove, onRemove }) => (
  <div className={styles.ImageDownloader}>
    <div className={styles.Image}>
      <IconPicture className={styles.Icon} width="30px" />
      <div className={styles.File}>{`portada.${String(url).split('.').pop()}`}</div>
    </div>
    <Button
      href={`${route}?token=${localStorage.getItem('token')}`}
      className={classNames('c-button', 'c-button--secondary', styles.Download)}
      target="_blank"
    >
      <IconDownload className="c-button__icon" height="14px" /> Descargar
    </Button>
    {canRemove && (
      <Button
        className={classNames('c-button', 'c-button--danger', styles.Remove)}
        onClick={onRemove}
      >
        <IconClose className="c-button__icon" height="14px" /> Quitar
      </Button>
    )}
  </div>
);

ImageDownloader.defaultProps = {
  canRemove: true
};

ImageDownloader.propTypes = {
  url: PropTypes.string,
  route: PropTypes.string,
  canRemove: PropTypes.bool,
  onRemove: PropTypes.func
};

export default ImageDownloader;
