import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from 'react-bootstrap/Button';
import Video from '../Video/Video';
import IconClose from '../Icons/IconClose/IconClose';
import styles from './VideoPlayer.module.scss';

const VideoPlayer = (props) => (
  <div className={styles.VideoPlayer}>
    <Video
      src={URL.createObjectURL(props.video)}
      className={styles.Video}
      controls
    />
    <Button
      className={classNames('c-button', 'c-button--danger', styles.Remove)}
      onClick={props.onRemove}
    >
      <IconClose className="c-button__icon" height="14px" /> Quitar
    </Button>
  </div>
);

VideoPlayer.propTypes = {
  video: PropTypes.any,
  onRemove: PropTypes.func
};

export default VideoPlayer;
