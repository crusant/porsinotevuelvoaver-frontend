import React from 'react';
import styles from './ErrorTitle.module.scss';

const ErrorTitle = (props) => (
  <h6 className={styles.ErrorTitle}>
    {props.children}
  </h6>
);

export default ErrorTitle;
