import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import Logo from '../../Logo/Logo';
import styles from './Message.module.scss';

const Message = (props) => {
  const getCurrentYear = () => {
    return moment().year();
  };

  return (
    <div className={styles.Message}>
      <div className={styles.Header}>
        <div className={styles.Content}>
          <Logo white className={styles.Logo} />
          <h1 className={styles.Title}>{props.title}</h1>
        </div>
      </div>
      <div className={styles.Body}>
        <Logo className={styles.Logo} />
        {props.children}
      </div>
      <div className={styles.Footer}>
        <div className={styles.Contact}>
          <p className={styles.Text}>Recuerda que nunca estarás solo, nuestro equipo siempre te ayudará a cada paso.<br /> Si tienes dudas o consultas puedes contáctarnos cuando lo desees al correo<br /> <strong>contacto@porsinotevuelvoaver.com</strong></p>
          <p className={classNames(styles.Text, styles.Large)}>¡Siempre estamos listos para ayudarte!</p>
        </div>
        <div className={styles.Copyright}>&copy; {getCurrentYear()} Por si no te vuelvo a ver. Todos los derechos reservados.</div>
      </div>
    </div>
  );
};

Message.propTypes = {
  title: PropTypes.string.isRequired
};

const Content = (props) => {
  return (
    <p className={styles.Content}>
      {props.children}
    </p>
  );
};

const Highlight = (props) => (
  <strong className={styles.Highlight}>
    {props.children}
  </strong>
);

Message.Content = Content;
Message.Highlight = Highlight;

export default Message;
