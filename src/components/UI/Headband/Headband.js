import React from 'react';
import styles from './Headband.module.scss';

const Headband = (props) => (
  <div className={styles.Headband}>
    {props.children(styles)}
  </div>
);

export default Headband;
