import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import get from 'lodash/get';
import Button from 'react-bootstrap/Button';
import IconPicture from '../Icons/IconPicture/IconPicture';
import IconClose from '../Icons/IconClose/IconClose';
import styles from './ImageSelected.module.scss';

const ImageSelected = ({ image, onRemove }) => (
  <div className={styles.ImageSelected}>
    <div className={styles.Image}>
      <IconPicture className={styles.Icon} width="30px" />
      <div className={styles.File}>{get(image, 'name', '')}</div>
    </div>
    <Button
      className={classNames('c-button', 'c-button--danger', styles.Remove)}
      onClick={onRemove}
    >
      <IconClose className="c-button__icon" height="14px" /> Quitar
    </Button>
  </div>
);

ImageSelected.propTypes = {
  image: PropTypes.any,
  onRemove: PropTypes.func
};

export default ImageSelected;
