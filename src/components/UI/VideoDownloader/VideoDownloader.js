import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from 'react-bootstrap/Button';
import Video from '../Video/Video';
import IconDownload from '../Icons/IconDownload/IconDownload';
import IconClose from '../Icons/IconClose/IconClose';
import styles from './VideoDownloader.module.scss';

const VideoDownloader = (props) => (
  <div className={styles.VideoDownloader}>
    <Video
      src={props.video}
      className={styles.Video}
      controls
    />
    <Button
      className={classNames('c-button', 'c-button--secondary', styles.Download)}
      disabled={props.isDownloading}
      onClick={props.onDownload}
    >
      <IconDownload className="c-button__icon" height="14px" /> {props.isDownloading ? 'Descargando...' : 'Descargar'}
    </Button>
    {props.canRemove && (
      <Button
        className={classNames('c-button', 'c-button--danger', styles.Remove)}
        onClick={props.onRemove}
      >
        <IconClose className="c-button__icon" height="14px" /> Quitar
      </Button>
    )}
  </div>
);

VideoDownloader.defaultProps = {
  canRemove: true
};

VideoDownloader.propTypes = {
  video: PropTypes.any,
  canRemove: PropTypes.bool,
  isDownloading: PropTypes.bool,
  onDownload: PropTypes.func,
  onRemove: PropTypes.func
};

export default VideoDownloader;
