import React from 'react';
import IconEyeShow from '../Icons/IconEyeShow/IconEyeShow';
import IconEyeHide from '../Icons/IconEyeHide/IconEyeHide';
import styles from './TogglePassword.module.scss';

const TogglePassword = (props) => {
  const [toggle, setToggle] = React.useState(false);

  const onShow = () => {
    setToggle(true);
  };

  const onHide = () => {
    setToggle(false);
  };

  return (
    <div className={styles.TogglePassword}>
      {toggle ? (
        <React.Fragment>
          {props.password} <IconEyeHide className={styles.Icon} height="15px" onClick={onHide} />
        </React.Fragment>
      ) : (
        <React.Fragment>
          &#x25cf;&#x25cf;&#x25cf;&#x25cf;&#x25cf;&#x25cf;&#x25cf;&#x25cf; <IconEyeShow className={styles.Icon} height="15px" onClick={onShow} />
        </React.Fragment>
      )}
    </div>
  );
};

export default TogglePassword;
