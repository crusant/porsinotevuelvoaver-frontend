import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import Dropdown from 'react-bootstrap/Dropdown';
import Audio from '../Audio/Audio';
import IconDownload from '../Icons/IconDownload/IconDownload';
import IconClose from '../Icons/IconClose/IconClose';
import styles from './AudioDownloader.module.scss';

const AudioDownloader = (props) => (
  <div className={styles.AudioDownloader}>
    <Audio
      src={props.audio}
      className={styles.Audio}
      controls
    />
    <Dropdown
      as={ButtonGroup}
      className={classNames('c-dropdown', styles.Downloader)}
    >
      <Button
        className="c-button c-button--secondary"
        disabled={props.isDownloading}
        onClick={props.onDownloadInMp3}
      >
        <IconDownload className="c-button__icon" height="14px" /> {props.isDownloading ? 'Descargando...' : 'Descargar en .mp3'}
      </Button>
      <Dropdown.Toggle
        className="c-dropdown__toggle c-dropdown__toggle--secondary"
        disabled={props.isDownloading}
      />
      <Dropdown.Menu alignRight>
        <Dropdown.Item
          className="c-dropdown__item"
          disabled={props.isDownloading}
          onClick={props.onDownloadInStandard}
        >
          Descargar en otro formato
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
    {props.canRemove && (
      <Button
        className={classNames('c-button', 'c-button--danger', styles.Remove)}
        onClick={props.onRemove}
      >
        <IconClose className="c-button__icon" height="14px" /> Quitar
      </Button>
    )}
  </div>
);

AudioDownloader.defaultProps = {
  canRemove: true
};

AudioDownloader.propTypes = {
  audio: PropTypes.any,
  canRemove: PropTypes.bool,
  isDownloading: PropTypes.bool,
  onDownloadInMp3: PropTypes.func,
  onDownloadInStandard: PropTypes.func,
  onRemove: PropTypes.func
};

export default AudioDownloader;
