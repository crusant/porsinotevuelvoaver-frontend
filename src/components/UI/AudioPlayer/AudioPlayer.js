import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from 'react-bootstrap/Button';
import Audio from '../Audio/Audio';
import IconClose from '../Icons/IconClose/IconClose';
import styles from './AudioPlayer.module.scss';

const AudioPlayer = (props) => (
  <div className={styles.AudioPlayer}>
    <Audio
      src={URL.createObjectURL(props.audio)}
      className={styles.Audio}
      controls
    />
    <Button
      className={classNames('c-button', 'c-button--danger', styles.Remove)}
      onClick={props.onRemove}
    >
      <IconClose className="c-button__icon" height="14px" /> Quitar
    </Button>
  </div>
);

AudioPlayer.propTypes = {
  audio: PropTypes.any,
  onRemove: PropTypes.func
};

export default AudioPlayer;
