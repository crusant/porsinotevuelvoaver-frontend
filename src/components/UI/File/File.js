import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import classNames from 'classnames';
import Button from '../Button/Button';
import IconFile from '../Icons/IconFile/IconFile';
import IconDownload from '../Icons/IconDownload/IconDownload';
import IconClose from '../Icons/IconClose/IconClose';
import * as helpers from '../../../helpers';
import styles from './File.module.scss';

const File = (props) => {
  const user = useSelector((state) => state.auth.user);
  const isFile = helpers.isFile(props.file);
  const name = isFile
    ? helpers.truncate(props.file.name, 12)
    : `documento.${props.file.split('.').pop()}`;

  const canWrite = () => {
    return _.get(user, 'can_write', false);
  };

  return (
    <div className={styles.File}>
      <div className={styles.Name}>
        <IconFile className={styles.Icon} /> {name}
      </div>
      {props.canDownload && !isFile && (
        <Button
          variant="secondary"
          align="right"
          className={classNames(styles.Button, styles.Download)}
          small
          disabled={props.isDownloading}
          onClick={() => props.onDownload(props.file)}
        >
          <IconDownload height="14px" /> {props.isDownloading ? 'Descargando documento...' : 'Descargar documento'}
        </Button>
      )}
      {canWrite() && (
        <Button
          variant="danger"
          align="right"
          className={styles.Button}
          small
          onClick={props.onRemove}
        >
          <IconClose height="14px" /> Quitar documento
        </Button>
      )}
    </div>
  );
};

File.propTypes = {
  file: PropTypes.any.isRequired,
  canDownload: PropTypes.bool,
  isDownloading: PropTypes.bool,
  onDownload: PropTypes.func,
  onRemove: PropTypes.func.isRequired
};

export default File;
