import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';

const InputSecurityCode = (props) => {
  const [securityCode, setSecurityCode] = useState('');

  const {
    className,
    isDisabled
  } = props;

  const onChangeSecurityCode = (event) => {
    const value = event.target.value;
    const pattern = /^\d{0,4}$/;

    if (pattern.test(value)) {
      setSecurityCode(value);
    }
  };

  return (
    <Form.Control
      type="text"
      className={className}
      data-checkout="securityCode"
      disabled={isDisabled}
      autoComplete="off"
      onChange={onChangeSecurityCode}
      placeholder="###"
      value={securityCode}
    />
  );
};

InputSecurityCode.propTypes = {
  className: PropTypes.string,
  isDisabled: PropTypes.bool
};

export default InputSecurityCode;
