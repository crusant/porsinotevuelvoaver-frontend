import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import bytes from 'bytes';
import nth from 'lodash/nth';
import { useDropzone } from 'react-dropzone';
import { isMobile } from 'react-device-detect';
import styles from './DragAndDrop.module.scss';

const DragAndDrop = ({
  accept,
  disabled,
  maxFiles,
  maxSize,
  minSize,
  multiple,
  onDrop,
  onDropAccepted
}) => {
  const [errors, setErrors] = React.useState([]);

  const {
    getRootProps,
    getInputProps,
    isDragActive
  } = useDropzone({
    accept,
    disabled,
    maxFiles,
    maxSize,
    minSize,
    multiple,
    onDrop,
    onDropAccepted: (files, event) => {
      setErrors([]);
      onDropAccepted(files, event);
    },
    onDropRejected: (files) => {
      const errors = getErrorMessages(files);
      setErrors(errors);
    },
  });

  const getErrorMessage = ({ file, errors }) => {
    const error = nth(errors, 0);

    switch (error.code) {
      case 'file-too-large':
        return `El archivo ${file.name} no debe pesar más de ${bytes(maxSize)}.`;
      case 'file-invalid-type':
        return `El archivo ${file.name} debe ser de tipo ${accept}.`;
      default:
        return `El archivo ${file.name} es invalido.`;
    }
  };

  const getErrorMessages = (rejections) => {
    return rejections.reduce((errors, rejection) => {
      return [...errors, getErrorMessage(rejection)];
    }, []);
  };

  return (
    <React.Fragment>
      <div {...getRootProps({ className: classNames(styles.DragAndDrop, {
        [styles.isActive]: isDragActive,
        [styles.isDisbled]: disabled,
        [styles.isInvalid]: (errors.length > 0)
      }) })}>
        <input {...getInputProps()} />
        {isMobile ? 'Haz clic aquí para seleccionar un archivo' : 'Haz clic o arrastra el archivo aquí'}
      </div>
      {errors.map((error, index) => (
        <div key={index} className="invalid-feedback d-block">{error}</div>
      ))}
    </React.Fragment>
  );
};

DragAndDrop.defaultProps = {
  disabled: false,
  maxFiles: 0,
  maxSize: Infinity,
  minSize: 0,
  multiple: true
};

DragAndDrop.propTypes = {
  accept: PropTypes.string,
  disabled: PropTypes.bool,
  maxFiles: PropTypes.number,
  maxSize: PropTypes.number,
  minSize: PropTypes.number,
  multiple: PropTypes.bool,
  onDrop: PropTypes.func,
  onDropAccepted: PropTypes.func
};

export default DragAndDrop;
