import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import * as helpers from '../../../helpers';
import styles from './StepProgressBar.module.scss';

const Context = React.createContext({});

const StepProgressBar = (props) => {
  const [activeKeys, setActiveKeys] = React.useState([]);
  const classes = classNames(styles.StepProgressBar, props.className);

  const onSelectKey = (activeKey) => {
    setActiveKeys(helpers.getDecrementNumbers(+activeKey));
  };

  React.useEffect(() => {
    onSelectKey(props.activeKey);
  }, [props.activeKey]);

  return (
    <Context.Provider value={{ activeKeys, onSelectKey }}>
      <div className={classes}>
        {props.children}
      </div>
    </Context.Provider>
  );
};

StepProgressBar.defaultProps = {
  activeKey: '0'
};

StepProgressBar.propTypes = {
  className: PropTypes.string,
  activeKey: PropTypes.string
};

const Step = (props) => {
  const { activeKeys, onSelectKey } = React.useContext(Context);

  const classes = classNames(styles.Step, {
    [styles.isActive]: (activeKeys.includes(+props.eventKey))
  });

  const onSelectStep = () => {
    onSelectKey(props.eventKey);
    props.onSelect(props.eventKey);
  };

  return (
    <div
      className={classes}
      onClick={onSelectStep}
    >
      {props.children}
    </div>
  );
};

Step.defaultProps = {
  onSelect: () => {}
};

Step.propTypes = {
  eventKey: PropTypes.string.isRequired,
  onSelect: PropTypes.func
};

StepProgressBar.Step = Step;

export default StepProgressBar;
