import React from 'react';
import PropTypes from 'prop-types';
import Logo from '../../Logo/Logo';
import styles from './Loader.module.scss';

const Loader = (props) => (
  <div className={styles.Loader}>
    <Logo className={styles.Logo} />
    <h1 className={styles.Title}>{props.title}<span className={styles.Dot}>.</span><span className={styles.Dot}>.</span><span className={styles.Dot}>.</span></h1>
  </div>
);

Loader.propTypes = {
  title: PropTypes.string.isRequired
};

export default Loader;
