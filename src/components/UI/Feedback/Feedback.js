import React from 'react';
import styles from './Feedback.module.scss';

const Feedback = (props) => (
  <h4 className={styles.Feedback}>{props.children}</h4>
);

export default Feedback;
