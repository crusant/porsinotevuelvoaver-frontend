import React, { useState } from 'react';
import PropTypes from 'prop-types';
import InputGroup from 'react-bootstrap/InputGroup';
import Form from 'react-bootstrap/Form';

const InputExpirationDate = (props) => {
  const [expirationMonth, setExpirationMonth] = useState('');
  const [expirationYear, setExpirationYear] = useState('');

  const {
    className,
    isDisabled
  } = props;

  const onChangeExpirationMonth = (event) => {
    const value = event.target.value;
    const pattern = /^\d{0,2}$/;

    if (pattern.test(value)) {
      setExpirationMonth(value);
    }
  };

  const onChangeExpirationYear = (event) => {
    const value = event.target.value;
    const pattern = /^\d{0,2}$/;

    if (pattern.test(value)) {
      setExpirationYear(value);
    }
  };

  return (
    <InputGroup>
      <Form.Control
        type="text"
        id="cardExpirationMonth"
        className={className}
        data-checkout="cardExpirationMonth"
        disabled={isDisabled}
        autoComplete="off"
        onChange={onChangeExpirationMonth}
        placeholder="mes"
        value={expirationMonth}
      />
      <Form.Control
        type="text"
        id="cardExpirationYear"
        className={className}
        data-checkout="cardExpirationYear"
        disabled={isDisabled}
        autoComplete="off"
        onChange={onChangeExpirationYear}
        placeholder="año"
        value={expirationYear}
      />
    </InputGroup>
  );
};

InputExpirationDate.propTypes = {
  className: PropTypes.string,
  isDisabled: PropTypes.bool
};

export default InputExpirationDate;
