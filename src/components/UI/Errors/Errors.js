import React from 'react';
import styles from './Errors.module.scss';

const Errors = (props) => (
  <ul className={styles.Errors}>
    {props.children}
  </ul>
);

const Error = (props) => (
  <li className={styles.Error}>
    {props.children}
  </li>
);

Errors.Error = Error;

export default Errors;
