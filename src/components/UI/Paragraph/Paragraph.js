import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Paragraph.module.scss';

const Paragraph = (props) => {
  const classes =classNames(styles.Paragraph, props.className, {
    [styles.Title]: props.title,
    [styles.MarginBottom]: props.marginBotom,
    [styles.Center]: props.center
  });

  return (
    <p className={classes}>
      {props.children}
    </p>
  );
};

Paragraph.defaultProps = {
  center: false
};

Paragraph.propTypes = {
  title: PropTypes.bool,
  marginBotom: PropTypes.bool,
  center: PropTypes.bool
};

export default Paragraph;
