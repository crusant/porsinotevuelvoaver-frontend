import React from 'react';
import classNames from 'classnames';
import styles from './List.module.scss';

const List = (props) => (
  <ul className={classNames(styles.List, props.className)} {...props}>
    {props.children}
  </ul>
);


const Item = (props) => (
  <li className={classNames(styles.Item, props.className)} {...props}>
    {props.children}
  </li>
);

List.Item = Item;

export default List;
