import React from 'react';
import PropTypes from 'prop-types';
import classNames from  'classnames';
import _ from 'lodash';
import AudioDownloader from '../../UI/AudioDownloader/AudioDownloader';
import VideoDownloader from '../../UI/VideoDownloader/VideoDownloader';
import * as helpers from '../../../helpers';
import styles from './MiHistoria.module.scss';

const MiHistoria = (props) => (
  <div className={styles.MiHistoria}>
    {props.miHistoria.map((miHistoria) => {
      const hasMiHistoria = () => {
        const texto = _.get(miHistoria, 'texto', '');
        const audio = _.get(miHistoria, 'audio', '');
        const video = _.get(miHistoria, 'video', '');

        return !helpers.isEmptyString(texto)
            || !helpers.isEmptyString(audio)
            || !helpers.isEmptyString(video);
      };

      const hasTexto = () => {
        const texto = _.get(miHistoria, 'texto', '');

        return !helpers.isEmptyString(texto);
      };

      const hasMedia = () => {
        const audio = _.get(miHistoria, 'audio', '');
        const video = _.get(miHistoria, 'video', '');

        return !helpers.isEmptyString(audio)
            || !helpers.isEmptyString(video);
      };

      if (!hasMiHistoria()) {
        return '';
      }

      return (
        <div key={miHistoria.id} className={styles.Card}>
          <div className={classNames(styles.Header, { [styles.isOnly]: !hasMedia() })}>
            <h2 className={classNames(styles.Title, { [styles.isOnly]: !hasTexto() })}>
              {_.get(miHistoria, 'pregunta.nombre', '')}
            </h2>
            {hasTexto() && helpers.textToParagraph(miHistoria.texto, styles.Paragraph)}
          </div>
          {hasMedia() && (
            <div className={styles.Body}>
              {!helpers.isEmptyString(_.get(miHistoria, 'audio', '')) && (
                <React.Fragment>
                  <h3 className={styles.Title}>Escucha mi respuesta</h3>
                  <AudioDownloader
                    audio={_.get(miHistoria, 'audio', '')}
                    canRemove={false}
                    isDownloading={props.isDownloadingAudio}
                    onDownloadInMp3={() => props.onDownloadAudioInMp3(miHistoria.id, 'mp3')}
                    onDownloadInStandard={() => props.onDownloadAudioInStandard(miHistoria.id, '')}
                  />
                </React.Fragment>
              )}
              {!helpers.isEmptyString(_.get(miHistoria, 'video', '')) && (
                <React.Fragment>
                  <h3 className={styles.Title}>Mira mi respuesta</h3>
                  <VideoDownloader
                    video={_.get(miHistoria, 'video', '')}
                    canRemove={false}
                    isDownloading={props.isDownloadingVideo}
                    onDownload={() => props.onDownloadVideo(miHistoria.id)}
                  />
                </React.Fragment>
              )}
            </div>
          )}
        </div>
      );
    })}
  </div>
);

MiHistoria.propTypes = {
  miHistoria: PropTypes.array,
  isDownloadingAudio: PropTypes.bool,
  isDownloadingVideo: PropTypes.bool,
  onDownloadAudioInMp3: PropTypes.func,
  onDownloadAudioInStandard: PropTypes.func,
  onDownloadVideo: PropTypes.func
};

export default MiHistoria;
