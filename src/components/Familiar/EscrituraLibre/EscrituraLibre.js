import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import AudioDownloader from '../../UI/AudioDownloader/AudioDownloader';
import VideoDownloader from '../../UI/VideoDownloader/VideoDownloader';
import * as helpers from '../../../helpers';
import styles from './EscrituraLibre.module.scss';

const EscrituraLibre = (props) => (
  <div className={styles.EscrituraLibre}>
    {!helpers.isEmptyString(_.get(props.escrituraLibre, 'texto', '')) && (
      <div className={styles.Quote}>
        <div className={styles.Content}>
          <div className={styles.Text}>
            {helpers.textToParagraph(_.get(props.escrituraLibre, 'texto', ''), styles.Paragraph)}
          </div>
        </div>
        <div className={styles.Author}>
          - {_.get(props.user, 'perfil.nombre_completo', '')}
        </div>
      </div>
    )}
    {!helpers.isEmptyString(_.get(props.escrituraLibre, 'audio', '')) && (
      <React.Fragment>
        <h3 className={styles.Title}>Escucha mi historia</h3>
        <AudioDownloader
          audio={_.get(props.escrituraLibre, 'audio', '')}
          canRemove={false}
          isDownloading={props.isDownloadingAudio}
          onDownloadInMp3={() => props.onDownloadAudioInMp3('mp3')}
          onDownloadInStandard={() => props.onDownloadAudioInStandard('')}
        />
      </React.Fragment>
    )}
    {!helpers.isEmptyString(_.get(props.escrituraLibre, 'video', '')) && (
      <React.Fragment>
        <h3 className={styles.Title}>Mira mi historia</h3>
        <VideoDownloader
          video={_.get(props.escrituraLibre, 'video', '')}
          canRemove={false}
          isDownloading={props.isDownloadingVideo}
          onDownload={props.onDownloadVideo}
        />
      </React.Fragment>
    )}
  </div>
);

EscrituraLibre.propTypes = {
  escrituraLibre: PropTypes.object,
  user: PropTypes.object,
  isDownloadingAudio: PropTypes.bool,
  isDownloadingVideo: PropTypes.bool,
  onDownloadAudioInMp3: PropTypes.func,
  onDownloadAudioInStandard: PropTypes.func,
  onDownloadVideo: PropTypes.func
};

export default EscrituraLibre;
