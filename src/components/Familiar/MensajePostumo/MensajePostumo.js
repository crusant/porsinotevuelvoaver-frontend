import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import _ from 'lodash';
import Button from 'react-bootstrap/Button';
import IconDownload from '../../UI/Icons/IconDownload/IconDownload';
import AudioDownloader from '../../UI/AudioDownloader/AudioDownloader';
import VideoDownloader from '../../UI/VideoDownloader/VideoDownloader';
import { API_URL } from '../../../constants'
import * as helpers from '../../../helpers';
import styles from './MensajePostumo.module.scss';

const MensajePostumo = (props) => (
  <div className={styles.MensajePostumo}>
    <h2 className={styles.Recipient}>{props.familiar}, este mensaje es para ti</h2>
    {!helpers.isEmptyString(_.get(props.mensajePostumo, 'fotografia', '')) && (
      <img src={_.get(props.mensajePostumo, 'fotografia', '')} className={styles.Photography} alt="" />
    )}
    {props.showButton && (
      <Button
          href={`${API_URL}mensajes-postumos/${props.url}/descargar`}
          className={classNames('c-button c-button--secondary', styles.Download)}
          target="_blank"
        >
          <IconDownload className="c-button__icon" height="14px" /> Descargar mensaje póstumo
      </Button>
    )}
    {!helpers.isEmptyString(_.get(props.mensajePostumo, 'mensaje', '')) && (
      <div className={styles.Quote}>
        <div className={styles.Content}>
          <div className={styles.Text}>
            {helpers.textToParagraph(_.get(props.mensajePostumo, 'mensaje', ''), styles.Paragraph)}
          </div>
        </div>
        <div className={styles.Author}>
          - {_.get(props.user, 'perfil.nombre_completo', '')}
        </div>
      </div>
    )}
    {!helpers.isEmptyString(_.get(props.mensajePostumo, 'audio', '')) && (
      <React.Fragment>
        <h3 className={styles.Title}>Escucha mi mensaje</h3>
        <AudioDownloader
          audio={_.get(props.mensajePostumo, 'audio', '')}
          canRemove={false}
          isDownloading={props.isDownloadingAudio}
          onDownloadInMp3={() => props.onDownloadAudioInMp3(_.get(props.mensajePostumo, 'id', ''), 'mp3')}
          onDownloadInStandard={() => props.onDownloadAudioInStandard(_.get(props.mensajePostumo, 'id', ''), '')}
        />
      </React.Fragment>
    )}
    {!helpers.isEmptyString(_.get(props.mensajePostumo, 'video', '')) && (
      <React.Fragment>
        <h3 className={styles.Title}>Mira mi mensaje</h3>
        <VideoDownloader
          video={_.get(props.mensajePostumo, 'video', '')}
          canRemove={false}
          isDownloading={props.isDownloadingVideo}
          onDownload={() => props.onDownloadVideo(_.get(props.mensajePostumo, 'id', ''))}
        />
      </React.Fragment>
    )}
  </div>
);

MensajePostumo.propTypes = {
  mensajePostumo: PropTypes.object,
  familiar: PropTypes.string,
  user: PropTypes.object,
  isDownloadingAudio: PropTypes.bool,
  isDownloadingVideo: PropTypes.bool,
  showButton: PropTypes.bool,
  url: PropTypes.string,
  onDownloadAudioInMp3: PropTypes.func,
  onDownloadAudioInStandard: PropTypes.func,
  onDownloadVideo: PropTypes.func
};

export default MensajePostumo;
