import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Headband from '../UI/Headband/Headband';
import Button from '../UI/Button/Button';
import Navigation from '../Navigation/Navigation';
import Container from '../Container/Container';
import Header from '../Header/Header';
import Content from '../Content/Content';
import Paragraph from '../UI/Paragraph/Paragraph';
import useUser from '../../hooks/useUser';
import styles from './Layout.module.scss';

const Layout = (props) => {
  const {
    is_delivered,
    isAdministrador,
    isPlanPremium,
    bePlanPremium,
    isSuscripcionActiva,
    isSuscripcionPagada
  } = useUser();

  return (
    <React.Fragment>
      {(!isAdministrador() && !isPlanPremium()) && (
        <Headband>
          {(styles) => (
            bePlanPremium() ? (
              <>Tu plan <strong>Premium</strong> está en proceso de acreditación.</>
            ) : (
              <>Tienes una cuenta gratis con acceso limitado. Accede a todos los beneficios que tenemos para tí. <Button variant="light" className={classNames(styles.Button, styles.Right)} link to="/planes">Comprar plan premium</Button></>
            )
          )}
        </Headband>
      )}
      {(!isAdministrador() && isPlanPremium()) && (
        <React.Fragment>
          {(!isSuscripcionActiva() && !is_delivered) && (
            <Headband>
              {(styles) => (
                <>Tu suscripción PREMIUM ha expirado, por lo que ahora tu cuenta únicamente tiene los beneficios del plan GRATIS.<br /> Renueva tu suscripción y sigue disfrutando de los beneficios que tenemos para tí. <Button variant="light" className={classNames(styles.Button, styles.Right)} link to="/planes">Renovar suscripción</Button></>
              )}
            </Headband>
          )}
          {!isSuscripcionPagada() && (
            <Headband>
              {(styles) => (
                <>Tu nuevo plan <strong>Premium</strong> está en proceso de acreditación.</>
              )}
            </Headband>
          )}
        </React.Fragment>
      )}
      <div className={styles.Layout}>
        <Navigation />
        <Container>
          <Header title={props.title} />
          <Content>
            <Paragraph className={styles.Legend}>Recuerda guardar tu información<br /> antes de salir de esta sección</Paragraph>
            {props.children}
          </Content>
        </Container>
      </div>
    </React.Fragment>
  );
};

Layout.propTypes = {
  title: PropTypes.string.isRequired
};

export default Layout;
