import React from 'react';
import Alert from 'react-bootstrap/Alert';
import Paragraph from '../UI/Paragraph/Paragraph';
import Button from '../UI/Button/Button';
import IconCamera from '../UI/Icons/IconCamera/IconCamera';
import IconFocus from '../UI/Icons/IconFocus/IconFocus';
import styles from './Camera.module.scss';

const hasGetUserMedia = !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);

const Camera = (props) => {
  const streaming = React.useRef(false);
  const video = React.useRef(null);
  const canvas = React.useRef(null);
  const width = React.useRef(1024);
  const height = React.useRef(0);
  const [isCompatible, setIsCompatible] = React.useState(true);
  const [isCameraError, setIsCameraError] = React.useState(false);

  const captureStream = () => {
    navigator.mediaDevices.getUserMedia({
      video: true,
      audio: false
    }).then((stream) => {
      video.current.srcObject = stream;
      video.current.play();
    }).catch(() => {
      setIsCameraError(true);
    });
  };

  const onCanPlay = () => {
    if (!streaming.current) {
      height.current = video.current.videoHeight / (video.current.videoWidth / width.current);
      streaming.current = true;
    }
  };

  const onTake = () => {
    canvas.current.width = width.current;
    canvas.current.height = height.current;
    const context = canvas.current.getContext('2d');
    context.drawImage(video.current, 0, 0, width.current, height.current);
    canvas.current.toBlob(props.onTakenPicture);
  };

  React.useEffect(() => {
    setIsCompatible(hasGetUserMedia);
    captureStream();
  }, []);

  if (!isCompatible) {
    return (
      <Alert variant="danger">
        <Paragraph>Lo sentimos su navegador no puede tomar fotografías, le sugerimos cambiar a la última versión de Chrome o Firefox.</Paragraph>
      </Alert>
    );
  }

  return (
    <div className={styles.Camera}>
      {isCameraError ? (
        <div className={styles.Error}>
          <IconCamera className={styles.Icon} /> Lo sentimos no podemos acceder a su cámara.
        </div>
      ) : (
        <video ref={video} className={styles.Video} preload="auto" onCanPlay={onCanPlay}>
          <source />
          Lo sentimos su navegador no puede reproducir video, le sugerimos cambiar a la última versión de Chrome o Firefox.
        </video>
      )}
      <canvas ref={canvas} className={styles.Hidden}></canvas>
      <Button
        variant="primary"
        align="center"
        className={styles.Button}
        small
        disabled={isCameraError}
        onClick={onTake}
      >
        <IconFocus height="14px" /> Tomar fotografía
      </Button>
    </div>
  );
};

export default Camera;
