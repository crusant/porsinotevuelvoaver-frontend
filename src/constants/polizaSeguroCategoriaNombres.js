export const polizaSeguroCategoriaNombres = {
  1: 'Seguro de vida',
  2: 'Seguro o fideicomiso educativo',
  3: 'Seguro de accidentes personales',
  4: 'Seguro de casa',
  5: 'Seguro de automóvil',
  6: 'Gastos médicos mayores',
  7: 'Otro'
};
