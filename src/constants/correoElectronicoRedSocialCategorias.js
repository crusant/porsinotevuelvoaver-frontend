export const correoElectronicoRedSocialCategorias = {
  ID: {
    CORREO_ELECTRONICO: 1,
    FACEBOOK: 2,
    YOUTUBE: 3,
    WHATSAPP: 4,
    TWITTER: 5,
    INSTAGRAM: 6,
    TIKTOK: 7,
    OTRO: 8,
  },
  NAME: {
    1: 'Correo',
    2: 'Facebook',
    3: 'YouTube',
    4: 'WhatsApp',
    5: 'Twitter',
    6: 'Instagram',
    7: 'Tik Tok',
    8: 'Otro'
  }
};
