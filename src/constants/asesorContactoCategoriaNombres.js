export const asesorContactoCategoriaNombres = {
  1: 'Abogado',
  2: 'Albacea o notario',
  3: 'Tutor de hijos',
  4: 'Contador',
  5: 'Médico familiar',
  6: 'Guía espiritual',
  7: 'Otro'
};
