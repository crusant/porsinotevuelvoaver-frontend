export const asesorContactoCategoriaIds = {
  ABOGADO: 1,
  ALBACEA_NOTARIO: 2,
  TUTOR_HIJOS: 3,
  CONTADOR: 4,
  MEDICO_FAMILIAR: 5,
  GUIA_ESPIRITUAL: 6,
  OTRO: 7
};
