export const mensajesPostumosTipoDestinatario = {
  ID: {
    CUSTODIO: 1,
    FAMILIAR: 2,
    OTRO: 3
  },
  NAME: {
    1: 'Custodio',
    2: 'Red familiar',
    3: 'Otro'
  }
};
