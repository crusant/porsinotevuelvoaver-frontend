import React, { useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Layout from '../../components/Layout/Layout';
import Table from '../../components/PanelAdministrador/EnlacesExpirados/Table/Table';
import * as actions from '../../actions';

const EnlacesExpirados = () => {
  const preregistros = useSelector((state) => state.preregistros.preregistros);
  const isLoading = useSelector((state) => state.preregistros.isLoading);
  const dispatch = useDispatch();

  const columns = useMemo(() => [{
    Header: 'Nombre completo',
    accessor: 'nombre_completo'
  }, {
    Header: 'Celular',
    accessor: 'celular',
  }, {
    Header: 'Correo electrónico',
    accessor: 'email'
  }, {
    Header: 'Fecha de registro',
    Cell: ({ row }) => {
      return moment(row.original.created_at).format('DD [de] MMMM [de] YYYY hh:mm:ss');
    }
  }, {
    Header: 'Fecha de expiración',
    Cell: ({ row }) => {
      return moment(row.original.fecha_expiracion).format('DD [de] MMMM [de] YYYY hh:mm:ss');
    }
  }], []);

  useEffect(() => {
    dispatch(actions.getPreregistrosExpirados());

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Enlaces Expirados">
      <Table
        columns={columns}
        data={preregistros}
        isLoading={isLoading}
      />
    </Layout>
  );
};

export default EnlacesExpirados;
