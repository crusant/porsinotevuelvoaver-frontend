import React from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import isEmpty from 'lodash/isEmpty';
import Loader from '../../components/UI/Loader/Loader';
import PruebaVidaInvalida from '../../components/Messages/PruebaVidaInvalida/PruebaVidaInvalida';
import PruebaVidaConfirmada from '../../components/Messages/PruebaVidaConfirmada/PruebaVidaConfirmada';
import DatosAccesoEntregados from '../../components/Messages/DatosAccesoEntregados/DatosAccesoEntregados';
import PruebaVidaExpirada from '../../components/Messages/PruebaVidaExpirada/PruebaVidaExpirada';
import ConfirmacionDatosAcceso from '../../components/Messages/ConfirmacionDatosAcceso/ConfirmacionDatosAcceso';
import * as actions from '../../actions';

const EntregandoDatosAcceso = () => {
  const pruebaVida = useSelector((state) => state.custodios.pruebaVida);
  const isEntregando = useSelector((state) => state.custodios.isEntregando);
  const dispatch = useDispatch();
  const { url } = useParams();

  const getPrimerNombre = (nombreCompleto) => {
    return String(nombreCompleto)
      .split(' ')
      .shift();
  };

  const getUserNombreCompleto = () => {
    return pruebaVida?.user?.perfil?.nombre_completo;
  };

  const getFechaRespuesta = () => {
    return moment(pruebaVida?.fecha_respuesta).format('DD [de] MMMM [de] YYYY [a las] HH:mm');
  };

  const getFechaLimite = () => {
    return moment(pruebaVida?.fecha_limite, 'YYYY-MM-DD').format('LL');
  };

  React.useEffect(() => {
    dispatch(actions.entregarDatosAccesoCustodio(url));

    // eslint-disable-next-line
  }, []);

  if (isEntregando) {
    return <Loader title="Enviando datos de acceso" />;
  }

  if (isEmpty(pruebaVida)) {
    return <PruebaVidaInvalida custodio />
  }

  if (pruebaVida?.is_confirmed) {
    return (
      <PruebaVidaConfirmada
        custodio
        primerNombre={getPrimerNombre(getUserNombreCompleto())}
        fechaRespuesta={getFechaRespuesta()}
      />
    );
  }

  if (!pruebaVida?.is_delivery && pruebaVida?.user.is_delivered) {
    return (
      <DatosAccesoEntregados
        primerNombre={getPrimerNombre(getUserNombreCompleto())}
      />
    );
  }

  if (pruebaVida?.is_expired) {
    return (
      <PruebaVidaExpirada
        custodio
        primerNombre={getPrimerNombre(getUserNombreCompleto())}
        fechaLimite={getFechaLimite()}
      />
    );
  }

  return (
    <ConfirmacionDatosAcceso
      primerNombre={getPrimerNombre(getUserNombreCompleto())}
    />
  );
};

export default EntregandoDatosAcceso;
