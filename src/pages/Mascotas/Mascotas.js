import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Feedback from '../../components/UI/Feedback/Feedback';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormHelpText from '../../components/UI/Form/FormHelpText/FormHelpText';
import FormText from '../../components/UI/Form/FormText/FormText';
import FormInput from '../../components/UI/Form/FormInput/FormInput';
import FormFile from '../../components/UI/Form/FormFile/FormFile';
import File from '../../components/UI/File/File';
import FormSeparator from '../../components/UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../components/UI/Form/FormTitle/FormTitle';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import FormMargin from '../../components/UI/Form/FormMargin/FormMargin';
import FormTable from '../../components/UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../components/UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../components/UI/Button/Button';
import IconDownload from '../../components/UI/Icons/IconDownload/IconDownload';
import * as actions from '../../actions/mascotas';
import * as helpers from '../../helpers';

const Mascotas = () => {
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const [columnIndex, setColumnIndex] = React.useState(null);
  const user = useSelector((state) => state.auth.user);
  const mascotas = useSelector((state) => state.mascotas.mascotas);
  const mascota = useSelector((state) => state.mascotas.mascota);
  const isLoading = useSelector((state) => state.mascotas.isLoading);
  const isSaving = useSelector((state) => state.mascotas.isSaving);
  const isUpdating = useSelector((state) => state.mascotas.isUpdating);
  const isDownloading = useSelector((state) => state.mascotas.isDownloading);
  const errors = useSelector((state) => state.mascotas.errors);
  const dispatch = useDispatch();

  const hasMascotas = (mascotas.length > 0);

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [mascota] = mascotas.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    if (isLoading) {
      return 'Cargando...';
    }

    const updatedAt = mascota?.updated_at;

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const onChangeMascotaInput = (event) => {
    const [, attribute] = event.target.name.split('.');

    dispatch(actions.changeMascotaInput(attribute, event.target.value));
  };

  const onChangeCartillaVacunacionDigitalInput = (value) => {
    dispatch(actions.changeMascotaInput('cartilla_vacunacion_digital', value));
  };

  const onRemoveCartillaVacunacionDigitalInput = () => {
    dispatch(actions.changeMascotaInput('cartilla_vacunacion_digital', ''));
  };

  const onResetMascotaInputs = () => {
    dispatch(actions.resetMascotaInputs());
  };

  const onEditMascota = (mascota) => {
    window.scrollTo(0, 0);
    dispatch(actions.editMascota(mascota));
  };

  const onSaveOrUpdateMascota = async (event) => {
    event.preventDefault();

    setShowToast(false);
    setToastMessage('');

    const formData = new FormData();
    helpers.buildFormData(formData, mascota);

    if (!mascota.id && await dispatch(actions.saveMascota(formData))) {
      setToastMessage('Los datos de la mascota se guardaron con éxito.');
      setShowToast(true);
    }

    if (mascota.id && await dispatch(actions.updateMascota(formData))) {
      setToastMessage('Los datos de la mascota se actualizaron con éxito.');
      setShowToast(true);
    }
  };

  const onDeleteMascota = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar esta mascota?');
    setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteMascota(id))) {
      setToastMessage('Los datos de la mascota se eliminaron con éxito.');
      setShowToast(true);
    }
  };

  const onDownloadCartillaVacunacion = (mascota, index) => {
    const extension = String(mascota.cartilla_vacunacion_digital).split('.').pop();

    setColumnIndex(index);
    dispatch(actions.downloadMascotaDocumento(mascota.id, extension));
  };

  const onResetMascotasErrors = () => {
    dispatch(actions.resetMascotasErrors());
  };

  React.useEffect(() => {
    if (!helpers.isNull(errors)) {
      window.scrollTo(0, 0);
    }
  }, [errors]);

  React.useEffect(() => {
    dispatch(actions.getMascotas());

    return () => {
      onResetMascotaInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Mascotas">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Mascotas</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetMascotasErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onResetMascotasErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {helpers.isObject(errors) && Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <Paragraph title>Las mascotas brindan compañía y amor incondicional.<br /> En este apartado podrás registrar tus deseos e instrucciones con respecto a ellas. Puedes agregar algunas fotografías de tus mascotas en la sección "Documentos digitales y ubicación física".</Paragraph>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateMascota}>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="mascota.nombre">Nombre*</FormLabel>
                <FormInput
                  type="text"
                  name="mascota.nombre"
                  id="mascota.nombre"
                  required
                  maxLength={100}
                  onChange={onChangeMascotaInput}
                  value={mascota.nombre}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left strict>
                <FormLabel htmlFor="mascota.especie">Especie*</FormLabel>
                <FormInput
                  type="text"
                  name="mascota.especie"
                  id="mascota.especie"
                  required
                  maxLength={100}
                  onChange={onChangeMascotaInput}
                  value={mascota.especie}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="mascota.cartilla_vacunacion_digital">Cartilla de vacunación u otro documento digital <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF, Word, PowerPoint, Excel, .jpg o .png.</FormHelpText>
                {helpers.isEmptyString(mascota.cartilla_vacunacion_digital) ? (
                  <FormFile accept=".pdf,.doc,.docx,.ppt,.pptx,.xsl,.xlsx,.jpg,.jpeg,.png" onChange={onChangeCartillaVacunacionDigitalInput} />
                ) : (
                  <File
                    file={mascota.cartilla_vacunacion_digital}
                    onRemove={onRemoveCartillaVacunacionDigitalInput}
                  />
                )}
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="mascota.cartilla_vacunacion_fisica">Donde encontrar la cartilla de vacunación u otro documento <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="mascota.cartilla_vacunacion_fisica"
                  id="mascota.cartilla_vacunacion_fisica"
                  maxLength={500}
                  onChange={onChangeMascotaInput}
                  value={mascota.cartilla_vacunacion_fisica}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="mascota.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="mascota.indicaciones"
                  id="mascota.indicaciones"
                  rows={3}
                  maxLength={500}
                  onChange={onChangeMascotaInput}
                  value={mascota.indicaciones}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormTitle>Datos del veterinario</FormTitle>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="mascota.veterinario">Nombre completo <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="mascota.veterinario"
                  id="mascota.veterinario"
                  maxLength={100}
                  onChange={onChangeMascotaInput}
                  value={mascota.veterinario}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="mascota.telefono">Teléfono <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="tel"
                  name="mascota.telefono"
                  id="mascota.telefono"
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeMascotaInput}
                  value={mascota.telefono}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetMascotaInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!mascota.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormToast
            variant="success"
            show={showToast}
            title="Mensaje de éxito"
            message={toastMessage}
            onClose={() => setShowToast(false)}
          />
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th rowSpan={2}>Nombre</th>
            <th rowSpan={2}>Especie</th>
            <th rowSpan={2}>Cartilla de vacunación u otro documento digital</th>
            <th rowSpan={2}>Donde encontrar la cartilla de vacunación u otro documento</th>
            <th rowSpan={2}>Instrucciones</th>
            <th colSpan={2}>Datos del veterinario</th>
            {canWrite() && (<th rowSpan={2}></th>)}
          </tr>
          <tr>
            <th>Nombre completo</th>
            <th>Teléfono</th>
          </tr>
        </thead>
        <tbody>
          {isLoading ? (
            <tr>
              <td colSpan={8}>Cargando...</td>
            </tr>
          ) : hasMascotas ? mascotas.map((mascota, index) => (
            <tr key={index}>
              <td>{mascota.nombre}</td>
              <td>{mascota.especie}</td>
              <td>
                {!helpers.isEmptyString(mascota.cartilla_vacunacion_digital) && (
                  <Button
                    variant="primary"
                    small
                    disabled={isDownloading && (columnIndex === index)}
                    onClick={() => onDownloadCartillaVacunacion(mascota, index)}
                  >
                    <IconDownload height="14px" /> {isDownloading && (columnIndex === index) ? 'Descargando...' : 'Descargar'}
                  </Button>
                )}
              </td>
              <td>{mascota.cartilla_vacunacion_fisica}</td>
              <td>{mascota.indicaciones}</td>
              <td>{mascota.veterinario}</td>
              <td>{mascota.telefono}</td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="secondary"
                      small
                      onClick={() => onEditMascota(mascota)}
                    >
                      Editar
                    </Button>
                    <Button
                      variant="danger"
                      small
                      onClick={() => onDeleteMascota(mascota.id)}
                    >
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={8}>
                {canWrite() ? 'No has agregado ninguna mascota.' : 'No se agregó ninguna mascota.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </Layout>
  );
};

export default Mascotas;
