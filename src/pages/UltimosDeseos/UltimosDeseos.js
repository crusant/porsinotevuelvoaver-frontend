import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Feedback from '../../components/UI/Feedback/Feedback';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import DatosTestamento from '../../components/PanelUsuario/UltimosDeseos/DatosTestamento/DatosTestamento';
import DonacionOrganos from '../../components/PanelUsuario/UltimosDeseos/DonacionOrganos/DonacionOrganos';
import VidaArtificial from '../../components/PanelUsuario/UltimosDeseos/VidaArtificial/VidaArtificial';
import SecuestroDesaparicion from '../../components/PanelUsuario/UltimosDeseos/SecuestroDesaparicion/SecuestroDesaparicion';
import Restos from '../../components/PanelUsuario/UltimosDeseos/Restos/Restos';
import * as datosTestamentoActions from '../../actions/ultimos-deseos/datos-testamento';
import * as donacionOrganosActions from '../../actions/ultimos-deseos/donacion-organos';
import * as vidaArtificialActions from '../../actions/ultimos-deseos/vida-artificial';
import * as secuestroDesaparicionActions from '../../actions/ultimos-deseos/secuestro-desaparicion';
import * as restosActions from '../../actions/ultimos-deseos/restos';
import * as helpers from '../../helpers';

const UltimosDeseos = () => {
  const [activeKey, setActiveKey] = React.useState('0');
  const [updatedAt, setUpdatedAt] = React.useState(null);
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const user = useSelector((state) => state.auth.user);

  const isLoading = useSelector((state) => {
    return state.datosTestamento.isLoading
        || state.donacionOrganos.isLoading
        || state.vidaArtificial.isLoading
        || state.secuestroDesaparicion.isLoading
        || state.restos.isLoading;
  });

  const errors = useSelector((state) => {
    return state.datosTestamento.errors
        || state.donacionOrganos.errors
        || state.vidaArtificial.errors
        || state.secuestroDesaparicion.errors
        || state.restos.errors;
  });

  const dispatch = useDispatch();

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    if (isLoading) {
      return 'Cargando...';
    }

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const onSelectKey = (key) =>
    setActiveKey(key);

  const onHideErrors = () => {
    dispatch(datosTestamentoActions.resetDatosTestamentoErrors());
    dispatch(donacionOrganosActions.resetErrors());
    dispatch(vidaArtificialActions.resetErrors());
    dispatch(secuestroDesaparicionActions.resetErrors());
    dispatch(restosActions.resetRestosErrors());
  };

  React.useEffect(() => {
    if (!helpers.isNull(errors)) {
      window.scrollTo(0, 0);
    }
  }, [errors]);

  return (
    <Layout title="Deseos póstumos">
      <StepProgressBar activeKey={activeKey}>
        <StepProgressBar.Step eventKey="0" onSelect={onSelectKey}>1. Datos del testamento</StepProgressBar.Step>
        <StepProgressBar.Step eventKey="1" onSelect={onSelectKey}>2. Donación de órganos</StepProgressBar.Step>
        <StepProgressBar.Step eventKey="2" onSelect={onSelectKey}>3. Vida artificial</StepProgressBar.Step>
        <StepProgressBar.Step eventKey="3" onSelect={onSelectKey}>4. Secuestro o desaparición</StepProgressBar.Step>
        <StepProgressBar.Step eventKey="4" onSelect={onSelectKey}>5. Restos</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert
          variant="danger"
          dismissible
          onClose={onHideErrors}
        >
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert
          variant="danger"
          dismissible
          onClose={onHideErrors}
        >
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>{error}</Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      {(activeKey === '0') && (
        <DatosTestamento
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(activeKey === '1') && (
        <DonacionOrganos
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(activeKey === '2') && (
        <VidaArtificial
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(activeKey === '3') && (
        <SecuestroDesaparicion
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(activeKey === '4') && (
        <Restos
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      <FormToast
        variant="success"
        show={showToast}
        title="Mensaje de éxito"
        message={toastMessage}
        onClose={() => setShowToast(false)}
      />
    </Layout>
  );
};

export default UltimosDeseos;
