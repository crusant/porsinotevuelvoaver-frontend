import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Feedback from '../../components/UI/Feedback/Feedback';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormHelpText from '../../components/UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../components/UI/Form/FormInput/FormInput';
import FormFile from '../../components/UI/Form/FormFile/FormFile';
import File from '../../components/UI/File/File';
import Button from '../../components/UI/Button/Button';
import * as helpers from '../../helpers';
import * as actions from '../../actions';

const ActualizarAvisoPrivacidad = () => {
  const avisoPrivacidad = useSelector((state) => state.avisoPrivacidad.avisoPrivacidad);
  const isUpdating = useSelector((state) => state.avisoPrivacidad.isUpdating);
  const errors = useSelector((state) => state.avisoPrivacidad.errors);
  const dispatch = useDispatch();

  const onChangeAvisoPrivacidadInput = (event) => {
    const input = event.target;

    dispatch(actions.changeAvisoPrivacidadInputs(input.name, input.value));
  };

  const onChangeDocumentoDigitalInput = (value) => {
    dispatch(actions.changeAvisoPrivacidadInputs('documento_digital', value));
  };

  const onRemoveDocumentoDigitalInput = () => {
    dispatch(actions.changeAvisoPrivacidadInputs('documento_digital', ''));
  };

  const onResetAvisoPrivacidadErrors = () => {
    dispatch(actions.resetAvisoPrivacidadErrors());
  };

  const onUpdateAvisoPrivacidad = (event) => {
    event.preventDefault();

    const formData = new FormData();
    helpers.buildFormData(formData, avisoPrivacidad);
    dispatch(actions.updateAvisoPrivacidad(formData));
  };

  React.useEffect(() => {
    dispatch(actions.getAvisoPrivacidad());

    return () => {
      dispatch(actions.resetAvisoPrivacidadToStartup());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Actualizar aviso de privacidad">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Actualizar aviso de privacidad</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>*Campos obligatorios</Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetAvisoPrivacidadErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onResetAvisoPrivacidadErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <form onSubmit={onUpdateAvisoPrivacidad}>
        <FormRow>
          <FormColumn>
            <FormLabel htmlFor="nombre">Nombre*</FormLabel>
            <FormInput
              type="text"
              name="nombre"
              id="nombre"
              required
              maxLength={100}
              onChange={onChangeAvisoPrivacidadInput}
              value={avisoPrivacidad.nombre}
            />
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            <FormLabel htmlFor="documento_digital">Archivo*</FormLabel>
            <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF.</FormHelpText>
            {helpers.isEmptyString(avisoPrivacidad.documento_digital) ? (
              <FormFile onChange={onChangeDocumentoDigitalInput} />
            ) : (
              <File
                file={avisoPrivacidad.documento_digital}
                onRemove={onRemoveDocumentoDigitalInput}
              />
            )}
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            <Button
              type="submit"
              variant="primary"
              align="right"
              disabled={isUpdating}
            >
              {isUpdating ? 'Actualizando...' : 'Actualizar'}
            </Button>
          </FormColumn>
        </FormRow>
      </form>
    </Layout>
  );
};

export default ActualizarAvisoPrivacidad;
