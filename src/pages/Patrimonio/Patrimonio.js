import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Feedback from '../../components/UI/Feedback/Feedback';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormCategories from '../../components/UI/Form/FormCategories/FormCategories';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import IconPayment from '../../components/UI/Icons/IconPayment/IconPayment';
import IconDiploma from '../../components/UI/Icons/IconDiploma/IconDiploma';
import IconPay from '../../components/UI/Icons/IconPay/IconPay';
import IconCar from '../../components/UI/Icons/IconCar/IconCar';
import IconHome from '../../components/UI/Icons/IconHome/IconHome';
import IconBars from '../../components/UI/Icons/IconBars/IconBars';
import IconHeart from '../../components/UI/Icons/IconHeart/IconHeart';
import Inversiones from '../../components/PanelUsuario/Patrimonio/Inversiones/Inversiones';
import Adeudos from '../../components/PanelUsuario/Patrimonio/Adeudos/Adeudos';
import Vehiculos from '../../components/PanelUsuario/Patrimonio/Vehiculos/Vehiculos';
import BienesInmuebles from '../../components/PanelUsuario/Patrimonio/BienesInmuebles/BienesInmuebles';
import AccionesNegocios from '../../components/PanelUsuario/Patrimonio/AccionesNegocios/AccionesNegocios';
import BienesValoresSentimentales from '../../components/PanelUsuario/Patrimonio/BienesValoresSentimentales/BienesValoresSentimentales';
import OtrosPatrimonios from '../../components/PanelUsuario/Patrimonio/OtrosPatrimonios/OtrosPatrimonios';
import * as inversionesActions from '../../actions/patrimonio/inversiones';
import * as adeudosActions from '../../actions/patrimonio/adeudos';
import * as vehiculosActions from '../../actions/patrimonio/vehiculos';
import * as bienesInmueblesActions from '../../actions/patrimonio/bienes-inmuebles';
import * as accionesNegociosActions from '../../actions/patrimonio/acciones-negocios';
import * as bienesValoresSentimentalesActions from '../../actions/patrimonio/bienes-valores-sentimentales';
import * as otrosPatrimoniosActions from '../../actions/patrimonio/otros-patrimonios';
import * as helpers from '../../helpers';
import * as constants from '../../constants';

const Patrimonio = () => {
  const [categoria, setCategoria] = React.useState(1);
  const [updatedAt, setUpdatedAt] = React.useState(null);
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const user = useSelector((state) => state.auth.user);

  const isLoading = useSelector((state) => {
    return state.inversiones.isGetting
        || state.adeudos.isGetting
        || state.vehiculos.isGetting
        || state.bienesInmuebles.isGetting
        || state.accionesNegocios.isGetting
        || state.bienesValoresSentimentales.isGetting
        || state.otrosPatrimonios.isGetting;
  });

  const errors = useSelector((state) => {
    return state.inversiones.errors
        || state.adeudos.errors
        || state.vehiculos.errors
        || state.bienesInmuebles.errors
        || state.accionesNegocios.errors
        || state.bienesValoresSentimentales.errors
        || state.otrosPatrimonios.errors;
  });

  const dispatch = useDispatch();

  const categorias = constants.patrimonioCategorias;

  const canWrite = () =>
    user?.can_write;

    const getUpdatedAt = () => {
      if (isLoading) {
        return 'Cargando...';
      }

      return updatedAt ? moment(updatedAt).format('LL') : '';
    };

  const onChangeCategoriaInput = (event) => {
    const categoria = parseInt(event.target.value, 10);

    setCategoria(categoria);
  };

  const onHideErrors = () => {
    dispatch(inversionesActions.resetErrors());
    dispatch(adeudosActions.resetErrors());
    dispatch(vehiculosActions.resetErrors());
    dispatch(bienesInmueblesActions.resetErrors());
    dispatch(accionesNegociosActions.resetErrors());
    dispatch(bienesValoresSentimentalesActions.resetErrors());
    dispatch(otrosPatrimoniosActions.resetErrors());
  };

  React.useEffect(() => {
    if (!helpers.isNull(errors)) {
      window.scrollTo(0, 0);
    }
  }, [errors]);

  return (
    <Layout title="Patrimonio">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Patrimonio</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onHideErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onHideErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>{error}</Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <Paragraph title>Esta sección permite llevar a cabo un inventario de tu patrimonio.<br /> Este inventario te permitirá dar a conocer cuáles son tus deseos con cada uno de los bienes que conforman tu patrimonio.<br /> En caso de requerir anexar documentos adicionales, puedes hacerlo en la sección "Documentos digitales y ubicación física".</Paragraph>
      <FormRow>
        <FormColumn>
          <FormLabel center htmlFor="categoria_inversiones">Selecciona una categoría</FormLabel>
          <FormCategories>
            <FormCategories.Category
              name="categoria"
              id="categoria_inversiones"
              checked={(categoria === categorias.ID.INVERSIONES)}
              onChange={onChangeCategoriaInput}
              label={categorias.NAME[categorias.ID.INVERSIONES]}
              value={categorias.ID.INVERSIONES}
            >
              {(styles) => <IconDiploma className={styles.Icon} height="52px" />}
            </FormCategories.Category>
            <FormCategories.Category
              name="categoria"
              id="categoria_adeudos"
              checked={(categoria === categorias.ID.ADEUDOS)}
              onChange={onChangeCategoriaInput}
              label={categorias.NAME[categorias.ID.ADEUDOS]}
              value={categorias.ID.ADEUDOS}
            >
              {(styles) => <IconPay className={styles.Icon} height="52px" />}
            </FormCategories.Category>
            <FormCategories.Category
              name="categoria"
              id="categoria_vehiculos"
              checked={(categoria === categorias.ID.VEHICULOS)}
              onChange={onChangeCategoriaInput}
              label={categorias.NAME[categorias.ID.VEHICULOS]}
              value={categorias.ID.VEHICULOS}
            >
              {(styles) => <IconCar className={styles.Icon} height="52px" />}
            </FormCategories.Category>
            <FormCategories.Category
              name="categoria"
              id="categoria_bienes_inmuebles"
              checked={(categoria === categorias.ID.BIENES_INMUEBLES)}
              onChange={onChangeCategoriaInput}
              label={categorias.NAME[categorias.ID.BIENES_INMUEBLES]}
              value={categorias.ID.BIENES_INMUEBLES}
            >
              {(styles) => <IconHome className={styles.Icon} height="52px" />}
            </FormCategories.Category>
            <FormCategories.Category
              name="categoria"
              id="categoria_acciones_negocios"
              checked={(categoria === categorias.ID.ACCIONES_NEGOCIOS)}
              onChange={onChangeCategoriaInput}
              label={categorias.NAME[categorias.ID.ACCIONES_NEGOCIOS]}
              value={categorias.ID.ACCIONES_NEGOCIOS}
            >
              {(styles) => <IconBars className={styles.Icon} height="52px" />}
            </FormCategories.Category>
            <FormCategories.Category
              name="categoria"
              id="categoria_bienes_valor_sentimental"
              checked={(categoria === categorias.ID.BIENES_VALOR_SENTIMENTAL)}
              onChange={onChangeCategoriaInput}
              label={categorias.NAME[categorias.ID.BIENES_VALOR_SENTIMENTAL]}
              value={categorias.ID.BIENES_VALOR_SENTIMENTAL}
            >
              {(styles) => <IconHeart className={styles.Icon} height="52px" />}
            </FormCategories.Category>
            <FormCategories.Category
              name="categoria"
              id="categoria_otro"
              checked={(categoria === categorias.ID.OTROS_PATRIMONIOS)}
              onChange={onChangeCategoriaInput}
              label={categorias.NAME[categorias.ID.OTROS_PATRIMONIOS]}
              value={categorias.ID.OTROS_PATRIMONIOS}
            >
              {(styles) => <IconPayment className={styles.Icon} height="52px" />}
            </FormCategories.Category>
          </FormCategories>
        </FormColumn>
      </FormRow>
      {(categoria === categorias.ID.INVERSIONES) && (
        <Inversiones
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(categoria === categorias.ID.ADEUDOS) && (
        <Adeudos
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(categoria === categorias.ID.VEHICULOS) && (
        <Vehiculos
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(categoria === categorias.ID.BIENES_INMUEBLES) && (
        <BienesInmuebles
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(categoria === categorias.ID.ACCIONES_NEGOCIOS) && (
        <AccionesNegocios
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(categoria === categorias.ID.BIENES_VALOR_SENTIMENTAL) && (
        <BienesValoresSentimentales
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(categoria === categorias.ID.OTROS_PATRIMONIOS) && (
        <OtrosPatrimonios
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      <FormToast
        variant="success"
        show={showToast}
        title="Mensaje de éxito"
        message={toastMessage}
        onClose={() => setShowToast(false)}
      />
    </Layout>
  );
};

export default Patrimonio;
