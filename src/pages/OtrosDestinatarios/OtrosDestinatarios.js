import React from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/UI/Loader/Loader';
import MensajePostumo from '../../components/Familiar/MensajePostumo/MensajePostumo';
import * as helpers from '../../helpers';
import * as actions from '../../actions';
import styles from './OtrosDestinatarios.module.scss';
import logo from '../../assets/images/logotipo-blanco.png';

const OtrosDestinatarios = () => {
  const { url } = useParams();
  const mensajePostumo = useSelector((state) => state.mensajesPostumos.mensajePostumo);
  const isGettingMensajePostumo = useSelector((state) => state.mensajesPostumos.isGetting);
  const isDownloadingAudio = useSelector((state) => state.mensajesPostumos.isDownloadingAudio);
  const isDownloadingVideo = useSelector((state) => state.mensajesPostumos.isDownloadingVideo);
  const avisoPrivacidad = useSelector((state) => state.avisoPrivacidad.avisoPrivacidad);
  const isGettingAvisoPrivacidad = useSelector((state) => state.avisoPrivacidad.isGetting);
  const terminosCondiciones = useSelector((state) => state.terminosCondiciones.terminosCondiciones);
  const isGettingTerminosCondiciones = useSelector((state) => state.terminosCondiciones.isGetting);
  const dispatch = useDispatch();

  const isGetting = () => {
    return isGettingMensajePostumo
        || isGettingAvisoPrivacidad
        || isGettingTerminosCondiciones;
  };

  const hasMensajePostumo = () => {
    const fotografia = _.get(mensajePostumo, 'fotografia', '');
    const mensaje = _.get(mensajePostumo, 'mensaje', '');
    const audio = _.get(mensajePostumo, 'audio', '');
    const video = _.get(mensajePostumo, 'video', '');

    return !helpers.isEmptyString(fotografia)
        || !helpers.isEmptyString(mensaje)
        || !helpers.isEmptyString(audio)
        || !helpers.isEmptyString(video);
  };

  const onDownloadMensajePostumoAudio = (id, defaultExtension) => {
    const audio = _.get(mensajePostumo, 'audio', '');
    const extension = defaultExtension || audio.split('.').pop();

    dispatch(actions.downloadMensajePostumoAudioByUrl(url, extension));
  };

  const onDownloadMensajePostumoVideo = (id) => {
    const video = _.get(mensajePostumo, 'video', '');
    const extension = video.split('.').pop();

    dispatch(actions.downloadMensajePostumoVideoByUrl(url, extension));
  };

  React.useEffect(() => {
    (async () => {
      await dispatch(actions.getMensajePostumo(url));
      await dispatch(actions.getAvisoPrivacidad());
      await dispatch(actions.getTerminosCondiciones());
    })();

    // eslint-disable-next-line
  }, []);

  if (isGetting()) {
    return <Loader title="Cargando el legado de amor y seguridad para tus seres queridos" />;
  }

  return (
    <div className={styles.MensajePostumo}>
      {hasMensajePostumo() && (
        <MensajePostumo
          mensajePostumo={mensajePostumo}
          familiar={mensajePostumo.destinatario}
          user={_.get(mensajePostumo, 'user', {})}
          isDownloadingAudio={isDownloadingAudio}
          isDownloadingVideo={isDownloadingVideo}
          showButton
          url={url}
          onDownloadAudioInMp3={onDownloadMensajePostumoAudio}
          onDownloadAudioInStandard={onDownloadMensajePostumoAudio}
          onDownloadVideo={onDownloadMensajePostumoVideo}
        />
      )}
      <div className={styles.LogoContainer}>
        <img src={logo} className={styles.Logo} alt="Logotipo en blanco de Por si no te vuelvo a ver" />
      </div>
      <div className={styles.Copyright}>&#169;2020 Por si no te vuelvo a ver. Todos los derechos reservados. <a href={avisoPrivacidad.documento_digital} className={styles.Link} rel="noopener noreferrer" target="_blank">{avisoPrivacidad.nombre}</a>. <a href={terminosCondiciones.documento_digital} className={styles.Link} rel="noopener noreferrer" target="_blank">{terminosCondiciones.nombre}</a>.</div>
    </div>
  );
};

export default OtrosDestinatarios;
