import React from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/UI/Loader/Loader';
import MessageConfirmacionPruebaVidaByUsuario from '../../components/Messages/MessageConfirmacionPruebaVidaByUsuario/MessageConfirmacionPruebaVidaByUsuario';
import MessagePruebaVidaConfirmadaByUsuario from '../../components/Messages/MessagePruebaVidaConfirmadaByUsuario/MessagePruebaVidaConfirmadaByUsuario';
import MessagePruebaVidaCaducadaByUsuario from '../../components/Messages/MessagePruebaVidaCaducadaByUsuario/MessagePruebaVidaCaducadaByUsuario';
import MessagePruebaVidaInvalidaByUsuario from '../../components/Messages/MessagePruebaVidaInvalidaByUsuario/MessagePruebaVidaInvalidaByUsuario';
import * as helpers from '../../helpers';
import * as actions from '../../actions/configuracion-envio-pruebas-vida';

const ConfirmarPruebaVidaUsuario = () => {
  const { url } = useParams();
  const pruebaVida = useSelector((state) => state.configuracionEnvioPruebasVida.pruebaVidaUsuario);
  const isConfirming = useSelector((state) => state.configuracionEnvioPruebasVida.isConfirming);
  const errors = useSelector((state) => state.configuracionEnvioPruebasVida.errors);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(actions.confirmarPruebaVidaUsuario(url));

    // eslint-disable-next-line
  }, []);

  if (isConfirming) {
    return (
      <Loader title="Confirmando la prueba de vida" />
    );
  }

  if (helpers.isString(errors) && !_.isEmpty(pruebaVida.fecha_respuesta)) {
    return (
      <MessagePruebaVidaConfirmadaByUsuario pruebaVida={pruebaVida} />
    );
  }

  if (helpers.isString(errors) && pruebaVida.is_expired) {
    return (
      <MessagePruebaVidaCaducadaByUsuario pruebaVida={pruebaVida} />
    );
  }

  if (helpers.isString(errors)) {
    return (
      <MessagePruebaVidaInvalidaByUsuario />
    );
  }

  return !_.isNull(pruebaVida.fecha_respuesta) && (
    <MessageConfirmacionPruebaVidaByUsuario pruebaVida={pruebaVida} />
  );
};

export default ConfirmarPruebaVidaUsuario;
