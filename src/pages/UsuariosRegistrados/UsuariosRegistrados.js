import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import _ from 'lodash';
import Alert from 'react-bootstrap/Alert';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Layout from '../../components/Layout/Layout';
import Table from '../../components/UI/Table/Table';
import FormTable from '../../components/UI/Form/FormTable/FormTable';
import Button from '../../components/UI/Button/Button';
import EstatusPruebaVidaUsuarioAndCustodio from '../../components/PanelAdministrador/EstatusPruebaVidaUsuarioAndCustodio/EstatusPruebaVidaUsuarioAndCustodio';
import EstatusPruebaVidaUsuario from '../../components/PanelAdministrador/EstatusPruebaVidaUsuario/EstatusPruebaVidaUsuario';
import EstatusPruebaVidaCustodio from '../../components/PanelAdministrador/EstatusPruebaVidaCustodio/EstatusPruebaVidaCustodio';
import IconMinus from '../../components/UI/Icons/IconMinus/IconMinus';
import IconPlus from '../../components/UI/Icons/IconPlus/IconPlus';
import * as actions from '../../actions/usuarios-registrados';
import * as helpers from '../../helpers';

const UsuariosRegistrados = () => {
  const [columnIndex, setColumnIndex] = React.useState(null);
  const users = useSelector((state) => state.usuariosRegistrados.users);
  const isGetting = useSelector((state) => state.usuariosRegistrados.isGetting);
  const isActivating = useSelector((state) => state.usuariosRegistrados.isActivating);
  const isDelivering = useSelector((state) => state.usuariosRegistrados.isDelivering);
  const isConfirming = useSelector((state) => state.usuariosRegistrados.isConfirming);
  const errors = useSelector((state) => state.usuariosRegistrados.errors);
  const dispatch = useDispatch();

  const columns = React.useMemo(() => [
    {
      Header: 'Nombre completo',
      accessor: 'perfil.nombre_completo'
    }, {
      Header: 'Correo electrónico',
      accessor: 'email'
    }, {
      Header: 'Celular',
      accessor: 'perfil.celular'
    }, {
      Header: 'Plan',
      accessor: 'suscripcion.plan.nombre'
    }, {
      Header: 'Método de pago',
      accessor: 'suscripcion.metodo_pago'
    }, {
      Header: 'Fecha de alta',
      Cell: ({ row }) => {
        return moment(row.original.suscripcion.fecha_inicio).format('DD MMM YYYY');
      }
    }, {
      Header: 'Fecha de vencimiento',
      Cell: ({ row }) => {
        return moment(row.original.suscripcion.fecha_terminacion).format('DD MMM YYYY');
      }
    }, {
      Header: 'Estatus',
      accessor: 'prueba_vida_usuario.status'
    }, {
      Header: () => null,
      id: 'expander',
      Cell: ({ row }) => {
        const metodoPago = _.get(row.original, 'suscripcion.metodo_pago', '');
        const isActive = (_.get(row.original, 'is_active', false) && _.get(row.original, 'suscripcion.is_pagado', false));
        const pruebaVidaCustodio = _.get(row.original, 'prueba_vida_usuario.prueba_vida_custodio', null);

        return (
          <React.Fragment>
            <ButtonGroup>
              <Button
                variant="primary"
                small
                disabled={isDelivering && (row.index === columnIndex)}
                onClick={() => onEntregarDatosAcceso(row.original, row.index)}
              >
                {isDelivering && (row.index === columnIndex) ? 'Entregando...' : 'Entregar'}
              </Button>
              {(metodoPago === 'OXXO' && !isActive) && (
                <Button
                  variant="secondary"
                  small
                  disabled={isActivating && (row.index === columnIndex)}
                  onClick={() => onActivarUsuarioRegistrado(row.original, row.index)}
                >
                  {isActivating && (row.index === columnIndex) ? 'Activando...' : 'Activar suscripción'}
                </Button>
              )}
              {(pruebaVidaCustodio
                && _.isNull(pruebaVidaCustodio.fecha_respuesta)
                && pruebaVidaCustodio.is_expired
              ) && (
                <Button
                  variant="secondary"
                  small
                  disabled={isConfirming && (row.index === columnIndex)}
                  onClick={() => onConfirmarPruebaVida(row.original, row.index)}
                >
                  {isConfirming && (row.index === columnIndex) ? 'Confirmando...' : 'Confirmar la prueba de vida'}
                </Button>
              )}
            </ButtonGroup>
            <span {...row.getToggleRowExpandedProps()}>
              &nbsp;&nbsp;&nbsp;
              <EstatusPruebaVidaUsuarioAndCustodio
                pruebaVidaUsuario={row.original.prueba_vida_usuario}
              />
              {row.isExpanded ? (
                <IconMinus fill="#C7D2DC" width="21px" />
              ) : (
                <IconPlus fill="#C7D2DC" width="21px" />
              )}
            </span>
          </React.Fragment>
        );
      }
    }

    // eslint-disable-next-line
  ], [columnIndex, isDelivering, isActivating, isConfirming]);

  const onEntregarDatosAcceso = (user, index) => {
    if (window.confirm('¿Estás seguro(a) de querer entregar los datos de acceso a los custodios?')) {
      setColumnIndex(index);
      dispatch(actions.entregarDatosAcceso(user.id));
    }
  };

  const onActivarUsuarioRegistrado = (user, index) => {
    const nombreCompleto = _.get(user, 'perfil.nombre_completo', '');

    if (window.confirm(`¿Estás seguro(a) de querer activar la suscripción del usuario "${nombreCompleto}"?`)) {
      setColumnIndex(index);
      dispatch(actions.activarUsuarioRegistrado(user.id));
    }
  };

  const onConfirmarPruebaVida = (user, index) => {
    const nombreCompleto = _.get(user, 'perfil.nombre_completo', '');
    const pruebaVidaCustodio = _.get(user, 'prueba_vida_usuario.prueba_vida_custodio', null);

    if (window.confirm(`¿Estás seguro(a) de querer confirmar la prueba de vida del usuario "${nombreCompleto}"?`)) {
      setColumnIndex(index);

      if (pruebaVidaCustodio) {
        dispatch(actions.confirmarPruebaVida(pruebaVidaCustodio.id));
      }
    }
  };

  const onResetUsuariosRegistradosErrors = () => {
    dispatch(actions.resetUsuariosRegistradosErrors());
  };

  const renderRowSubComponent = React.useCallback(({ row }) => {
    const pruebaVidaUsuario = row.original.prueba_vida_usuario;
    const pruebaVidaCustodio = _.get(pruebaVidaUsuario, 'prueba_vida_custodio', null);
    const custodios = row.original.custodios;

    return (
      <React.Fragment>
        <EstatusPruebaVidaUsuario pruebaVida={pruebaVidaUsuario} />
        <EstatusPruebaVidaCustodio pruebaVida={pruebaVidaCustodio} />
        <FormTable sub>
          <thead>
            <tr>
              <th colSpan="4">Custodios</th>
            </tr>
            <tr>
              <th>Nombre completo</th>
              <th>Correo electrónico</th>
              <th>Celular</th>
              <th>Parentesco</th>
            </tr>
          </thead>
          <tbody>
            {(custodios.length > 0) ? custodios.map((custodio, index) => (
              <tr key={index}>
                <td>{custodio.nombre_completo}</td>
                <td>{custodio.correo_electronico}</td>
                <td>{custodio.celular}</td>
                <td>{custodio.parentesco.nombre}</td>
              </tr>
            )) : (
              <tr>
                <td colSpan="4">No tiene ningún custodio.</td>
              </tr>
            )}
          </tbody>
        </FormTable>
      </React.Fragment>
    );
  }, []);

  React.useEffect(() => {
    dispatch(actions.getUsuariosRegistrados());

    return () => {
      dispatch(actions.resetUsuariosRegistrados());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Usuarios registrados">
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetUsuariosRegistradosErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      <Table
        columns={columns}
        data={users}
        loading={isGetting}
        canFilteringByPlan
        canFilteringByStatus
        canDownloadInExcel
        renderRowSubComponent={renderRowSubComponent}
      />
    </Layout>
  );
};

export default UsuariosRegistrados;
