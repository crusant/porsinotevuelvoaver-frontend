import React from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import classNames from 'classnames';
import _ from 'lodash';
import Button from 'react-bootstrap/Button';
import Loader from '../../../components/UI/Loader/Loader';
import IconDownload from '../../../components/UI/Icons/IconDownload/IconDownload';
import EscrituraLibre from '../../../components/Familiar/EscrituraLibre/EscrituraLibre';
import MiHistoria from '../../../components/Familiar/MiHistoria/MiHistoria';
import MensajePostumo from '../../../components/Familiar/MensajePostumo/MensajePostumo';
import { API_URL } from '../../../constants';
import * as helpers from '../../../helpers';
import * as actions from '../../../actions';
import logo from '../../../assets/images/logotipo-blanco.png';
import styles from './MiHistoriaLegado.module.scss';

const MiHistoriaLegado = () => {
  const { url } = useParams();
  const familiar = useSelector((state) => state.redFamiliar.familiar);
  const isGettingFamiliar = useSelector((state) => state.redFamiliar.isGetting);
  const isDownloadingAudio = useSelector((state) => state.redFamiliar.isDownloadingAudio);
  const isDownloadingVideo = useSelector((state) => state.redFamiliar.isDownloadingVideo);
  const avisoPrivacidad = useSelector((state) => state.avisoPrivacidad.avisoPrivacidad);
  const isGettingAvisoPrivacidad = useSelector((state) => state.avisoPrivacidad.isGetting);
  const terminosCondiciones = useSelector((state) => state.terminosCondiciones.terminosCondiciones);
  const isGettingTerminosCondiciones = useSelector((state) => state.terminosCondiciones.isGetting);
  const dispatch = useDispatch();

  const isGetting = () => {
    return isGettingFamiliar
        || isGettingAvisoPrivacidad
        || isGettingTerminosCondiciones;
  };

  const hasEscrituraLibre = () => {
    const escrituraLibre = _.get(familiar, 'escritura_libre', {});
    const texto = _.get(escrituraLibre, 'texto', '');
    const audio = _.get(escrituraLibre, 'audio', '');
    const video = _.get(escrituraLibre, 'video', '');

    return !helpers.isEmptyString(texto)
        || !helpers.isEmptyString(audio)
        || !helpers.isEmptyString(video);
  };

  const hasMiHistoria = () => {
    const miHistoria = _.get(familiar, 'mi_historia', []);

    return (miHistoria.length > 0);
  };

  const hasMensajePostumo = () => {
    const mensajePostumo = _.get(familiar, 'mensaje_postumo', {});
    const fotografia = _.get(mensajePostumo, 'fotografia', '');
    const mensaje = _.get(mensajePostumo, 'mensaje', '');
    const audio = _.get(mensajePostumo, 'audio', '');
    const video = _.get(mensajePostumo, 'video', '');

    return !helpers.isEmptyString(fotografia)
        || !helpers.isEmptyString(mensaje)
        || !helpers.isEmptyString(audio)
        || !helpers.isEmptyString(video);
  };

  const onDownloadEscrituraLibreAudio = (defaultExtension) => {
    const audio = _.get(familiar, 'escritura_libre.audio', '');
    const extension = defaultExtension || audio.split('.').pop();

    dispatch(actions.downloadFamiliarEscrituraLibreAudio(url, extension));
  };

  const onDownloadEscrituraLibreVideo = () => {
    const video = _.get(familiar, 'escritura_libre.video', '');
    const extension = video.split('.').pop();

    dispatch(actions.downloadFamiliarEscrituraLibreVideo(url, extension));
  };

  const onDownloadMiHistoriaAudio = (id, defaultExtension) => {
    const extension = defaultExtension || _.flow(
      (familiar) => _.get(familiar, 'mi_historia', []),
      (miHistoria) => _.find(miHistoria, (miHistoria) => miHistoria.id === id),
      (miHistoria) => _.get(miHistoria, 'audio', ''),
      (audio) => audio.split('.').pop()
    )(familiar);

    dispatch(actions.downloadFamiliarMiHistoriaAudio(url, id, extension));
  };

  const onDownloadMiHistoriaVideo = (id) => {
    const extension = _.flow(
      (familiar) => _.get(familiar, 'mi_historia', []),
      (miHistoria) => _.find(miHistoria, (miHistoria) => miHistoria.id === id),
      (miHistoria) => _.get(miHistoria, 'video', ''),
      (video) => video.split('.').pop()
    )(familiar);

    dispatch(actions.downloadFamiliarMiHistoriaVideo(url, id, extension));
  };

  const onDownloadMensajePostumoAudio = (id, defaultExtension) => {
    const audio = _.get(familiar, 'mensaje_postumo.audio', '');
    const extension = defaultExtension || audio.split('.').pop();

    dispatch(actions.downloadFamiliarMensajePostumoAudio(url, id, extension));
  };

  const onDownloadMensajePostumoVideo = (id) => {
    const video = _.get(familiar, 'mensaje_postumo.video', '');
    const extension = video.split('.').pop();

    dispatch(actions.downloadFamiliarMensajePostumoVideo(url, id, extension));
  };

  const getHeaderStyles = () => {
    const styles = {};

    if (_.get(familiar, 'escritura_libre.portada', null)) {
      styles['backgroundImage'] = `url(${_.get(familiar, 'escritura_libre.portada', null)})`;
    }

    return styles;
  };

  React.useEffect(() => {
    (async () => {
      await dispatch(actions.getFamiliar(url));
      await dispatch(actions.getAvisoPrivacidad());
      await dispatch(actions.getTerminosCondiciones());
    })();

    // eslint-disable-next-line
  }, []);

  if (isGetting()) {
    return <Loader title="Cargando el legado de amor y seguridad para tus seres queridos" />;
  }

  return (
    <div className={styles.MiHistoriaLegado}>
      <div className={styles.Header} style={getHeaderStyles()}>
        <h2 className={styles.Subtitle}>
          {_.get(familiar, 'user.perfil.nombre_completo', '')}
        </h2>
        <h1 className={styles.Title}>Mi historia y legado</h1>
      </div>
      <Button
        href={`${API_URL}red-familiar/${url}/mi-historia-y-legado/descargar`}
        className={classNames('c-button c-button--secondary', styles.Download)}
        target="_blank"
      >
        <IconDownload className="c-button__icon" height="14px" /> Descargar mi historia y legado
      </Button>
      {hasEscrituraLibre() && (
        <EscrituraLibre
          escrituraLibre={_.get(familiar, 'escritura_libre', {})}
          user={_.get(familiar, 'user', {})}
          isDownloadingAudio={isDownloadingAudio}
          isDownloadingVideo={isDownloadingVideo}
          onDownloadAudioInMp3={onDownloadEscrituraLibreAudio}
          onDownloadAudioInStandard={onDownloadEscrituraLibreAudio}
          onDownloadVideo={onDownloadEscrituraLibreVideo}
        />
      )}
      {hasMiHistoria() && (
        <MiHistoria
          miHistoria={_.get(familiar, 'mi_historia', [])}
          isDownloadingAudio={isDownloadingAudio}
          isDownloadingVideo={isDownloadingVideo}
          onDownloadAudioInMp3={onDownloadMiHistoriaAudio}
          onDownloadAudioInStandard={onDownloadMiHistoriaAudio}
          onDownloadVideo={onDownloadMiHistoriaVideo}
        />
      )}
      {hasMensajePostumo() && (
        <MensajePostumo
          mensajePostumo={_.get(familiar, 'mensaje_postumo', {})}
          familiar={familiar.nombre_completo}
          user={_.get(familiar, 'user', {})}
          isDownloadingAudio={isDownloadingAudio}
          isDownloadingVideo={isDownloadingVideo}
          onDownloadAudioInMp3={onDownloadMensajePostumoAudio}
          onDownloadAudioInStandard={onDownloadMensajePostumoAudio}
          onDownloadVideo={onDownloadMensajePostumoVideo}
        />
      )}
      <div className={styles.LogoContainer}>
        <img src={logo} className={styles.Logo} alt="Logotipo en blanco de Por si no te vuelvo a ver" />
      </div>
      <div className={styles.Copyright}>&#169;2020 Por si no te vuelvo a ver. Todos los derechos reservados. <a href={avisoPrivacidad.documento_digital} className={styles.Link} rel="noopener noreferrer" target="_blank">{avisoPrivacidad.nombre}</a>. <a href={terminosCondiciones.documento_digital} className={styles.Link} rel="noopener noreferrer" target="_blank">{terminosCondiciones.nombre}</a>.</div>
    </div>
  );
};

export default MiHistoriaLegado;
