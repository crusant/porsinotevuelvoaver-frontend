import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Feedback from '../../components/UI/Feedback/Feedback';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormText from '../../components/UI/Form/FormText/FormText';
import FormHelpText from '../../components/UI/Form/FormHelpText/FormHelpText';
import FormCategories from '../../components/UI/Form/FormCategories/FormCategories';
import FormInput from '../../components/UI/Form/FormInput/FormInput';
import FormControl from '../../components/UI/Form/FormControl/FormControl';
import FormFile from '../../components/UI/Form/FormFile/FormFile';
import File from '../../components/UI/File/File';
import FormSeparator from '../../components/UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../components/UI/Form/FormTitle/FormTitle';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import FormMargin from '../../components/UI/Form/FormMargin/FormMargin';
import FormTable from '../../components/UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../components/UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../components/UI/Button/Button';
import IconMedicalInsance from '../../components/UI/Icons/IconMedicalInsurance/IconMedicalInsurance';
import IconStudent from '../../components/UI/Icons/IconStudent/IconStudent';
import IconInsurance from '../../components/UI/Icons/IconInsurance/IconInsurance';
import IconHomeInsurance from '../../components/UI/Icons/IconHomeInsurance/IconHomeInsurance';
import IconCarInsurance from '../../components/UI/Icons/IconCarInsurance/IconCarInsurance';
import IconShieldInsurance from '../../components/UI/Icons/IconShieldInsurance/IconShieldInsurance';
import IconShield from '../../components/UI/Icons/IconShield/IconShield';
import * as polizasSegurosActions from '../../actions/polizas-seguros';
import * as monedasActions from '../../actions/monedas';
import * as helpers from '../../helpers';
import * as constants from '../../constants';
import IconDownload from '../../components/UI/Icons/IconDownload/IconDownload';

const PolizasSeguros = () => {
  const [rowKey, setRowKey] = React.useState(-1);
  const [showBeneficiariosErrors, setShowBeneficiariosErrors] = React.useState(false);
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const [columnIndex, setColumnIndex] = React.useState(null);
  const user = useSelector((state) => state.auth.user);
  const polizaSeguro = useSelector((state) => state.polizasSeguros.polizaSeguro);
  const beneficiario = useSelector((state) => state.polizasSeguros.beneficiario);
  const beneficiarios = useSelector((state) => state.polizasSeguros.polizaSeguro.beneficiarios);
  const asesor = useSelector((state) => state.polizasSeguros.polizaSeguro.asesor);
  const polizasSeguros = useSelector((state) => state.polizasSeguros.polizasSeguros);
  const isGetting = useSelector((state) => state.polizasSeguros.isGetting);
  const isSaving = useSelector((state) => state.polizasSeguros.isSaving);
  const isUpdating = useSelector((state) => state.polizasSeguros.isUpdating);
  const isDownloading = useSelector((state) => state.polizasSeguros.isDownloading);
  const monedas = useSelector((state) => state.monedas.monedas);
  const errors = useSelector((state) => state.polizasSeguros.errors);
  const dispatch = useDispatch();

  const categoriaIds = { ...constants.polizaSeguroCategoriaIds };
  const categoriaNombres = { ...constants.polizaSeguroCategoriaNombres };

  const hasMonedas = (monedas.length > 0);
  const hasBeneficiarios = (beneficiarios.length > 0);
  const hasPolizasSeguros = (polizasSeguros.length > 0);

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [polizaSeguro] = polizasSeguros.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    if (isGetting) {
      return 'Cargando...';
    }

    const updatedAt = polizaSeguro?.updated_at;

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const onChangeInput = (event) => {
    const input = event.target;
    const [entity, attribute] = input.name.split('.');
    let value = input.value;

    if (attribute === 'tipo' || attribute === 'moneda_id') {
      value = parseInt(value, 10);
    }

    if (entity === 'asesor') {
      dispatch(polizasSegurosActions.changeAsesorInputs(attribute, value));
    } else {
      dispatch(polizasSegurosActions.changePolizaSeguroInputs(entity, attribute, value));
    }
  };

  const onChangePolizaDigitalInput = (value) => {
    dispatch(polizasSegurosActions.changePolizaSeguroInputs('polizaSeguro', 'poliza_digital', value));
  };

  const onRemovePolizaDigitalInput = () => {
    dispatch(polizasSegurosActions.changePolizaSeguroInputs('polizaSeguro', 'poliza_digital', ''));
  };

  const sumPorcentajesBeneficiarios = (porcentaje, beneficiario) => {
    return porcentaje + parseInt(beneficiario.porcentaje, 10);
  };

  const getPorcentajeTotal = (initialPorcentaje = 0) => {
    return polizaSeguro.beneficiarios.reduce(sumPorcentajesBeneficiarios, initialPorcentaje);
  };

  const isProcentajeEqualTo = (porcentaje) => {
    return getPorcentajeTotal() === porcentaje;
  };

  const onAddBeneficiario = () => {
    const nombreCompleto = beneficiario.nombre_completo;
    const porcentaje = parseInt(beneficiario.porcentaje, 10);
    const porcentajes = getPorcentajeTotal(porcentaje);
    const isGreaterThan100 = (porcentajes > 100);

    setShowBeneficiariosErrors(false);

    if (isGreaterThan100) {
      setShowBeneficiariosErrors(true);
    }

    if (helpers.isEmptyString(nombreCompleto)|| !helpers.isInteger(porcentaje) || porcentaje <= 0 || isGreaterThan100) {
      return;
    }

    dispatch(polizasSegurosActions.addBeneficiario(beneficiario));
    dispatch(polizasSegurosActions.resetBeneficiario());
  };

  const onHideBeneficiariosErrors = () => {
    setShowBeneficiariosErrors(false);
  };

  const onRemoveBeneficiario = (index) => {
    dispatch(polizasSegurosActions.removeBeneficiario(index));
  };

  const onResetPolizaSeguroInputs = () => {
    dispatch(polizasSegurosActions.resetPolizaSeguroInputs());
  };

  const onSaveOrUpdatePolizaSeguro = async (event) => {
    event.preventDefault();

    setShowToast(false);
    setToastMessage('');

    const formData = new FormData();
    helpers.buildFormData(formData, polizaSeguro);

    if (!polizaSeguro.id && await dispatch(polizasSegurosActions.savePolizaSeguro(formData))) {
      setToastMessage('Los datos de la póliza de seguros se guardaron con éxito.');
      setShowToast(true);
    }

    if (polizaSeguro.id && await dispatch(polizasSegurosActions.updatePolizaSeguro(formData))) {
      setToastMessage('Los datos de la póliza de seguros se actualizaron con éxito.');
      setShowToast(true);
    }
  };

  const onResetPolizasSegurosErrors = () => {
    dispatch(polizasSegurosActions.resetPolizasSegurosErrors());
  };

  const getMonedaNombre = (id, otra) => {
    const moneda = monedas.find((moneda) => moneda.id === id);

    if (!moneda) {
      return '';
    }

    if (moneda.id === 5) {
      return otra;
    }

    return moneda.nombre;
  };

  const onToggleRow = (key) => {
    if (rowKey !== key) {
      setRowKey(key);
    } else {
      setRowKey(-1);
    }
  };

  const onEditPolizaSeguro = (polizaSeguro) => {
    window.scrollTo(0, 0);
    dispatch(polizasSegurosActions.editPolizaSeguro(polizaSeguro));
  };

  const onDeletePolizaSeguro = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar esta póliza de seguros?');
    setShowToast(false);

    if (isConfirmed && await dispatch(polizasSegurosActions.deletePolizaSeguro(id))) {
      setToastMessage('Los datos de la póliza de seguros se eliminaron con éxito.');
      setShowToast(true);
    }
  };

  const onDownloadPolizaSeguro = (polizaSeguro, index) => {
    const extension = String(polizaSeguro.poliza_digital).split('.').pop();

    setColumnIndex(index);
    dispatch(polizasSegurosActions.downloadPolizaSeguro(polizaSeguro.id, extension));
  };

  React.useEffect(() => {
    if (!helpers.isNull(errors)) {
      window.scrollTo(0, 0);
    }
  }, [errors]);

  React.useEffect(() => {
    (async () => {
      await dispatch(polizasSegurosActions.getPolizasSeguros());
      await dispatch(monedasActions.getMonedas());
    })();

    return () => {
      onResetPolizaSeguroInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Pólizas de seguros">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Pólizas de seguros</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetPolizasSegurosErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {canWrite() && (
        <React.Fragment>
          {helpers.isObject(errors) && (
            <Alert variant="danger" onClose={onResetPolizasSegurosErrors} dismissible>
              <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
              <Errors>
                {Object.values(errors).map((error, index) => (
                  <Errors.Error key={index}>
                    {error}
                  </Errors.Error>
                ))}
              </Errors>
            </Alert>
          )}
          <Paragraph title>En esa sección se lleva a cabo el registro de todo lo relacionado con pólizas de seguro y fideicomisos.<br /> Contar con esta información le permite a los beneficiarios comenzar los trámites del cobro y disfrutar lo antes posible de sus beneficios.<br /> En caso de requerir anexar documentos adicionales, puedes hacerlo en la sección "Documentos digitales y ubicación física".</Paragraph>
          <form onSubmit={onSaveOrUpdatePolizaSeguro}>
            <FormRow>
              <FormColumn>
                <FormLabel center htmlFor="polizaSeguro.tipo">Selecciona una categoría</FormLabel>
                <FormCategories>
                  <FormCategories.Category
                    name="polizaSeguro.tipo"
                    id="polizaSeguro.tipo_seguro_vida"
                    checked={(polizaSeguro.tipo === categoriaIds.SEGURO_VIDA)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.SEGURO_VIDA]}
                    value={categoriaIds.SEGURO_VIDA}
                  >
                    {(styles) => <IconMedicalInsance className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="polizaSeguro.tipo"
                    id="polizaSeguro.tipo_seguro_fideicomiso_educativo"
                    checked={(polizaSeguro.tipo === categoriaIds.SEGURO_FIDEICOMISO_EDUCATIVO)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.SEGURO_FIDEICOMISO_EDUCATIVO]}
                    value={categoriaIds.SEGURO_FIDEICOMISO_EDUCATIVO}
                  >
                    {(styles) => <IconStudent className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="polizaSeguro.tipo"
                    id="polizaSeguro.tutor_seguro_accidentes_personales"
                    checked={(polizaSeguro.tipo === categoriaIds.SEGURO_ACCIDENTES_PERSONALES)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.SEGURO_ACCIDENTES_PERSONALES]}
                    value={categoriaIds.SEGURO_ACCIDENTES_PERSONALES}
                  >
                    {(styles) => <IconInsurance className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="polizaSeguro.tipo"
                    id="polizaSeguro.tipo_seguro_casa"
                    checked={(polizaSeguro.tipo === categoriaIds.SEGURO_CASA)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.SEGURO_CASA]}
                    value={categoriaIds.SEGURO_CASA}
                  >
                    {(styles) => <IconHomeInsurance className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="polizaSeguro.tipo"
                    id="polizaSeguro.tipo_seguro_automovil"
                    checked={(polizaSeguro.tipo === categoriaIds.SEGURO_AUTOMOVIL)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.SEGURO_AUTOMOVIL]}
                    value={categoriaIds.SEGURO_AUTOMOVIL}
                  >
                    {(styles) => <IconCarInsurance className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="polizaSeguro.tipo"
                    id="polizaSeguro.tipo_gastos_medicos_mayores"
                    checked={(polizaSeguro.tipo === categoriaIds.GASTOS_MEDICOS_MAYORES)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.GASTOS_MEDICOS_MAYORES]}
                    value={categoriaIds.GASTOS_MEDICOS_MAYORES}
                  >
                    {(styles) => <IconShieldInsurance className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="polizaSeguro.tipo"
                    id="polizaSeguro.tipo_otro"
                    checked={(polizaSeguro.tipo === categoriaIds.OTRO)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.OTRO]}
                    value={categoriaIds.OTRO}
                  >
                    {(styles) => <IconShield className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                </FormCategories>
              </FormColumn>
            </FormRow>
            {(polizaSeguro.tipo === categoriaIds.SEGURO_AUTOMOVIL) && (
              <React.Fragment>
                <FormTitle>Datos del automóvil</FormTitle>
                <FormRow>
                  <FormColumn left>
                    <FormLabel htmlFor="polizaSeguro.marca_tipo">Marca y tipo*</FormLabel>
                    <FormInput
                      type="text"
                      name="polizaSeguro.marca_tipo"
                      id="polizaSeguro.marca_tipo"
                      required
                      maxLength={100}
                      onChange={onChangeInput}
                      value={polizaSeguro.marca_tipo}
                    />
                  </FormColumn>
                  <FormColumn right>
                    <FormLabel htmlFor="polizaSeguro.modelo">Modelo*</FormLabel>
                    <FormInput
                      type="number"
                      name="polizaSeguro.modelo"
                      id="polizaSeguro.modelo"
                      required
                      digits={4}
                      min={0}
                      max={9999}
                      step={1}
                      onChange={onChangeInput}
                      value={polizaSeguro.modelo}
                    />
                  </FormColumn>
                </FormRow>
                <FormRow>
                  <FormColumn left>
                    <FormLabel htmlFor="polizaSeguro.serie">Número de serie*</FormLabel>
                    <FormInput
                      type="text"
                      name="polizaSeguro.serie"
                      id="polizaSeguro.serie"
                      required
                      maxLength={50}
                      onChange={onChangeInput}
                      value={polizaSeguro.serie}
                    />
                  </FormColumn>
                  <FormColumn right>
                    <FormLabel htmlFor="polizaSeguro.lugar_registro">Lugar de registro <FormText>(Opcional)</FormText></FormLabel>
                    <FormInput
                      type="text"
                      name="polizaSeguro.lugar_registro"
                      id="polizaSeguro.lugar_registro"
                      maxLength={100}
                      onChange={onChangeInput}
                      value={polizaSeguro.lugar_registro}
                    />
                  </FormColumn>
                </FormRow>
                <FormSeparator />
              </React.Fragment>
            )}
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="polizaSeguro.compania">Compañía de seguros*</FormLabel>
                <FormInput
                  type="text"
                  name="polizaSeguro.compania"
                  id="polizaSeguro.compania"
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={polizaSeguro.compania}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="polizaSeguro.numero_poliza">Número de póliza*</FormLabel>
                <FormInput
                  type="text"
                  name="polizaSeguro.numero_poliza"
                  id="polizaSeguro.numero_poliza"
                  required
                  maxLength={20}
                  onChange={onChangeInput}
                  value={polizaSeguro.numero_poliza}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="polizaSeguro.vigencia">Vigencia*</FormLabel>
                <FormInput
                  type="date"
                  name="polizaSeguro.vigencia"
                  id="polizaSeguro.vigencia"
                  required
                  onChange={onChangeInput}
                  value={polizaSeguro.vigencia}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="polizaSeguro.telefono">Teléfono*</FormLabel>
                <FormInput
                  type="tel"
                  name="polizaSeguro.telefono"
                  id="polizaSeguro.telefono"
                  required
                  integer
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeInput}
                  value={polizaSeguro.telefono}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="polizaSeguro.suma_asegurada">Suma asegurada*</FormLabel>
                <FormInput
                  type="number"
                  name="polizaSeguro.suma_asegurada"
                  id="polizaSeguro.suma_asegurada"
                  required
                  decimal
                  min={0.01}
                  step={0.01}
                  onChange={onChangeInput}
                  value={polizaSeguro.suma_asegurada}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="polizaSeguro.moneda_id">Moneda*</FormLabel>
                <FormInput
                  as="select"
                  name="polizaSeguro.moneda_id"
                  id="polizaSeguro.moneda_id"
                  disabled={!hasMonedas}
                  required
                  onChange={onChangeInput}
                  value={polizaSeguro.moneda_id}
                >
                  <option value="">Seleccione...</option>
                  {monedas.map(({ id, nombre }) => (
                    <option key={id} value={id}>
                      {nombre}
                    </option>
                  ))}
                </FormInput>
              </FormColumn>
            </FormRow>
            {(polizaSeguro.moneda_id === 5) && (
              <FormRow>
                <FormColumn left strict>
                  <FormLabel htmlFor="polizaSeguro.moneda">Especifique*</FormLabel>
                  <FormInput
                    type="text"
                    name="polizaSeguro.moneda"
                    id="polizaSeguro.moneda"
                    required
                    maxLength={42}
                    onChange={onChangeInput}
                    value={polizaSeguro.moneda}
                  />
                </FormColumn>
              </FormRow>
            )}
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="polizaSeguro.beneficios_invalidez">Beneficios por invalidez <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  as="textarea"
                  name="polizaSeguro.beneficios_invalidez"
                  id="polizaSeguro.beneficios_invalidez"
                  rows={3}
                  maxLength={500}
                  onChange={onChangeInput}
                  value={polizaSeguro.beneficios_invalidez}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="polizaSeguro.beneficios_muerte">Beneficios por muerte <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  as="textarea"
                  name="polizaSeguro.beneficios_muerte"
                  id="polizaSeguro.beneficios_muerte"
                  rows={3}
                  maxLength={500}
                  onChange={onChangeInput}
                  value={polizaSeguro.beneficios_muerte}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="polizaSeguro.poliza_digital">Póliza digital <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF, Word, PowerPoint, Excel, .jpg o .png.</FormHelpText>
                {helpers.isEmptyString(polizaSeguro.poliza_digital) ? (
                  <FormFile accept=".pdf,.doc,.docx,.ppt,.pptx,.xsl,.xlsx,.jpg,.jpeg,.png" onChange={onChangePolizaDigitalInput} />
                ) : (
                  <File
                    file={polizaSeguro.poliza_digital}
                    onRemove={onRemovePolizaDigitalInput}
                  />
                )}
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="polizaSeguro.poliza_fisica">Donde encontrar la póliza <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="polizaSeguro.poliza_fisica"
                  id="polizaSeguro.poliza_fisica"
                  maxLength={500}
                  onChange={onChangeInput}
                  value={polizaSeguro.poliza_fisica}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="polizaSeguro.indicaciones">Instrucciones*</FormLabel>
                <FormInput
                  as="textarea"
                  name="polizaSeguro.indicaciones"
                  id="polizaSeguro.indicaciones"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeInput}
                  value={polizaSeguro.indicaciones}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormTitle>Beneficiarios*</FormTitle>
            <FormHelpText underTitle>El porcentaje total debe ser del 100%.</FormHelpText>
            {showBeneficiariosErrors && (
              <Alert variant="danger" onClose={onHideBeneficiariosErrors} dismissible>
                <Paragraph>No se puede agregar al beneficiario, porque el porcentaje total no puede ser mayor al 100%.</Paragraph>
              </Alert>
            )}
            {isProcentajeEqualTo(100) && (
              <Alert variant="success">
                <Paragraph>Haz alcanzado el porcentaje total del 100%, por lo que ya no puedes agregar mas beneficiarios.</Paragraph>
              </Alert>
            )}
            <FormTable>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre completo</th>
                  <th>Porcentaje</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td>
                    <FormControl
                      type="text"
                      name="beneficiario.nombre_completo"
                      id="beneficiario.nombre_completo"
                      disabled={isProcentajeEqualTo(100)}
                      maxLength={100}
                      onChange={onChangeInput}
                      value={beneficiario.nombre_completo}
                    />
                  </td>
                  <td>
                    <FormControl
                      type="number"
                      name="beneficiario.porcentaje"
                      id="beneficiario.porcentaje"
                      disabled={isProcentajeEqualTo(100)}
                      min={1}
                      max={100}
                      step={1}
                      onChange={onChangeInput}
                      value={beneficiario.porcentaje}
                    />
                  </td>
                  <td>
                    <Button
                      variant="primary"
                      small
                      disabled={isProcentajeEqualTo(100)}
                      onClick={onAddBeneficiario}
                    >
                      Agregar
                    </Button>
                  </td>
                </tr>
                {hasBeneficiarios ? beneficiarios.map(({ nombre_completo, porcentaje }, index) => (
                  <tr key={nombre_completo}>
                    <td>{(index + 1)}</td>
                    <td>{nombre_completo}</td>
                    <td>{porcentaje} %</td>
                    <td>
                      <Button
                        variant="danger"
                        small
                        onClick={() => onRemoveBeneficiario(index)}
                      >
                        Quitar
                      </Button>
                    </td>
                  </tr>
                )) : (
                  <tr>
                    <td colSpan={4}>No has agregado ningún beneficiario.</td>
                  </tr>
                )}
                <tr>
                  <td colSpan={2}><strong>Porcentaje total</strong></td>
                  <td><strong>{getPorcentajeTotal()} %</strong></td>
                  <td></td>
                </tr>
              </tbody>
            </FormTable>
            <FormSeparator />
            <FormTitle>Datos del asesor</FormTitle>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="asesor.nombre_completo">Nombre completo <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="text"
                  name="asesor.nombre_completo"
                  id="asesor.nombre_completo"
                  maxLength={100}
                  onChange={onChangeInput}
                  value={asesor.nombre_completo}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="asesor.telefono_oficina">Teléfono de oficina <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="tel"
                  name="asesor.telefono_oficina"
                  id="asesor.telefono_oficina"
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeInput}
                  value={asesor.telefono_oficina}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="asesor.telefono_particular">Teléfono particular <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="tel"
                  name="asesor.telefono_particular"
                  id="asesor.telefono_particular"
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeInput}
                  value={asesor.telefono_particular}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left strict>
                <FormLabel htmlFor="asesor.correo_electronico">Correo electrónico <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                <FormInput
                  type="text"
                  name="asesor.correo_electronico"
                  id="asesor.correo_electronico"
                  maxLength={320}
                  pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                  title="Por favor ingrese un correo electrónico valido."
                  onChange={onChangeInput}
                  value={asesor.correo_electronico}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetPolizaSeguroInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!polizaSeguro.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormToast
            variant="success"
            show={showToast}
            title="Mensaje de éxito"
            message={toastMessage}
            onClose={() => setShowToast(false)}
          />
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Categoría</th>
            <th>Compañía de seguros</th>
            <th>Número de póliza</th>
            <th>Vigencia</th>
            <th>Teléfono</th>
            <th>Suma asegurada</th>
            <th>Beneficios por invalidez</th>
            <th>Beneficios por muerte</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan={9}>Cargando...</td>
            </tr>
          ) : hasPolizasSeguros ? polizasSeguros.map((polizaSeguro, index) => (
            <React.Fragment key={polizaSeguro.id}>
              <tr>
                <td>{categoriaNombres[polizaSeguro.tipo]}</td>
                <td>{polizaSeguro.compania}</td>
                <td>{polizaSeguro.numero_poliza}</td>
                <td>{polizaSeguro.vigencia}</td>
                <td>{polizaSeguro.telefono}</td>
                <td>{helpers.money(polizaSeguro.suma_asegurada)} {getMonedaNombre(polizaSeguro.moneda_id, polizaSeguro.moneda)}</td>
                <td>{polizaSeguro.beneficios_invalidez}</td>
                <td>{polizaSeguro.beneficios_muerte}</td>
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="light"
                      small
                      onClick={() => onToggleRow(index)}
                    >
                      Ver más
                    </Button>
                    {canWrite() && (
                      <React.Fragment>
                        <Button
                          variant="secondary"
                          small
                          onClick={() => onEditPolizaSeguro(polizaSeguro)}
                        >
                          Editar
                        </Button>
                        <Button
                          variant="danger"
                          small
                          onClick={() => onDeletePolizaSeguro(polizaSeguro.id)}
                        >
                          Eliminar
                        </Button>
                      </React.Fragment>
                    )}
                  </FormButtonGroup>
                </td>
              </tr>
              {(rowKey === index) && (
                <tr>
                  <td colSpan={9}>
                    <ul>
                      {(polizaSeguro.tipo === categoriaIds.SEGURO_AUTOMOVIL) && (
                        <React.Fragment>
                          <li><strong>Marca y tipo</strong> {polizaSeguro.marca_tipo}</li>
                          <li><strong>Modelo</strong> {polizaSeguro.modelo}</li>
                          <li><strong>Número de serie</strong> {polizaSeguro.serie}</li>
                          <li><strong>Lugar de registro</strong> {polizaSeguro.lugar_registro}</li>
                        </React.Fragment>
                      )}
                      {!helpers.isEmptyString(polizaSeguro.poliza_digital) && (
                        <li>
                          <strong>Póliza digital</strong>
                          <Button
                            variant="primary"
                            small
                            disabled={isDownloading && (index === columnIndex)}
                            onClick={() => onDownloadPolizaSeguro(polizaSeguro, index)}
                          >
                            <IconDownload height="14px" /> {isDownloading && (index === columnIndex) ? 'Descargando...' : 'Descargar'}
                          </Button>
                        </li>
                      )}
                      <li><strong>Donde encontrar la póliza</strong> {polizaSeguro.poliza_fisica}</li>
                      <li><strong>Instrucciones</strong> {polizaSeguro.indicaciones}</li>
                    </ul>
                    <FormTable sub>
                      <thead>
                        <tr>
                          <th colSpan={2}>Beneficiarios</th>
                        </tr>
                        <tr>
                          <th>Nombre completo</th>
                          <th>Porcentaje</th>
                        </tr>
                      </thead>
                      <tbody>
                        {polizaSeguro.beneficiarios.map((beneficiario, index) => (
                          <tr key={index}>
                            <td>{beneficiario.nombre_completo}</td>
                            <td>{beneficiario.porcentaje}</td>
                          </tr>
                        ))}
                      </tbody>
                    </FormTable>
                    <h6>Datos del asesor</h6>
                    <ul>
                      <li><strong>Nombre completo</strong> {polizaSeguro.asesor.nombre_completo}</li>
                      <li><strong>Teléfono de oficina</strong> {polizaSeguro.asesor.telefono_oficina}</li>
                      <li><strong>Teléfono particular</strong> {polizaSeguro.asesor.telefono_particular}</li>
                      <li><strong>Correo electrónico</strong> {polizaSeguro.asesor.correo_electronico}</li>
                    </ul>
                  </td>
                </tr>
              )}
            </React.Fragment>
          )) : (
            <tr>
              <td colSpan={9}>
                {canWrite() ? 'No has agregado ninguna póliza de seguros.' : 'No se agregó ninguna póliza de seguros.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </Layout>
  );
}

export default PolizasSeguros;
