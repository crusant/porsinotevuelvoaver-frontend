import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Feedback from '../../components/UI/Feedback/Feedback';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormHelpText from '../../components/UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../components/UI/Form/FormInput/FormInput';
import Button from '../../components/UI/Button/Button';
import ColorPrimary from '../../components/UI/Colors/ColorPrimary/ColorPrimary';
import * as actions from '../../actions';
import * as helpers from '../../helpers';

const ChangePassword = () => {
  const [user, setUser] = React.useState({ password: '', password_confirmation: '' });
  const [isSuccessful, setIsSuccessful] = React.useState(false);
  const isChanging = useSelector((state) => state.users.isChanging);
  const errors = useSelector((state) => state.users.errors);
  const dispatch = useDispatch();

  const onChangeUserInput = (event) => {
    const input = event.target;

    setUser((prevState) => ({
      ...prevState,
      [input.name]: input.value
    }));
  };

  const onChangePassword = async (event) => {
    event.preventDefault();

    setIsSuccessful(false);

    const success = await dispatch(actions.changePassword(user));

    if (success) {
      setIsSuccessful(true);
      setUser({ password: '', password_confirmation: '' });
    }
  };

  const onHideSuccessMessage = () => {
    setIsSuccessful(false);
  };

  const onResetPasswordErrors = () => {
    dispatch(actions.resetPasswordErrors());
  };

  React.useEffect(() => {

    return () => {
      onResetPasswordErrors();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Cambiar contraseña">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Cambiar contraseña</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>*Campos obligatorios</Feedback>
      {isSuccessful && (
        <Alert variant="success" onClose={onHideSuccessMessage} dismissible>
          <Paragraph>Tu contraseña se cambió exitosamente.</Paragraph>
        </Alert>
      )}
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetPasswordErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onResetPasswordErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {helpers.isObject(errors) && Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <form onSubmit={onChangePassword}>
        <FormRow>
          <FormColumn left>
            <FormLabel htmlFor="password">Nueva contraseña*</FormLabel>
            <FormHelpText><ColorPrimary>*</ColorPrimary>8 Caracteres mínimo <ColorPrimary>*</ColorPrimary>Un número <ColorPrimary>*</ColorPrimary>Una letra</FormHelpText>
            <FormInput
              type="password"
              name="password"
              id="password"
              autoFocus
              required
              onChange={onChangeUserInput}
              value={user.password}
            />
          </FormColumn>
          <FormColumn right>
            <FormLabel htmlFor="password_confirmation">Confirmar nueva contraseña*</FormLabel>
            <FormHelpText>&nbsp;</FormHelpText>
            <FormInput
              type="password"
              name="password_confirmation"
              id="password_confirmation"
              required
              onChange={onChangeUserInput}
              value={user.password_confirmation}
            />
          </FormColumn>
        </FormRow>
        <FormRow right>
          <FormColumn auto>
            <Button
              type="submit"
              variant="primary"
              align="right"
              disabled={isChanging}
            >
              {isChanging ? 'Guardando...' : 'Guardar'}
            </Button>
          </FormColumn>
        </FormRow>
      </form>
    </Layout>
  );
};

export default ChangePassword;
