import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import classNames from 'classnames';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import isPlainObject from 'lodash/isPlainObject';
import dot from 'dot-object';
import Recaptcha from 'react-recaptcha';
import Form from 'react-bootstrap/Form';
import Layout from '../../components/Auth/Layout/Layout';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import ColorPrimary from '../../components/UI/Colors/ColorPrimary/ColorPrimary';
import TogglePassword from '../../components/Auth/TogglePassword/TogglePassword';
import Button from '../../components/UI/Button/Button';
import Message from '../../components/UI/Message/Message';
import * as actions from '../../actions';

const defaultPreregistro = {
  nombre_completo: '',
  celular: '',
  email: '',
  password: '',
  politicas_privacidad: false,
  terminos_condiciones: false,
  recaptcha: false,
  custodio: {
    nombre_completo: '',
    celular: '',
    correo_electronico: '',
    motivos: '',
    parentesco_id: ''
  }
};

const RegisterPage = () => {
  const recaptchaRef = React.useRef(null);
  const [togglePassword, setTogglePassword] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const [isVerified, setIsVerified] = React.useState(false);
  const [isSaved, setIsSaved] = React.useState(false);
  const parentescos = useSelector((state) => state.parentescos.parentescos);
  const avisoPrivacidad = useSelector((state) => state.avisoPrivacidad.avisoPrivacidad);
  const terminosCondiciones = useSelector((state) => state.terminosCondiciones.terminosCondiciones);
  const errors = useSelector((state) => state.preregistros.errors);
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {...defaultPreregistro},
    validationSchema: Yup.object({
      nombre_completo: Yup.string()
        .required('El campo "Nombre completo" es obligatorio.'),
      celular: Yup.string()
        .matches(/^\d+$/, 'Debe de ingresar un número de celular valido.')
        .length(10, 'El campo celular debe ser un número de 10 dígitos.')
        .required('El campo "Celular" es obligatorio.'),
      email: Yup.string()
        .email('El campo "Correo electrónico" debe ser una dirección de correo válida.')
        .required('El campo "Correo electrónico" es obligatorio.'),
      password: Yup.string()
        .matches(/^(?=.*[0-9])(?=.*[a-zA-Z])(.*){8,}$/, 'El formato del campo "Contraseña" es inválido.')
        .required('El campo "Contraseña" es obligatorio.'),
      politicas_privacidad: Yup.bool().oneOf([true]),
      terminos_condiciones: Yup.bool().oneOf([true]),
      recaptcha: Yup.bool().oneOf([true], 'Debe confirmar que no es un robot.'),
      custodio: Yup.object().shape({
        nombre_completo: Yup.string()
          .required('El campo "Nombre completo" es obligatorio.'),
        celular: Yup.string()
          .matches(/^\d+$/, 'Debe de ingresar un número de celular valido.')
          .length(10, 'El campo celular debe ser un número de 10 dígitos.')
          .required('El campo "Celular" es obligatorio.'),
        correo_electronico: Yup.string()
          .email('El campo "Correo electrónico" debe ser una dirección de correo válida.')
          .required('El campo "Correo electrónico" es obligatorio.'),
        parentesco_id: Yup.number()
          .required('El campo "Parentesco" es obligatorio.')
      })
    }),
    onSubmit: async (values) => {
      if (!isVerified) {
        return false;
      }

      if (await dispatch(actions.savePreregistro(values))) {
        setTogglePassword(false);
        setIsSaved(true);

        formik.resetForm({...defaultPreregistro});

        window.scrollTo(0, 0);
      }

      setIsVerified(false);
      recaptchaRef.current.reset();
      formik.setSubmitting(false);
    }
  });

  const onTogglePassword = (value) => {
    setTogglePassword(value);
  };

  const onRecaptchaLoaded = () => {
    setIsLoading(false);
  };

  const onVerifyRecaptcha = (response) => {
    if (response) {
      setIsVerified(true);
      formik.setFieldValue('recaptcha', true);
    } else {
      formik.setFieldValue('recaptcha', false);
    }
  };

  React.useEffect(() => {
    if (isPlainObject(errors)) {
      dot.object(errors);
      formik.setErrors(errors);
      formik.setSubmitting(false);
    }

    // eslint-disable-next-line
  }, [errors]);

  React.useEffect(() => {
    (async () => {
      await dispatch(actions.getParentescos());
      await dispatch(actions.getAvisoPrivacidad());
      await dispatch(actions.getTerminosCondiciones());
    })();

    window.scrollTo(0, 0);
    // eslint-disable-next-line
  }, []);

  if (isSaved) {
    return (
      <Message title="¡Continua con tu registro!">
        <Message.Content>
          Te hemos enviado un enlace a tu correo electrónico<br /> para poder continuar con tu registro.
        </Message.Content>
        <Button variant="primary" align="center" link to="/">
          Regresar al inicio
        </Button>
      </Message>
    );
  }

  return (
    <Layout page="register">
      {(styles) => (
        <>
          <h2 className={styles.Title}>Hoy, mañana y siempre</h2>
          <p className={styles.Paragraph}>Un legado de amor y seguridad para sus seres queridos por si algún día llegas a faltar.</p>
          <p className={styles.Paragraph}>Deja a resguardo en línea todo lo que a tus seres queridos desees transmitir como bienes, pensamientos, documentos y recuerdos importantes para ti.</p>
          <Form className={styles.Form} noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
            <Form.Group className={styles.Group} controlId="nombre_completo">
              <Form.Label className={styles.Label}>Nombre completo</Form.Label>
              <Form.Control
                type="text"
                name="nombre_completo"
                className={styles.Control}
                autoFocus
                maxLength={100}
                isInvalid={formik.errors.nombre_completo && formik.touched.nombre_completo}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.nombre_completo}
              />
              {formik.errors.nombre_completo && formik.touched.nombre_completo && (
                <Form.Control.Feedback type="invalid">
                  {formik.errors.nombre_completo}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Form.Group className={styles.Group} controlId="celular">
              <Form.Label className={styles.Label}>Número de celular</Form.Label>
              <Form.Text className={classNames(styles.Text, styles.Top)}>Debe ingresar 10 dígitos</Form.Text>
              <Form.Control
                type="tel"
                name="celular"
                minLength="10"
                maxLength="10"
                className={styles.Control}
                isInvalid={formik.errors.celular && formik.touched.celular}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.celular}
              />
              {formik.errors.celular && formik.touched.celular && (
                <Form.Control.Feedback type="invalid">
                  {formik.errors.celular}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Form.Group className={styles.Group} controlId="email">
              <Form.Label className={styles.Label}>Correo electrónico</Form.Label>
              <Form.Text className={classNames(styles.Text, styles.Top)}>Tu correo electrónico será tu nombre de usuario</Form.Text>
              <Form.Control
                type="email"
                name="email"
                className={styles.Control}
                isInvalid={formik.errors.email && formik.touched.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.email}
              />
              {formik.errors.email && formik.touched.email && (
                <Form.Control.Feedback type="invalid">
                  {formik.errors.email}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Form.Group className={styles.Group} controlId="password">
              <Form.Label className={classNames(styles.Label, styles.Password)}>
                Contraseña <TogglePassword onToggle={onTogglePassword} />
              </Form.Label>
              <Form.Control
                type={togglePassword ? 'text' : 'password'}
                name="password"
                className={styles.Control}
                isInvalid={formik.errors.password && formik.touched.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.password}
              />
              <Form.Text className={classNames(styles.Text, styles.Bottom)}><ColorPrimary>*</ColorPrimary>8 Caracteres mínimo <ColorPrimary>*</ColorPrimary>Un número <ColorPrimary>*</ColorPrimary>Una letra</Form.Text>
              {formik.errors.password && formik.touched.password && (
                <Form.Control.Feedback type="invalid">
                  {formik.errors.password}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <h3 className={styles.Subtitle}>Datos de tu custodio</h3>
            <Form.Group className={styles.Group} controlId="custodio.nombre_completo">
              <Form.Label className={styles.Label}>Nombre completo</Form.Label>
              <Form.Control
                type="text"
                name="custodio.nombre_completo"
                className={styles.Control}
                maxLength={100}
                isInvalid={formik.errors.custodio?.nombre_completo && formik.touched.custodio?.nombre_completo}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.custodio?.nombre_completo}
              />
              {formik.errors.custodio?.nombre_completo && formik.touched.custodio?.nombre_completo && (
                <Form.Control.Feedback type="invalid">
                  {formik.errors.custodio?.nombre_completo}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Form.Group className={styles.Group} controlId="custodio.celular">
              <Form.Label className={styles.Label}>Número de celular</Form.Label>
              <Form.Text className={classNames(styles.Text, styles.Top)}>Debe ingresar 10 dígitos</Form.Text>
              <Form.Control
                type="tel"
                name="custodio.celular"
                minLength="10"
                maxLength="10"
                className={styles.Control}
                isInvalid={formik.errors.custodio?.celular && formik.touched.custodio?.celular}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.custodio?.celular}
              />
              {formik.errors.custodio?.celular && formik.touched.custodio?.celular && (
                <Form.Control.Feedback type="invalid">
                  {formik.errors.custodio?.celular}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Form.Group className={styles.Group} controlId="custodio.correo_electronico">
              <Form.Label className={styles.Label}>Correo electrónico</Form.Label>
              <Form.Control
                type="email"
                name="custodio.correo_electronico"
                className={styles.Control}
                isInvalid={formik.errors.custodio?.correo_electronico && formik.touched.custodio?.correo_electronico}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.custodio?.correo_electronico}
              />
              {formik.errors.custodio?.correo_electronico && formik.touched.custodio?.correo_electronico && (
                <Form.Control.Feedback type="invalid">
                  {formik.errors.custodio?.correo_electronico}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Form.Group className={styles.Group} controlId="custodio.motivos">
              <Form.Label className={styles.Label}>¿Porqué te escogí a ti como custodio?</Form.Label>
              <Form.Text className={classNames(styles.Text, styles.Top)}>Opcional</Form.Text>
              <Form.Control
                as="textarea"
                name="custodio.motivos"
                className={styles.Control}
                rows={2}
                maxLength={500}
                isInvalid={formik.errors.custodio?.motivos && formik.touched.custodio?.motivos}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.custodio?.motivos}
              />
              {formik.errors.custodio?.motivos && formik.touched.custodio?.motivos && (
                <Form.Control.Feedback type="invalid">
                  {formik.errors.custodio?.motivos}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Form.Group className={styles.Group} controlId="custodio.parentesco_id">
              <Form.Label className={styles.Label}>Parentesco</Form.Label>
              <Form.Control
                as="select"
                name="custodio.parentesco_id"
                className={styles.Control}
                isInvalid={formik.errors.custodio?.parentesco_id && formik.touched.custodio?.parentesco_id}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.custodio?.parentesco_id}
              >
                <option value="">Seleccione...</option>
                {parentescos.map((parentesco) => (
                  <option
                    key={parentesco.id}
                    value={parentesco.id}
                  >
                    {parentesco.nombre}
                  </option>
                ))}
              </Form.Control>
              {formik.errors.custodio?.parentesco_id && formik.touched.custodio?.parentesco_id && (
                <Form.Control.Feedback type="invalid">
                  {formik.errors.custodio?.parentesco_id}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Form.Group className={styles.Group}>
              <Form.Check
                custom
                type="checkbox"
                name="politicas_privacidad"
                id="politicas_privacidad"
                className={styles.Check}
                checked={formik.values.politicas_privacidad}
                label={<>Acepto la <a href={avisoPrivacidad.documento_digital} className={styles.Link} rel="noopener noreferrer" target="_blank"><ColorPrimary>política de privacidad</ColorPrimary></a></>}
                isInvalid={formik.errors.politicas_privacidad && formik.touched.politicas_privacidad}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.politicas_privacidad}
              />
              <Form.Check
                custom
                type="checkbox"
                name="terminos_condiciones"
                id="terminos_condiciones"
                className={styles.Check}
                checked={formik.values.terminos_condiciones}
                label={<>Acepto los <a href={terminosCondiciones.documento_digital} className={styles.Link} rel="noopener noreferrer" target="_blank"><ColorPrimary>términos y condiciones</ColorPrimary></a></>}
                isInvalid={formik.errors.terminos_condiciones && formik.touched.terminos_condiciones}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.terminos_condiciones}
              />
            </Form.Group>
            <Form.Group>
              {isLoading && <Paragraph>Cargando captcha...</Paragraph>}
              <Recaptcha
                ref={recaptchaRef}
                sitekey="6LdhmKsZAAAAAKyKuj9NRlhu0tTejyb6AmNT2x3E"
                render="explicit"
                hl="es-419"
                verifyCallback={onVerifyRecaptcha}
                onloadCallback={onRecaptchaLoaded}
              />
              {formik.errors.recaptcha && formik.touched.recaptcha && (
                <Form.Control.Feedback type="invalid" className="d-block">
                  {formik.errors.recaptcha}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Button
              type="submit"
              variant="primary"
              className={styles.Button}
              disabled={formik.isSubmitting}
            >
              {formik.isSubmitting ? 'Registrándote...' : 'Registrarme'}
            </Button>
          </Form>
        </>
      )}
    </Layout>
  );
};

export default RegisterPage;
