import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import Recaptcha from 'react-recaptcha';
import Alert from 'react-bootstrap/Alert';
import Form from 'react-bootstrap/Form';
import Layout from '../../components/Auth/Layout/Layout';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Button from '../../components/UI/Button/Button';
import * as helpers from '../../helpers';
import * as actions from '../../actions';

const PasswordReset = () => {
  const recaptchaRef = React.useRef(null);
  const [isSucceed, setIsSucceed] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const [isVerified, setIsVerified] = React.useState(false);
  const errors = useSelector((state) => state.auth.errors);
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      email: '',
      confirm_email: '',
      recaptcha: false
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('El campo "Correo electrónico" debe ser una dirección de correo válida.')
        .required('El campo "Correo electrónico" es obligatorio.'),
      confirm_email: Yup.string()
        .oneOf([Yup.ref('email'), null], 'Los correos electrónicos deben coincidir.'),
      recaptcha: Yup.bool()
        .oneOf([true], 'Debe confirmar que no es un robot.')
    }),
    onSubmit: async (values) => {
      setIsSucceed(false);

      if (!isVerified) {
        return false;
      }

      const response = await dispatch(actions.passwordReset(values));

      if (response) {
        setIsSucceed(true);
        formik.resetForm();
      }

      recaptchaRef.current.reset();
      formik.setSubmitting(false);
    }
  });

  const onRecaptchaLoaded = () => {
    setIsLoading(false);
  };

  const onVerifyRecaptcha = (response) => {
    if (response) {
      setIsVerified(true);
      formik.setFieldValue('recaptcha', true);
    } else {
      formik.setFieldValue('recaptcha', false);
    }
  };

  const onHideSuccessMessage = () => {
    setIsSucceed(false);
  };

  const onResetErrors = () => {
    dispatch(actions.resetAuthErrors());
  };

  React.useEffect(() => {
    if (helpers.isObject(errors)) {
      formik.setErrors(errors);
      formik.setSubmitting(false);
      setIsVerified(false);
      recaptchaRef.current.reset();
    }

    // eslint-disable-next-line
  }, [errors]);

  return (
    <React.Fragment>
      {isSucceed && (
        <Alert variant="success" className="position-absolute top center shadow" onClose={onHideSuccessMessage} dismissible>
          <Paragraph>Tu nueva contraseña ha sido enviada al correo electrónico que capturaste.</Paragraph>
        </Alert>
      )}
      {helpers.isString(errors) && (
        <Alert variant="danger" className="position-absolute top center shadow" onClose={onResetErrors} dismissible>
          <Paragraph>
            {helpers.isString(errors) && errors}
          </Paragraph>
        </Alert>
      )}
      <Layout page="login">
        {(styles) => (
          <>
            <h2 className={styles.Title}>Restablecer contraseña</h2>
            <Form className={styles.Form} noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
              <Form.Group className={styles.Group} controlId="email">
                <Form.Label className={styles.Label}>Correo electrónico</Form.Label>
                <Form.Control
                  type="email"
                  name="email"
                  className={styles.Control}
                  autoFocus
                  isInvalid={formik.errors.email && formik.touched.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                />
                {formik.errors.email && formik.touched.email && (
                  <Form.Control.Feedback type="invalid">
                    {formik.errors.email}
                  </Form.Control.Feedback>
                )}
              </Form.Group>
              <Form.Group className={styles.Group} controlId="confirm_email">
                <Form.Label className={styles.Label}>Ingresa nuevamente tu correo electrónico</Form.Label>
                <Form.Control
                  type="email"
                  name="confirm_email"
                  className={styles.Control}
                  isInvalid={formik.errors.confirm_email && formik.touched.confirm_email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.confirm_email}
                />
                {formik.errors.confirm_email && formik.touched.confirm_email && (
                  <Form.Control.Feedback type="invalid">
                    {formik.errors.confirm_email}
                  </Form.Control.Feedback>
                )}
              </Form.Group>
              <Form.Group>
                {isLoading && <Paragraph>Cargando captcha...</Paragraph>}
                <Recaptcha
                  ref={recaptchaRef}
                  sitekey="6LdhmKsZAAAAAKyKuj9NRlhu0tTejyb6AmNT2x3E"
                  render="explicit"
                  hl="es-419"
                  verifyCallback={onVerifyRecaptcha}
                  onloadCallback={onRecaptchaLoaded}
                />
                {formik.errors.recaptcha && formik.touched.recaptcha && (
                  <Form.Control.Feedback type="invalid" className="d-block">
                    {formik.errors.recaptcha}
                  </Form.Control.Feedback>
                )}
              </Form.Group>
              <Button
                type="submit"
                variant="primary"
                className={styles.Button}
                disabled={formik.isSubmitting}
              >
                {formik.isSubmitting ? 'Restableciendo...' : 'Restablecer'}
              </Button>
            </Form>
          </>
        )}
      </Layout>
    </React.Fragment>
  );
};

export default PasswordReset;
