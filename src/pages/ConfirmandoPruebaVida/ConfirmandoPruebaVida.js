import React from 'react';
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import isEmpty from 'lodash/isEmpty';
import Loader from '../../components/UI/Loader/Loader';
import PruebaVidaInvalida from '../../components/Messages/PruebaVidaInvalida/PruebaVidaInvalida';
import PruebaVidaConfirmada from '../../components/Messages/PruebaVidaConfirmada/PruebaVidaConfirmada';
import PruebaVidaExpirada from '../../components/Messages/PruebaVidaExpirada/PruebaVidaExpirada';
import ConfirmacionPruebaVida from '../../components/Messages/ConfirmacionPruebaVida/ConfirmacionPruebaVida';
import * as actions from '../../actions';
import DatosAccesoEntregados from '../../components/Messages/DatosAccesoEntregados/DatosAccesoEntregados';

const ConfirmandoPruebaVida = ({ custodio }) => {
  const pruebaVida = useSelector((state) => state.custodios.pruebaVida);
  const isConfirmando = useSelector((state) => state.custodios.isConfirmando);
  const dispatch = useDispatch();
  const { url } = useParams();

  const getPrimerNombre = (nombreCompleto) => {
    return String(nombreCompleto)
      .split(' ')
      .shift();
  };

  const getUserNombreCompleto = () => {
    return pruebaVida?.user?.perfil?.nombre_completo;
  };

  const getFechaRespuesta = () => {
    return moment(pruebaVida?.fecha_respuesta).format('DD [de] MMMM [de] YYYY [a las] HH:mm');
  };

  const getFechaLimite = () => {
    return moment(pruebaVida?.fecha_limite, 'YYYY-MM-DD').format('LL');
  };

  React.useEffect(() => {
    dispatch(actions.confirmarPruebaVidaCustodio(url));

    // eslint-disable-next-line
  }, []);

  if (isConfirmando) {
    return <Loader title="Confirmando la prueba de vida" />;
  }

  if (isEmpty(pruebaVida)) {
    return <PruebaVidaInvalida custodio={custodio} />
  }

  if (!pruebaVida?.is_confirmation && pruebaVida?.is_confirmed) {
    return (
      <PruebaVidaConfirmada
        custodio={custodio}
        primerNombre={getPrimerNombre(getUserNombreCompleto())}
        fechaRespuesta={getFechaRespuesta()}
      />
    );
  }

  if (pruebaVida?.user.is_delivered) {
    return (
      <DatosAccesoEntregados
        primerNombre={getPrimerNombre(getUserNombreCompleto())}
      />
    );
  }

  if (pruebaVida?.is_expired) {
    return (
      <PruebaVidaExpirada
        custodio={custodio}
        primerNombre={getPrimerNombre(getUserNombreCompleto())}
        fechaLimite={getFechaLimite()}
      />
    );
  }

  return (
    <ConfirmacionPruebaVida
      custodio={custodio}
      primerNombre={getPrimerNombre(getUserNombreCompleto())}
      fechaRespuesta={getFechaRespuesta()}
    />
  );
};

ConfirmandoPruebaVida.defaultProps = {
  custodio: false
};

ConfirmandoPruebaVida.propTypes = {
  custodio: PropTypes.bool
};

export default ConfirmandoPruebaVida;
