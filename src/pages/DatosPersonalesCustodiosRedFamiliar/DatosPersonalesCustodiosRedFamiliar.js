import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Feedback from '../../components/UI/Feedback/Feedback';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import DatosPersonales from '../../components/PanelUsuario/DatosPersonalesCustodiosRedFamiliar/DatosPersonales/DatosPersonales';
import Custodios from '../../components/PanelUsuario/DatosPersonalesCustodiosRedFamiliar/Custodios/Custodios';
import RedFamiliar from '../../components/PanelUsuario/DatosPersonalesCustodiosRedFamiliar/RedFamiliar/RedFamiliar';
import * as datosPersonalesActions from '../../actions/datos-personales-custodios-red-familiar/datos-personales';
import * as custodiosActions from '../../actions/datos-personales-custodios-red-familiar/custodios';
import * as redFamiliarActions from '../../actions/datos-personales-custodios-red-familiar/red-familiar';
import * as helpers from '../../helpers';

const DatosPersonalesCustodiosRedFamiliar = () => {
  const [activeKey, setActiveKey] = React.useState('0');
  const [updatedAt, setUpdatedAt] = React.useState(null);
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const user = useSelector((state) => state.auth.user);

  const isLoading = useSelector((state) => {
    return state.datosPersonales.isLoading
        || state.custodios.isLoading
        || state.redFamiliar.isGetting;
  });

  const errors = useSelector((state) => {
    return state.datosPersonales.errors
        || state.custodios.errors
        || state.redFamiliar.errors;
  });

  const dispatch = useDispatch();

  const isPlanPremium = () =>
    user?.suscripcion?.is_premium;

  const isSuscripcionActiva = () =>
    user?.suscripcion?.is_activo;

  const onSelectKey = (key) =>
    setActiveKey(key);

  const getUpdatedAt = () => {
    if (isLoading) {
      return 'Cargando...';
    }

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const onHideErrors = () => {
    dispatch(datosPersonalesActions.resetDatosPersonalesErrors());
    dispatch(custodiosActions.resetErrors());
    dispatch(redFamiliarActions.resetErrors());
  };

  React.useEffect(() => {
    if (!helpers.isNull(errors)) {
      window.scrollTo(0, 0);
    }
  }, [errors]);

  return (
    <Layout title="Datos personales, custodios y red familiar">
      <StepProgressBar activeKey={activeKey}>
        <StepProgressBar.Step eventKey="0" onSelect={onSelectKey}>1. Datos personales</StepProgressBar.Step>
        <StepProgressBar.Step eventKey="1" onSelect={onSelectKey}>2. Custodios</StepProgressBar.Step>
        {(isPlanPremium() && isSuscripcionActiva()) && (
          <StepProgressBar.Step eventKey="2" onSelect={onSelectKey}>3. Red familiar</StepProgressBar.Step>
        )}
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>*Campos obligatorios</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onHideErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onHideErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      {(activeKey === '0') && (
        <DatosPersonales
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(activeKey === '1') && (
        <Custodios
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(activeKey === '2' && (isPlanPremium() && isSuscripcionActiva())) && (
        <RedFamiliar
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      <FormToast
        variant="success"
        show={showToast}
        title="Mensaje de éxito"
        message={toastMessage}
        onClose={() => setShowToast(false)}
      />
    </Layout>
  );
};

export default DatosPersonalesCustodiosRedFamiliar;
