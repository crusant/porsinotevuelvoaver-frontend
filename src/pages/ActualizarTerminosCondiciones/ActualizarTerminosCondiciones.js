import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Feedback from '../../components/UI/Feedback/Feedback';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormHelpText from '../../components/UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../components/UI/Form/FormInput/FormInput';
import FormFile from '../../components/UI/Form/FormFile/FormFile';
import File from '../../components/UI/File/File';
import Button from '../../components/UI/Button/Button';
import * as helpers from '../../helpers';
import * as actions from '../../actions';

const ActualizarTerminosCondiciones = () => {
  const terminosCondiciones = useSelector((state) => state.terminosCondiciones.terminosCondiciones);
  const isUpdating = useSelector((state) => state.terminosCondiciones.isUpdating);
  const errors = useSelector((state) => state.terminosCondiciones.errors);
  const dispatch = useDispatch();

  const onChangeTerminosCondicionesInput = (event) => {
    const input = event.target;

    dispatch(actions.changeTerminosCondicionesInputs(input.name, input.value));
  };

  const onChangeDocumentoDigitalInput = (value) => {
    dispatch(actions.changeTerminosCondicionesInputs('documento_digital', value));
  };

  const onRemoveDocumentoDigitalInput = () => {
    dispatch(actions.changeTerminosCondicionesInputs('documento_digital', ''));
  };

  const onResetTerminosCondicionesErrors = () => {
    dispatch(actions.resetTerminosCondicionesErrors());
  };

  const onUpdateTerminosCondiciones = (event) => {
    event.preventDefault();

    const formData = new FormData();
    helpers.buildFormData(formData, terminosCondiciones);
    dispatch(actions.updateTerminosCondiciones(formData));
  };

  React.useEffect(() => {
    dispatch(actions.getTerminosCondiciones());

    return () => {
      dispatch(actions.resetTerminosCondicionesToStartup());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Actualizar términos y condiciones">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Actualizar términos y condiciones</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>*Campos obligatorios</Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetTerminosCondicionesErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onResetTerminosCondicionesErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <form onSubmit={onUpdateTerminosCondiciones}>
        <FormRow>
          <FormColumn>
            <FormLabel htmlFor="nombre">Nombre*</FormLabel>
            <FormInput
              type="text"
              name="nombre"
              id="nombre"
              required
              maxLength={100}
              onChange={onChangeTerminosCondicionesInput}
              value={terminosCondiciones.nombre}
            />
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            <FormLabel htmlFor="documento_digital">Archivo*</FormLabel>
            <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF.</FormHelpText>
            {helpers.isEmptyString(terminosCondiciones.documento_digital) ? (
              <FormFile onChange={onChangeDocumentoDigitalInput} />
            ) : (
              <File
                file={terminosCondiciones.documento_digital}
                onRemove={onRemoveDocumentoDigitalInput}
              />
            )}
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            <Button
              type="submit"
              variant="primary"
              align="right"
              disabled={isUpdating}
            >
              {isUpdating ? 'Actualizando...' : 'Actualizar'}
            </Button>
          </FormColumn>
        </FormRow>
      </form>
    </Layout>
  );
};

export default ActualizarTerminosCondiciones;
