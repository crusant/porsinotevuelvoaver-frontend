import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { isChrome } from 'react-device-detect';
import classNames from 'classnames';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Headband from '../../components/UI/Headband/Headband';
import Button from '../../components/UI/Button/Button';
import Logo from '../../components/Logo/Logo';
import Title from '../../components/Title/Title';
import User from '../../components/User/User';
import IconCRM from '../../components/UI/Icons/IconCRM/IconCRM';
import IconMen from '../../components/UI/Icons/IconMen/IconMen';
import IconExpiratedDate from '../../components/UI/Icons/IconExpiratedDate/IconExpiratedDate';
import IconShield from '../../components/UI/Icons/IconShield/IconShield';
import IconPayment from '../../components/UI/Icons/IconPayment/IconPayment';
import IconWork from '../../components/UI/Icons/IconWork/IconWork';
import IconWishList from '../../components/UI/Icons/IconWishList/IconWishList';
import IconPet from '../../components/UI/Icons/IconPet/IconPet';
import IconLetter from '../../components/UI/Icons/IconLetter/IconLetter';
import IconSocialMedia from '../../components/UI/Icons/IconSocialMedia/IconSocialMedia';
import IconTime from '../../components/UI/Icons/IconTime/IconTime';
import IconHistory from '../../components/UI/Icons/IconHistory/IconHistory';
import IconGabinet from '../../components/UI/Icons/IconGabinet/IconGabinet';
import IconPremium from '../../components/UI/Icons/IconPremium/IconPremium';
import IconChecked from '../../components/UI/Icons/IconChecked/IconChecked';
import IconHealthy from '../../components/UI/Icons/IconHealthy/IconHealthy';
import IconMessage from '../../components/UI/Icons/IconMessage/IconMenssage';
import IconHandHeart from '../../components/UI/Icons/IconHandHeart/IconHandHeart';
import IconLocked from '../../components/UI/Icons/IconLocked/IconLocked';
import IconContract from '../../components/UI/Icons/IconContract/IconContract';
import IconDownload from '../../components/UI/Icons/IconDownload/IconDownload';
import * as actions from '../../actions';
import useUser from '../../hooks/useUser';
import { API_URL } from '../../constants';
import styles from './MenuPrincipal.module.scss';

const MenuPrincipal = () => {
  const {
    token,
    is_delivered,
    isAdministrador,
    isPlanPremium,
    bePlanPremium,
    isSuscripcionActiva,
    isSuscripcionPagada
  } = useUser();

  const menuPrincipal = useSelector((state) => state.menuPrincipal.menuPrincipal);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(actions.getMenuPrincipal());

    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      {(!isAdministrador() && !isPlanPremium()) && (
        <Headband>
          {(styles) => (
            bePlanPremium() ? (
              <>Tu plan <strong>Premium</strong> está en proceso de acreditación.</>
            ) : (
              <>Tienes una cuenta gratis con acceso limitado. Accede a todos los beneficios que tenemos para tí. <Button variant="light" className={classNames(styles.Button, styles.Right)} link to="/planes">Comprar plan premium</Button></>
            )
          )}
        </Headband>
      )}
      {(!isAdministrador() && isPlanPremium()) && (
        <React.Fragment>
          {(!isSuscripcionActiva() && !is_delivered) && (
            <Headband>
              {(styles) => (
                <>Tu suscripción PREMIUM ha expirado, por lo que ahora tu cuenta únicamente tiene los beneficios del plan GRATIS.<br /> Renueva tu suscripción y sigue disfrutando de los beneficios que tenemos para tí. <Button variant="light" className={classNames(styles.Button, styles.Right)} link to="/planes">Renovar suscripción</Button></>
              )}
            </Headband>
          )}
          {!isSuscripcionPagada() && (
            <Headband>
              {(styles) => (
                <>Tu nuevo plan <strong>Premium</strong> está en proceso de acreditación.</>
              )}
            </Headband>
          )}
        </React.Fragment>
      )}
      <Container fluid className={styles.Header}>
        <div className={styles.LogoContainer}>
          <Link to="/">
            <Logo height="70px" />
          </Link>
        </div>
        <div className={styles.TitleContainer}>
          <Title className={styles.Title}>
            {isAdministrador() ? 'Bienvenido' : 'Bienvenido al menú principal'}
          </Title>
        </div>
        <div className={styles.UserContainer}>
          <User />
        </div>
      </Container>
      {isAdministrador() ? (
        <Container className={styles.Body}>
          <Row>
            <Col>
              <Link className={classNames(styles.Item, styles.Left)} to="/configuracion-de-envio-de-pruebas-de-vida">
                <IconHealthy className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Configuración de envío de pruebas de vida</p>
              </Link>
            </Col>
            <Col>
              <Link className={classNames(styles.Item, styles.Right)} to="/configuracion-de-tiempo-de-respuesta-a-pruebas-de-vida">
                <IconMessage className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Configuración de tiempo de respuesta a pruebas de vida</p>
              </Link>
            </Col>
          </Row>
          <Row>
            <Col>
              <Link className={classNames(styles.Item, styles.Left)} to="/configuracion-de-tiempo-de-respuesta-de-pruebas-de-vida-a-custodios">
                <IconHandHeart className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Configuración de tiempo de respuesta de pruebas de vida a custodios</p>
              </Link>
            </Col>
            <Col>
              <Link className={classNames(styles.Item, styles.Right)} to="/usuarios-registrados">
                <IconMen className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Usuarios registrados</p>
              </Link>
            </Col>
          </Row>
          <Row>
            <Col>
              <Link className={classNames(styles.Item, styles.Left)} to="/enlaces-expirados">
                <IconExpiratedDate className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Enlaces expirados</p>
              </Link>
            </Col>
            <Col>
              <Link className={classNames(styles.Item, styles.Right)} to="/actualizar-aviso-de-privacidad">
                <IconLocked className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Actualizar aviso de privacidad</p>
              </Link>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col md={6}>
              <Link className={classNames(styles.Item, styles.Center)} to="/actualizar-terminos-y-condiciones">
                <IconContract className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Actualizar términos y condiciones</p>
              </Link>
            </Col>
          </Row>
        </Container>
      ) : (
        <Container className={styles.Body}>
          <Row className={classNames(styles.Introduction, 'justify-content-center')}>
            <Col md={11}>
              <p className={styles.Paragraph}>Cada uno de los apartados y formularios del menú esta diseñado para cubrir las necesidades de información que tendrán tus seres queridos si llegarás a faltar en su vida.</p>
              <p className={styles.Paragraph}>Te sugerimos explorarlos y llenar aquellos que se adapten a tus necesidades y deseos.</p>
              {!isChrome && (
                <p className={classNames(styles.Paragraph, styles.Primary)}>Te recomendamos ingresar a Por si no te vuelvo a ver desde el navegador <a href="https://www.google.com/intl/es_mx/chrome/" className={styles.Link} rel="noopener noreferrer" target="_blank">Google Chrome</a>.</p>
              )}
            </Col>
          </Row>
          <Row>
            <Col>
              <Link className={classNames(styles.Item, styles.Left, { [styles.Free]: !isPlanPremium() || !isSuscripcionActiva() })} to="/datos-personales-custodios-y-red-familiar">
                {menuPrincipal.isCompletedDatosPersonalesCustodiosRedFamiliar && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconCRM className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Datos personales, custodia y red familiar</p>
              </Link>
            </Col>
            <Col>
              <Link className={classNames(styles.Item, styles.Right, { [styles.Free]: !isPlanPremium() || !isSuscripcionActiva() })} to="/asesores-y-contactos">
                {menuPrincipal.isCompletedAsesoresContactos && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconMen className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Asesores y contactos</p>
              </Link>
            </Col>
          </Row>
          <Row>
            <Col>
              <Link className={classNames(styles.Item, styles.Left, { [styles.Disabled]: !isPlanPremium() || !isSuscripcionActiva() })} to="/polizas-de-seguros">
                {!isPlanPremium() && (
                  <div className={classNames(styles.Indicator, styles.Uppercase)}>
                    <IconPremium className={styles.Icon} height="16px" /> Plan premium
                  </div>
                )}
              {menuPrincipal.isCompletedPolizasSeguros && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconShield className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Pólizas de seguros</p>
              </Link>
            </Col>
            <Col>
              <Link className={classNames(styles.Item, styles.Right, { [styles.Disabled]: !isPlanPremium() || !isSuscripcionActiva() })} to="/patrimonio">
                {!isPlanPremium() && (
                  <div className={classNames(styles.Indicator, styles.Uppercase)}>
                    <IconPremium className={styles.Icon} height="16px" /> Plan premium
                  </div>
                )}
                {menuPrincipal.isCompletedPatrimonio && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconPayment className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Patrimonio</p>
              </Link>
            </Col>
          </Row>
          <Row>
            <Col>
              <Link className={classNames(styles.Item, styles.Left, { [styles.Disabled]: !isPlanPremium() || !isSuscripcionActiva() })} to="/empleo-y-pension">
                {!isPlanPremium() && (
                  <div className={classNames(styles.Indicator, styles.Uppercase)}>
                    <IconPremium className={styles.Icon} height="16px" /> Plan premium
                  </div>
                )}
                {menuPrincipal.isCompletedEmpleoPension && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconWork className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Empleo y pensión</p>
              </Link>
            </Col>
            <Col>
              <Link className={classNames(styles.Item, styles.Right, { [styles.Disabled]: !isPlanPremium() || !isSuscripcionActiva() })} to="/deseos-postumos">
                {!isPlanPremium() && (
                  <div className={classNames(styles.Indicator, styles.Uppercase)}>
                    <IconPremium className={styles.Icon} height="16px" /> Plan premium
                  </div>
                )}
                {menuPrincipal.isCompletedUltimosDeseos && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconWishList className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Deseos póstumos</p>
              </Link>
            </Col>
          </Row>
          <Row>
            <Col>
              <Link className={classNames(styles.Item, styles.Left, { [styles.Disabled]: !isPlanPremium() || !isSuscripcionActiva() })} to="/mascotas">
                {!isPlanPremium() && (
                  <div className={classNames(styles.Indicator, styles.Uppercase)}>
                    <IconPremium className={styles.Icon} height="16px" /> Plan premium
                  </div>
                )}
                {menuPrincipal.isCompletedMascotas && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconPet className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Mascotas</p>
              </Link>
            </Col>
            <Col>
              <Link className={classNames(styles.Item, styles.Right, { [styles.Free]: !isPlanPremium() || !isSuscripcionActiva() })} to="/instrucciones-particulares">
                {menuPrincipal.isCompletedInstruccionesParticulares && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconLetter className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Instrucciones a custodios</p>
              </Link>
            </Col>
          </Row>
          <Row>
            <Col>
              <Link className={classNames(styles.Item, styles.Left, { [styles.Disabled]: !isPlanPremium() || !isSuscripcionActiva() })} to="/correo-y-redes-sociales">
                {!isPlanPremium() && (
                  <div className={classNames(styles.Indicator, styles.Uppercase)}>
                    <IconPremium className={styles.Icon} height="16px" /> Plan premium
                  </div>
                )}
                {menuPrincipal.isCompletedCorreosElectronicosRedesSociales && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconSocialMedia className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Correo y redes sociales</p>
              </Link>
            </Col>
            <Col>
              <Link className={classNames(styles.Item, styles.Right, { [styles.Disabled]: !isPlanPremium() || !isSuscripcionActiva() })} to="/mensajes-postumos">
                {!isPlanPremium() && (
                  <div className={classNames(styles.Indicator, styles.Uppercase)}>
                    <IconPremium className={styles.Icon} height="16px" /> Plan premium
                  </div>
                )}
                {menuPrincipal.isCompletedMensajesPostumos && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconTime className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Mensajes postumos</p>
              </Link>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col>
              <Link className={classNames(styles.Item, styles.Left, { [styles.Disabled]: !isPlanPremium() || !isSuscripcionActiva() })} to="/mi-historia-y-legado">
                {!isPlanPremium() && (
                  <div className={classNames(styles.Indicator, styles.Uppercase)}>
                    <IconPremium className={styles.Icon} height="16px" /> Plan premium
                  </div>
                )}
                {menuPrincipal.isCompletedMiHistoriaLegado && (
                  <div className={styles.Indicator}>
                    <IconChecked className={styles.Icon} height="16px" /> 100 % completado
                  </div>
                )}
                <IconHistory className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Mi historia y legado</p>
              </Link>
            </Col>
            <Col>
              <Link className={classNames(styles.Item, styles.Right, { [styles.Disabled]: !isPlanPremium() || !isSuscripcionActiva() })} to="/documentos-digitales-y-ubicacion-fisica">
                {!isPlanPremium() && (
                  <div className={classNames(styles.Indicator, styles.Uppercase)}>
                    <IconPremium className={styles.Icon} height="16px" /> Plan premium
                  </div>
                )}
                <IconGabinet className={styles.Icon} width="80px" />
                <p className={styles.Paragraph}>Documentos digitales y ubicación física</p>
              </Link>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col md="6">
              <a
                href={isPlanPremium() ? `${API_URL}aplicacion/formularios?token=${token}` : '!#'}
                className={classNames(styles.Item, styles.Center, { [styles.Disabled]: !isPlanPremium() || !isSuscripcionActiva() })}
                target="_blank"
                rel="noopener noreferrer"
              >
                {!isPlanPremium() && (
                  <div className={classNames(styles.Indicator, styles.Uppercase)}>
                    <IconPremium className={styles.Icon} height="16px" /> Plan premium
                  </div>
                )}
                <IconDownload className={styles.Icon} width="30px" />
                <p className={styles.Paragraph}>Descargar formularios</p>
              </a>
            </Col>
          </Row>
        </Container>
      )}
    </React.Fragment>
  );
};

export default MenuPrincipal;
