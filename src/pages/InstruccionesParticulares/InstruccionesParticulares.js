import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Feedback from '../../components/UI/Feedback/Feedback';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormText from '../../components/UI/Form/FormText/FormText';
import FormHelpText from '../../components/UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../components/UI/Form/FormInput/FormInput';
import FormFile from '../../components/UI/Form/FormFile/FormFile';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import Button from '../../components/UI/Button/Button';
import AudioRecorder from '../../components/AudioRecorder/AudioRecorder';
import AudioPlayer from '../../components/UI/AudioPlayer/AudioPlayer';
import AudioDownloader from '../../components/UI/AudioDownloader/AudioDownloader';
import VideoRecorder from '../../components/VideoRecorder/VideoRecorder';
import SelectedVideo from '../../components/SelectedVideo/SelectedVideo';
import SavedVideo from '../../components/SavedVideo/SavedVideo';
import * as actions from '../../actions';
import * as helpers from '../../helpers';

const InstruccionesParticulares = () => {
  const [audio, setAudio] = React.useState(null);
  const [video, setVideo] = React.useState(null);
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const user = useSelector((state) => state.auth.user);
  const instruccionesParticulares = useSelector((state) => state.instruccionesParticulares.instruccionesParticulares);
  const isLoading = useSelector((state) => state.instruccionesParticulares.isLoading);
  const isSaving = useSelector((state) => state.instruccionesParticulares.isSaving);
  const isDownloadingAudio = useSelector((state) => state.instruccionesParticulares.isDownloadingAudio);
  const isDownloadingVideo = useSelector((state) => state.instruccionesParticulares.isDownloadingVideo);
  const errors = useSelector((state) => state.instruccionesParticulares.errors);
  const dispatch = useDispatch();

  const canWrite = () =>
    user?.can_write;

  const isPlanPremium = () =>
    user?.suscripcion?.is_premium;

  const hasAudio = () => {
    return (audio || !helpers.isEmptyString(instruccionesParticulares.audio));
  };

  const hasVideo = () => {
    return (video || !helpers.isEmptyString(instruccionesParticulares.video));
  };

  const getUpdatedAt = () => {
    if (isLoading) {
      return 'Cargando...';
    }

    const updatedAt = instruccionesParticulares?.updated_at;

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const onChangeInstruccionesParticularesInput = (event) => {
    const input = event.target;
    const [, attribute] = input.name.split('.');
    const value = input.type === 'checkbox' ? input.checked : input.value;

    dispatch(actions.changeInstruccionesParticularesInput(attribute, value));
  };

  const onChangeAudioInput = (value) => setAudio(value);
  const onChangeVideoInput = (value) => setVideo(value);
  const onRemoveAudio = () => setAudio(null);
  const onRemoveVideo = () => setVideo(null);
  const onDeleteAudio = () => dispatch(actions.changeInstruccionesParticularesInput('audio', ''));
  const onDeleteVideo = () => dispatch(actions.changeInstruccionesParticularesInput('video', ''));

  const onSaveInstruccionesParticulares = async (event) => {
    event.preventDefault();

    setShowToast(false);
    setToastMessage('');

    const data = {...instruccionesParticulares}

    if (helpers.isFile(audio) || helpers.isBlob(audio)) {
      data.audio = audio;
    }

    if (helpers.isFile(video) || helpers.isBlob(video)) {
      data.video = video;
    }

    const formData = new FormData();
    helpers.buildFormData(formData, data);

    if (await dispatch(actions.saveInstruccionesParticulares(formData))) {
      setAudio(null);
      setVideo(null);
      setToastMessage('Los datos de instrucciones a custodios se guardaron con éxito.');
      setShowToast(true);
    }

    window.scrollTo(0, 0);
  };

  const onDownloadAudioInMp3 = () => dispatch(actions.downloadInstruccionesParticularesAudioInMp3());

  const onDownloadAudioInStandard = () => {
    const extension = instruccionesParticulares.audio.split('.').pop();

    dispatch(actions.downloadInstruccionesParticularesAudioInStandard(extension));
  };

  const onDownloadVideo = () => {
    const extension = instruccionesParticulares.video.split('.').pop();

    dispatch(actions.downloadInstruccionesParticularesVideo(extension));
  };

  const onHideErrors = () => dispatch(actions.resetInstruccionesParticularesErrors());

  React.useEffect(() => {
    if (!helpers.isNull(errors)) {
      window.scrollTo(0, 0);
    }
  }, [errors]);

  React.useEffect(() => {
    dispatch(actions.getInstruccionesParticulares());

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Instrucciones a custodios">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Instrucciones a custodios</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onHideErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onHideErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {helpers.isObject(errors) && Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      {!isPlanPremium() && (
        <Alert variant="warning">
          <Paragraph>Para subir o grabar audio y/o video, debes actualiza tu plan gratuito a un plan de pago.</Paragraph>
        </Alert>
      )}
      <form onSubmit={onSaveInstruccionesParticulares}>
        <FormRow>
          <FormColumn>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="instruccionesParticulares.texto">Esta sección es libre, aquí puedes dejar mediante voz, video o una carta escrita alguna acción o instrucciones que desees lleven a cabo tus custodios una vez que ya no estés. Siéntete libre de expresar todos y cada uno de tus deseos*</FormLabel>
                <FormInput
                  as="textarea"
                  name="instruccionesParticulares.texto"
                  id="instruccionesParticulares.texto"
                  rows={3}
                  required
                  maxLength={6000}
                  onChange={onChangeInstruccionesParticularesInput}
                  value={instruccionesParticulares.texto}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>Esta sección es libre, aquí puedes dejar mediante voz, video o una carta escrita alguna acción o instrucciones que desees lleven a cabo tus custodios una vez que ya no estés. Siéntete libre de expresar todos y cada uno de tus deseos*</FormLabel>
                <Paragraph>{instruccionesParticulares.texto}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
        {canWrite() ? (
          <React.Fragment>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="instruccionesParticulares.is_grabar_audio">Audio <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>El audio debe de pesar como máximo 8 MB y ser de tipo .mp3, .webm, .oga, .weba o .wav.</FormHelpText>
                <FormInput
                  as="switch"
                  name="instruccionesParticulares.is_grabar_audio"
                  id="instruccionesParticulares.is_grabar_audio"
                  disabled={!isPlanPremium()}
                  checked={instruccionesParticulares.is_grabar_audio}
                  falseText="Subir"
                  trueText="Grabar"
                  onChange={onChangeInstruccionesParticularesInput}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                {isPlanPremium() ? (
                  <React.Fragment>
                    {instruccionesParticulares.is_grabar_audio ? (
                      !hasAudio() && <AudioRecorder onRecorded={onChangeAudioInput} />
                    ) : (
                      !hasAudio() && <FormFile disabled={!isPlanPremium()} onChange={onChangeAudioInput} />
                    )}
                    {helpers.isFileOrBlob(audio) && (
                      <AudioPlayer audio={audio} onRemove={onRemoveAudio} />
                    )}
                    {helpers.isUrl(instruccionesParticulares.audio) && (
                      <AudioDownloader
                        audio={instruccionesParticulares.audio}
                        canRemove={canWrite()}
                        isDownloading={isDownloadingAudio}
                        onDownloadInMp3={onDownloadAudioInMp3}
                        onDownloadInStandard={onDownloadAudioInStandard}
                        onRemove={onDeleteAudio}
                      />
                    )}
                  </React.Fragment>
                ) : (
                  <Alert variant="warning">
                    <Paragraph>Esta funcionalidad pertenece al Plan Premium. <Alert.Link as={Link} to="planes">Actualiza tu plan aquí</Alert.Link></Paragraph>
                  </Alert>
                )}
              </FormColumn>
            </FormRow>
          </React.Fragment>
        ) : (
          <FormRow>
            <FormColumn>
              <FormLabel>Audio</FormLabel>
              {helpers.isUrl(instruccionesParticulares.video) ? (
                <AudioDownloader
                  audio={instruccionesParticulares.audio}
                  canRemove={canWrite()}
                  isDownloading={isDownloadingAudio}
                  onDownloadInMp3={onDownloadAudioInMp3}
                  onDownloadInStandard={onDownloadAudioInStandard}
                  onRemove={onDeleteAudio}
                />
              ) : (
                <Paragraph>El usuario no subió/grabo audio.</Paragraph>
              )}
            </FormColumn>
          </FormRow>
        )}
        {canWrite() ? (
          <React.Fragment>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="instruccionesParticulares.is_grabar_video">Video <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>El video debe de pesar como máximo 64 MB y ser de tipo .mp4, .mpeg, .avi, .mkv o .webm.</FormHelpText>
                <FormInput
                  as="switch"
                  name="instruccionesParticulares.is_grabar_video"
                  id="instruccionesParticulares.is_grabar_video"
                  disabled={!isPlanPremium()}
                  checked={instruccionesParticulares.is_grabar_video}
                  falseText="Subir"
                  trueText="Grabar"
                  onChange={onChangeInstruccionesParticularesInput}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                {isPlanPremium() ? (
                  <React.Fragment>
                    {instruccionesParticulares.is_grabar_video ? (
                      !hasVideo() && <VideoRecorder onRecorded={onChangeVideoInput} />
                    ) : (
                      !hasVideo() && <FormFile disabled={!isPlanPremium()} onChange={onChangeVideoInput} />
                    )}
                    {helpers.isFileOrBlob(video) && (
                      <SelectedVideo video={video} onRemove={onRemoveVideo} />
                    )}
                    {helpers.isUrl(instruccionesParticulares.video) && (
                      <SavedVideo
                        video={instruccionesParticulares.video}
                        onDownload={onDownloadVideo}
                        isDownloading={isDownloadingVideo}
                        onDelete={onDeleteVideo}
                      />
                    )}
                  </React.Fragment>
                ) : (
                  <Alert variant="warning">
                    <Paragraph>Esta funcionalidad pertenece al Plan Premium. <Alert.Link as={Link} to="planes">Actualiza tu plan aquí</Alert.Link></Paragraph>
                  </Alert>
                )}
              </FormColumn>
            </FormRow>
          </React.Fragment>
        ) : (
          <FormRow>
            <FormColumn>
              <FormLabel>Video</FormLabel>
              {helpers.isUrl(instruccionesParticulares.video) ? (
                <SavedVideo
                  video={instruccionesParticulares.video}
                  onDownload={onDownloadVideo}
                  isDownloading={isDownloadingVideo}
                  onDelete={onDeleteVideo}
                />
              ) : (
                <Paragraph>El usuario no subió/grabo video.</Paragraph>
              )}
            </FormColumn>
          </FormRow>
        )}
        <FormRow>
          <FormColumn>
            {canWrite() ? (
              <React.Fragment>
                <FormLabel htmlFor="instruccionesParticulares.secreto_asunto">¿Existe algún secreto o asunto inconcluso sobre el cual dejar instrucciones particulares? <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  as="textarea"
                  name="instruccionesParticulares.secreto_asunto"
                  id="instruccionesParticulares.secreto_asunto"
                  rows={3}
                  maxLength={500}
                  onChange={onChangeInstruccionesParticularesInput}
                  value={instruccionesParticulares.secreto_asunto}
                />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <FormLabel>¿Existe algún secreto o asunto inconcluso sobre el cual dejar instrucciones particulares?</FormLabel>
                <Paragraph>{instruccionesParticulares.secreto_asunto || 'El usuario no contesto esta pregunta.'}</Paragraph>
              </React.Fragment>
            )}
          </FormColumn>
        </FormRow>
        {canWrite() && (
          <FormRow>
            <FormColumn>
              <Button
                type="submit"
                variant="secondary"
                align="right"
                disabled={isSaving}
              >
                {isSaving ? 'Guardando cambios...' : 'Guardar cambios'}
              </Button>
            </FormColumn>
          </FormRow>
        )}
      </form>
      <FormToast
        variant="success"
        show={showToast}
        title="Mensaje de éxito"
        message={toastMessage}
        onClose={() => setShowToast(false)}
      />
    </Layout>
  );
};

export default InstruccionesParticulares;
