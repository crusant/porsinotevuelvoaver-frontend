import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Feedback from '../../components/UI/Feedback/Feedback';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import DatosEmpleo from '../../components/PanelUsuario/EmpleoPension/DatosEmpleo/DatosEmpleo';
import SeguridadSocial from '../../components/PanelUsuario/EmpleoPension/SeguridadSocial/SeguridadSocial';
import PlanPension from '../../components/PanelUsuario/EmpleoPension/PlanPension/PlanPension';
import * as actions from '../../actions';
import * as helpers from '../../helpers';

const EmpleoPension = () => {
  const [activeKey, setActiveKey] = React.useState('0');
  const [updatedAt, setUpdatedAt] = React.useState(null);
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const user = useSelector((state) => state.auth.user);

  const isLoading = useSelector((state) => {
    return state.datosEmpleo.isLoading
        || state.seguridadSocial.isLoading
        || state.planPension.isLoading;
  });

  const errors = useSelector((state) => {
    return state.datosEmpleo.errors
        || state.seguridadSocial.errors
        || state.planPension.errors;
  });

  const dispatch = useDispatch();

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    if (isLoading) {
      return 'Cargando...';
    }

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const onSelectKey = (key) =>
    setActiveKey(key);

  const onHideErrors = () => {
    dispatch(actions.resetDatosEmpleoErrors());
    dispatch(actions.resetSeguridadSocialErrors());
    dispatch(actions.resetPlanPensionErrors());
  };

  React.useEffect(() => {
    if (!helpers.isNull(errors)) {
      window.scrollTo(0, 0);
    }
  }, [errors]);

  return (
    <Layout title="Empleo y pensión">
      <StepProgressBar activeKey={activeKey}>
        <StepProgressBar.Step eventKey="0" onSelect={onSelectKey}>1. Datos del empleo</StepProgressBar.Step>
        <StepProgressBar.Step eventKey="1" onSelect={onSelectKey}>2. Seguridad social</StepProgressBar.Step>
        <StepProgressBar.Step eventKey="2" onSelect={onSelectKey}>3. Plan de pensión</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert
          variant="danger"
          dismissible
          onClose={onHideErrors}
        >
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert
          variant="danger"
          dismissible
          onClose={onHideErrors}
        >
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>{error}</Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      {(activeKey === '0') && (
        <DatosEmpleo
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(activeKey === '1') && (
        <SeguridadSocial
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(activeKey === '2') && (
        <PlanPension
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      <FormToast
        variant="success"
        show={showToast}
        title="Mensaje de éxito"
        message={toastMessage}
        onClose={() => setShowToast(false)}
      />
    </Layout>
  );
};

export default EmpleoPension;
