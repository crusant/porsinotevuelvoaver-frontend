import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Feedback from '../../components/UI/Feedback/Feedback';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormRange from '../../components/UI/Form/FormRange/FormRange';
import Button from '../../components/UI/Button/Button';
import * as helpers from '../../helpers';
import * as actions from '../../actions/configuracion-tiempo-respuesta-pruebas-vida-custodios';

const ConfiguracionTiempoRespuestaPruebasVidaCustodios = () => {
  const configuracionPruebaVidaCustodio = useSelector((state) => state.configuracionTiempoRespuestaPruebasVidaCustodios.configuracionPruebaVidaCustodio);
  const isSaving = useSelector((state) => state.configuracionTiempoRespuestaPruebasVidaCustodios.isSaving);
  const errors = useSelector((state) => state.configuracionTiempoRespuestaPruebasVidaCustodios.errors);
  const dispatch = useDispatch();

  const onChangeConfiguracionTiempoRespuestaPruebasVidaCustodiosInput = (event) => {
    const input = event.target;
    const [, attribute] = input.name.split('.');

    dispatch(actions.changeConfiguracionTiempoRespuestaPruebasVidaCustodiosInput(attribute, input.value));
  };

  const onResetConfiguracionTiempoRespuestaPruebasVidaCustodiosInputs = () => {
    dispatch(actions.resetConfiguracionTiempoRespuestaPruebasVidaCustodiosInputs());
  };

  const onSaveConfiguracionTiempoRespuestaPruebasVidaCustodios = async (event) => {
    event.preventDefault();

    await dispatch(actions.saveConfiguracionTiempoRespuestaPruebasVidaCustodios(configuracionPruebaVidaCustodio));

    window.scrollTo(0, 0);
  };

  const onResetConfiguracionTiempoRespuestaPruebasVidaCustodiosErrors = () => {
    dispatch(actions.resetConfiguracionTiempoRespuestaPruebasVidaCustodiosErrors());
  };

  React.useEffect(() => {
    dispatch(actions.getConfiguracionTiempoRespuestaPruebasVidaCustodios());

    return () => {
      onResetConfiguracionTiempoRespuestaPruebasVidaCustodiosInputs();
      onResetConfiguracionTiempoRespuestaPruebasVidaCustodiosErrors();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Configuración de tiempo de respuesta de pruebas de vida a custodios">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Tiempo de respuesta</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>*Campos obligatorios</Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetConfiguracionTiempoRespuestaPruebasVidaCustodiosErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onResetConfiguracionTiempoRespuestaPruebasVidaCustodiosErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {helpers.isObject(errors) && Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <form onSubmit={onSaveConfiguracionTiempoRespuestaPruebasVidaCustodios}>
        <FormRow>
          <FormColumn>
            <FormLabel htmlFor="configuracionPruebaVidaCustodio.dias_respuesta">Seleccione el número de días que tiene el custodio para responder a la prueba de vida*</FormLabel>
            <FormRange
              name="configuracionPruebaVidaCustodio.dias_respuesta"
              min={0}
              max={30}
              onChange={onChangeConfiguracionTiempoRespuestaPruebasVidaCustodiosInput}
              value={configuracionPruebaVidaCustodio.dias_respuesta}
            />
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            <Button
              type="submit"
              variant="primary"
              align="right"
              disabled={isSaving}
            >
              {isSaving ? 'Finalizando...' : 'Finalizar'}
            </Button>
          </FormColumn>
        </FormRow>
      </form>
    </Layout>
  );
};

export default ConfiguracionTiempoRespuestaPruebasVidaCustodios;
