import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Feedback from '../../components/UI/Feedback/Feedback';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormRange from '../../components/UI/Form/FormRange/FormRange';
import Button from '../../components/UI/Button/Button';
import * as helpers from '../../helpers';
import * as actions from '../../actions/configuracion-envio-pruebas-vida';

const ConfiguracionEnvioPruebasVida = () => {
  const configuracionPruebaVidaUsuario = useSelector((state) => state.configuracionEnvioPruebasVida.configuracionPruebaVidaUsuario);
  const isSaving = useSelector((state) => state.configuracionEnvioPruebasVida.isSaving);
  const errors = useSelector((state) => state.configuracionEnvioPruebasVida.errors);
  const dispatch = useDispatch();

  const onChangeConfiguracionEnvioPruebasVidaInput = (event) => {
    const input = event.target;
    const [, attribute] = input.name.split('.');

    dispatch(actions.changeConfiguracionEnvioPruebasVidaInput(attribute, input.value));
  };

  const onResetConfiguracionEnvioPruebasVidaInputs = () => {
    dispatch(actions.resetConfiguracionEnvioPruebasVidaInputs());
  };

  const onSaveConfiguracionEnvioPruebasVida = async (event) => {
    event.preventDefault();

    await dispatch(actions.saveConfiguracionEnvioPruebasVida(configuracionPruebaVidaUsuario));

    window.scrollTo(0, 0);
  };

  const onResetConfiguracionEnvioPruebasVidaErrors = () => {
    dispatch(actions.resetConfiguracionEnvioPruebasVidaErrors());
  };

  React.useEffect(() => {
    dispatch(actions.getConfiguracionEnvioPruebasVida());

    return () => {
      onResetConfiguracionEnvioPruebasVidaInputs();
      onResetConfiguracionEnvioPruebasVidaErrors();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Configuración de envío de pruebas de vida">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Pruebas de vida</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>*Campos obligatorios</Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetConfiguracionEnvioPruebasVidaErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onResetConfiguracionEnvioPruebasVidaErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {helpers.isObject(errors) && Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <form onSubmit={onSaveConfiguracionEnvioPruebasVida}>
        <FormRow>
          <FormColumn>
            <FormLabel htmlFor="configuracionPruebaVidaUsuario.dias_envio">Seleccione el número de días que deberán transcurrir para que la prueba de vida sea enviada a los usuarios registrados*</FormLabel>
            <FormRange
              name="configuracionPruebaVidaUsuario.dias_envio"
              min={0}
              max={30}
              onChange={onChangeConfiguracionEnvioPruebasVidaInput}
              value={configuracionPruebaVidaUsuario.dias_envio}
            />
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            <Button
              type="submit"
              variant="primary"
              align="right"
              disabled={isSaving}
            >
              {isSaving ? 'Finalizando...' : 'Finalizar'}
            </Button>
          </FormColumn>
        </FormRow>
      </form>
    </Layout>
  );
};

export default ConfiguracionEnvioPruebasVida;
