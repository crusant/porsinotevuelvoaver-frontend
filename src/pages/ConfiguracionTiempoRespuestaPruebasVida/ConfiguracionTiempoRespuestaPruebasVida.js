import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Feedback from '../../components/UI/Feedback/Feedback';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormRange from '../../components/UI/Form/FormRange/FormRange';
import Button from '../../components/UI/Button/Button';
import * as helpers from '../../helpers';
import * as actions from '../../actions/configuracion-tiempo-respuesta-pruebas-vida';

const ConfiguracionTiempoRespuestaPruebasVida = () => {
  const configuracionPruebaVidaUsuario = useSelector((state) => state.configuracionTiempoRespuestaPruebasVida.configuracionPruebaVidaUsuario);
  const isSaving = useSelector((state) => state.configuracionTiempoRespuestaPruebasVida.isSaving);
  const errors = useSelector((state) => state.configuracionTiempoRespuestaPruebasVida.errors);
  const dispatch = useDispatch();

  const onChangeConfiguracionTiempoRespuestaPruebasVidaInput = (event) => {
    const input = event.target;
    const [, attribute] = input.name.split('.');

    dispatch(actions.changeConfiguracionTiempoRespuestaPruebasVidaInput(attribute, input.value));
  };

  const onResetConfiguracionTiempoRespuestaPruebasVidaInputs = () => {
    dispatch(actions.resetConfiguracionTiempoRespuestaPruebasVidaInputs());
  };

  const onSaveConfiguracionTiempoRespuestaPruebasVida = async (event) => {
    event.preventDefault();

    await dispatch(actions.saveConfiguracionTiempoRespuestaPruebasVida(configuracionPruebaVidaUsuario));

    window.scrollTo(0, 0);
  };

  const onResetConfiguracionTiempoRespuestaPruebasVidaErrors = () => {
    dispatch(actions.resetConfiguracionTiempoRespuestaPruebasVidaErrors());
  };

  React.useEffect(() => {
    dispatch(actions.getConfiguracionTiempoRespuestaPruebasVida());

    return () => {
      onResetConfiguracionTiempoRespuestaPruebasVidaInputs();
      onResetConfiguracionTiempoRespuestaPruebasVidaErrors();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Configuración de tiempo de respuesta a pruebas de vida">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Tiempo de respuesta</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>*Campos obligatorios</Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetConfiguracionTiempoRespuestaPruebasVidaErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onResetConfiguracionTiempoRespuestaPruebasVidaErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {helpers.isObject(errors) && Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <form onSubmit={onSaveConfiguracionTiempoRespuestaPruebasVida}>
        <FormRow>
          <FormColumn>
            <FormLabel htmlFor="configuracionPruebaVidaUsuario.dias_respuesta">Seleccione el número de días que tiene el usuario para responder a la prueba de vida*</FormLabel>
            <FormRange
              name="configuracionPruebaVidaUsuario.dias_respuesta"
              min={0}
              max={30}
              onChange={onChangeConfiguracionTiempoRespuestaPruebasVidaInput}
              value={configuracionPruebaVidaUsuario.dias_respuesta}
            />
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            <Button
              type="submit"
              variant="primary"
              align="right"
              disabled={isSaving}
            >
              {isSaving ? 'Finalizando...' : 'Finalizar'}
            </Button>
          </FormColumn>
        </FormRow>
      </form>
    </Layout>
  );
};

export default ConfiguracionTiempoRespuestaPruebasVida;
