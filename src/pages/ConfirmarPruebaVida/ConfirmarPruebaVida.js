import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import classNames from 'classnames';
import moment from 'moment';
import isEmpty from 'lodash/isEmpty';
import Button from 'react-bootstrap/Button';
import Loader from '../../components/UI/Loader/Loader';
import Layout from '../../components/Messages/Layout/Layout';
import Logo from '../../components/Logo/Logo';
import PruebaVidaInvalida from '../../components/Messages/PruebaVidaInvalida/PruebaVidaInvalida';
import PruebaVidaConfirmada from '../../components/Messages/PruebaVidaConfirmada/PruebaVidaConfirmada';
import DatosAccesoEntregados from '../../components/Messages/DatosAccesoEntregados/DatosAccesoEntregados';
import PruebaVidaExpirada from '../../components/Messages/PruebaVidaExpirada/PruebaVidaExpirada';
import * as actions from '../../actions';
import styles from './ConfirmarPruebaVida.module.scss';

const ConfirmarPruebaVida = () => {
  const pruebaVida = useSelector((state) => state.custodios.pruebaVida);
  const isRecibiendo = useSelector((state) => state.custodios.isRecibiendo);
  const dispatch = useDispatch();
  const { id, url } = useParams();

  const getPrimerNombre = (nombreCompleto) => {
    return String(nombreCompleto)
      .split(' ')
      .shift();
  };

  const getCustodioNombreCompleto = () => {
    return pruebaVida?.user?.custodio?.nombre_completo;
  };

  const getUserNombreCompleto = () => {
    return pruebaVida?.user?.perfil?.nombre_completo;
  };

  const getFechaRespuesta = () => {
    return moment(pruebaVida?.fecha_respuesta).format('DD [de] MMMM [de] YYYY [a las] HH:mm');
  };

  const getFechaLimite = () => {
    return moment(pruebaVida?.fecha_limite, 'YYYY-MM-DD').format('LL');
  };

  React.useEffect(() => {
    dispatch(actions.pruebaVidaCustodio(id, url));

    // eslint-disable-next-line
  }, []);

  if (isRecibiendo) {
    return <Loader title="Cargando la prueba de vida" />;
  }

  if (isEmpty(pruebaVida)) {
    return <PruebaVidaInvalida custodio />
  }

  if (pruebaVida?.is_confirmed) {
    return (
      <PruebaVidaConfirmada
        custodio
        primerNombre={getPrimerNombre(getUserNombreCompleto())}
        fechaRespuesta={getFechaRespuesta()}
      />
    );
  }

  if (pruebaVida?.user.is_delivered) {
    return (
      <DatosAccesoEntregados
        primerNombre={getPrimerNombre(getUserNombreCompleto())}
      />
    );
  }

  if (pruebaVida?.is_expired) {
    return (
      <PruebaVidaExpirada
        custodio
        primerNombre={getPrimerNombre(getUserNombreCompleto())}
        fechaLimite={getFechaLimite()}
      />
    );
  }

  return (
    <Layout
      title="¡Confirma la prueba de vida!"
      contentClass={styles.Layout}
    >
      <h2 className={styles.Title}>Prueba de vida de <Logo className={styles.Logo} /></h2>
      <p className={styles.Text}>Hola <strong>{getCustodioNombreCompleto()}</strong>. Te enviamos este mensaje para confirmar la supervivencia de<br /> <strong>{getUserNombreCompleto()}</strong>, ya que no recibimos respuesta en su <strong className="color-primary">Prueba de Vida</strong>.</p>
      <p className={styles.Text}>Te recordamos que tienes hasta el {getFechaLimite()} para hacerlo.</p>
      <p className={styles.Text}>¿<strong>{getUserNombreCompleto()}</strong> se encuentra con vida?</p>
      <Button
        as={Link}
        className={classNames('c-button', 'c-button--primary', styles.Positive)}
        to={`/custodio/${url}`}
      >
        Si
      </Button>
      <Button
        as={Link}
        className={classNames('c-button', 'c-button--primary', styles.Negative)}
        to={`/datos-de-acceso/${url}`}
      >
        No
      </Button>
    </Layout>
  );
};

export default ConfirmarPruebaVida;
