import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import classNames from 'classnames';
import Recaptcha from 'react-recaptcha';
import Form from 'react-bootstrap/Form';
import Layout from '../../components/Auth/Layout/Layout';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import TogglePassword from '../../components/Auth/TogglePassword/TogglePassword';
import Button from '../../components/UI/Button/Button';
import useQuery from '../../hooks/useQuery'
import * as actions from '../../actions';
import * as helpers from '../../helpers';

const Login = () => {
  const passwordRef = React.useRef(null);
  const recaptchaRef = React.useRef(null);
  const [togglePassword, setTogglePassword] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const [isVerified, setIsVerified] = React.useState(false);
  const errors = useSelector((state) => state.auth.errors);
  const dispatch = useDispatch();
  const query = useQuery();

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      recaptcha: false
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('El campo "Correo electrónico" debe ser una dirección de correo válida.')
        .required('El campo "Correo electrónico" es obligatorio.'),
      password: Yup.string().required('El campo "Contraseña" es obligatorio.'),
      recaptcha: Yup.bool().oneOf([true], 'Debe confirmar que no es un robot.')
    }),
    onSubmit: async (values)  => {
      if (!isVerified) {
        return false;
      }

      if (await dispatch(actions.login(values))) {
        formik.resetForm();
      }

      setIsVerified(false);
      recaptchaRef.current.reset();
      formik.setSubmitting(false);
    }
  });

  const onTogglePassword = (value) => {
    setTogglePassword(value);
  };

  const onRecaptchaLoaded = () => {
    setIsLoading(false);
  };

  const onVerifyRecaptcha = (response) => {
    if (response) {
      setIsVerified(true);
      formik.setFieldValue('recaptcha', true);
    } else {
      formik.setFieldValue('recaptcha', false);
    }
  };

  const setEmail = () => {
    if (query.has('email')) {
      passwordRef.current && passwordRef.current.focus();

      formik.setFieldValue('email', query.get('email'));
      formik.setErrors({});
    }
  };

  React.useEffect(() => {
    if (helpers.isObject(errors)) {
      formik.setErrors(errors);
      formik.setSubmitting(false);
    }

    // eslint-disable-next-line
  }, [errors]);

  React.useEffect(() => {
    setEmail();

    return () => {
      setTogglePassword(false);
      formik.resetForm({});
      formik.setSubmitting(false);
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout page="login">
      {(styles) => (
        <>
          <h2 className={styles.Title}>Ingresa a tu cuenta</h2>
          <Form className={styles.Form} noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
          <Form.Group className={styles.Group} controlId="email">
            <Form.Label className={styles.Label}>Correo electrónico</Form.Label>
            <Form.Control
              type="email"
              name="email"
              className={styles.Control}
              autoFocus
              isInvalid={formik.errors.email && formik.touched.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.email}
            />
            {formik.errors.email && formik.touched.email && (
              <Form.Control.Feedback type="invalid">
                {formik.errors.email}
              </Form.Control.Feedback>
            )}
            </Form.Group>
            <Form.Group className={styles.Group} controlId="password">
              <Form.Label className={classNames(styles.Label, styles.Password)}>
                Contraseña <TogglePassword onToggle={onTogglePassword} />
              </Form.Label>
              <Form.Control
                ref={passwordRef}
                type={togglePassword ? 'text' : 'password'}
                name="password"
                className={styles.Control}
                isInvalid={formik.errors.password && formik.touched.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.password}
              />
              {formik.errors.password && formik.touched.password && (
                <Form.Control.Feedback type="invalid">
                  {formik.errors.password}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Form.Group>
              {isLoading && <Paragraph>Cargando captcha...</Paragraph>}
              <Recaptcha
                ref={recaptchaRef}
                sitekey="6LdhmKsZAAAAAKyKuj9NRlhu0tTejyb6AmNT2x3E"
                render="explicit"
                hl="es-419"
                verifyCallback={onVerifyRecaptcha}
                onloadCallback={onRecaptchaLoaded}
              />
              {formik.errors.recaptcha && formik.touched.recaptcha && (
                <Form.Control.Feedback type="invalid" className="d-block">
                  {formik.errors.recaptcha}
                </Form.Control.Feedback>
              )}
            </Form.Group>
            <Button
              type="submit"
              variant="primary"
              className={styles.Button}
              disabled={formik.isSubmitting}
            >
              {formik.isSubmitting ? 'Ingresando...' : 'Ingresar'}
            </Button>
            <Link
              className={styles.PasswordReset}
              to="/restablecer-contrasena"
            >
              ¿Olvidaste tu contraseña?
            </Link>
          </Form>
        </>
      )}
    </Layout>
  );
};

export default Login;
