import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Feedback from '../../components/UI/Feedback/Feedback';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormHelpText from '../../components/UI/Form/FormHelpText/FormHelpText';
import FormCategories from '../../components/UI/Form/FormCategories/FormCategories';
import FormInput from '../../components/UI/Form/FormInput/FormInput';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import FormMargin from '../../components/UI/Form/FormMargin/FormMargin';
import FormTable from '../../components/UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../components/UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../components/UI/Button/Button';
import TogglePassword from '../../components/UI/TogglePassword/TogglePassword';
import IconEmail from '../../components/UI/Icons/IconEmail/IconEmail';
import IconFacebook from '../../components/UI/Icons/IconFacebook/IconFacebook';
import IconYouTube from '../../components/UI/Icons/IconYouTube/IconYouTube';
import IconWhatsApp from '../../components/UI/Icons/IconWhatsApp/IconWhatsApp';
import IconTwitter from '../../components/UI/Icons/IconTwitter/IconTwitter';
import IconInstagram from '../../components/UI/Icons/IconInstagram/IconInstagram';
import IconTikTok from '../../components/UI/Icons/IconTikTok/IconTikTok';
import IconWebSearch from '../../components/UI/Icons/IconWebSearch/IconWebSearch';
import * as actions from '../../actions/correos-electronicos-redes-sociales';
import * as helpers from '../../helpers';
import * as constants from '../../constants';

const CorreoRedesSociales = () => {
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const user = useSelector((state) => state.auth.user);
  const correosElectronicosRedesSociales = useSelector((state) => state.correosElectronicosRedesSociales.correosElectronicosRedesSociales);
  const correoElectronicoRedSocial = useSelector((state) => state.correosElectronicosRedesSociales.correoElectronicoRedSocial);
  const isLoading = useSelector((state) => state.correosElectronicosRedesSociales.isLoading);
  const isSaving = useSelector((state) => state.correosElectronicosRedesSociales.isSaving);
  const isUpdating = useSelector((state) => state.correosElectronicosRedesSociales.isUpdating);
  const errors = useSelector((state) => state.correosElectronicosRedesSociales.errors);
  const dispatch = useDispatch();

  const hasCorreosElectronicosRedesSociales = (correosElectronicosRedesSociales.length > 0);
  const categorias = constants.correoElectronicoRedSocialCategorias;

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [correoElectronicoRedesSociales] = correosElectronicosRedesSociales.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    if (isLoading) {
      return 'Cargando...';
    }

    const updatedAt = correoElectronicoRedesSociales?.updated_at;

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const onChangeCategoriaInput = (event) => {
    dispatch(actions.changeCorreoElectronicoRedSocialInput('tipo', parseInt(event.target.value, 10)));
  };

  const onChangeCorreoElectronicoRedSocialInput = (event) => {
    const input = event.target;
    const name = input.name;
    const value = input.type === 'checkbox' ? input.checked : input.value;

    if (name === 'can_borrar_cuenta' && value) {
      dispatch(actions.changeCorreoElectronicoRedSocialInput('can_pagina_en_memoria', false));
    }

    dispatch(actions.changeCorreoElectronicoRedSocialInput(name, value));
  };

  const onResetCorreoElectronicoRedSocialInputs = () => {
    dispatch(actions.resetCorreoElectronicoRedSocialInputs());
  };

  const onEditCorreoElectronicoRedSocial = (correoElectronicoRedSocial) => {
    window.scrollTo(0, 0);
    dispatch(actions.editCorreoElectronicoRedSocial(correoElectronicoRedSocial));
  };

  const onSaveOrUpdateCorreoElectronicoRedSocial = async (event) => {
    event.preventDefault();

    setShowToast(false);
    setToastMessage('');

    if (!correoElectronicoRedSocial.id && await dispatch(actions.saveCorreoElectronicoRedSocial(correoElectronicoRedSocial))) {
      setToastMessage('Los datos del correo electrónico o red social se guardaron con éxito.');
      setShowToast(true);
    }

    if (correoElectronicoRedSocial.id && await dispatch(actions.updateCorreoElectronicoRedSocial(correoElectronicoRedSocial))) {
      setToastMessage('Los datos del correo electrónico o red social se actualizaron con éxito.');
      setShowToast(true);
    }
  };

  const onDeleteCorreoElectronicoRedSocial = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este correo electrónico o red social?');
    setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteCorreoElectronicoRedSocial(id))) {
      setToastMessage('Los datos del correo electrónico o red social se eliminaron con éxito.');
      setShowToast(true);
    }
  };

  const onHideErrors = () => {
    dispatch(actions.resetErrors());
  };

  React.useEffect(() => {
    if (!helpers.isNull(errors)) {
      window.scrollTo(0, 0);
    }
  }, [errors]);

  React.useEffect(() => {
    dispatch(actions.getCorreosElectronicosRedesSociales());

    return () => {
      onResetCorreoElectronicoRedSocialInputs();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Correo y redes sociales">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Correo y redes sociales</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onHideErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onHideErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {helpers.isObject(errors) && Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <Paragraph title>En esta sección podrás registrar toda la información relativa a tus redes sociales y perfiles de correo electrónico.<br /> Podrás dejar instrucciones a tus custodios con respecto a publicaciones, notificaciones o si deseas borrar total o parcialmente la información contenida en tus cuentas.</Paragraph>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateCorreoElectronicoRedSocial}>
            <FormRow>
              <FormColumn>
                <FormLabel center htmlFor="categoria_correo_electronico">Selecciona una categoría</FormLabel>
                <FormCategories>
                  <FormCategories.Category
                    name="categoria"
                    id="categoria_correo_electronico"
                    checked={(correoElectronicoRedSocial.tipo === categorias.ID.CORREO_ELECTRONICO)}
                    onChange={onChangeCategoriaInput}
                    label={categorias.NAME[categorias.ID.CORREO_ELECTRONICO]}
                    value={categorias.ID.CORREO_ELECTRONICO}
                  >
                    {(styles) => <IconEmail className={styles.Icon} height="42px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="categoria"
                    id="categoria_facebook"
                    checked={(correoElectronicoRedSocial.tipo === categorias.ID.FACEBOOK)}
                    onChange={onChangeCategoriaInput}
                    label={categorias.NAME[categorias.ID.FACEBOOK]}
                    value={categorias.ID.FACEBOOK}
                  >
                    {(styles) => <IconFacebook className={styles.Icon} height="42px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="categoria"
                    id="categoria_youtube"
                    checked={(correoElectronicoRedSocial.tipo === categorias.ID.YOUTUBE)}
                    onChange={onChangeCategoriaInput}
                    label={categorias.NAME[categorias.ID.YOUTUBE]}
                    value={categorias.ID.YOUTUBE}
                  >
                    {(styles) => <IconYouTube className={styles.Icon} height="42px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="categoria"
                    id="categoria_whatsapp"
                    checked={(correoElectronicoRedSocial.tipo === categorias.ID.WHATSAPP)}
                    onChange={onChangeCategoriaInput}
                    label={categorias.NAME[categorias.ID.WHATSAPP]}
                    value={categorias.ID.WHATSAPP}
                  >
                    {(styles) => <IconWhatsApp className={styles.Icon} height="42px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="categoria"
                    id="categoria_twitter"
                    checked={(correoElectronicoRedSocial.tipo === categorias.ID.TWITTER)}
                    onChange={onChangeCategoriaInput}
                    label={categorias.NAME[categorias.ID.TWITTER]}
                    value={categorias.ID.TWITTER}
                  >
                    {(styles) => <IconTwitter className={styles.Icon} height="42px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="categoria"
                    id="categoria_instagram"
                    checked={(correoElectronicoRedSocial.tipo === categorias.ID.INSTAGRAM)}
                    onChange={onChangeCategoriaInput}
                    label={categorias.NAME[categorias.ID.INSTAGRAM]}
                    value={categorias.ID.INSTAGRAM}
                  >
                    {(styles) => <IconInstagram className={styles.Icon} height="42px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="categoria"
                    id="categoria_tiktok"
                    checked={(correoElectronicoRedSocial.tipo === categorias.ID.TIKTOK)}
                    onChange={onChangeCategoriaInput}
                    label={categorias.NAME[categorias.ID.TIKTOK]}
                    value={categorias.ID.TIKTOK}
                  >
                    {(styles) => <IconTikTok className={styles.Icon} height="42px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="categoria"
                    id="categoria_otro"
                    checked={(correoElectronicoRedSocial.tipo === categorias.ID.OTRO)}
                    onChange={onChangeCategoriaInput}
                    label={categorias.NAME[categorias.ID.OTRO]}
                    value={categorias.ID.OTRO}
                  >
                    {(styles) => <IconWebSearch className={styles.Icon} height="42px" />}
                  </FormCategories.Category>
                </FormCategories>
              </FormColumn>
            </FormRow>
            {(correoElectronicoRedSocial.tipo === categorias.ID.OTRO) && (
              <FormRow>
                <FormColumn left>
                  <FormLabel htmlFor="nombre">Nombre*</FormLabel>
                  <FormInput
                    type="text"
                    name="nombre"
                    id="nombre"
                    required
                    maxLength={50}
                    onChange={onChangeCorreoElectronicoRedSocialInput}
                    value={correoElectronicoRedSocial.nombre}
                  />
                </FormColumn>
                <FormColumn right>
                  <FormLabel htmlFor="url">Dirección (www)*</FormLabel>
                  <FormInput
                    type="text"
                    name="url"
                    id="url"
                    required
                    maxLength={150}
                    pattern="^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$"
                    title="Por favor ingresa una URL valida."
                    onChange={onChangeCorreoElectronicoRedSocialInput}
                    value={correoElectronicoRedSocial.url}
                  />
                </FormColumn>
              </FormRow>
            )}
            <FormRow>
              {(correoElectronicoRedSocial.tipo === categorias.ID.CORREO_ELECTRONICO) ? (
                <FormColumn left>
                  <FormLabel htmlFor="username">Correo electrónico*</FormLabel>
                  <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                  <FormInput
                    type="text"
                    name="username"
                    id="username"
                    required
                    maxLength={320}
                    pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                    title="Por favor ingrese un correo electrónico valido."
                    onChange={onChangeCorreoElectronicoRedSocialInput}
                    value={correoElectronicoRedSocial.username}
                  />
                </FormColumn>
              ) : (correoElectronicoRedSocial.tipo === categorias.ID.WHATSAPP) ? (
                <FormColumn left>
                  <FormLabel htmlFor="username">Número de celular*</FormLabel>
                  <FormInput
                    type="tel"
                    name="username"
                    id="username"
                    required
                    minLength={10}
                    maxLength={10}
                    onChange={onChangeCorreoElectronicoRedSocialInput}
                    value={correoElectronicoRedSocial.username}
                  />
                </FormColumn>
              ) : (
                <FormColumn left>
                  <FormLabel htmlFor="username">Usuario*</FormLabel>
                  <FormInput
                    type="text"
                    name="username"
                    id="username"
                    required
                    maxLength={100}
                    onChange={onChangeCorreoElectronicoRedSocialInput}
                    value={correoElectronicoRedSocial.username}
                  />
                </FormColumn>
              )}
              <FormColumn right>
                <FormLabel htmlFor="password">Contraseña*</FormLabel>
                <FormHelpText>&nbsp;</FormHelpText>
                <FormInput
                  type="text"
                  name="password"
                  id="password"
                  required
                  maxLength={20}
                  onChange={onChangeCorreoElectronicoRedSocialInput}
                  value={correoElectronicoRedSocial.password}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              {[
                categorias.ID.CORREO_ELECTRONICO,
                categorias.ID.FACEBOOK,
                categorias.ID.WHATSAPP,
                categorias.ID.TWITTER
              ].includes(correoElectronicoRedSocial.tipo) && (
                <FormColumn left strict>
                  <FormLabel htmlFor="can_notificar_contactos">Deseo notificar a toda la lista de contactos del fallecimiento</FormLabel>
                  <FormInput
                    as="switch"
                    name="can_notificar_contactos"
                    id="can_notificar_contactos"
                    checked={correoElectronicoRedSocial.can_notificar_contactos}
                    falseText="No"
                    trueText="Sí"
                    onChange={onChangeCorreoElectronicoRedSocialInput}
                  />
                </FormColumn>
              )}
              {(correoElectronicoRedSocial.tipo === categorias.ID.CORREO_ELECTRONICO) ? (
                <FormColumn right>
                  <FormLabel htmlFor="can_borrar_cuenta">Deseo borrar la cuenta</FormLabel>
                  <FormInput
                    as="switch"
                    name="can_borrar_cuenta"
                    id="can_borrar_cuenta"
                    checked={correoElectronicoRedSocial.can_borrar_cuenta}
                    falseText="No"
                    trueText="Sí"
                    onChange={onChangeCorreoElectronicoRedSocialInput}
                  />
                </FormColumn>
              ) : (
                <FormColumn
                  right={[
                    categorias.ID.FACEBOOK,
                    categorias.ID.WHATSAPP,
                    categorias.ID.TWITTER
                  ].includes(correoElectronicoRedSocial.tipo)}
                >
                  <FormLabel htmlFor="can_ultima_publicacion">Deseo que los custodios realicen una última publicación</FormLabel>
                  <FormInput
                    as="switch"
                    name="can_ultima_publicacion"
                    id="can_ultima_publicacion"
                    checked={correoElectronicoRedSocial.can_ultima_publicacion}
                    falseText="No"
                    trueText="Sí"
                    onChange={onChangeCorreoElectronicoRedSocialInput}
                  />
                </FormColumn>
              )}
            </FormRow>
            {(
                (correoElectronicoRedSocial.tipo === categorias.ID.CORREO_ELECTRONICO)
              || correoElectronicoRedSocial.can_ultima_publicacion
            ) && (
              <FormRow>
                <FormColumn>
                  <FormLabel htmlFor="instrucciones">
                    {(correoElectronicoRedSocial.tipo === categorias.ID.CORREO_ELECTRONICO) ? (
                      'Instrucciones particulares*'
                    ) : (
                      'Instrucciones de publicación*'
                    )}
                  </FormLabel>
                  <FormInput
                    as="textarea"
                    name="instrucciones"
                    id="instrucciones"
                    rows={3}
                    maxLength={500}
                    onChange={onChangeCorreoElectronicoRedSocialInput}
                    value={correoElectronicoRedSocial.instrucciones}
                  />
                </FormColumn>
              </FormRow>
            )}
            {(correoElectronicoRedSocial.tipo !== categorias.ID.CORREO_ELECTRONICO) && (
              <FormRow>
                <FormColumn left strict>
                  <FormLabel htmlFor="can_borrar_cuenta">Deseo borrar la cuenta</FormLabel>
                  <FormInput
                    as="switch"
                    name="can_borrar_cuenta"
                    id="can_borrar_cuenta"
                    checked={correoElectronicoRedSocial.can_borrar_cuenta}
                    falseText="No"
                    trueText="Sí"
                    onChange={onChangeCorreoElectronicoRedSocialInput}
                  />
                </FormColumn>
                {(!correoElectronicoRedSocial.can_borrar_cuenta && (correoElectronicoRedSocial.tipo !== categorias.ID.WHATSAPP)) && (
                  <FormColumn right>
                    <FormLabel htmlFor="can_pagina_en_memoria">Deseo sea creada una página "En Memoria"</FormLabel>
                    <FormInput
                      as="switch"
                      name="can_pagina_en_memoria"
                      id="can_pagina_en_memoria"
                      checked={correoElectronicoRedSocial.can_pagina_en_memoria}
                      falseText="No"
                      trueText="Sí"
                      onChange={onChangeCorreoElectronicoRedSocialInput}
                    />
                  </FormColumn>
                )}
              </FormRow>
            )}
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetCorreoElectronicoRedSocialInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!correoElectronicoRedSocial.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormToast
            variant="success"
            show={showToast}
            title="Mensaje de éxito"
            message={toastMessage}
            onClose={() => setShowToast(false)}
          />
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Tipo</th>
            <th>Nombre</th>
            <th>Dirección (wwww)</th>
            <th>Correo electrónico, Usuario o número de celular</th>
            <th>Contraseña</th>
            <th>Deseo notificar a toda la lista de contactos del fallecimiento</th>
            <th>Deseo que los custodios realicen una última publicación</th>
            <th>Instrucciones particulares o de publicación</th>
            <th>Deseo borrar la cuenta</th>
            <th>Deseo sea creada una página "En Memoria"</th>
            {canWrite() && (<th></th>)}
          </tr>
        </thead>
        <tbody>
          {isLoading ? (
            <tr>
              <td colSpan={11}>Cargando...</td>
            </tr>
          ) : hasCorreosElectronicosRedesSociales ? correosElectronicosRedesSociales.map((correoElectronicoRedSocial, index) => (
            <tr key={correoElectronicoRedSocial.id}>
              <td className="text-nowrap">{categorias.NAME[correoElectronicoRedSocial.tipo]}</td>
              <td>{!helpers.isEmptyString(correoElectronicoRedSocial.nombre) ? correoElectronicoRedSocial.nombre : '-'}</td>
              <td>{!helpers.isEmptyString(correoElectronicoRedSocial.url) ? correoElectronicoRedSocial.url : '-'}</td>
              <td>{correoElectronicoRedSocial.username}</td>
              <td>
                <TogglePassword password={correoElectronicoRedSocial.password} />
              </td>
              <td>
                {[
                  categorias.ID.CORREO_ELECTRONICO,
                  categorias.ID.FACEBOOK,
                  categorias.ID.WHATSAPP,
                  categorias.ID.TWITTER
                ].includes(correoElectronicoRedSocial.tipo) ? (
                  correoElectronicoRedSocial.can_notificar_contactos ? 'Sí' : 'No'
                ) : '-'}
              </td>
              <td>
                {(correoElectronicoRedSocial.tipo !== categorias.ID.CORREO_ELECTRONICO) ? (
                  correoElectronicoRedSocial.can_ultima_publicacion ? 'Sí' : 'No'
                ) : '-'}
              </td>
              <td>{correoElectronicoRedSocial.instrucciones}</td>
              <td>{correoElectronicoRedSocial.can_borrar_cuenta ? 'Sí' : 'No'}</td>
              <td>
                {![
                  categorias.ID.CORREO_ELECTRONICO,
                  categorias.ID.WHATSAPP
                ].includes(correoElectronicoRedSocial.tipo) ? (
                  correoElectronicoRedSocial.can_pagina_en_memoria ? 'Sí' : 'No'
                ) : '-'}
              </td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="secondary"
                      small
                      onClick={() => onEditCorreoElectronicoRedSocial(correoElectronicoRedSocial)}
                    >
                      Editar
                    </Button>
                    <Button
                      variant="danger"
                      small
                      onClick={() => onDeleteCorreoElectronicoRedSocial(correoElectronicoRedSocial.id)}
                    >
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={11}>
                {canWrite() ? 'No has agregado ningún correo o red social.' : 'No se agregó ningún correo o red social.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </Layout>
  );
};

export default CorreoRedesSociales;
