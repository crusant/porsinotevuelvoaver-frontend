import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Feedback from '../../components/UI/Feedback/Feedback';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormText from '../../components/UI/Form/FormText/FormText';
import FormHelpText from '../../components/UI/Form/FormHelpText/FormHelpText';
import FormCategories from '../../components/UI/Form/FormCategories/FormCategories';
import FormInput from '../../components/UI/Form/FormInput/FormInput';
import FormSeparator from '../../components/UI/Form/FormSeparator/FormSeparator';
import FormTitle from '../../components/UI/Form/FormTitle/FormTitle';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import FormMargin from '../../components/UI/Form/FormMargin/FormMargin';
import FormTable from '../../components/UI/Form/FormTable/FormTable';
import FormButtonGroup from '../../components/UI/Form/FormButtonGroup/FormButtonGroup';
import Button from '../../components/UI/Button/Button';
import IconTrubunal from '../../components/UI/Icons/IconTribunal/IconTribunal';
import IconBriefcase from '../../components/UI/Icons/IconBriefcase/IconBriefcase';
import IconFamily from '../../components/UI/Icons/IconFamily/IconFamily';
import IconAccounting from '../../components/UI/Icons/IconAccounting/IconAccounting';
import IconStethoscope from '../../components/UI/Icons/IconStethoscope/IconStethoscope';
import IconChurch from '../../components/UI/Icons/IconChurch/IconChurch';
import IconEmployees from '../../components/UI/Icons/IconEmployees/IconEmployees';
import * as actions from '../../actions/asesores-contactos';
import * as helpers from '../../helpers';
import * as constants from '../../constants';

const AsesoresContactos = () => {
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const user = useSelector((state) => state.auth.user);
  const asesorContacto = useSelector((state) => state.asesoresContactos.asesorContacto);
  const asesoresContactos = useSelector((state) => state.asesoresContactos.asesoresContactos);
  const isGetting = useSelector((state) => state.asesoresContactos.isGetting);
  const isSaving = useSelector((state) => state.asesoresContactos.isSaving);
  const isUpdating = useSelector((state) => state.asesoresContactos.isUpdating);
  const errors = useSelector((state) => state.asesoresContactos.errors);
  const dispatch = useDispatch();

  const hasAsesoresContactos = (asesoresContactos.length > 0);

  const categoriaIds = { ...constants.asesorContactoCategoriaIds };
  const categoriaNombres = { ...constants.asesorContactoCategoriaNombres };

  const canWrite = () =>
    user?.can_write;

  const isPlanFree = () => {
    if (!user) {
      return false;
    }

    return parseInt(user.suscripcion.plan.tipo, 10) === 1;
  };

  const isDisabledForm = () => {
    return isPlanFree() && (asesoresContactos.length >= 1) && (asesorContacto && !asesorContacto.id);
  };

  const getUpdatedAt = () => {
    const [asesorContacto] = asesoresContactos.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    if (isGetting) {
      return 'Cargando...';
    }

    const updatedAt = asesorContacto?.updated_at;

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const onChangeInput = (event) => {
    const input = event.target;
    const [entity, attribute] = input.name.split('.');
    let value = input.value;

    if (attribute === 'tipo') {
      value = parseInt(value, 10);
    }

    dispatch(actions.changeAsesorContactoInputs(entity, attribute, value));
  };

  const onHideErrors = () => {
    dispatch(actions.resetErrors());
  };

  const onResetAsesorContactoInputs = () => {
    dispatch(actions.resetAsesorContactoInputs());
  };

  const onSaveOrUpdateAsesorContacto = async (event) => {
    event.preventDefault();

    setShowToast(false);
    setToastMessage('');

    if (!asesorContacto.id && await dispatch(actions.saveAsesorContacto(asesorContacto))) {
      setToastMessage('Los datos del asesor o contacto se guardaron con éxito.');
      setShowToast(true);
    }

    if (asesorContacto.id && await dispatch(actions.updateAsesorContacto(asesorContacto))) {
      setToastMessage('Los datos del asesor o contacto se actualizaron con éxito.');
      setShowToast(true);
    }
  };

  const onLoadInputs = (asesorContacto) => {
    window.scrollTo(0, 0);
    dispatch(actions.loadAsesorContactoInputs(asesorContacto));
  };

  const onDeleteAsesorContacto = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar a este asesor o contacto?');
    setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteAsesorContacto(id))) {
      setToastMessage('Los datos del asesor o contacto se eliminaron con éxito.');
      setShowToast(true);
    }
  };

  React.useEffect(() => {
    if (!helpers.isNull(errors)) {
      window.scrollTo(0, 0);
    }
  }, [errors]);

  React.useEffect(() => {
    dispatch(actions.getAsesoresContactos());

    return () => {
      onHideErrors();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Asesores y contactos">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Asesores y contactos</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onHideErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {canWrite() && (
        <React.Fragment>
          {helpers.isObject(errors) && (
            <Alert variant="danger" onClose={onHideErrors} dismissible>
              <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
              <Errors>
                {Object.values(errors).map((error, index) => (
                  <Errors.Error key={index}>
                    {error}
                  </Errors.Error>
                ))}
              </Errors>
            </Alert>
          )}
          {isPlanFree() && (
            <Alert variant="warning">
              <Paragraph>Para agregar más asesores o contactos, debes actualizar tu plan gratuito a un plan de pago.</Paragraph>
            </Alert>
          )}
          <Paragraph title>En esta sección se registra un directorio con los datos de asesores, contactos y prestadores de servicios que pueden llegar a ser relevantes en caso de tu fallecimiento o desaparición.<br /> Es una manera de concentrar los datos de un solo lugar para facilitar su localización a tus custodios.</Paragraph>
          <form onSubmit={onSaveOrUpdateAsesorContacto}>
            <FormRow>
              <FormColumn>
                <FormLabel center htmlFor="asesorContacto.tipo">Selecciona una categoría</FormLabel>
                <FormCategories>
                  <FormCategories.Category
                    name="asesorContacto.tipo"
                    id="asesorContacto.tipo_abogado"
                    disabled={isDisabledForm()}
                    checked={(asesorContacto.tipo === categoriaIds.ABOGADO)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.ABOGADO]}
                    value={categoriaIds.ABOGADO}
                  >
                    {(styles) => <IconTrubunal className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="asesorContacto.tipo"
                    id="asesorContacto.tipo_albacea_notario"
                    disabled={isDisabledForm()}
                    checked={(asesorContacto.tipo === categoriaIds.ALBACEA_NOTARIO)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.ALBACEA_NOTARIO]}
                    value={categoriaIds.ALBACEA_NOTARIO}
                  >
                    {(styles) => <IconBriefcase className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="asesorContacto.tipo"
                    id="asesorContacto.tutor_hijos"
                    disabled={isDisabledForm()}
                    checked={(asesorContacto.tipo === categoriaIds.TUTOR_HIJOS)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.TUTOR_HIJOS]}
                    value={categoriaIds.TUTOR_HIJOS}
                  >
                    {(styles) => <IconFamily className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="asesorContacto.tipo"
                    id="asesorContacto.tipo_contador"
                    disabled={isDisabledForm()}
                    checked={(asesorContacto.tipo === categoriaIds.CONTADOR)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.CONTADOR]}
                    value={categoriaIds.CONTADOR}
                  >
                    {(styles) => <IconAccounting className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="asesorContacto.tipo"
                    id="asesorContacto.tipo_medico_familiar"
                    disabled={isDisabledForm()}
                    checked={(asesorContacto.tipo === categoriaIds.MEDICO_FAMILIAR)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.MEDICO_FAMILIAR]}
                    value={categoriaIds.MEDICO_FAMILIAR}
                  >
                    {(styles) => <IconStethoscope className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="asesorContacto.tipo"
                    id="asesorContacto.tipo_guia_espiritual"
                    disabled={isDisabledForm()}
                    checked={(asesorContacto.tipo === categoriaIds.GUIA_ESPIRITUAL)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.GUIA_ESPIRITUAL]}
                    value={categoriaIds.GUIA_ESPIRITUAL}
                  >
                    {(styles) => <IconChurch className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                  <FormCategories.Category
                    name="asesorContacto.tipo"
                    id="asesorContacto.tipo_otro"
                    disabled={isDisabledForm()}
                    checked={(asesorContacto.tipo === categoriaIds.OTRO)}
                    onChange={onChangeInput}
                    label={categoriaNombres[categoriaIds.OTRO]}
                    value={categoriaIds.OTRO}
                  >
                    {(styles) => <IconEmployees className={styles.Icon} height="52px" />}
                  </FormCategories.Category>
                </FormCategories>
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="asesorContacto.nombre_completo">Nombre completo*</FormLabel>
                <FormInput
                  type="text"
                  name="asesorContacto.nombre_completo"
                  id="asesorContacto.nombre_completo"
                  disabled={isDisabledForm()}
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={asesorContacto.nombre_completo}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="asesorContacto.telefono_particular">Teléfono particular*</FormLabel>
                <FormInput
                  type="tel"
                  name="asesorContacto.telefono_particular"
                  id="asesorContacto.telefono_particular"
                  disabled={isDisabledForm()}
                  required
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeInput}
                  value={asesorContacto.telefono_particular}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="asesorContacto.telefono_oficina">Teléfono de oficina <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="tel"
                  name="asesorContacto.telefono_oficina"
                  id="asesorContacto.telefono_oficina"
                  disabled={isDisabledForm()}
                  minLength={10}
                  maxLength={10}
                  onChange={onChangeInput}
                  value={asesorContacto.telefono_oficina}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left strict>
                <FormLabel htmlFor="asesorContacto.correo_electronico">Correo electrónico <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                <FormInput
                  type="text"
                  name="asesorContacto.correo_electronico"
                  id="asesorContacto.correo_electronico"
                  disabled={isDisabledForm()}
                  maxLength={320}
                  pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                  title="Por favor ingrese un correo electrónico valido."
                  onChange={onChangeInput}
                  value={asesorContacto.correo_electronico}
                />
              </FormColumn>
            </FormRow>
            <FormSeparator />
            <FormTitle>Domicilio</FormTitle>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="asesorContacto.calle_numero">Calle y número*</FormLabel>
                <FormInput
                  type="text"
                  name="asesorContacto.calle_numero"
                  id="asesorContacto.calle_numero"
                  disabled={isDisabledForm()}
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={asesorContacto.calle_numero}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="asesorContacto.colonia">Colonia*</FormLabel>
                <FormInput
                  type="text"
                  name="asesorContacto.colonia"
                  id="asesorContacto.colonia"
                  disabled={isDisabledForm()}
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={asesorContacto.colonia}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn left>
                <FormLabel htmlFor="asesorContacto.codigo_postal">C.P. <FormText>(Opcional)</FormText></FormLabel>
                <FormInput
                  type="number"
                  name="asesorContacto.codigo_postal"
                  id="asesorContacto.codigo_postal"
                  disabled={isDisabledForm()}
                  digits={5}
                  min={0}
                  max={99999}
                  step={1}
                  onChange={onChangeInput}
                  value={asesorContacto.codigo_postal}
                />
              </FormColumn>
              <FormColumn center>
                <FormLabel htmlFor="asesorContacto.municipio">Municipio*</FormLabel>
                <FormInput
                  type="text"
                  name="asesorContacto.municipio"
                  id="asesorContacto.municipio"
                  disabled={isDisabledForm()}
                  required
                  maxLength={100}
                  onChange={onChangeInput}
                  value={asesorContacto.municipio}
                />
              </FormColumn>
              <FormColumn right>
                <FormLabel htmlFor="asesorContacto.estado">Estado*</FormLabel>
                <FormInput
                  type="text"
                  name="asesorContacto.estado"
                  id="asesorContacto.estado"
                  disabled={isDisabledForm()}
                  required
                  maxLength={40}
                  onChange={onChangeInput}
                  value={asesorContacto.estado}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating || isDisabledForm()}
                  onClick={onResetAsesorContactoInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!asesorContacto.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving || isDisabledForm()}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormToast
            variant="success"
            show={showToast}
            title="Mensaje de éxito"
            message={toastMessage}
            onClose={() => setShowToast(false)}
          />
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th rowSpan={2}>Categoría</th>
            <th rowSpan={2}>Nombre completo</th>
            <th rowSpan={2}>Teléfono particular</th>
            <th rowSpan={2}>Teléfono de oficina</th>
            <th rowSpan={2}>Correo electrónico</th>
            <th colSpan={5}>Domicilio</th>
            {canWrite() && (<th rowSpan={2}></th>)}
          </tr>
          <tr>
            <th>Calle y número</th>
            <th>Colonia</th>
            <th>C.P.</th>
            <th>Municipio</th>
            <th>Estado</th>
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan={11}>Cargando...</td>
            </tr>
          ) : hasAsesoresContactos ? asesoresContactos.map((asesorContacto) => (
            <tr key={asesorContacto.id}>
              <td>{categoriaNombres[asesorContacto.tipo]}</td>
              <td>{asesorContacto.nombre_completo}</td>
              <td>{asesorContacto.telefono_particular}</td>
              <td>{asesorContacto.telefono_oficina}</td>
              <td>{asesorContacto.correo_electronico}</td>
              <td>{asesorContacto.calle_numero}</td>
              <td>{asesorContacto.colonia}</td>
              <td>{asesorContacto.codigo_postal}</td>
              <td>{asesorContacto.municipio}</td>
              <td>{asesorContacto.estado}</td>
              {canWrite() && (
                <td>
                  <FormButtonGroup>
                    <Button
                      variant="secondary"
                      small
                      onClick={() => onLoadInputs(asesorContacto)}
                    >
                      Editar
                    </Button>
                    <Button
                      variant="danger"
                      small
                      onClick={() => onDeleteAsesorContacto(asesorContacto.id)}
                    >
                      Eliminar
                    </Button>
                  </FormButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan={11}>
                {canWrite() ? 'No has agregado ningún asesor o contacto.' : 'No se agregó ningún asesor o contacto.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </Layout>
  );
};

export default AsesoresContactos;
