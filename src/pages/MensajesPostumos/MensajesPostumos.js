import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import set from 'lodash/set';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import Dropdown from 'react-bootstrap/Dropdown';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import Feedback from '../../components/UI/Feedback/Feedback';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormText from '../../components/UI/Form/FormText/FormText';
import FormHelpText from '../../components/UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../components/UI/Form/FormInput/FormInput';
import FormFile from '../../components/UI/Form/FormFile/FormFile';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import FormMargin from '../../components/UI/Form/FormMargin/FormMargin';
import FormTable from '../../components/UI/Form/FormTable/FormTable';
import AudioPlayer from '../../components/UI/AudioPlayer/AudioPlayer';
import AudioDownloader from '../../components/UI/AudioDownloader/AudioDownloader';
import SelectedVideo from '../../components/SelectedVideo/SelectedVideo';
import SelectedFotogradia from '../../components/SelectedFotografia/SelectedFotografia';
import AudioRecorder from '../../components/AudioRecorder/AudioRecorder';
import VideoRecorder from '../../components/VideoRecorder/VideoRecorder';
import Camera from '../../components/Camera/Camera';
import SavedVideo from '../../components/SavedVideo/SavedVideo';
import SavedFotografia from '../../components/SavedFotografia/SavedFotografia';
import IconDownload from '../../components/UI/Icons/IconDownload/IconDownload';
import useUser from '../../hooks/useUser';
import * as actions from '../../actions/mi-historia-legado/mensajes-postumos';
import * as constants from '../../constants';
import * as helpers from '../../helpers';

const MensajesPostumos = () => {
  const [columnIndex, setColumnIndex] = React.useState(null);
  const [audio, setAudio] = React.useState(null);
  const [video, setVideo] = React.useState(null);
  const [fotografia, setFotografia] = React.useState(null);
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const mensajesPostumos = useSelector((state) => state.mensajesPostumos.mensajesPostumos);
  const custodios = useSelector((state) => state.mensajesPostumos.custodios);
  const familiares = useSelector((state) => state.mensajesPostumos.familiares);
  const mensajePostumo = useSelector((state) => state.mensajesPostumos.mensajePostumo);
  const isGetting = useSelector((state) => state.mensajesPostumos.isGetting);
  const isSaving = useSelector((state) => state.mensajesPostumos.isSaving);
  const isUpdating = useSelector((state) => state.mensajesPostumos.isUpdating);
  const isDownloadingAudio = useSelector((state) => state.mensajesPostumos.isDownloadingAudio);
  const isDownloadingVideo = useSelector((state) => state.mensajesPostumos.isDownloadingVideo);
  const isDownloadingFotografia = useSelector((state) => state.mensajesPostumos.isDownloadingFotografia);
  const errors = useSelector((state) => state.mensajesPostumos.errors);
  const dispatch = useDispatch();

  const { canWrite } = useUser();

  const hasMensajesPostumos = (mensajesPostumos.length > 0);
  const hasCustodios = (custodios.length > 0);
  const hasFamiliares = (familiares.length > 0);
  const tipoDestinatario = constants.mensajesPostumosTipoDestinatario;

  const hasAudio = () => {
    return (audio || !helpers.isEmptyString(mensajePostumo.audio));
  };

  const isSubmitting = () => {
    return (isSaving || isUpdating);
  };

  const isDownloadingMultimedia = (index) => {
    return (isDownloadingAudio && (index === columnIndex))
        || (isDownloadingVideo && (index === columnIndex))
        || (isDownloadingFotografia && (index === columnIndex));
  };

  const getUpdatedAt = () => {
    const [mensajePostumo] = mensajesPostumos.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    if (isGetting) {
      return 'Cargando...';
    }

    const updatedAt = mensajePostumo?.updated_at;

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const getCorreoElectronico = (mensajePostumo) => {
    const custodio = mensajePostumo.custodio;
    const familiar = mensajePostumo.familiar;

    if (custodio) {
      return custodio.correo_electronico;
    }

    if (familiar) {
      return familiar.correo_electronico;
    }

    return mensajePostumo.correo_electronico;
  };

  const onChangeMensajePostumoInput = (event) => {
    const input = event.target;
    const [, attribute] = input.name.split('.');
    const value = input.type === 'checkbox'
      ? input.checked : input.type === 'radio'
      ? parseInt(input.value, 10) : input.value;

    dispatch(actions.changeMensajePostumoInput(attribute, value));
  };

  const onResetMensajePostumoInputs = () => dispatch(actions.resetMensajePostumoInputs());

  const onChangeMensajePostumoAudioInput = (value) => setAudio(value);

  const onRemoveMensajePostumoAudio = () => {
    setAudio(null);
    onDeleteMensajePostumoAudio();
  };

  const onDeleteMensajePostumoAudio = () => dispatch(actions.changeMensajePostumoInput('audio', ''));

  const onChangeMensajePostumoVideoInput = (value) => setVideo(value);

  const onRemoveMensajePostumoVideo = () => {
    setVideo(null);
    onDeleteMensajePostumoVideo();
  };

  const onDeleteMensajePostumoVideo = () => dispatch(actions.changeMensajePostumoInput('video', ''));

  const onChangeMensajePostumoFotografiaInput = (value) => setFotografia(value);

  const onRemoveMensajePostumoFotografia = () => {
    setFotografia(null);
    onDeleteMensajePostumoFotografia();
  };

  const onDeleteMensajePostumoFotografia = () => dispatch(actions.changeMensajePostumoInput('fotografia', ''));

  const onEditMensajePostumo = (mensajePostumo) => {
    window.scrollTo(0, 0);
    dispatch(actions.editMensajePostumo(mensajePostumo));
  };

  const onSaveOrUpdateMensajePostumo = async (event) => {
    event.preventDefault();

    const isAudio = helpers.isFileOrBlob(audio);
    const isVideo = helpers.isFileOrBlob(video);
    const isFotografia = helpers.isFileOrBlob(fotografia);

    set(mensajePostumo, 'audio', isAudio ? audio : mensajePostumo.audio);
    set(mensajePostumo, 'video', isVideo ? video : mensajePostumo.video);
    set(mensajePostumo, 'fotografia', isFotografia ? fotografia : mensajePostumo.fotografia);

    setShowToast(false);
    setToastMessage('');

    if (!mensajePostumo.id && await dispatch(actions.saveMensajePostumo(mensajePostumo))) {
      setAudio(null);
      setVideo(null);
      setFotografia(null);
      setToastMessage('Los datos del mensaje póstumo se guardaron con éxito.');
      setShowToast(true);
    }

    if (mensajePostumo.id && await dispatch(actions.updateMensajePostumo(mensajePostumo))) {
      setAudio(null);
      setVideo(null);
      setFotografia(null);
      setToastMessage('Los datos del mensaje póstumo se actualizaron con éxito.');
      setShowToast(true);
    }
  };

  const getCustodioNombreCompletoById = (id) => {
    const custodio = custodios.find((custodio) => custodio.id === parseInt(id, 10));

    return custodio ? custodio.nombre_completo : '-';
  };

  const getFamiliarNombreCompletoById = (id) => {
    const familiar = familiares.find((familiar) => familiar.id === parseInt(id, 10));

    return familiar ? familiar.nombre_completo : '-';
  };

  const onDeleteMensajePostumo = async (id) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este mensaje póstumo?');
    setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteMensajePostumo(id))) {
      setToastMessage('Los datos del mensaje póstumo se eliminaron con éxito.');
      setShowToast(true);
    }
  };

  const onDownloadMensajePostumoInMp3 = (mensajePostumo, index = null) => {
    setColumnIndex(index);
    dispatch(actions.downloadMensajePostumoAudio(mensajePostumo.id, 'mp3'));
  };

  const onDownloadMensajePostumoInStandard = (mensajePostumo, index = null) => {
    const extension = mensajePostumo.audio.split('.').pop();

    setColumnIndex(index);
    dispatch(actions.downloadMensajePostumoAudio(mensajePostumo.id, extension));
  };

  const onDownloadMensajePostumoVideo = (mensajePostumo, index) => {
    const extension = mensajePostumo.video.split('.').pop();

    setColumnIndex(index);
    dispatch(actions.downloadMensajePostumoVideo(mensajePostumo.id, extension));
  };

  const onDownloadMensajePostumoFotografia = (mensajePostumo, index) => {
    const extension = mensajePostumo.fotografia.split('.').pop();

    setColumnIndex(index);
    dispatch(actions.downloadMensajePostumoFotografia(mensajePostumo.id, extension));
  };

  const onResetErrors = () => {
    dispatch(actions.resetMensajesPostumosErrors());
  };

  React.useEffect(() => {
    window.scrollTo(0, 0);

    dispatch(actions.getMensajesPostumos());

    return () => {
      dispatch(actions.resetMensajesPostumosErrors());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Mensajes póstumos">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Mensajes póstumos</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onResetErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {helpers.isObject(errors) && Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <Paragraph title>La sección Mensajes póstumos te permite el envío de un mensaje personal dirigido exclusivamente a una persona, ya sea un custodio, parte de la red familiar o a quien tú elijas, solo requieres contar con su correo electrónico.<br /> Los mensajes son enviados cuando los custodios confirmen tu fallecimiento mediante la prueba de supervivencia.<br /><br /> Esta sección cuenta con privacidad adicional, ya que a ella no tendrán acceso tus custodios, por lo que cuentas con toda libertad para expresarte.</Paragraph>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateMensajePostumo}>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="mensajePostumo.tipo_destinatario">Tipo de destinatario</FormLabel>
                <FormInput
                  as="radio"
                  inline
                  name="mensajePostumo.tipo_destinatario"
                  id="mensajePostumo.tipo_destinatario_custorio"
                  checked={mensajePostumo.tipo_destinatario === tipoDestinatario.ID.CUSTODIO}
                  label={tipoDestinatario.NAME[tipoDestinatario.ID.CUSTODIO]}
                  onChange={onChangeMensajePostumoInput}
                  value={tipoDestinatario.ID.CUSTODIO}
                />
                <FormInput
                  as="radio"
                  inline
                  name="mensajePostumo.tipo_destinatario"
                  id="mensajePostumo.tipo_destinatario_familiar"
                  checked={mensajePostumo.tipo_destinatario === tipoDestinatario.ID.FAMILIAR}
                  label={tipoDestinatario.NAME[tipoDestinatario.ID.FAMILIAR]}
                  onChange={onChangeMensajePostumoInput}
                  value={tipoDestinatario.ID.FAMILIAR}
                />
                <FormInput
                  as="radio"
                  inline
                  name="mensajePostumo.tipo_destinatario"
                  id="mensajePostumo.tipo_destinatario_otro"
                  checked={mensajePostumo.tipo_destinatario === tipoDestinatario.ID.OTRO}
                  label={tipoDestinatario.NAME[tipoDestinatario.ID.OTRO]}
                  onChange={onChangeMensajePostumoInput}
                  value={tipoDestinatario.ID.OTRO}
                />
              </FormColumn>
            </FormRow>
            {(mensajePostumo.tipo_destinatario === 1) && (
              <FormRow>
                <FormColumn>
                  <FormLabel htmlFor="mensajePostumo.custodio_id">Custodio*</FormLabel>
                  <FormInput
                    as="select"
                    name="mensajePostumo.custodio_id"
                    id="mensajePostumo.custodio_id"
                    disabled={!hasCustodios}
                    required
                    onChange={onChangeMensajePostumoInput}
                    value={mensajePostumo.custodio_id}
                  >
                    <option value="">Seleccione...</option>
                    {custodios.map(({ id, nombre_completo }) => (
                      <option key={id} value={id}>
                        {nombre_completo}
                      </option>
                    ))}
                  </FormInput>
                </FormColumn>
              </FormRow>
            )}
            {(mensajePostumo.tipo_destinatario === 2) && (
              <FormRow>
                <FormColumn>
                  <FormLabel htmlFor="mensajePostumo.familiar_id">Familiar*</FormLabel>
                  <FormInput
                    as="select"
                    name="mensajePostumo.familiar_id"
                    id="mensajePostumo.familiar_id"
                    disabled={!hasFamiliares}
                    required
                    onChange={onChangeMensajePostumoInput}
                    value={mensajePostumo.familiar_id}
                  >
                    <option value="">Seleccione...</option>
                    {familiares.map(({ id, nombre_completo }) => (
                      <option key={id} value={id}>
                        {nombre_completo}
                      </option>
                    ))}
                  </FormInput>
                </FormColumn>
              </FormRow>
            )}
            {(mensajePostumo.tipo_destinatario === 3) && (
              <FormRow>
                <FormColumn left>
                  <FormLabel htmlFor="mensajePostumo.destinatario">Destinatario*</FormLabel>
                  <FormHelpText>&nbsp;</FormHelpText>
                  <FormInput
                    type="text"
                    name="mensajePostumo.destinatario"
                    id="mensajePostumo.destinatario"
                    required
                    maxLength={100}
                    onChange={onChangeMensajePostumoInput}
                    value={mensajePostumo.destinatario}
                  />
                </FormColumn>
                <FormColumn right>
                  <FormLabel htmlFor="mensajePostumo.correo_electronico">Correo electrónico*</FormLabel>
                  <FormHelpText>Ejemplo: micorreo@correo.com</FormHelpText>
                  <FormInput
                    type="text"
                    name="mensajePostumo.correo_electronico"
                    id="mensajePostumo.correo_electronico"
                    required
                    maxLength={191}
                    pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                    title="Por favor ingrese un correo electrónico valido."
                    onChange={onChangeMensajePostumoInput}
                    value={mensajePostumo.correo_electronico}
                  />
                </FormColumn>
              </FormRow>
            )}
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="mensajePostumo.instrucciones_entrega">Instrucciones de entrega*</FormLabel>
                <FormInput
                  as="textarea"
                  name="mensajePostumo.instrucciones_entrega"
                  id="mensajePostumo.instrucciones_entrega"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeMensajePostumoInput}
                  value={mensajePostumo.instrucciones_entrega}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="mensajePostumo.mensaje">Mensaje*</FormLabel>
                <FormInput
                  as="textarea"
                  name="mensajePostumo.mensaje"
                  id="mensajePostumo.mensaje"
                  rows={3}
                  required
                  maxLength={500}
                  onChange={onChangeMensajePostumoInput}
                  value={mensajePostumo.mensaje}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="mensajePostumo.is_grabar_audio">Audio <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>El audio debe de pesar como máximo 8 MB y ser de tipo .mp3, .webm, .oga, .weba o .wav.</FormHelpText>
                <FormInput
                  as="switch"
                  name="mensajePostumo.is_grabar_audio"
                  id="mensajePostumo.is_grabar_audio"
                  checked={mensajePostumo.is_grabar_audio}
                  falseText="Subir"
                  trueText="Grabar"
                  onChange={onChangeMensajePostumoInput}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                {mensajePostumo.is_grabar_audio ? (
                  !hasAudio() && <AudioRecorder onRecorded={onChangeMensajePostumoAudioInput} />
                ) : (
                  !hasAudio() && <FormFile onChange={onChangeMensajePostumoAudioInput} />
                )}
                {helpers.isFileOrBlob(audio) && <AudioPlayer audio={audio} onRemove={onRemoveMensajePostumoAudio} />}
                {helpers.isUrl(mensajePostumo.audio) && (
                  <AudioDownloader
                    audio={mensajePostumo.audio}
                    isDownloading={isDownloadingAudio && (columnIndex === null)}
                    onDownloadInMp3={() => onDownloadMensajePostumoInMp3(mensajePostumo)}
                    onDownloadInStandard={() => onDownloadMensajePostumoInStandard(mensajePostumo)}
                    onRemove={onDeleteMensajePostumoAudio}
                  />
                )}
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="mensajePostumo.is_grabar_video">Video <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>El video debe de pesar como máximo 64 MB y ser de tipo .mp4, .mpeg, .avi, .mkv o .webm.</FormHelpText>
                <FormInput
                  as="switch"
                  name="mensajePostumo.is_grabar_video"
                  id="mensajePostumo.is_grabar_video"
                  checked={mensajePostumo.is_grabar_video}
                  falseText="Subir"
                  trueText="Grabar"
                  onChange={onChangeMensajePostumoInput}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              {mensajePostumo.is_grabar_video ? (
                (!video && helpers.isEmptyString(mensajePostumo.video)) && (
                  <FormColumn>
                    <VideoRecorder onRecorded={onChangeMensajePostumoVideoInput} />
                  </FormColumn>
                )
              ) : (
                (!video && helpers.isEmptyString(mensajePostumo.video)) && (
                  <FormColumn>
                    <FormFile onChange={onChangeMensajePostumoVideoInput} />
                  </FormColumn>
                )
              )}
              {(helpers.isFile(video) || helpers.isBlob(video)) && (
                <FormColumn>
                  <SelectedVideo video={video} onRemove={onRemoveMensajePostumoVideo} />
                </FormColumn>
              )}
              {(!helpers.isEmptyString(mensajePostumo.video) && !helpers.isFile(video)) && (
                <FormColumn>
                  <SavedVideo
                    video={mensajePostumo.video}
                    onDownload={() => onDownloadMensajePostumoVideo(mensajePostumo, null)}
                    isDownloading={isDownloadingVideo && (columnIndex === null)}
                    onDelete={onDeleteMensajePostumoVideo}
                  />
                </FormColumn>
              )}
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="mensajePostumo.is_tomar_fotografia">Fotografía <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>La fotografía debe de pesar como máximo 5 MB y ser de tipo .jpeg, .bmp o .png.</FormHelpText>
                <FormInput
                  as="switch"
                  name="mensajePostumo.is_tomar_fotografia"
                  id="mensajePostumo.is_tomar_fotografia"
                  checked={mensajePostumo.is_tomar_fotografia}
                  falseText="Subir"
                  trueText="Grabar"
                  onChange={onChangeMensajePostumoInput}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              {mensajePostumo.is_tomar_fotografia ? (
                (!fotografia && helpers.isEmptyString(mensajePostumo.fotografia)) && (
                  <FormColumn>
                    <Camera onTakenPicture={onChangeMensajePostumoFotografiaInput} />
                  </FormColumn>
                )
              ) : (
                (!fotografia && helpers.isEmptyString(mensajePostumo.fotografia)) && (
                  <FormColumn>
                    <FormFile onChange={onChangeMensajePostumoFotografiaInput} />
                  </FormColumn>
                )
              )}
              {(helpers.isFile(fotografia) || helpers.isBlob(fotografia)) && (
                <FormColumn>
                  <SelectedFotogradia fotografia={fotografia} onRemove={onRemoveMensajePostumoFotografia} />
                </FormColumn>
              )}
              {(!helpers.isEmptyString(mensajePostumo.fotografia) && !helpers.isFile(fotografia)) && (
                <FormColumn>
                  <SavedFotografia
                    fotografia={mensajePostumo.fotografia}
                    onDownload={() => onDownloadMensajePostumoFotografia(mensajePostumo, null)}
                    isDownloading={isDownloadingFotografia && (columnIndex === null)}
                    onDelete={onDeleteMensajePostumoFotografia}
                  />
                </FormColumn>
              )}
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  align="right"
                  className="c-button c-button--light"
                  disabled={isSubmitting()}
                  onClick={onResetMensajePostumoInputs}
                >
                  Restablecer
                </Button>
                &nbsp;
              </FormColumn>
              <FormColumn auto>
                {!mensajePostumo.id ? (
                  <Button
                    type="submit"
                    align="right"
                    className="c-button c-button--primary"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    align="right"
                    className="c-button c-button--primary"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormToast
            variant="success"
            show={showToast}
            title="Mensaje de éxito"
            message={toastMessage}
            onClose={() => setShowToast(false)}
          />
          <FormMargin />
        </React.Fragment>
      )}
      <FormTable>
        <thead>
          <tr>
            <th>Tipo de destinatario</th>
            <th>Custodio</th>
            <th>Familiar</th>
            <th>Otro destinatario</th>
            <th>Correo electrónico</th>
            <th>Instrucciones de entrega</th>
            <th>Mensaje</th>
            <th>Descargar multimedia</th>
            {canWrite() && (<th></th>)}
          </tr>
        </thead>
        <tbody>
          {isGetting ? (
            <tr>
              <td colSpan="9">Cargando...</td>
            </tr>
          ) : hasMensajesPostumos ? mensajesPostumos.map((mensajePostumo, index) => (
            <tr key={mensajePostumo.id}>
              <td>{tipoDestinatario.NAME[mensajePostumo.tipo_destinatario]}</td>
              <td>{getCustodioNombreCompletoById(mensajePostumo.custodio_id)}</td>
              <td>{getFamiliarNombreCompletoById(mensajePostumo.familiar_id)}</td>
              <td>{mensajePostumo.destinatario}</td>
              <td>{getCorreoElectronico(mensajePostumo)}</td>
              <td>{mensajePostumo.instrucciones_entrega}</td>
              <td>{mensajePostumo.mensaje}</td>
              <td>
                {(!helpers.isEmptyString(mensajePostumo.audio)
                  || !helpers.isEmptyString(mensajePostumo.video)
                  || !helpers.isEmptyString(mensajePostumo.fotografia)) ? (
                    <Dropdown
                      as={ButtonGroup}
                      className="c-dropdown"
                    >
                      <Button
                        className="c-button c-button--secondary text-nowrap"
                        disabled={isDownloadingMultimedia(index)}
                        onClick={() => {
                          if (!helpers.isEmptyString(mensajePostumo.audio)) {
                            onDownloadMensajePostumoInMp3(mensajePostumo, index);
                          } else if (!helpers.isEmptyString(mensajePostumo.video)) {
                            onDownloadMensajePostumoVideo(mensajePostumo, index);
                          } else if (!helpers.isEmptyString(mensajePostumo.fotografia)) {
                            onDownloadMensajePostumoFotografia(mensajePostumo, index);
                          }
                        }}
                      >
                        <IconDownload className="c-button__icon" height="14px" />&nbsp;
                        {isDownloadingMultimedia(index) ? (
                          'Descargando...'
                        ) : (
                          !helpers.isEmptyString(mensajePostumo.audio)
                            ? 'Descargar audio en .mp3'
                            : !helpers.isEmptyString(mensajePostumo.video)
                              ? 'Descargar video'
                              : !helpers.isEmptyString(mensajePostumo.fotografia)
                                ? 'Descargar fotografía'
                                : null
                        )}
                      </Button>
                      {([
                        mensajePostumo.audio,
                        mensajePostumo.audio,
                        mensajePostumo.video,
                        mensajePostumo.fotografia
                      ].reduce((count, item) => {
                        if (!helpers.isEmptyString(item)) {
                          return count + 1;
                        }

                        return count;
                      }, 0) >= 2) && (
                        <>
                          <Dropdown.Toggle
                            className="c-dropdown__toggle c-dropdown__toggle--secondary"
                            disabled={isDownloadingMultimedia(index)}
                          />
                          <Dropdown.Menu alignRight>
                            {!helpers.isEmptyString(mensajePostumo.audio) && (
                              <Dropdown.Item
                                className="c-dropdown__item"
                                onClick={() => onDownloadMensajePostumoInStandard(mensajePostumo, index)}
                              >
                                Descargar audio en otro formato
                              </Dropdown.Item>
                            )}
                            {(!helpers.isEmptyString(mensajePostumo.audio) && !helpers.isEmptyString(mensajePostumo.video)) && (
                              <Dropdown.Item
                                className="c-dropdown__item"
                                onClick={() => onDownloadMensajePostumoVideo(mensajePostumo, index)}
                              >
                                Descargar video
                              </Dropdown.Item>
                            )}
                            {!helpers.isEmptyString(mensajePostumo.fotografia) && (
                              <Dropdown.Item
                                className="c-dropdown__item"
                                onClick={() => onDownloadMensajePostumoFotografia(mensajePostumo, index)}
                              >
                                Descargar fotografía
                              </Dropdown.Item>
                            )}
                          </Dropdown.Menu>
                        </>
                      )}
                    </Dropdown>
                  ) : '-'}
              </td>
              {canWrite() && (
                <td>
                  <ButtonGroup>
                    <Button
                      className="c-button c-button--secondary"
                      onClick={() => onEditMensajePostumo(mensajePostumo)}
                    >
                      Editar
                    </Button>
                    <Button
                      className="c-button c-button--danger"
                      onClick={() => onDeleteMensajePostumo(mensajePostumo.id)}
                    >
                      Eliminar
                    </Button>
                  </ButtonGroup>
                </td>
              )}
            </tr>
          )) : (
            <tr>
              <td colSpan="9">
                {canWrite() ? 'No has agregado ningún mensaje póstumo.' : 'No se agregó ningún mensaje póstumo.'}
              </td>
            </tr>
          )}
        </tbody>
      </FormTable>
    </Layout>
  );
};

export default MensajesPostumos;
