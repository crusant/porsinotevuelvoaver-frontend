import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import nth from 'lodash/nth';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Feedback from '../../components/UI/Feedback/Feedback';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import FormRow from '../../components/UI/Form/FormRow/FormRow';
import FormColumn from '../../components/UI/Form/FormColumn/FormColumn';
import FormLabel from '../../components/UI/Form/FormLabel/FormLabel';
import FormText from '../../components/UI/Form/FormText/FormText';
import FormHelpText from '../../components/UI/Form/FormHelpText/FormHelpText';
import FormInput from '../../components/UI/Form/FormInput/FormInput';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import FormMargin from '../../components/UI/Form/FormMargin/FormMargin';
import FormButtonGroup from '../../components/UI/Form/FormButtonGroup/FormButtonGroup';
import File from '../../components/UI/File/File';
import Button from '../../components/UI/Button/Button';
import Table from '../../components/UI/Table/Table';
import IconDownload from '../../components/UI/Icons/IconDownload/IconDownload';
import * as actions from '../../actions/documentos-digitales-ubicacion-fisica';
import * as datosPersonalesActions from '../../actions/datos-personales-custodios-red-familiar/datos-personales';
import * as polizasSegurosActions from '../../actions/polizas-seguros';
import * as datosEmpleoActions from '../../actions/empleo-pension/datos-empleo';
import * as datosTestamentoActions from '../../actions/ultimos-deseos/datos-testamento';
import * as restosActions from '../../actions/ultimos-deseos/restos';
import * as mascotasActions from '../../actions/mascotas';
import * as helpers from '../../helpers';
import DragAndDrop from '../../components/UI/DragAndDrop/DragAndDrop';

const DocumentosDigitalesUbicacionFisica = () => {
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const [columnIndex, setColumnIndex] = React.useState(null);
  const user = useSelector((state) => state.auth.user);
  const documento = useSelector((state) => state.documentosDigitalesUbicacionFisica.documento);
  const documentos = useSelector((state) => state.documentosDigitalesUbicacionFisica.documentos);
  const isLoading = useSelector((state) => state.documentosDigitalesUbicacionFisica.isLoading);
  const isSaving = useSelector((state) => state.documentosDigitalesUbicacionFisica.isSaving);
  const isUpdating = useSelector((state) => state.documentosDigitalesUbicacionFisica.isUpdating);
  const isDeleting = useSelector((state) => state.documentosDigitalesUbicacionFisica.isDeleting);
  const isDownloading = useSelector((state) => state.documentosDigitalesUbicacionFisica.isDownloading);
  const isDownloadingActaNacimiento = useSelector((state) => state.datosPersonales.isDownloadingActaNacimiento);
  const isDownloadingActaMatrimonio = useSelector((state) => state.datosPersonales.isDownloadingActaMatrimonio);
  const isDownloadingPolizaSeguro = useSelector((state) => state.polizasSeguros.isDownloading);
  const isDownloadingContrato = useSelector((state) => state.datosEmpleo.isDownloadingContrato);
  const isDownloadingReciboPago = useSelector((state) => state.datosEmpleo.isDownloadingReciboPago);
  const isDownloadingTestamento = useSelector((state) => state.datosTestamento.isDownloading);
  const isDownloadingContratoOrDocumento = useSelector((state) => state.restos.isDownloading);
  const isDownloadingCartillaVacunacionOrDocumento = useSelector((state) => state.mascotas.isDownloading);
  const errors = useSelector((state) => state.documentosDigitalesUbicacionFisica.errors);
  const dispatch = useDispatch();

  const columns = React.useMemo(() => [
    {
      Header: 'Nombre documento',
      accessor: 'nombre'
    },
    {
      Header: 'Documento digital',
      id: 'documento_digital',
      Cell: ({ row: { index, original: documento} }) => {
        const isCurrentDownloading = (
          (isDownloading
          || isDownloadingActaNacimiento
          || isDownloadingActaMatrimonio
          || isDownloadingPolizaSeguro
          || isDownloadingContrato
          || isDownloadingReciboPago
          || isDownloadingTestamento
          || isDownloadingContratoOrDocumento
          || isDownloadingCartillaVacunacionOrDocumento)
          && (index === columnIndex)
        );

        return !helpers.isEmptyString(documento.documento_digital) && (
          <Button
            variant="primary"
            small
            disabled={isCurrentDownloading}
            onClick={() => onDownloadDocumento(documento, index)}
          >
            <IconDownload height="14px" /> {isCurrentDownloading ? 'Descargando...' : 'Descargar'}
          </Button>
        );
      }
    },
    {
      Header: 'Ubicación física',
      accessor: 'documento_fisico'
    },
    {
      Header: () => null,
      id: 'buttons',
      Cell: ({ row: { index, original: documento } }) => {
        const isCurrentDeleting = (isDeleting && (index === columnIndex));

        if (documento.hasOwnProperty('index')) {
          return null;
        }

        return (
          <FormButtonGroup>
            <Button
              variant="secondary"
              small
              onClick={() => onEditDocumento(documento)}
            >
              Editar
            </Button>
            <Button
              variant="danger"
              small
              disabled={isCurrentDeleting}
              onClick={() => onDeleteDocumento(documento.id, index)}
            >
              {isCurrentDeleting ? 'Eliminando...' : 'Eliminar'}
            </Button>
          </FormButtonGroup>
        );
      }
    }

    // eslint-disable-next-line
  ], [
    columnIndex,
    isDeleting,
    isDownloading,
    isDownloadingActaNacimiento,
    isDownloadingActaMatrimonio,
    isDownloadingPolizaSeguro,
    isDownloadingContrato,
    isDownloadingReciboPago,
    isDownloadingTestamento,
    isDownloadingContratoOrDocumento,
    isDownloadingCartillaVacunacionOrDocumento
  ]);

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    const [documento] = documentos.sort((a, b) => {
      return a.updated_at > b.updated_at ? -1
        : a.updated_at < b.updated_at ? 1
        : 0;
    });

    if (isLoading) {
      return 'Cargando...';
    }

    const updatedAt = documento?.updated_at;

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const onChangeDocumentoInput = (event) => {
    const input = event.target;
    const [, attribute] = input.name.split('.');

    dispatch(actions.changeDocumentoInput(attribute, input.value));
  };

  const onChangeDocumentoDigitalInput = (files) => {
    dispatch(actions.changeDocumentoInput('documento_digital', nth(files, 0)));
  };

  const onRemoveDocumentoDigitalInput = () => {
    dispatch(actions.changeDocumentoInput('documento_digital', ''));
  };

  const onResetDocumentoInputs = () => {
    dispatch(actions.resetDocumentoInputs())
  };

  const onEditDocumento = (documento) => {
    window.scrollTo(0, 0);
    dispatch(actions.editDocumento(documento));
  };

  const onSaveOrUpdateDocumento = async (event) => {
    event.preventDefault();

    const formData = new FormData();
    helpers.buildFormData(formData, documento);

    setShowToast(false);
    setToastMessage('');

    if (!documento.id && await dispatch(actions.saveDocumento(formData))) {
      setToastMessage('Los datos del documento se guardaron con éxito.');
      setShowToast(true);
    }

    if (documento.id && await dispatch(actions.updateDocumento(formData))) {
      setToastMessage('Los datos del documento se actualizaron con éxito.');
      setShowToast(true);
    }
  };

  const onDeleteDocumento = async (id, index) => {
    const isConfirmed = window.confirm('¿Está seguro(a) de querer eliminar este documento?');
    setShowToast(false);

    if (isConfirmed && await dispatch(actions.deleteDocumento(id))) {
      setColumnIndex(index);
      setToastMessage('Los datos del documento se eliminaron con éxito.');
      setShowToast(true);
    }
  };

  const onDownloadDocumento = (documento, index) => {
    const extension = String(documento.documento_digital).split('.').pop();

    setColumnIndex(index);

    switch (documento.index) {
      case 0:
        dispatch(datosPersonalesActions.downloadDatosPersonalesActaNacimiento(extension));
        break;
      case 1:
        dispatch(datosPersonalesActions.downloadDatosPersonalesActaMatrimonio(extension));
        break;
      case 2:
        dispatch(polizasSegurosActions.downloadPolizaSeguro(documento.id, extension));
        break;
      case 3:
        dispatch(datosEmpleoActions.downloadDatosEmpleoContrato(extension));
        break;
      case 4:
        dispatch(datosEmpleoActions.downloadDatosEmpleoReciboPago(extension));
        break;
      case 5:
        dispatch(datosTestamentoActions.downloadDatosTestamentoDocumento(extension));
        break;
      case 6:
        dispatch(restosActions.downloadRestosDocumento(documento.tipo, extension));
        break;
      case 7:
        dispatch(mascotasActions.downloadMascotaDocumento(documento.id, extension));
        break;
      default:
        dispatch(actions.downloadDocumento(documento.id, extension));
        break;
    }
  };

  const onResetDocumentosErrors = () => {
    dispatch(actions.resetDocumentosErrors());
  };

  React.useEffect(() => {
    window.scrollTo(0, 0);

    dispatch(actions.getDocumentos());

    return () => {
      onResetDocumentosErrors();
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout title="Documentos digitales y ubicación física">
      <StepProgressBar activeKey="0">
        <StepProgressBar.Step eventKey="0">1. Documentos digitales y ubicación fisica</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetDocumentosErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onResetDocumentosErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>
                {error}
              </Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      <Paragraph title>Esta sección es un repositorio de documentos digitales. Aquí puedes cargar archivos de documentos, fotografías de recuerdos e incluso imágenes de cartas escritas a mano, el límite es tu imaginación.<br /><br /> Puedes llevar a cabo el resguardo de documentos digitalizados en formato PDF, Word, Excel, JPG o PNG.</Paragraph>
      {canWrite() && (
        <React.Fragment>
          <form onSubmit={onSaveOrUpdateDocumento}>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="documento.nombre">Nombre del documento*</FormLabel>
                <FormInput
                  type="text"
                  name="documento.nombre"
                  id="documento.nombre"
                  required
                  maxLength={100}
                  onChange={onChangeDocumentoInput}
                  value={documento.nombre}
                />
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="documento.documento_digital">Documento digital <FormText>(Opcional)</FormText></FormLabel>
                <FormHelpText>El archivo debe de pesar como máximo 5 MB y ser de tipo PDF, Word, PowerPoint, Excel, .jpg o .png.</FormHelpText>
                {helpers.isEmptyString(documento.documento_digital) ? (
                  <DragAndDrop
                    accept=".pdf, .doc, .docx, .ppt, .pptx, .xls, .xlsx, .jpg, .jpeg, .png"
                    maxSize={5242880}
                    multiple={false}
                    onDropAccepted={onChangeDocumentoDigitalInput}
                  />
                ) : (
                  <File
                    file={documento.documento_digital}
                    onRemove={onRemoveDocumentoDigitalInput}
                  />
                )}
              </FormColumn>
            </FormRow>
            <FormRow>
              <FormColumn>
                <FormLabel htmlFor="documento.documento_fisico">Donde encontrar el documento*</FormLabel>
                <FormInput
                  type="text"
                  name="documento.documento_fisico"
                  id="documento.documento_fisico"
                  required
                  maxLength={500}
                  onChange={onChangeDocumentoInput}
                  value={documento.documento_fisico}
                />
              </FormColumn>
            </FormRow>
            <FormRow right>
              <FormColumn auto>
                <Button
                  variant="light"
                  align="right"
                  marginRight
                  disabled={isSaving || isUpdating}
                  onClick={onResetDocumentoInputs}
                >
                  Restablecer
                </Button>
              </FormColumn>
              <FormColumn auto>
                {!documento.id ? (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isSaving}
                  >
                    {isSaving ? 'Guardando...' : 'Guardar'}
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    variant="primary"
                    align="right"
                    disabled={isUpdating}
                  >
                    {isUpdating ? 'Actualizando...' : 'Actualizar'}
                  </Button>
                )}
              </FormColumn>
            </FormRow>
          </form>
          <FormToast
            variant="success"
            show={showToast}
            title="Mensaje de éxito"
            message={toastMessage}
            onClose={() => setShowToast(false)}
          />
          <FormMargin />
        </React.Fragment>
      )}
      <Table
        columns={columns}
        hiddenColumns={[!canWrite() ? 'buttons': '']}
        data={documentos}
        loading={isLoading}
      />
    </Layout>
  );
};

export default DocumentosDigitalesUbicacionFisica;
