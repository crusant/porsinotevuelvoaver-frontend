import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import Alert from 'react-bootstrap/Alert';
import ErrorTitle from '../../components/UI/ErrorTitle/ErrorTitle';
import Errors from '../../components/UI/Errors/Errors';
import Layout from '../../components/Layout/Layout';
import StepProgressBar from '../../components/UI/StepProgressBar/StepProgressBar';
import Feedback from '../../components/UI/Feedback/Feedback';
import Paragraph from '../../components/UI/Paragraph/Paragraph';
import FormToast from '../../components/UI/Form/FormToast/FormToast';
import EscrituraLibre from '../../components/PanelUsuario/MiHistoriaLegado/EscrituraLibre/EscrituraLibre';
import MiHistoria from '../../components/PanelUsuario/MiHistoriaLegado/MiHistoria/MiHistoria';
import * as escrituraLibreActions from '../../actions/mi-historia-legado/escritura-libre';
import * as miHistoriaActions from '../../actions/mi-historia-legado/mi-historia';
import * as helpers from '../../helpers';

const MiHistoriaLegado = () => {
  const [activeKey, setActiveKey] = React.useState('0');
  const [updatedAt, setUpdatedAt] = React.useState(null);
  const [showToast, setShowToast] = React.useState(false);
  const [toastMessage, setToastMessage] = React.useState('');
  const user = useSelector((state) => state.auth.user);

  const isLoading = useSelector((state) => {
    return state.escrituraLibre.isGetting
        || state.miHistoria.isGetting;
  });

  const errors = useSelector((state) => {
    return state.escrituraLibre.errors
        || state.miHistoria.errors;
  });

  const dispatch = useDispatch();

  const canWrite = () =>
    user?.can_write;

  const getUpdatedAt = () => {
    if (isLoading) {
      return 'Cargando...';
    }

    return updatedAt ? moment(updatedAt).format('LL') : '';
  };

  const onSelectKey = (key) =>
    setActiveKey(key);

  const onResetErrors = () => {
    dispatch(escrituraLibreActions.resetEscrituraLibreErrors());
    dispatch(miHistoriaActions.resetMiHistoriaErrors());
  };

  React.useEffect(() => {
    if (!helpers.isNull(errors)) {
      window.scrollTo(0, 0);
    }
  }, [errors]);

  return (
    <Layout title="Mi historia y legado">
      <StepProgressBar activeKey={activeKey}>
        <StepProgressBar.Step eventKey="0" onSelect={onSelectKey}>1. Cuenta tu historia</StepProgressBar.Step>
        <StepProgressBar.Step eventKey="1" onSelect={onSelectKey}>2. Mi historia y legado</StepProgressBar.Step>
      </StepProgressBar>
      <Feedback>
        <span><strong>Última modificación:</strong> {getUpdatedAt()}</span>
        <span>{canWrite() ? '*Campos obligatorios' : <>&nbsp;</>}</span>
      </Feedback>
      {helpers.isString(errors) && (
        <Alert variant="danger" onClose={onResetErrors} dismissible>
          <Paragraph>{errors}</Paragraph>
        </Alert>
      )}
      {helpers.isObject(errors) && (
        <Alert variant="danger" onClose={onResetErrors} dismissible>
          <ErrorTitle>Por favor corrija los siguientes errores.</ErrorTitle>
          <Errors>
            {Object.values(errors).map((error, index) => (
              <Errors.Error key={index}>{error}</Errors.Error>
            ))}
          </Errors>
        </Alert>
      )}
      {(activeKey === '0') && (
        <EscrituraLibre
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      {(activeKey === '1') && (
        <MiHistoria
          onSelectKey={onSelectKey}
          setUpdatedAt={setUpdatedAt}
          setShowToast={setShowToast}
          setToastMessage={setToastMessage}
        />
      )}
      <FormToast
        variant="success"
        show={showToast}
        title="Mensaje de éxito"
        message={toastMessage}
        onClose={() => setShowToast(false)}
      />
    </Layout>
  );
};

export default MiHistoriaLegado;
