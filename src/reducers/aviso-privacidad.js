import _ from 'lodash';
import * as types from '../actions/types/aviso-privacidad';

const defaultAvisoPrivacidad = {
  nombre: '',
  documento_digital: ''
};

const initialState = {
  avisoPrivacidad: {...defaultAvisoPrivacidad},
  isGetting: false,
  isUpdating: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_AVISO_PRIVACIDAD_INIT:
      return {
        ...state,
        avisoPrivacidad: {...defaultAvisoPrivacidad},
        isGetting: true,
        errors: null
      };
    case types.GET_AVISO_PRIVACIDAD_DONE:
      return {
        ...state,
        avisoPrivacidad: {...action.avisoPrivacidad},
        isGetting: false
      };
    case types.GET_AVISO_PRIVACIDAD_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    case types.CHANGE_AVISO_PRIVACIDAD_INPUTS:
      return {
        ...state,
        avisoPrivacidad: {
          ...state.avisoPrivacidad,
          [action.attribute]: action.value
        }
      };
    case types.UPDATE_AVISO_PRIVACIDAD_INIT:
      return {
        ...state,
        isUpdating: true,
        errors: null
      };
    case types.UPDATE_AVISO_PRIVACIDAD_DONE:
      return {
        ...state,
        avisoPrivacidad: {...action.avisoPrivacidad},
        isUpdating: false
      };
    case types.UPDATE_AVISO_PRIVACIDAD_FAIL:
      return {
        ...state,
        isUpdating: false,
        errors: action.errors
      };
    case types.RESET_AVISO_PRIVACIDAD_ERRORS:
      return {
        ...state,
        errors: null
      };
    case types.RESET_AVISO_PRIVACIDAD_TO_STARTUP:
      return _.cloneDeep(initialState);
    default:
      return state;
  }
};

export default reducer;
