import * as types from '../actions/types/app';

const initialState = {
  isDownloading: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.DOWNLOAD_FORMULARIOS_INIT:
      return {
        ...state,
        isDownloading: true,
        errors: null
      };
    case types.DOWNLOAD_FORMULARIOS_DONE:
      return {
        ...state,
        isDownloading: false
      };
    case types.DOWNLOAD_FORMULARIOS_FAIL:
      return {
        ...state,
        isDownloading: false,
        errors: action.errors
      };
    default:
      return state;
  }
};

export default reducer;
