import _ from 'lodash';
import * as types from '../actions/types/terminos-condiciones';

const defaultTerminosCondiciones = {
  nombre: '',
  documento_digital: ''
};

const initialState = {
  terminosCondiciones: {...defaultTerminosCondiciones},
  isGetting: false,
  isUpdating: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_TERMINOS_CONDICIONES_INIT:
      return {
        ...state,
        terminosCondiciones: {...defaultTerminosCondiciones},
        isGetting: true,
        errors: null
      };
    case types.GET_TERMINOS_CONDICIONES_DONE:
      return {
        ...state,
        terminosCondiciones: {...action.terminosCondiciones},
        isGetting: false
      };
    case types.GET_TERMINOS_CONDICIONES_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    case types.CHANGE_TERMINOS_CONDICIONES_INPUTS:
      return {
        ...state,
        terminosCondiciones: {
          ...state.terminosCondiciones,
          [action.attribute]: action.value
        }
      };
    case types.UPDATE_TERMINOS_CONDICIONES_INIT:
      return {
        ...state,
        isUpdating: true,
        errors: null
      };
    case types.UPDATE_TERMINOS_CONDICIONES_DONE:
      return {
        ...state,
        terminosCondiciones: {...action.terminosCondiciones},
        isUpdating: false
      };
    case types.UPDATE_TERMINOS_CONDICIONES_FAIL:
      return {
        ...state,
        isUpdating: false,
        errors: action.errors
      };
    case types.RESET_TERMINOS_CONDICIONES_ERRORS:
      return {
        ...state,
        errors: null
      };
    case types.RESET_TERMINOS_CONDICIONES_TO_STARTUP:
      return _.cloneDeep(initialState);
    default:
      return state;
  }
};

export default reducer;
