import * as types from '../actions/types/auth';

const initialState = {
  token: null,
  user: {},
  isChecking: true,
  isRegisteringFreePlan: false,
  isPayingWithPayPal: false,
  isPayingWithCreditCard: false,
  ticket: '',
  isPayingWithOxxo: false,
  isPaidWithOxxo: false,
  isPaid: false,
  isResettingPassword: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN_START:
      return {
        ...state,
        token: null,
        user: {},
        errors: null
      };
    case types.LOGIN_DONE:
      return {
        ...state,
        token: action.token,
        user: action.user
      };
    case types.LOGIN_FAIL:
      return {
        ...state,
        errors: action.errors
      };
    case types.CHECK_IF_USER_IS_AUTHENTICATED:
      return {
          ...state,
          token: action.token,
          user: action.user,
          isChecking: false
      };

    case types.REGISTER_FREE_PLAN_START:
      return {
        ...state,
        token: null,
        user: {},
        isRegisteringFreePlan: true,
        errors: null
      };
    case types.REGISTER_FREE_PLAN_DONE:
      return {
        ...state,
        token: action.token,
        user: action.user,
        isRegisteringFreePlan: false
      };
    case types.REGISTER_FREE_PLAN_FAIL:
      return {
        ...state,
        isRegisteringFreePlan: false,
        errors: action.errors
      };

    case types.PAYPAL_CREATE_ORDER_START:
      return {
        ...state,
        errors: null
      };
    case types.PAYPAL_CREATE_ORDER_FAIL:
      return {
        ...state,
        errors: action.errors
      };
    case types.PAY_WITH_PAYPAL_AND_REGISTER_INIT:
      return {
        ...state,
        token: null,
        user: {},
        isPayingWithPayPal: true,
        errors: null
      };
    case types.PAY_WITH_PAYPAL_AND_REGISTER_DONE:
      return {
        ...state,
        token: action.token,
        user: action.user,
        isPayingWithPayPal: false
      };
    case types.PAY_WITH_PAYPAL_AND_REGISTER_FAIL:
      return {
        ...state,
        isPayingWithPayPal: false,
        errors: action.errors
      };
    case types.PAY_WITH_PAYPAL_AND_RENEW_INIT:
      return {
        ...state,
        isPayingWithPayPal: true,
        errors: null
      };
    case types.PAY_WITH_PAYPAL_AND_RENEW_DONE:
      return {
        ...state,
        token: action.token,
        user: action.user,
        isPayingWithPayPal: false
      };
    case types.PAY_WITH_PAYPAL_AND_RENEW_FAIL:
      return {
        ...state,
        isPayingWithPayPal: false,
        errors: action.errors
      };

    case types.PAY_WITH_CREDIT_CARD_AND_REGISTER_INIT:
      return {
        ...state,
        token: null,
        user: {},
        isPayingWithCreditCard: true,
        errors: null
      };
    case types.PAY_WITH_CREDIT_CARD_AND_REGISTER_DONE:
      return {
        ...state,
        token: action.token,
        user: action.user,
        isPayingWithCreditCard: false
      };
    case types.PAY_WITH_CREDIT_CARD_AND_REGISTER_FAIL:
      return {
        ...state,
        isPayingWithCreditCard: false,
        errors: action.errors
      };
    case types.PAY_WITH_CREDIT_CARD_AND_RENEW_INIT:
      return {
        ...state,
        isPayingWithCreditCard: true,
        errors: null
      };
    case types.PAY_WITH_CREDIT_CARD_AND_RENEW_DONE:
      return {
        ...state,
        token: action.token,
        user: action.user,
        isPayingWithCreditCard: false
      };
    case types.PAY_WITH_CREDIT_CARD_AND_RENEW_FAIL:
      return {
        ...state,
        isPayingWithCreditCard: false,
        errors: action.errors
      };

    case types.PAY_WITH_OXXO_AND_REGISTER_INIT:
      return {
        ...state,
        ticket: '',
        isPayingWithOxxo: true,
        isPaidWithOxxo: false,
        errors: null
      };
    case types.PAY_WITH_OXXO_AND_REGISTER_DONE:
      return {
        ...state,
        ticket: action.ticket,
        isPayingWithOxxo: false,
        isPaidWithOxxo: true
      };
    case types.PAY_WITH_OXXO_AND_REGISTER_FAIL:
      return {
        ...state,
        isPayingWithOxxo: false,
        errors: action.errors
      };
    case types.PAY_WITH_OXXO_AND_RENEW_INIT:
      return {
        ...state,
        ticket: '',
        isPayingWithOxxo: true,
        isPaidWithOxxo: false,
        errors: null
      };
    case types.PAY_WITH_OXXO_AND_RENEW_DONE:
      return {
        ...state,
        token: action.token,
        user: action.user,
        ticket: action.ticket,
        isPayingWithOxxo: false,
        isPaidWithOxxo: true
      };
    case types.PAY_WITH_OXXO_AND_RENEW_FAIL:
      return {
        ...state,
        isPayingWithOxxo: false,
        errors: action.errors
      };

    case types.PASSWORD_RESET_INIT:
      return {
        ...state,
        isResettingPassword: true,
        errors: null
      };
    case types.PASSWORD_RESET_DONE:
      return {
        ...state,
        isResettingPassword: false
      };
    case types.PASSWORD_RESET_FAIL:
      return {
        ...state,
        isResettingPassword: false,
        errors: action.errors
      };

    case types.RESET_AUTH_ERRORS:
      return {
        ...state,
        errors: null
      };

    case types.LOGOUT:
      return {
        ...state,
        token: null,
        user: {},
        isChecking: false,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
