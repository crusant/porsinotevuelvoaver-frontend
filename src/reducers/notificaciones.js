import * as types from '../actions/types/notificaciones';

const defaultPruebaVida = {
  fecha_respuesta: '',
  fecha_limite: '',
  url: '',
  is_expired: false
};

const defaultNotificaciones = {
  pruebaVida: {...defaultPruebaVida},
  countUsers: 0
};

const initialState = {
  notificaciones: {...defaultNotificaciones},
  isGetting: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_NOTIFICACIONES_INIT:
      return {
        ...state,
        notificaciones: {...defaultNotificaciones},
        isGetting: true,
        errors: null
      };
    case types.GET_NOTIFICACIONES_DONE:
      return {
        ...state,
        notificaciones: {
          ...state.notificaciones,
          pruebaVida: {
            ...state.notificaciones.pruebaVida,
            ...action.notificaciones.pruebaVida
          },
          countUsers: action.notificaciones.countUsers
        },
        isGetting: false
      };
    case types.GET_NOTIFICACIONES_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    case types.CONFIRMAR_NOTIFICACION_PRUEBA_VIDA_INIT:
      return {
        ...state,
        isConfirming: true,
        isConfirmed: false
      };
    case types.CONFIRMAR_NOTIFICACION_PRUEBA_VIDA_DONE:
      return {
        ...state,
        notificaciones: {
          ...state.notificaciones,
          pruebaVida: {
            ...state.notificaciones.pruebaVida,
            ...action.pruebaVida
          }
        },
        isConfirming: false,
        isConfirmed: true
      };
    case types.CONFIRMAR_NOTIFICACION_PRUEBA_VIDA_FAIL:
      return {
        ...state,
        isConfirming: false,
        errors: action.errors
      };
    default:
      return state;
  }
};

export default reducer;

