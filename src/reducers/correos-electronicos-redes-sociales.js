import * as types from '../actions/types/correos-electronicos-redes-sociales';

const defaultCorreoElectronicoRedSocial = {
  id: null,
  tipo: 1,
  nombre: '',
  url: '',
  username: '',
  password: '',
  instrucciones: '',
  can_notificar_contactos: false,
  can_borrar_cuenta: false,
  can_ultima_publicacion: false,
  can_pagina_en_memoria: false
};

const initialState = {
  correoElectronicoRedSocial: {...defaultCorreoElectronicoRedSocial},
  correosElectronicosRedesSociales: [],
  isLoading: false,
  isSaving: false,
  isDecrypting: false,
  isUpdating: false,
  isDeleting: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  let correosElectronicosRedesSociales = [];

  switch (action.type) {
    case types.GET_CORREOS_ELECTRONICOS_REDES_SOCIALES_INIT:
      return {
        ...state,
        correoElectronicoRedSocial: {...defaultCorreoElectronicoRedSocial},
        correosElectronicosRedesSociales: [],
        isLoading: true,
        errors: null
      };
    case types.GET_CORREOS_ELECTRONICOS_REDES_SOCIALES_DONE:
      return {
        ...state,
        correosElectronicosRedesSociales: [...action.correosElectronicosRedesSociales],
        isLoading: false
      };
    case types.GET_CORREOS_ELECTRONICOS_REDES_SOCIALES_FAIL:
      return {
        ...state,
        isLoading: false,
        errors: action.errors
      };
    case types.CHANGE_CORREO_ELECTRONICO_RED_SOCIAL_INPUT:
      return {
        ...state,
        correoElectronicoRedSocial: {
          ...state.correoElectronicoRedSocial,
          [action.attribute]: action.value
        }
      };
    case types.RESET_CORREO_ELECTRONICO_RED_SOCIAL_INPUTS:
      return {
        ...state,
        correoElectronicoRedSocial: {...defaultCorreoElectronicoRedSocial},
        errors: null
      };
    case types.SAVE_CORREO_ELECTRONICO_RED_SOCIAL_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_CORREO_ELECTRONICO_RED_SOCIAL_DONE:
      return {
        ...state,
        correoElectronicoRedSocial: {...defaultCorreoElectronicoRedSocial},
        correosElectronicosRedesSociales: [
          ...state.correosElectronicosRedesSociales,
          action.correoElectronicoRedSocial
        ],
        isSaving: false
      };
    case types.SAVE_CORREO_ELECTRONICO_RED_SOCIAL_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.EDIT_CORREO_ELECTRONICO_RED_SOCIAL:
      return {
        ...state,
        correoElectronicoRedSocial: {...action.correoElectronicoRedSocial}
      };
    case types.UPDATE_CORREO_ELECTRONICO_RED_SOCIAL_INIT:
      return {
        ...state,
        isUpdating: true,
        errors: null
      };
    case types.UPDATE_CORREO_ELECTRONICO_RED_SOCIAL_DONE:
      correosElectronicosRedesSociales = state.correosElectronicosRedesSociales.map((correoElectronicoRedSocial) => {
        if (correoElectronicoRedSocial.id === action.correoElectronicoRedSocial.id) {
          return action.correoElectronicoRedSocial;
        }

        return correoElectronicoRedSocial;
      });

      return {
        ...state,
        correoElectronicoRedSocial: {...defaultCorreoElectronicoRedSocial},
        correosElectronicosRedesSociales,
        isUpdating: false
      };
    case types.UPDATE_CORREO_ELECTRONICO_RED_SOCIAL_FAIL:
      return {
        ...state,
        isUpdating: false,
        errors: action.errors
      };
    case types.DELETE_CORREO_ELECTRONICO_RED_SOCIAL_INIT:
      return {
        ...state,
        isDeleting: true,
        errors: null
      }
    case types.DELETE_CORREO_ELECTRONICO_RED_SOCIAL_DONE:
      correosElectronicosRedesSociales = state.correosElectronicosRedesSociales.filter((correoElectronicoRedSocial) => correoElectronicoRedSocial.id !== action.id);

      return {
        ...state,
        correosElectronicosRedesSociales,
        isDeleting: false
      };
    case types.DELETE_CORREO_ELECTRONICO_RED_SOCIAL_FAIL:
      return {
        ...state,
        isDeleting: false,
        errors: action.errors
      };
    case types.RESET_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
