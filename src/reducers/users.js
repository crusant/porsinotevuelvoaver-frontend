import * as types from '../actions/types/users';

const initialState = {
  isChanging: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.CHANGE_PASSWORD_INIT:
      return {
        ...state,
        isChanging: true,
        errors: null
      };
    case types.CHANGE_PASSWORD_DONE:
      return {
        ...state,
        isChanging: false
      };
    case types.CHANGE_PASSWORD_FAIL:
      return {
        ...state,
        isChanging: false,
        errors: action.errors
      };
    case types.RESET_PASSWORD_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
