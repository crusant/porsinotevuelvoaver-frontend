import * as types from '../actions/types/usuarios-registrados';
import * as helpers from '../helpers';

const initialState = {
  users: [],
  isGetting: false,
  isDelivering: false,
  isActivating: false,
  isConfirming: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_USUARIOS_REGISTRADOS_INIT:
      return {
        ...state,
        users: [],
        isGetting: true,
        errors: null
      };
    case types.GET_USUARIOS_REGISTRADOS_DONE:
      return {
        ...state,
        users: [...action.users],
        isGetting: false
      };
    case types.GET_USUARIOS_REGISTRADOS_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    case types.ENTREGAR_DATOS_ACCESO_INIT:
      return {
        ...state,
        isDelivering: true,
        errors: null
      };
    case types.ENTREGAR_DATOS_ACCESO_DONE:
      return {
        ...state,
        users: helpers.updateBy(state.users, action.user, ['id', action.user.id]),
        isDelivering: false
      };
    case types.ENTREGAR_DATOS_ACCESO_FAIL:
      return {
        ...state,
        isDelivering: false,
        errors: action.errors
      };
    case types.ACTIVAR_USUARIO_REGISTRADO_INIT:
      return {
        ...state,
        isActivating: true,
        errors: null
      };
    case types.ACTIVAR_USUARIO_REGISTRADO_DONE:
      return {
        ...state,
        users: helpers.updateBy(state.users, action.user, ['id', action.user.id]),
        isActivating: false
      };
    case types.ACTIVAR_USUARIO_REGISTRADO_FAIL:
      return {
        ...state,
        isActivating: false,
        errors: action.errors
      };
    case types.CONFIRMAR_PRUEBA_VIDA_INIT:
      return {
        ...state,
        isConfirming: true,
        errors: null
      };
    case types.CONFIRMAR_PRUEBA_VIDA_DONE:
      return {
        ...state,
        users: helpers.updateBy(state.users, action.user, ['id', action.user.id]),
        isConfirming: false
      };
    case types.CONFIRMAR_PRUEBA_VIDA_FAIL:
      return {
        ...state,
        isConfirming: false,
        errors: action.errors
      };
    case types.RESET_USUARIOS_REGISTRADOS:
      return {
        ...state,
        users: [],
        isGetting: false,
        isActivating: false,
        errors: null
      };
    case types.RESET_USUARIOS_REGISTRADOS_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;

