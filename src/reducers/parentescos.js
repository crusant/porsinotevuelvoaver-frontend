import * as types from '../actions/types/parentescos';

const initialState = {
  parentescos: [],
  isLoading: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_PARENTESCOS_INIT:
      return {
        ...state,
        parentescos: [],
        isLoading: true,
        errors: null
      };
    case types.GET_PARENTESCOS_DONE:
      return {
        ...state,
        parentescos: [...action.parentescos],
        isLoading: false
      };
    case types.GET_PARENTESCOS_FAIL:
      return {
        ...state,
        isLoading: false,
        errors: action.errors
      };
    default:
      return {...state};
  }
};

export default reducer;
