import * as types from '../../actions/types/mi-historia-legado/mi-historia';

const defaultRespuesta = {
  texto: '',
  audio: '',
  video: '',
  is_grabar_audio: false,
  is_grabar_video: false
};

const initialState = {
  preguntas: [],
  isGetting: false,
  isSaving: false,
  isDownloadingAudio: false,
  isDownloadingVideo: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_MI_HISTORIA_INIT:
      return {
        ...state,
        preguntas: [],
        isGetting: true,
        errors: null
      };
    case types.GET_MI_HISTORIA_DONE: {
      const preguntas = action.preguntas.map((pregunta) => {
        if (!pregunta.respuesta) {
          pregunta.respuesta = {...defaultRespuesta};
        }

        return pregunta;
      });

      return {
        ...state,
        preguntas,
        isGetting: false
      };
    }
    case types.GET_MI_HISTORIA_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    case types.CHANGE_MI_HISTORIA_INPUT: {
      const preguntas = [...state.preguntas];
      preguntas[action.index].respuesta[action.attribute] = action.value;

      return {
        ...state,
        preguntas
      };
    }
    case types.SAVE_MI_HISTORIA_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_MI_HISTORIA_DONE: {
      const preguntas = state.preguntas.map((pregunta) => {
        if (pregunta.id === action.respuesta.pregunta_id) {
          pregunta.respuesta = action.respuesta;
          return pregunta;
        }

        return pregunta;
      });

      return {
        ...state,
        preguntas,
        isSaving: false
      };
    }
    case types.SAVE_MI_HISTORIA_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.DOWNLOAD_MI_HISTORIA_AUDIO_INIT:
      return {
        ...state,
        isDownloadingAudio: true,
        errors: null
      };
    case types.DOWNLOAD_MI_HISTORIA_AUDIO_DONE:
      return {
        ...state,
        isDownloadingAudio: false
      };
    case types.DOWNLOAD_MI_HISTORIA_AUDIO_FAIL:
      return {
        ...state,
        isDownloadingAudio: false,
        errors: action.errors
      };
    case types.DOWNLOAD_MI_HISTORIA_VIDEO_INIT:
      return {
        ...state,
        isDownloadingVideo: true,
        errors: null
      };
    case types.DOWNLOAD_MI_HISTORIA_VIDEO_DONE:
      return {
        ...state,
        isDownloadingVideo: false
      };
    case types.DOWNLOAD_MI_HISTORIA_VIDEO_FAIL:
      return {
        ...state,
        isDownloadingVideo: false,
        errors: action.errors
      };
    case types.RESET_MI_HISTORIA_ERRORS:
      return {
        ...state,
        errors: null
      };
    case types.RESET_MI_HISTORIA:
      return {...initialState};
    default:
      return state;
  }
};

export default reducer;
