import * as types from '../../actions/types/mi-historia-legado/escritura-libre';

const defaultEscrituraLibre = {
  portada: '',
  texto: '',
  audio: '',
  video: '',
  is_grabar_audio: false,
  is_grabar_video: false,
  route: ''
};

const initialState = {
  escrituraLibre: {...defaultEscrituraLibre},
  isGetting: false,
  isSaving: false,
  isDownloadingPortada: false,
  isDownloadingAudio: false,
  isDownloadingVideo: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_ESCRITURA_LIBRE_INIT:
      return {
        ...state,
        escrituraLibre: {...defaultEscrituraLibre},
        isGetting: true,
        errors: null
      };
    case types.GET_ESCRITURA_LIBRE_DONE:
      return {
        ...state,
        escrituraLibre: {
          ...state.escrituraLibre,
          ...action.escrituraLibre
        },
        isGetting: false
      };
    case types.GET_ESCRITURA_LIBRE_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    case types.CHANGE_ESCRITURA_LIBRE_INPUT:
      return {
        ...state,
        escrituraLibre: {
          ...state.escrituraLibre,
          [action.attribute]: action.value
        }
      };
    case types.SAVE_ESCRITURA_LIBRE_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_ESCRITURA_LIBRE_DONE:
      return {
        ...state,
        escrituraLibre: {
          ...state.escrituraLibre,
          ...action.escrituraLibre
        },
        isSaving: false
      };
    case types.SAVE_ESCRITURA_LIBRE_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.DOWNLOAD_ESCRITURA_LIBRE_AUDIO_INIT:
      return {
        ...state,
        isDownloadingAudio: true,
        errors: null
      };
    case types.DOWNLOAD_ESCRITURA_LIBRE_AUDIO_DONE:
      return {
        ...state,
        isDownloadingAudio: false
      };
    case types.DOWNLOAD_ESCRITURA_LIBRE_AUDIO_FAIL:
      return {
        ...state,
        isDownloadingAudio: false,
        errors: action.errors
      };
    case types.DOWNLOAD_ESCRITURA_LIBRE_VIDEO_INIT:
      return {
        ...state,
        isDownloadingVideo: true,
        errors: null
      };
    case types.DOWNLOAD_ESCRITURA_LIBRE_VIDEO_DONE:
      return {
        ...state,
        isDownloadingVideo: false
      };
    case types.DOWNLOAD_ESCRITURA_LIBRE_VIDEO_FAIL:
      return {
        ...state,
        isDownloadingVideo: false,
        errors: action.errors
      };
    case types.RESET_ESCRITURA_LIBRE_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
