import * as types from '../../actions/types/mi-historia-legado/mensajes-postumos';

const defaultMensajePostumo = {
  id: null,
  tipo_destinatario: 1,
  destinatario: '',
  correo_electronico: '',
  instrucciones_entrega: '',
  mensaje: '',
  audio: '',
  video: '',
  fotografia: '',
  is_grabar_audio: false,
  is_grabar_video: false,
  is_tomar_fotografia: false,
  custodio_id: '',
  familiar_id: ''
};

const initialState = {
  mensajePostumo: {...defaultMensajePostumo},
  mensajesPostumos: [],
  custodios: [],
  familiares: [],
  isGetting: false,
  isSaving: false,
  isUpdating: false,
  isDeleting: false,
  isDownloadingAudio: false,
  isDownloadingVideo: false,
  isDownloadingFotografia: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  let mensajesPostumos = [];

  switch (action.type) {
    case types.GET_MENSAJES_POSTUMOS_INIT:
      return {
        ...state,
        mensajePostumo: {...defaultMensajePostumo},
        mensajesPostumos: [],
        custodios: [],
        familiares: [],
        isGetting: true,
        errors: null
      };
    case types.GET_MENSAJES_POSTUMOS_DONE:
      return {
        ...state,
        mensajesPostumos: [...action.data.mensajesPostumos],
        custodios: [...action.data.custodios],
        familiares: [...action.data.familiares],
        isGetting: false
      };
    case types.GET_MENSAJES_POSTUMOS_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    case types.CHANGE_MENSAJE_POSTUMO_INPUT:
      return {
        ...state,
        mensajePostumo: {
          ...state.mensajePostumo,
          [action.attribute]: action.value
        }
      };
    case types.RESET_MENSAJE_POSTUMO_INPUTS:
      return {
        ...state,
        mensajePostumo: {...defaultMensajePostumo},
        errors: null
      };
    case types.SAVE_MENSAJE_POSTUMO_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_MENSAJE_POSTUMO_DONE:
      return {
        ...state,
        mensajePostumo: {...defaultMensajePostumo},
        mensajesPostumos: [
          ...state.mensajesPostumos,
          action.mensajePostumo
        ],
        isSaving: false
      };
    case types.SAVE_MENSAJE_POSTUMO_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.GET_MENSAJE_POSTUMO_INIT:
      return {
        ...state,
        mensajePostumo: {...defaultMensajePostumo},
        isGetting: true,
        errors: null
      };
    case types.GET_MENSAJE_POSTUMO_DONE:
      return {
        ...state,
        mensajePostumo: {...action.mensajePostumo},
        isGetting: false
      };
    case types.GET_MENSAJE_POSTUMO_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: null
      };
    case types.EDIT_MENSAJE_POSTUMO:
      return {
        ...state,
        mensajePostumo: {...action.mensajePostumo}
      };
    case types.UPDATE_MENSAJE_POSTUMO_INIT:
      return {
        ...state,
        isUpdating: true,
        errors: null
      };
    case types.UPDATE_MENSAJE_POSTUMO_DONE:
      mensajesPostumos = state.mensajesPostumos.map((mensajePostumo) => {
        if (mensajePostumo.id === action.mensajePostumo.id) {
          return action.mensajePostumo;
        }

        return mensajePostumo;
      });

      return {
        ...state,
        mensajePostumo: {...defaultMensajePostumo},
        mensajesPostumos,
        isUpdating: false
      };
    case types.UPDATE_MENSAJE_POSTUMO_FAIL:
      return {
        ...state,
        isUpdating: false,
        errors: action.errors
      };
    case types.DELETE_MENSAJE_POSTUMO_INIT:
      return {
        ...state,
        isDeleting: true,
        errors: null
      };
    case types.DELETE_MENSAJE_POSTUMO_DONE:
      mensajesPostumos = state.mensajesPostumos.filter((mensajePostumo) => mensajePostumo.id !== action.id);

      return {
        ...state,
        mensajesPostumos,
        isDeleting: false
      };
    case types.DELETE_MENSAJE_POSTUMO_FAIL:
      return {
        ...state,
        isDeleting: false,
        errors: action.errors
      };
    case types.DOWNLOAD_MENSAJE_POSTUMO_AUDIO_INIT:
      return {
        ...state,
        isDownloadingAudio: true,
        errors: null
      };
    case types.DOWNLOAD_MENSAJE_POSTUMO_AUDIO_DONE:
      return {
        ...state,
        isDownloadingAudio: false
      };
    case types.DOWNLOAD_MENSAJE_POSTUMO_AUDIO_FAIL:
      return {
        ...state,
        isDownloadingAudio: false,
        errors: action.errors
      };
    case types.DOWNLOAD_MENSAJE_POSTUMO_VIDEO_INIT:
      return {
        ...state,
        isDownloadingVideo: true,
        errors: null
      };
    case types.DOWNLOAD_MENSAJE_POSTUMO_VIDEO_DONE:
      return {
        ...state,
        isDownloadingVideo: false
      };
    case types.DOWNLOAD_MENSAJE_POSTUMO_VIDEO_FAIL:
      return {
        ...state,
        isDownloadingVideo: false,
        errors: action.errors
      };
    case types.DOWNLOAD_MENSAJE_POSTUMO_FOTOGRAFIA_INIT:
      return {
        ...state,
        isDownloadingFotografia: true,
        errors: null
      };
    case types.DOWNLOAD_MENSAJE_POSTUMO_FOTOGRAFIA_DONE:
      return {
        ...state,
        isDownloadingFotografia: false
      };
    case types.DOWNLOAD_MENSAJE_POSTUMO_FOTOGRAFIA_FAIL:
      return {
        ...state,
        isDownloadingFotografia: false,
        errors: action.errors
      };
    case types.RESET_MENSAJES_POSTUMOS_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
