import * as types from '../actions/types/configuracion-tiempo-respuesta-pruebas-vida';

const defaultConfiguracionPruebaVidaUsuario = {
  dias_respuesta: 0
};

const defaultPruebaVidaCustodio = {
  fecha_respuesta: null
};

const initialState = {
  configuracionPruebaVidaUsuario: {...defaultConfiguracionPruebaVidaUsuario},
  pruebaVidaCustodio: {...defaultPruebaVidaCustodio},
  isGetting: false,
  isSaving: false,
  isConfirming: false,
  isDelivering: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_INIT:
      return {
        ...state,
        configuracionPruebaVidaUsuario: {...defaultConfiguracionPruebaVidaUsuario},
        isGetting: true,
        errors: null
      };
    case types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_DONE:
      return {
        ...state,
        configuracionPruebaVidaUsuario: {
          ...state.configuracionPruebaVidaUsuario,
          ...action.configuracionPruebaVidaUsuario
        },
        isGetting: false
      };
    case types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    case types.CHANGE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_INPUT:
      return {
        ...state,
        configuracionPruebaVidaUsuario: {
          ...state.configuracionPruebaVidaUsuario,
          [action.attribute]: action.value
        }
      };
    case types.RESET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_INPUTS:
      return {
        ...state,
        configuracionPruebaVidaUsuario: {...defaultConfiguracionPruebaVidaUsuario}
      };
    case types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_DONE:
      return {
        ...state,
        configuracionPruebaVidaUsuario: {
          ...state.configuracionPruebaVidaUsuario,
          ...action.configuracionPruebaVidaUsuario
        },
        isSaving: false
      };
    case types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.CONFIRMAR_PRUEBA_VIDA_CUSTODIO_INIT:
      return {
        ...state,
        pruebaVidaCustodio: {...defaultPruebaVidaCustodio},
        isConfirming: true
      };
    case types.CONFIRMAR_PRUEBA_VIDA_CUSTODIO_DONE:
      return {
        ...state,
        pruebaVidaCustodio: {...action.pruebaVidaCustodio},
        isConfirming: false
      };
    case types.CONFIRMAR_PRUEBA_VIDA_CUSTODIO_FAIL:
      return {
        ...state,
        isConfirming: false,
        pruebaVidaCustodio: {
          ...state.pruebaVidaCustodio,
          ...action.pruebaVida
        },
        errors: action.errors
      };
    case types.ENTREGAR_DATOS_ACCESO_CUSTODIO_INIT:
      return {
        ...state,
        isDelivering: true,
        errors: null
      };
    case types.ENTREGAR_DATOS_ACCESO_CUSTODIO_DONE:
      return {
        ...state,
        pruebaVidaCustodio: {...action.pruebaVida},
        isDelivering: false
      };
    case types.ENTREGAR_DATOS_ACCESO_CUSTODIO_FAIL:
      return {
        ...state,
        pruebaVidaCustodio: {
          ...state.pruebaVidaCustodio,
          ...action.pruebaVida
        },
        isDelivering: false,
        errors: action.errors
      };
    case types.RESET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
