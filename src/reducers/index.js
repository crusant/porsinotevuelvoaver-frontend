import { combineReducers } from 'redux';
import appReducer from './app';
import contactoReducer from './contacto';
import parentescosReducer from './parentescos';
import preregistrosReducer from './preregistros';
import authReducer from './auth';
import usersReducer from './users';
import menuPrincipalReducer from './menu-principal';
import configuracionEnvioPruebasVidaReducer from './configuracion-envio-pruebas-vida';
import configuracionTiempoRespuestaPruebasVidaReducer from './configuracion-tiempo-respuesta-pruebas-vida';
import configuracionTiempoRespuestaPruebasVidaCustodiosReducer from './configuracion-tiempo-respuesta-pruebas-vida-custodios';
import usuariosRegistradosReducer from './usuarios-registrados';
import avisoPrivacidadReducer from './aviso-privacidad';
import terminosCondicionesReducer from './terminos-condiciones';
import notificacionesReducer from './notificaciones';
import datosPersonalesReducer from './datos-personales-custodios-red-familiar/datos-personales';
import custodiosReducer from './datos-personales-custodios-red-familiar/custodios';
import redFamiliarReducer from './datos-personales-custodios-red-familiar/red-familiar';
import asesoresContactosReducer from './asesores-contactos';
import polizasSegurosReducer from './polizas-seguros';
import inversionesReducer from './patrimonio/inversiones';
import adeudosReducer from './patrimonio/adeudos';
import vehiculosReducer from './patrimonio/vehiculos';
import bienesInmueblesReducer from './patrimonio/bienes-inmuebles';
import accionesNegociosReducer from './patrimonio/acciones-negocios';
import bienesValoresSentimentalesReducer from './patrimonio/bienes-valores-sentimentales';
import otrosPatrimoniosReducer from './patrimonio/otros-patrimonios';
import datosEmpleoReducer from './empleo-pension/datos-empleo';
import seguridadSocialReducer from './empleo-pension/seguridad-social';
import planPensionReducer from './empleo-pension/plan-pension';
import datosTestamentoReducer from './ultimos-deseos/datos-testamento';
import donacionOrganosReducer from './ultimos-deseos/donacion-organos';
import vidaArtificialReducer from './ultimos-deseos/vida-artificial';
import secuestroDesaparicionReducer from './ultimos-deseos/secuestro-desaparicion';
import restosReducer from './ultimos-deseos/restos';
import mascotasReducer from './mascotas';
import instruccionesParticularesReducer from './instrucciones-particulares';
import correosElectronicosRedesSocialesReducer from './correos-electronicos-redes-sociales';
import escrituraLibreReducer from './mi-historia-legado/escritura-libre';
import miHistoriaReducer from './mi-historia-legado/mi-historia';
import mensajesPostumosReducer from './mi-historia-legado/mensajes-postumos';
import documentosDigitalesUbicacionFisicaReducer from './documentos-digitales-ubicacion-fisica';
import monedasReducer from './monedas';

const rootReducer = combineReducers({
  app: appReducer,
  contacto: contactoReducer,
  parentescos: parentescosReducer,
  preregistros: preregistrosReducer,
  auth: authReducer,
  users: usersReducer,
  menuPrincipal: menuPrincipalReducer,
  configuracionEnvioPruebasVida: configuracionEnvioPruebasVidaReducer,
  configuracionTiempoRespuestaPruebasVida: configuracionTiempoRespuestaPruebasVidaReducer,
  configuracionTiempoRespuestaPruebasVidaCustodios: configuracionTiempoRespuestaPruebasVidaCustodiosReducer,
  usuariosRegistrados: usuariosRegistradosReducer,
  avisoPrivacidad: avisoPrivacidadReducer,
  terminosCondiciones: terminosCondicionesReducer,
  notificaciones: notificacionesReducer,
  datosPersonales: datosPersonalesReducer,
  custodios: custodiosReducer,
  redFamiliar: redFamiliarReducer,
  asesoresContactos: asesoresContactosReducer,
  polizasSeguros: polizasSegurosReducer,
  inversiones: inversionesReducer,
  adeudos: adeudosReducer,
  vehiculos: vehiculosReducer,
  bienesInmuebles: bienesInmueblesReducer,
  accionesNegocios: accionesNegociosReducer,
  bienesValoresSentimentales: bienesValoresSentimentalesReducer,
  otrosPatrimonios: otrosPatrimoniosReducer,
  datosEmpleo: datosEmpleoReducer,
  seguridadSocial: seguridadSocialReducer,
  planPension: planPensionReducer,
  datosTestamento: datosTestamentoReducer,
  donacionOrganos: donacionOrganosReducer,
  vidaArtificial: vidaArtificialReducer,
  secuestroDesaparicion: secuestroDesaparicionReducer,
  restos: restosReducer,
  mascotas: mascotasReducer,
  instruccionesParticulares: instruccionesParticularesReducer,
  correosElectronicosRedesSociales: correosElectronicosRedesSocialesReducer,
  escrituraLibre: escrituraLibreReducer,
  miHistoria: miHistoriaReducer,
  mensajesPostumos: mensajesPostumosReducer,
  documentosDigitalesUbicacionFisica: documentosDigitalesUbicacionFisicaReducer,
  monedas: monedasReducer
});

export default rootReducer;
