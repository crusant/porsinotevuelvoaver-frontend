import * as types from '../actions/types/configuracion-envio-pruebas-vida';

const defaultConfiguracionPruebaVidaUsuario = {
  dias_envio: 0
};

const defaultPruebaVidaUsuario = {
  fecha_respuesta: null
};

const initialState = {
  configuracionPruebaVidaUsuario: {...defaultConfiguracionPruebaVidaUsuario},
  pruebaVidaUsuario: {...defaultPruebaVidaUsuario},
  isGetting: false,
  isSaving: false,
  isConfirming: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_CONFIGURACION_ENVIO_PRUEBAS_VIDA_INIT:
      return {
        ...state,
        configuracionPruebaVidaUsuario: {...defaultConfiguracionPruebaVidaUsuario},
        isGetting: true,
        errors: null
      };
    case types.GET_CONFIGURACION_ENVIO_PRUEBAS_VIDA_DONE:
      return {
        ...state,
        configuracionPruebaVidaUsuario: {
          ...state.configuracionPruebaVidaUsuario,
          ...action.configuracionPruebaVidaUsuario
        },
        isGetting: false
      };
    case types.GET_CONFIGURACION_ENVIO_PRUEBAS_VIDA_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    case types.CHANGE_CONFIGURACION_ENVIO_PRUEBAS_VIDA_INPUT:
      return {
        ...state,
        configuracionPruebaVidaUsuario: {
          ...state.configuracionPruebaVidaUsuario,
          [action.attribute]: action.value
        }
      };
    case types.RESET_CONFIGURACION_ENVIO_PRUEBAS_VIDA_INPUTS:
      return {
        ...state,
        configuracionPruebaVidaUsuario: {...defaultConfiguracionPruebaVidaUsuario}
      };
    case types.SAVE_CONFIGURACION_ENVIO_PRUEBAS_VIDA_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_CONFIGURACION_ENVIO_PRUEBAS_VIDA_DONE:
      return {
        ...state,
        configuracionPruebaVidaUsuario: {
          ...state.configuracionPruebaVidaUsuario,
          ...action.configuracionPruebaVidaUsuario
        },
        isSaving: false
      };
    case types.SAVE_CONFIGURACION_ENVIO_PRUEBAS_VIDA_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.CONFIRMAR_PRUEBA_VIDA_USUARIO_INIT:
      return {
        ...state,
        pruebaVidaUsuario: {...defaultPruebaVidaUsuario},
        isConfirming: true
      };
    case types.CONFIRMAR_PRUEBA_VIDA_USUARIO_DONE:
      return {
        ...state,
        pruebaVidaUsuario: {...action.pruebaVidaUsuario},
        isConfirming: false
      };
    case types.CONFIRMAR_PRUEBA_VIDA_USUARIO_FAIL:
      return {
        ...state,
        isConfirming: false,
        pruebaVidaUsuario: {
          ...state.pruebaVidaUsuario,
          ...action.pruebaVida
        },
        errors: action.errors
      };
    case types.RESET_CONFIGURACION_ENVIO_PRUEBAS_VIDA_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
