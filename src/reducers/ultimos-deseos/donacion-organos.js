import * as types from '../../actions/types/ultimos-deseos/donacion-organos';

const defaultDonacionOrgano = {
  indicaciones: '',
  is_donar: false,
};

const initialState = {
  donacionOrgano: {...defaultDonacionOrgano},
  isLoading: false,
  isSaving: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_DONACION_ORGANOS_INIT:
      return {
        ...state,
        donacionOrgano: {...defaultDonacionOrgano},
        isLoading: true,
        errors: null
      };
    case types.GET_DONACION_ORGANOS_DONE:
      return {
        ...state,
        donacionOrgano: {
          ...state.donacionOrgano,
          ...action.donacionOrgano
        },
        isLoading: false
      };
    case types.GET_DONACION_ORGANOS_FAIL:
      return {
        ...state,
        isLoading: false,
        errors: action.errors
      };
    case types.CHANGE_DONACION_ORGANOS_INPUT:
      return {
        ...state,
        donacionOrgano: {
          ...state.donacionOrgano,
          [action.attribute]: action.value
        }
      };
    case types.SAVE_DONACION_ORGANOS_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_DONACION_ORGANOS_DONE:
      return {
        ...state,
        donacionOrgano: {
          ...state.donacionOrgano,
          ...action.donacionOrgano
        },
        isSaving: false
      };
    case types.SAVE_DONACION_ORGANOS_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.RESET_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
