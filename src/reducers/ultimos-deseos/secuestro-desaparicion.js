import * as types from '../../actions/types/ultimos-deseos/secuestro-desaparicion';

const defaultSecuestroDesaparicion = {
  indicaciones: ''
};

const initialState = {
  secuestroDesaparicion: {...defaultSecuestroDesaparicion},
  isLoading: false,
  isSaving: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_SECUESTRO_DESAPARICION_INIT:
      return {
        ...state,
        secuestroDesaparicion: {...defaultSecuestroDesaparicion},
        isLoading: true,
        errors: null
      };
    case types.GET_SECUESTRO_DESAPARICION_DONE:
      return {
        ...state,
        secuestroDesaparicion: {
          ...state.secuestroDesaparicion,
          ...action.secuestroDesaparicion
        },
        isLoading: false
      };
    case types.GET_SECUESTRO_DESAPARICION_FAIL:
      return {
        ...state,
        isLoading: false,
        errors: action.errors
      };
    case types.CHANGE_SECUESTRO_DESAPARICION_INPUT:
      return {
        ...state,
        secuestroDesaparicion: {
          ...state.secuestroDesaparicion,
          [action.attribute]: action.value
        }
      };
    case types.SAVE_SECUESTRO_DESAPARICION_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_SECUESTRO_DESAPARICION_DONE:
      return {
        ...state,
        secuestroDesaparicion: {
          ...state.secuestroDesaparicion,
          ...action.secuestroDesaparicion
        },
        isSaving: false
      };
    case types.SAVE_SECUESTRO_DESAPARICION_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.RESET_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
