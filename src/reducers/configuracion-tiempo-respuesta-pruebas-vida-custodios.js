import * as types from '../actions/types/configuracion-tiempo-respuesta-pruebas-vida-custodios';

const defaultConfiguracionPruebaVidaCustodio = {
  dias_respuesta: 0
};

const initialState = {
  configuracionPruebaVidaCustodio: {...defaultConfiguracionPruebaVidaCustodio},
  isGetting: false,
  isSaving: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_INIT:
      return {
        ...state,
        configuracionPruebaVidaCustodio: {...defaultConfiguracionPruebaVidaCustodio},
        isGetting: true,
        errors: null
      };
    case types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_DONE:
      return {
        ...state,
        configuracionPruebaVidaCustodio: {
          ...state.configuracionPruebaVidaCustodio,
          ...action.configuracionPruebaVidaCustodio
        },
        isGetting: false
      };
    case types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    case types.CHANGE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_INPUT:
      return {
        ...state,
        configuracionPruebaVidaCustodio: {
          ...state.configuracionPruebaVidaCustodio,
          [action.attribute]: action.value
        }
      };
    case types.RESET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_INPUTS:
      return {
        ...state,
        configuracionPruebaVidaCustodio: {...defaultConfiguracionPruebaVidaCustodio}
      };
    case types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_DONE:
      return {
        ...state,
        configuracionPruebaVidaCustodio: {
          ...state.configuracionPruebaVidaCustodio,
          ...action.configuracionPruebaVidaCustodio
        },
        isSaving: false
      };
    case types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.RESET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
