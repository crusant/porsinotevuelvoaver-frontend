import * as types from '../actions/types/documentos-digitales-ubicacion-fisica';

const defaultDocumento = {
  id: null,
  nombre: '',
  documento_digital: '',
  documento_fisico: '',
};

const initialState = {
  documento: {...defaultDocumento},
  documentos: [],
  isLoading: false,
  isSaving: false,
  isUpdating: false,
  isDeleting: false,
  isDownloading: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  let documentos;

  switch (action.type) {
    case types.GET_DOCUMENTOS_INIT:
      return {
        ...state,
        documento: {...defaultDocumento},
        documentos: [],
        isLoading: true,
        errors: null
      };
    case types.GET_DOCUMENTOS_DONE:
      return {
        ...state,
        documentos: [...action.documentos],
        isLoading: false
      };
    case types.GET_DOCUMENTOS_FAIL:
      return {
        ...state,
        isLoading: false,
        errors: action.errors
      };
    case types.CHANGE_DOCUMENTO_INPUT:
      return {
        ...state,
        documento: {
          ...state.documento,
          [action.attribute]: action.value
        }
      };
    case types.RESET_DOCUMENTO_INPUTS:
      return {
        ...state,
        documento: {...defaultDocumento},
        errors: null
      };
    case types.SAVE_DOCUMENTO_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_DOCUMENTO_DONE:
      return {
        ...state,
        documento: {...defaultDocumento},
        documentos: [
          ...state.documentos,
          action.documento
        ],
        isSaving: false
      };
    case types.SAVE_DOCUMENTO_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.EDIT_DOCUMENTO:
      return {
        ...state,
        documento: {...action.documento}
      };
    case types.UPDATE_DOCUMENTO_INIT:
      return {
        ...state,
        isUpdating: true,
        errors: null
      };
    case types.UPDATE_DOCUMENTO_DONE:
      documentos = state.documentos.map((documento) => {
        if (documento.id === action.documento.id) {
          return action.documento;
        }

        return documento;
      });

      return {
        ...state,
        documento: {...defaultDocumento},
        documentos,
        isUpdating: false
      };
    case types.UPDATE_DOCUMENTO_FAIL:
      return {
        ...state,
        isUpdating: false,
        errors: action.errors
      };
    case types.DELETE_DOCUMENTO_INIT:
      return {
        ...state,
        isDeleting: true,
        errors: null
      };
    case types.DELETE_DOCUMENTO_DONE:
      documentos = state.documentos.filter((documento) => documento.id !== action.id);

      return {
        ...state,
        documentos,
        isDeleting: false
      };
    case types.DELETE_DOCUMENTO_FAIL:
      return {
        ...state,
        isDeleting: false,
        errors: action.errors
      };
    case types.DOWNLOAD_DOCUMENTO_INIT:
      return {
        ...state,
        isDownloading: true,
        errors: null
      };
    case types.DOWNLOAD_DOCUMENTO_DONE:
      return {
        ...state,
        isDownloading: false
      };
    case types.DOWNLOAD_DOCUMENTO_FAIL:
      return {
        ...state,
        isDownloading: false,
        errors: action.errors
      };
    case types.RESET_DOCUMENTOS_ERRORS:
      return {
        ...state,
        errors: null
      };
    default:
      return state;
  }
};

export default reducer;
