import * as types from '../actions/types/contacto';

const initialState = {
  isSending: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ENVIAR_MENSAJE_CONTACTO_INIT:
      return {
        ...state,
        isSending: true,
        errors: null
      };
    case types.ENVIAR_MENSAJE_CONTACTO_DONE:
      return {
        ...state,
        isSending: false
      };
    case types.ENVIAR_MENSAJE_CONTACTO_FAIL:
      return {
        ...state,
        isSending: false,
        errors: action.errors
      };
    default:
      return state;
  }
};

export default reducer;
