import * as types from '../actions/types/preregistros';

const initialState = {
  preregistros: [],
  preregistro: null,
  isLoading: false,
  isSaving: false,
  isGetting: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_PREREGISTROS_EXPIRADOS_INIT:
      return {
        ...state,
        preregistros: [],
        isLoading: true,
        errors: null
      };
    case types.GET_PREREGISTROS_EXPIRADOS_DONE:
      return {
        ...state,
        preregistros: [...action.preregistros],
        isLoading: false
      };
    case types.GET_PREREGISTROS_EXPIRADOS_FAIL:
      return {
        ...state,
        isLoading: false,
        errors: action.errors
      };
    case types.SAVE_PREREGISTRO_INIT:
      return {
        ...state,
        isSaving: true,
        errors: null
      };
    case types.SAVE_PREREGISTRO_DONE:
      return {
        ...state,
        isSaving: false
      };
    case types.SAVE_PREREGISTRO_FAIL:
      return {
        ...state,
        isSaving: false,
        errors: action.errors
      };
    case types.GET_PREREGISTRO_INIT:
      return {
        ...state,
        isGetting: true,
        preregistro: null,
        errors: null
      };
    case types.GET_PREREGISTRO_DONE:
      return {
        ...state,
        isGetting: false,
        preregistro: action.preregistro
      };
    case types.GET_PREREGISTRO_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    default:
      return state;
  }
};

export default reducer;

