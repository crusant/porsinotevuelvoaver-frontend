import * as types from '../actions/types/monedas';

const initialState = {
  monedas: [],
  isGetting: false,
  errors: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_MONEDAS_INIT:
      return {
        ...state,
        monedas: [],
        isGetting: true,
        errors: null
      };
    case types.GET_MONEDAS_DONE:
      return {
        ...state,
        monedas: [...action.monedas],
        isGetting: false
      };
    case types.GET_MONEDAS_FAIL:
      return {
        ...state,
        isGetting: false,
        errors: action.errors
      };
    default:
      return state;
  }
};

export default reducer;

