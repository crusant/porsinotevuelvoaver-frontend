import api from '../api';
import * as creators from './creators/usuarios-registrados';
import * as helpers from '../helpers';

export const getUsuariosRegistrados = () => {
  return async (dispatch) => {
    dispatch(creators.getUsuariosRegistradosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`usuarios-registrados?token=${token}`);

      dispatch(creators.getUsuariosRegistradosDone(response.data));
    } catch (error) {
      dispatch(creators.getUsuariosRegistradosFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const entregarDatosAcceso = (id) => {
  return async (dispatch) => {
    dispatch(creators.entregarDatosAccesoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`usuarios-registrados/${id}/entregar?token=${token}`);

      dispatch(creators.entregarDatosAccesoDone(response.data));
    } catch (error) {
      dispatch(creators.entregarDatosAccesoFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const activarUsuarioRegistrado = (id) => {
  return async (dispatch) => {
    dispatch(creators.activarUsuarioRegistradoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`usuarios-registrados/${id}/activar?token=${token}`);

      dispatch(creators.activarUsuarioRegistradoDone(response.data));
    } catch (error) {
      dispatch(creators.activarUsuarioRegistradoFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const confirmarPruebaVida = (id) => {
  return async (dispatch) => {
    dispatch(creators.confirmarPruebaVidaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`usuarios-registrados/confirmar-la-prueba-de-vida/${id}?token=${token}`);

      dispatch(creators.confirmarPruebaVidaDone(response.data));
    } catch (error) {
      dispatch(creators.confirmarPruebaVidaFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const resetUsuariosRegistrados = () => {
  return (dispatch) => {
    return dispatch(creators.resetUsuariosRegistrados());
  };
};

export const resetUsuariosRegistradosErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetUsuariosRegistradosErrors());
  };
};
