import api from '../api';
import * as creators from './creators/documentos-digitales-ubicacion-fisica';
import * as helpers from '../helpers';

export const getDocumentos = () => {
  return async (dispatch) => {
    dispatch(creators.getDocumentosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`documentos-digitales-y-ubicacion-fisica?token=${token}`);

      dispatch(creators.getDocumentosDone(response.data));
    } catch (error) {
      dispatch(creators.getDocumentosFail(helpers.resolveError(error)));
    }
  };
};

export const changeDocumentoInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeDocumentoInput(attribute, value));
  };
};

export const resetDocumentoInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetDocumentoInputs());
  };
};

export const saveDocumento = (documento) => {
  return async (dispatch) => {
    dispatch(creators.saveDocumentoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`documentos-digitales-y-ubicacion-fisica?token=${token}`, documento);

      dispatch(creators.saveDocumentoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveDocumentoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const editDocumento = (documento) => {
  return (dispatch) => {
    dispatch(creators.editDocumento(documento));
  };
};

export const updateDocumento = (documento) => {
  return async (dispatch) => {
    dispatch(creators.updateDocumentoInit());

    try {
      const token = localStorage.getItem('token');
      const id = documento.get('id');
      const response = await api.post(`documentos-digitales-y-ubicacion-fisica/${id}?token=${token}`, documento);

      dispatch(creators.updateDocumentoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateDocumentoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteDocumento = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteDocumentoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`documentos-digitales-y-ubicacion-fisica/${id}?token=${token}`);
      const documento = await response.data;

      dispatch(creators.deleteDocumentoDone(documento.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteDocumentoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const downloadDocumento = (id, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadDocumentoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`documentos-digitales-y-ubicacion-fisica/${id}/descargar?token=${token}`, {
        responseType: 'blob'
      });

      helpers.fileDownload(response.data, 'documento', extension);

      dispatch(creators.downloadDocumentoDone());
    } catch (error) {
      dispatch(creators.downloadDocumentoFail('Ocurrió un error al intentar descargar el documento, por favor inténtelo más tarde.'));
    }
  };
};

export const resetDocumentosErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetDocumentosErrors());
  };
};
