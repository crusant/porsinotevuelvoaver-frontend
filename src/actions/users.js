import api from '../api';
import * as creators from './creators/users';
import * as helpers from '../helpers';

export const changePassword = (user) => {
  return async (dispatch) => {
    dispatch(creators.changePasswordInit());

    try {
      const token = localStorage.getItem('token');
      await api.post(`cambiar-contrasena?token=${token}`, user);

      dispatch(creators.changePasswordDone());
      return true;
    } catch (error) {
      dispatch(creators.changePasswordFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const resetPasswordErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetPasswordErrors());
  };
};
