import api from '../api';
import * as creators from './creators/parentescos';
import * as helpers from '../helpers';

export const getParentescos = () => {
  return async (dispatch) => {
    dispatch(creators.getParentescosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`parentescos?token=${token}`);

      dispatch(creators.getParentescosDone(response.data));
    } catch (error) {
      dispatch(creators.getParentescosFail(helpers.resolveError(error)));
    }
  };
};
