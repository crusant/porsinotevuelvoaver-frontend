import api from '../../api';
import * as creators from '../creators/ultimos-deseos/datos-testamento';
import * as helpers from '../../helpers';

export const getDatosTestamento = () => {
  return async (dispatch) => {
    dispatch(creators.getDatosTestamentoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`datos-del-testamento?token=${token}`);

      dispatch(creators.getDatosTestamentoDone(response.data));
    } catch (error) {
      dispatch(creators.getDatosTestamentoFail(helpers.resolveError(error)));
    }
  };
};

export const changeDatosTestamentoInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeDatosTestamentoInput(attribute, value));
  };
};

export const saveDatosTestamento = (testamento) => {
  return async (dispatch) => {
    dispatch(creators.saveDatosTestamentoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`datos-del-testamento?token=${token}`, testamento);

      dispatch(creators.saveDatosTestamentoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveDatosTestamentoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const downloadDatosTestamentoDocumento = (extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadDatosTestamentoDocumentoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`datos-del-testamento/descargar?token=${token}`, {
        responseType: 'blob'
      });

      helpers.fileDownload(response.data, 'testamento', extension);

      dispatch(creators.downloadDatosTestamentoDocumentoDone());
    } catch (error) {
      dispatch(creators.downloadDatosTestamentoDocumentoFail('Ocurrió un error al intentar descargar el testamento, por favor inténtelo más tarde.'));
    }
  };
};

export const resetDatosTestamentoErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetDatosTestamentoErrors);
  };
};
