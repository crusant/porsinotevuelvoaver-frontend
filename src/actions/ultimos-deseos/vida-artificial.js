import api from '../../api';
import * as creators from '../creators/ultimos-deseos/vida-artificial';
import * as helpers from '../../helpers';

export const getVidaArtificial = () => {
  return async (dispatch) => {
    dispatch(creators.getVidaArtificialInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`vida-artificial?token=${token}`);

      dispatch(creators.getVidaArtificialDone(response.data));
    } catch (error) {
      dispatch(creators.getVidaArtificialFail(helpers.resolveError(error)));
    }
  };
};

export const changeVidaArtificialInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeVidaArtificialInput(attribute, value));
  };
};

export const saveVidaArtificial = (vidaArtificial) => {
  return async (dispatch) => {
    dispatch(creators.saveVidaArtificialInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`vida-artificial?token=${token}`, vidaArtificial);

      dispatch(creators.saveVidaArtificialDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveVidaArtificialFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetErrors());
  };
};
