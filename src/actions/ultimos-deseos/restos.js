import api from '../../api';
import * as creators from '../creators/ultimos-deseos/restos';
import * as helpers from '../../helpers';

export const getRestos = () => {
  return async (dispatch) => {
    dispatch(creators.getRestosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`restos?token=${token}`);

      dispatch(creators.getRestosDone(response.data));
    } catch (error) {
      dispatch(creators.getRestosFail(helpers.resolveError(error)));
    }
  };
};

export const changeRestoInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeRestoInput(attribute, value));
  };
};

export const changeSepultadoInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeSepultadoInput(attribute, value));
  };
};

export const changeCremadoInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeCremadoInput(attribute, value));
  };
};

export const saveRestos = (resto) => {
  return async (dispatch) => {
    dispatch(creators.saveRestosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`restos?token=${token}`, resto);

      dispatch(creators.saveRestosDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveRestosFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const downloadRestosDocumento = (tipo, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadRestosDocumentoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`restos/descargar?token=${token}`, {
        params: {
          tipo
        },
        responseType: 'blob'
      });

      helpers.fileDownload(response.data, (tipo === 1) ? 'documento' : 'contrato', extension);

      dispatch(creators.downloadRestosDocumentoDone());
    } catch (error) {
      dispatch(creators.downloadRestosDocumentoFail(`Ocurrió un error al intentar descargar el ${(tipo === 1) ? 'documento' : 'contrato'}, por favor inténtelo más tarde.`));
    }
  };
};

export const resetRestosErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetRestosErrors());
  };
};
