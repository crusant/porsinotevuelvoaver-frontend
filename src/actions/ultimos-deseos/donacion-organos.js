import api from '../../api';
import * as creators from '../creators/ultimos-deseos/donacion-organos';
import * as helpers from '../../helpers';

export const getDonacionOrganos = () => {
  return async (dispatch) => {
    dispatch(creators.getDonacionOrganosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`donacion-de-organos?token=${token}`);

      dispatch(creators.getDonacionOrganosDone(response.data));
    } catch (error) {
      dispatch(creators.getDonacionOrganosFail(helpers.resolveError(error)));
    }
  };
};

export const changeDonacionOrganosInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeDonacionOrganosInput(attribute, value));
  };
};

export const saveDonacionOrganos = (donacionOrgano) => {
  return async (dispatch) => {
    dispatch(creators.saveDonacionOrganosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`donacion-de-organos?token=${token}`, donacionOrgano);

      dispatch(creators.saveDonacionOrganosDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveDonacionOrganosFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetErrors());
  };
};
