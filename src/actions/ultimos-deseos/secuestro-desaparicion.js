import api from '../../api';
import * as creators from '../creators/ultimos-deseos/secuestro-desaparicion';
import * as helpers from '../../helpers';

export const getSecuestroDesaparicion = () => {
  return async (dispatch) => {
    dispatch(creators.getSecuestroDesaparicionInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`secuestro-o-desaparicion?token=${token}`);

      dispatch(creators.getSecuestroDesaparicionDone(response.data));
    } catch (error) {
      dispatch(creators.getSecuestroDesaparicionFail(helpers.resolveError(error)));
    }
  };
};

export const changeSecuestroDesaparicionInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeSecuestroDesaparicionInput(attribute, value));
  };
};

export const saveSecuestroDesaparicion = (secuestroDesaparicion) => {
  return async (dispatch) => {
    dispatch(creators.saveSecuestroDesaparicionInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`secuestro-o-desaparicion?token=${token}`, secuestroDesaparicion);

      dispatch(creators.saveSecuestroDesaparicionDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveSecuestroDesaparicionFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetErrors());
  };
};
