import api from '../api';
import * as creators from './creators/monedas';
import * as helpers from '../helpers';

export const getMonedas = () => {
  return async (dispatch) => {
    dispatch(creators.getMonedasInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`monedas?token=${token}`);

      dispatch(creators.getMonedasDone(response.data));
    } catch (error) {
      dispatch(creators.getMonedasFail(helpers.resolveErrors(error.response)));
    }
  };
};
