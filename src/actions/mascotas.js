import api from '../api';
import * as creators from './creators/mascotas';
import * as helpers from '../helpers';

export const getMascotas = () => {
  return async (dispatch) => {
    dispatch(creators.getMascotasInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`mascotas?token=${token}`);

      dispatch(creators.getMascotasDone(response.data));
    } catch (error) {
      dispatch(creators.getMascotasFail(helpers.resolveError(error)));
    }
  };
};

export const changeMascotaInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeMascotaInput(attribute, value));
  };
};

export const resetMascotaInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetMascotaInputs());
  };
};

export const saveMascota = (mascota) => {
  return async (dispatch) => {
    dispatch(creators.saveMascotaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`mascotas?token=${token}`, mascota);

      dispatch(creators.saveMascotaDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveMascotaFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const editMascota = (mascota) => {
  return (dispatch) => {
    dispatch(creators.editMascota(mascota));
  };
};

export const updateMascota = (mascota) => {
  return async (dispatch) => {
    dispatch(creators.updateMascotaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`mascotas/${mascota.get('id')}?token=${token}`, mascota);

      dispatch(creators.updateMascotaDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateMascotaFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteMascota = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteMascotaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`mascotas/${id}?token=${token}`);
      const mascota = await response.data;

      dispatch(creators.deleteMascotaDone(mascota.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteMascotaFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const downloadMascotaDocumento = (id, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadMascotaDocumentoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`mascotas/${id}/descargar?token=${token}`, {
        responseType: 'blob'
      });

      helpers.fileDownload(response.data, 'documento', extension);

      dispatch(creators.downloadMascotaDocumentoDone());
    } catch (error) {
      dispatch(creators.downloadMascotaDocumentoFail('Ocurrió un error al intentar descargar el documento, por favor inténtelo más tarde.'));
    }
  };
};

export const resetMascotasErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetMascotasErrors());
  };
};
