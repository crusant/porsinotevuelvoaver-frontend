import api from '../api';
import * as creators from './creators/menu-principal';
import * as helpers from '../helpers';

export const getMenuPrincipal = () => {
  return async (dispatch) => {
    dispatch(creators.getMenuPrincipalInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`menu-principal?token=${token}`);

      dispatch(creators.getMenuPrincipalDone(response.data));
    } catch (error) {
      dispatch(creators.getMenuPrincipalFail(helpers.resolveErrors(error.response)));
    }
  };
};
