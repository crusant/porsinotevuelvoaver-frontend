import api from '../api';
import * as creators from './creators/instrucciones-particulares';
import downloader from '../helpers/downloader';
import * as helpers from '../helpers';

export const getInstruccionesParticulares = () => {
  return async (dispatch) => {
    dispatch(creators.getInstruccionesParticularesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`instrucciones-particulares?token=${token}`);

      dispatch(creators.getInstruccionesParticularesDone(response.data));
    } catch (error) {
      dispatch(creators.getInstruccionesParticularesFail(helpers.resolveError(error)));
    }
  };
};

export const changeInstruccionesParticularesInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeInstruccionesParticularesInput(attribute, value));
  };
};

export const saveInstruccionesParticulares = (data) => {
  return async (dispatch) => {
    dispatch(creators.saveInstruccionesParticularesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`instrucciones-particulares?token=${token}`, data);

      dispatch(creators.saveInstruccionesParticularesDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveInstruccionesParticularesFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const downloadInstruccionesParticularesAudioInMp3 = () => {
  return async (dispatch) => {
    dispatch(creators.downloadInstruccionesParticularesAudioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`instrucciones-particulares/audio/descargar?token=${token}`, {
        responseType: 'blob'
      });

      downloader(response.data, 'audio', 'mp3', { type: 'audio/mpeg-3' });

      dispatch(creators.downloadInstruccionesParticularesAudioDone());
    } catch (error) {
      dispatch(creators.downloadInstruccionesParticularesAudioFail(helpers.resolveError(error)));
    }
  };
};

export const downloadInstruccionesParticularesAudioInStandard = (extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadInstruccionesParticularesAudioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`instrucciones-particulares/audio/descargar?token=${token}`, {
        responseType: 'blob'
      });

      downloader(response.data, 'audio', extension);

      dispatch(creators.downloadInstruccionesParticularesAudioDone());
    } catch (error) {
      dispatch(creators.downloadInstruccionesParticularesAudioFail(helpers.resolveError(error)));
    }
  };
};

export const downloadInstruccionesParticularesVideo = (extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadInstruccionesParticularesVideoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`instrucciones-particulares/video/descargar?token=${token}`, {
        responseType: 'blob'
      });

      downloader(response.data, 'video', extension);

      dispatch(creators.downloadInstruccionesParticularesVideoDone());
    } catch (error) {
      dispatch(creators.downloadInstruccionesParticularesVideoFail(helpers.resolveError(error)));
    }
  };
};

export const resetInstruccionesParticularesErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetInstruccionesParticularesErrors());
  };
};
