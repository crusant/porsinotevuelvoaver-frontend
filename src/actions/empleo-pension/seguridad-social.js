import api from '../../api';
import * as creators from '../creators/empleo-pension/seguridad-social';
import * as helpers from '../../helpers';

export const getSeguridadesSociales = () => {
  return async (dispatch) => {
    dispatch(creators.getSeguridadesSocialesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`seguridad-social?token=${token}`);

      dispatch(creators.getSeguridadesSocialesDone(response.data));
    } catch (error) {
      dispatch(creators.getSeguridadesSocialesFail(helpers.resolveError(error)));
    }
  };
};

export const changeSeguridadSocialInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeSeguridadSocialInput(attribute, value));
  };
};

export const resetSeguridadSocialInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetSeguridadSocialInputs());
  };
};

export const saveSeguridadSocial = (seguridadSocial) => {
  return async (dispatch) => {
    dispatch(creators.saveSeguridadSocialInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`seguridad-social?token=${token}`, seguridadSocial);

      dispatch(creators.saveSeguridadSocialDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveSeguridadSocialFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const editSeguridadSocial = (seguridadSocial) => {
  return (dispatch) => {
    dispatch(creators.editSeguridadSocial(seguridadSocial));
  };
};

export const updateSeguridadSocial = (seguridadSocial) => {
  return async (dispatch) => {
    dispatch(creators.updateSeguridadSocialInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`seguridad-social/${seguridadSocial.id}?token=${token}`, seguridadSocial);

      dispatch(creators.updateSeguridadSocialDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateSeguridadSocialFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteSeguridadSocial = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteSeguridadSocialInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`seguridad-social/${id}?token=${token}`);
      const seguridadSocial = await response.data;

      dispatch(creators.deleteSeguridadSocialDone(seguridadSocial.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteSeguridadSocialFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetSeguridadSocialErrors = () =>
  (dispatch) => dispatch(creators.resetSeguridadSocialErrors());
