import api from '../../api';
import * as creators from '../creators/empleo-pension/plan-pension';
import * as helpers from '../../helpers';

export const getPlanesPensiones = () => {
  return async (dispatch) => {
    dispatch(creators.getPlanesPensionesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`plan-de-pension?token=${token}`);

      dispatch(creators.getPlanesPensionesDone(response.data));
    } catch (error) {
      dispatch(creators.getPlanesPensionesFail(helpers.resolveError(error)));
    }
  };
};

export const changePlanPensionInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changePlanPensionInput(attribute, value));
  };
};

export const changeBeneficiarioInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeBeneficiarioInput(attribute, value));
  };
};

export const addBeneficiario = (beneficiario) => {
  return (dispatch) => {
    dispatch(creators.addBeneficiario(beneficiario));
  };
};

export const resetBeneficiario = () => {
  return (dispatch) => {
    dispatch(creators.resetBeneficiario());
  };
};

export const removeBeneficiario = (index) => {
  return (dispatch) => {
    dispatch(creators.removeBeneficiario(index));
  };
};

export const changeAsesorInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeAsesorInput(attribute, value));
  };
};

export const resetPlanPensionInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetPlanPensionInputs());
  };
};

export const savePlanPension = (planPension) => {
  return async (dispatch) => {
    dispatch(creators.savePlanPensionInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`plan-de-pension?token=${token}`, planPension);

      dispatch(creators.savePlanPensionDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.savePlanPensionFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const editPlanPension = (planPension) => {
  return (dispatch) => {
    dispatch(creators.editPlanPension(planPension));
  };
};

export const updatePlanPension = (planPension) => {
  return async (dispatch) => {
    dispatch(creators.updatePlanPensionInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`plan-de-pension/${planPension.id}?token=${token}`, planPension);

      dispatch(creators.updatePlanPensionDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updatePlanPensionFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deletePlanPension = (id) => {
  return async (dispatch) => {
    dispatch(creators.deletePlanPensionInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`plan-de-pension/${id}?token=${token}`);
      const planPension = await response.data;

      dispatch(creators.deletePlanPensionDone(planPension.id));
      return true;
    } catch (error) {
      dispatch(creators.deletePlanPensionFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetPlanPensionErrors = () =>
  (dispatch) => dispatch(creators.resetPlanPensionErrors());
