import api from '../../api';
import * as creators from '../creators/empleo-pension/datos-empleo';
import * as helpers from '../../helpers';

export const getDatosEmpleo = () => {
  return async (dispatch) => {
    dispatch(creators.getDatosEmpleoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`datos-del-empleo?token=${token}`);

      dispatch(creators.getDatosEmpleoDone(response.data));
    } catch (error) {
      dispatch(creators.getDatosEmpleoFail(helpers.resolveError(error)));
    }
  };
};

export const changeDatosEmpleoInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeDatosEmpleoInput(attribute, value));
  };
};

export const changePrestacionInput = (prestacion) => {
  return (dispatch) => {
    return dispatch(creators.changePrestacionInput(prestacion));
  };
};

export const changeDescripcionInput = (value) => {
  return (dispatch) => {
    return dispatch(creators.changeDescripcionInput(value));
  };
};

export const saveDatosEmpleo = (data) => {
  return async (dispatch) => {
    dispatch(creators.saveDatosEmpleoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`datos-del-empleo?token=${token}`, data);

      dispatch(creators.saveDatosEmpleoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveDatosEmpleoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const downloadDatosEmpleoContrato = (extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadDatosEmpleoContratoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`datos-del-empleo/descargar?token=${token}`, {
        params: {
          tipo: 1
        },
        responseType: 'blob'
      });

      helpers.fileDownload(response.data, 'contrato', extension);

      dispatch(creators.downloadDatosEmpleoContratoDone());
    } catch (error) {
      dispatch(creators.downloadDatosEmpleoContratoFail('Ocurrió un error al intentar descargar el contrato, por favor inténtelo más tarde.'));
    }
  };
};

export const downloadDatosEmpleoReciboPago = (extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadDatosEmpleoReciboPagoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`datos-del-empleo/descargar?token=${token}`, {
        params: {
          tipo: 2
        },
        responseType: 'blob'
      });

      helpers.fileDownload(response.data, 'recibo-pago', extension);

      dispatch(creators.downloadDatosEmpleoReciboPagoDone());
    } catch (error) {
      dispatch(creators.downloadDatosEmpleoReciboPagoFail('Ocurrió un error al intentar descargar el recibo de pago, por favor inténtelo más tarde.'));
    }
  };
};

export const resetDatosEmpleoErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetDatosEmpleoErrors());
  };
};
