import api from '../api';
import * as creators from './creators/configuracion-tiempo-respuesta-pruebas-vida-custodios';
import * as helpers from '../helpers';

export const getConfiguracionTiempoRespuestaPruebasVidaCustodios = () => {
  return async (dispatch) => {
    dispatch(creators.getConfiguracionTiempoRespuestaPruebasVidaCustodiosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`configuracion-de-tiempo-de-respuesta-de-pruebas-de-vida-a-custodios?token=${token}`);

      dispatch(creators.getConfiguracionTiempoRespuestaPruebasVidaCustodiosDone(response.data));
    } catch (error) {
      dispatch(creators.getConfiguracionTiempoRespuestaPruebasVidaCustodiosFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const changeConfiguracionTiempoRespuestaPruebasVidaCustodiosInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeConfiguracionTiempoRespuestaPruebasVidaCustodiosInput(attribute, value));
  };
};

export const resetConfiguracionTiempoRespuestaPruebasVidaCustodiosInputs = () => {
  return (dispatch) => {
    return dispatch(creators.resetConfiguracionTiempoRespuestaPruebasVidaCustodiosInputs());
  };
};

export const saveConfiguracionTiempoRespuestaPruebasVidaCustodios = (data) => {
  return async (dispatch) => {
    dispatch(creators.saveConfiguracionTiempoRespuestaPruebasVidaCustodiosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`configuracion-de-tiempo-de-respuesta-de-pruebas-de-vida-a-custodios?token=${token}`, data);

      dispatch(creators.saveConfiguracionTiempoRespuestaPruebasVidaCustodiosDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveConfiguracionTiempoRespuestaPruebasVidaCustodiosFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const resetConfiguracionTiempoRespuestaPruebasVidaCustodiosErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetConfiguracionTiempoRespuestaPruebasVidaCustodiosErrors());
  };
};
