export const GET_DATOS_EMPLEO_INIT = 'GET_DATOS_EMPLEO_INIT';
export const GET_DATOS_EMPLEO_DONE = 'GET_DATOS_EMPLEO_DONE';
export const GET_DATOS_EMPLEO_FAIL = 'GET_DATOS_EMPLEO_FAIL';

export const CHANGE_DATOS_EMPLEO_INPUT = 'CHANGE_DATOS_EMPLEO_INPUT';

export const CHANGE_PRESTACION_INPUT = 'CHANGE_PRESTACION_INPUT';

export const CHANGE_DESCRIPCION_INPUT = 'CHANGE_DESCRIPCION_INPUT';

export const SAVE_DATOS_EMPLEO_INIT = 'SAVE_DATOS_EMPLEO_INIT';
export const SAVE_DATOS_EMPLEO_DONE = 'SAVE_DATOS_EMPLEO_DONE';
export const SAVE_DATOS_EMPLEO_FAIL = 'SAVE_DATOS_EMPLEO_FAIL';

export const DOWNLOAD_DATOS_EMPLEO_CONTRATO_INIT = 'DOWNLOAD_DATOS_EMPLEO_CONTRATO_INIT';
export const DOWNLOAD_DATOS_EMPLEO_CONTRATO_DONE = 'DOWNLOAD_DATOS_EMPLEO_CONTRATO_DONE';
export const DOWNLOAD_DATOS_EMPLEO_CONTRATO_FAIL = 'DOWNLOAD_DATOS_EMPLEO_CONTRATO_FAIL';

export const DOWNLOAD_DATOS_EMPLEO_RECIBO_PAGO_INIT = 'DOWNLOAD_DATOS_EMPLEO_RECIBO_PAGO_INIT';
export const DOWNLOAD_DATOS_EMPLEO_RECIBO_PAGO_DONE = 'DOWNLOAD_DATOS_EMPLEO_RECIBO_PAGO_DONE';
export const DOWNLOAD_DATOS_EMPLEO_RECIBO_PAGO_FAIL = 'DOWNLOAD_DATOS_EMPLEO_RECIBO_PAGO_FAIL';

export const RESET_DATOS_EMPLEO_ERRORS = 'RESET_DATOS_EMPLEO_ERRORS';
