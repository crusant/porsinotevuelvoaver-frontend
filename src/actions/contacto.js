import api from '../api';
import * as creators from './creators/contacto';
import * as helpers from '../helpers';

export const enviarMensajeContacto = (contacto) => {
  return async (dispatch) => {
    dispatch(creators.enviarMensajeContactoInit());

    try {
      await api.post('contacto', contacto);

      dispatch(creators.enviarMensajeContactoDone());
      return true;
    } catch (error) {
      dispatch(creators.enviarMensajeContactoFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};
