import api from '../api';
import * as creators from './creators/app';
import downloader from '../helpers/downloader';
import * as helpers from '../helpers';

export const downloadFormularios = () => {
  return async (dispatch) => {
    dispatch(creators.downloadFormulariosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`aplicacion/formularios?token=${token}`, {
        responseType: 'blob'
      });

      downloader(response.data, 'formulario', 'pdf');

      dispatch(creators.downloadFormulariosDone());
    } catch (error) {
      dispatch(creators.downloadFormulariosFail(helpers.resolveErrors(error.response)));
    }
  };
};
