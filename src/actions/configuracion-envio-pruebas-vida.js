import api from '../api';
import _ from 'lodash';
import * as creators from './creators/configuracion-envio-pruebas-vida';
import * as helpers from '../helpers';

export const getConfiguracionEnvioPruebasVida = () => {
  return async (dispatch) => {
    dispatch(creators.getConfiguracionEnvioPruebasVidaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`configuracion-de-envio-de-pruebas-de-vida?token=${token}`);

      dispatch(creators.getConfiguracionEnvioPruebasVidaDone(response.data));
    } catch (error) {
      dispatch(creators.getConfiguracionEnvioPruebasVidaFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const changeConfiguracionEnvioPruebasVidaInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeConfiguracionEnvioPruebasVidaInput(attribute, value));
  };
};

export const resetConfiguracionEnvioPruebasVidaInputs = () => {
  return (dispatch) => {
    return dispatch(creators.resetConfiguracionEnvioPruebasVidaInputs());
  };
};

export const saveConfiguracionEnvioPruebasVida = (data) => {
  return async (dispatch) => {
    dispatch(creators.saveConfiguracionEnvioPruebasVidaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`configuracion-de-envio-de-pruebas-de-vida?token=${token}`, data);

      dispatch(creators.saveConfiguracionEnvioPruebasVidaDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveConfiguracionEnvioPruebasVidaFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const confirmarPruebaVidaUsuario = (url) => {
  return async (dispatch) => {
    dispatch(creators.confirmarPruebaVidaUsuarioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`usuario/confirmar-prueba-de-vida/${url}?token=${token}`);

      dispatch(creators.confirmarPruebaVidaUsuarioDone(response.data));
    } catch (error) {
      const pruebaVida = _.get(error, 'response.data.pruebaVida', null);

      dispatch(creators.confirmarPruebaVidaUsuarioFail(pruebaVida, helpers.resolveErrors(error.response)));
    }
  };
};

export const resetConfiguracionEnvioPruebasVidaErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetConfiguracionEnvioPruebasVidaErrors());
  };
};
