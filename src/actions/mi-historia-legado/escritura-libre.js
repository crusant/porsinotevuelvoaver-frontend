import api from '../../api';
import downloader from '../../helpers/downloader';
import * as helpers from '../../helpers';
import * as creators from '../creators/mi-historia-legado/escritura-libre';

export const getEscrituraLibre = () => {
  return async (dispatch) => {
    dispatch(creators.getEscrituraLibreInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`escritura-libre?token=${token}`);

      dispatch(creators.getEscrituraLibreDone(response.data));
    } catch (error) {
      dispatch(creators.getEscrituraLibreFail(helpers.resolveError(error)));
    }
  };
};

export const changeEscrituraLibreInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeEscrituraLibreInput(attribute, value));
  };
};

export const saveEscrituraLibre = (escrituraLibre) => {
  return async (dispatch) => {
    dispatch(creators.saveEscrituraLibreInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`escritura-libre?token=${token}`, helpers.objectToFormData(escrituraLibre));

      dispatch(creators.saveEscrituraLibreDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveEscrituraLibreFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const downloadEscrituraLibreAudio = (extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadEscrituraLibreAudioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`escritura-libre/audio/descargar?token=${token}`, {
        responseType: 'blob'
      });

      const options = {};

      if (extension === 'mp3') {
        options['type'] = 'audio/mpeg-3';
      }

      downloader(response.data, 'audio', extension, options);

      dispatch(creators.downloadEscrituraLibreAudioDone());
    } catch (error) {
      dispatch(creators.downloadEscrituraLibreAudioFail('Ocurrió un error al intentar descargar el audio, por favor inténtelo más tarde.'));
    }
  };
};

export const downloadEscrituraLibreVideo = (extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadEscrituraLibreVideoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`escritura-libre/video/descargar?token=${token}`, {
        responseType: 'blob'
      });

      downloader(response.data, 'video', extension);

      dispatch(creators.downloadEscrituraLibreVideoDone());
    } catch (error) {
      dispatch(creators.downloadEscrituraLibreVideoFail('Ocurrió un error al intentar descargar el video, por favor inténtelo más tarde.'));
    }
  };
};

export const resetEscrituraLibreErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetEscrituraLibreErrors());
  };
};
