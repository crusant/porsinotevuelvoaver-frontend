import api from '../../api';
import * as creators from '../creators/mi-historia-legado/mi-historia';
import downloader from '../../helpers/downloader';
import * as helpers from '../../helpers';

export const getMiHistoria = () => {
  return async (dispatch) => {
    dispatch(creators.getMiHistoriaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`mi-historia?token=${token}`);

      dispatch(creators.getMiHistoriaDone(response.data));
    } catch (error) {
      dispatch(creators.getMiHistoriaFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const changeMiHistoriaInput = (index, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeMiHistoriaInput(index, attribute, value));
  };
};

export const saveMiHistoria = (pregunta) => {
  return async (dispatch) => {
    dispatch(creators.saveMiHistoriaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`mi-historia?token=${token}`, helpers.objectToFormData(pregunta));

      dispatch(creators.saveMiHistoriaDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveMiHistoriaFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const downloadMiHistoriaAudio = (preguntaId, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadMiHistoriaAudioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`mi-historia/${preguntaId}/audio/descargar?token=${token}`, {
        responseType: 'blob'
      });

      const options = {};

      if (extension === 'mp3') {
        options['type'] = 'audio/mpeg-3';
      }

      downloader(response.data, 'audio', extension, options);

      dispatch(creators.downloadMiHistoriaAudioDone());
    } catch (error) {
      dispatch(creators.downloadMiHistoriaAudioFail('Ocurrió un error al intentar descargar el audio, por favor inténtelo más tarde.'));
    }
  };
};

export const downloadMiHistoriaVideo = (preguntaId, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadMiHistoriaVideoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`mi-historia/${preguntaId}/video/descargar?token=${token}`, {
        responseType: 'blob'
      });

      downloader(response.data, 'video', extension);

      dispatch(creators.downloadMiHistoriaVideoDone());
    } catch (error) {
      dispatch(creators.downloadMiHistoriaVideoFail('Ocurrió un error al intentar descargar el video, por favor inténtelo más tarde.'));
    }
  };
};

export const resetMiHistoriaErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetMiHistoriaErrors());
  };
};

export const resetMiHistoria = () => {
  return (dispatch) => {
    return dispatch(creators.resetMiHistoria());
  };
};
