import api from '../../api';
import downloader from '../../helpers/downloader';
import * as creators from '../creators/mi-historia-legado/mensajes-postumos';
import * as helpers from '../../helpers';

export const getMensajesPostumos = () => {
  return async (dispatch) => {
    dispatch(creators.getMensajesPostumosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`mensajes-postumos?token=${token}`);

      dispatch(creators.getMensajesPostumosDone(response.data));
    } catch (error) {
      dispatch(creators.getMensajesPostumosFail(helpers.resolveError(error)));
    }
  };
};

export const changeMensajePostumoInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeMensajePostumoInput(attribute, value));
  };
};

export const resetMensajePostumoInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetMensajePostumoInputs());
  };
};

export const saveMensajePostumo = (mensajePostumo) => {
  return async (dispatch) => {
    dispatch(creators.saveMensajePostumoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`mensajes-postumos?token=${token}`, helpers.objectToFormData(mensajePostumo));

      dispatch(creators.saveMensajePostumoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveMensajePostumoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const getMensajePostumo = (url) => {
  return async (dispatch) => {
    dispatch(creators.getMensajePostumoInit());

    try {
      const response = await api.get(`mensajes-postumos/${url}`);

      dispatch(creators.getMensajePostumoDone(response.data));
    } catch (error) {
      dispatch(creators.getMensajePostumoFail(helpers.resolveError(error)));
    }
  };
};

export const editMensajePostumo = (mensajePostumo) => {
  return (dispatch) => {
    dispatch(creators.editMensajePostumo(mensajePostumo));
  };
};

export const updateMensajePostumo = (mensajePostumo) => {
  return async (dispatch) => {
    dispatch(creators.updateMensajePostumoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`mensajes-postumos/${mensajePostumo.id}?token=${token}`, helpers.objectToFormData(mensajePostumo));

      dispatch(creators.updateMensajePostumoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateMensajePostumoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteMensajePostumo = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteMensajePostumoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`mensajes-postumos/${id}?token=${token}`);
      const mensajePostumo = await response.data;

      dispatch(creators.deleteMensajePostumoDone(mensajePostumo.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteMensajePostumoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const downloadMensajePostumoAudio = (id, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadMensajePostumoAudioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`mensajes-postumos/${id}/audio/descargar?token=${token}`, {
        responseType: 'blob'
      });

      const options = {};

      if (extension === 'mp3') {
        options['type'] = 'audio/mpeg-3';
      }

      downloader(response.data, 'audio', extension, options);

      dispatch(creators.downloadMensajePostumoAudioDone());
    } catch (error) {
      dispatch(creators.downloadMensajePostumoAudioFail('Ocurrió un error al intentar descargar el audio, por favor inténtelo más tarde.'));
    }
  };
};

export const downloadMensajePostumoAudioByUrl = (url, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadMensajePostumoAudioInit());

    try {
      const response = await api.get(`mensajes-postumos/${url}/audio`, {
        responseType: 'blob'
      });

      if (extension === 'mp3') {
        downloader(response.data, 'audio', 'mp3', { type: 'audio/mpeg-3' });
      } else {
        downloader(response.data, 'audio', extension);
      }

      dispatch(creators.downloadMensajePostumoAudioDone());
    } catch (error) {
      dispatch(creators.downloadMensajePostumoAudioFail('Ocurrió un error al intentar descargar el audio, por favor inténtelo más tarde.'));
    }
  };
};

export const downloadMensajePostumoVideo = (id, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadMensajePostumoVideoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`mensajes-postumos/${id}/video/descargar?token=${token}`, {
        responseType: 'blob'
      });

      downloader(response.data, 'video', extension);

      dispatch(creators.downloadMensajePostumoVideoDone());
    } catch (error) {
      dispatch(creators.downloadMensajePostumoVideoFail('Ocurrió un error al intentar descargar el video, por favor inténtelo más tarde.'));
    }
  };
};

export const downloadMensajePostumoVideoByUrl = (url, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadMensajePostumoVideoInit());

    try {
      const response = await api.get(`mensajes-postumos/${url}/video`, {
        responseType: 'blob'
      });

      downloader(response.data, 'video', extension);

      dispatch(creators.downloadMensajePostumoVideoDone());
    } catch (error) {
      dispatch(creators.downloadMensajePostumoVideoFail('Ocurrió un error al intentar descargar el video, por favor inténtelo más tarde.'));
    }
  };
};

export const downloadMensajePostumoFotografia = (id, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadMensajePostumoFotografiaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`mensajes-postumos/${id}/fotografia/descargar?token=${token}`, {
        responseType: 'blob'
      });

      downloader(response.data, 'fotografia', extension);

      dispatch(creators.downloadMensajePostumoFotografiaDone());
    } catch (error) {
      dispatch(creators.downloadMensajePostumoFotografiaFail('Ocurrió un error al intentar descargar la fotografía, por favor inténtelo más tarde.'));
    }
  };
};

export const resetMensajesPostumosErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetMensajesPostumosErrors());
  };
};
