import api from '../api';
import _ from 'lodash';
import * as creators from './creators/configuracion-tiempo-respuesta-pruebas-vida';
import * as helpers from '../helpers';

export const getConfiguracionTiempoRespuestaPruebasVida = () => {
  return async (dispatch) => {
    dispatch(creators.getConfiguracionTiempoRespuestaPruebasVidaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`configuracion-de-tiempo-de-respuesta-a-pruebas-de-vida?token=${token}`);

      dispatch(creators.getConfiguracionTiempoRespuestaPruebasVidaDone(response.data));
    } catch (error) {
      dispatch(creators.getConfiguracionTiempoRespuestaPruebasVidaFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const changeConfiguracionTiempoRespuestaPruebasVidaInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeConfiguracionTiempoRespuestaPruebasVidaInput(attribute, value));
  };
};

export const resetConfiguracionTiempoRespuestaPruebasVidaInputs = () => {
  return (dispatch) => {
    return dispatch(creators.resetConfiguracionTiempoRespuestaPruebasVidaInputs());
  };
};

export const saveConfiguracionTiempoRespuestaPruebasVida = (data) => {
  return async (dispatch) => {
    dispatch(creators.saveConfiguracionTiempoRespuestaPruebasVidaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`configuracion-de-tiempo-de-respuesta-a-pruebas-de-vida?token=${token}`, data);

      dispatch(creators.saveConfiguracionTiempoRespuestaPruebasVidaDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveConfiguracionTiempoRespuestaPruebasVidaFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const confirmarPruebaVidaCustodio = (url) => {
  return async (dispatch) => {
    dispatch(creators.confirmarPruebaVidaCustodioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`custodio/confirmar-prueba-de-vida-del-usuario/${url}?token=${token}`);

      dispatch(creators.confirmarPruebaVidaCustodioDone(response.data));
    } catch (error) {
      const pruebaVida = _.get(error, 'response.data.pruebaVida', null);

      dispatch(creators.confirmarPruebaVidaCustodioFail(pruebaVida, helpers.resolveErrors(error.response)));
    }
  };
};

export const entregarDatosAccesoCustodio = (url) => {
  return async (dispatch) => {
    dispatch(creators.entregarDatosAccesoCustodioInit());

    try {
      const response = await api.post(`custodio/enviar-datos-de-acceso/${url}`);

      dispatch(creators.entregarDatosAccesoCustodioDone(response.data));
    } catch (error) {
      const pruebaVida = _.get(error, 'response.data.pruebaVida', null);

      dispatch(creators.entregarDatosAccesoCustodioFail(pruebaVida, helpers.resolveErrors(error.response)));
    }
  };
};

export const resetConfiguracionTiempoRespuestaPruebasVidaErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetConfiguracionTiempoRespuestaPruebasVidaErrors());
  };
};
