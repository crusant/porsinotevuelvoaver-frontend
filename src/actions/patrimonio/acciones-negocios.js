import api from '../../api';
import * as creators from '../creators/patrimonio/acciones-negocios';
import * as helpers from '../../helpers';

export const getAccionesNegocios = () => {
  return async (dispatch) => {
    dispatch(creators.getAccionesNegociosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`acciones-o-negocios?token=${token}`);

      dispatch(creators.getAccionesNegociosDone(response.data));
    } catch (error) {
      dispatch(creators.getAccionesNegociosFail(helpers.resolveError(error)));
    }
  };
};

export const changeAccionNegocioInputs = (entity, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeAccionNegocioInputs(entity, attribute, value));
  };
};

export const addSocio = (socio) => {
  return (dispatch) => {
    dispatch(creators.addSocio(socio));
  };
};

export const resetSocio = () => {
  return (dispatch) => {
    dispatch(creators.resetSocio());
  };
};

export const removeSocio = (index) => {
  return (dispatch) => {
    dispatch(creators.removeSocio(index));
  };
};

export const resetAccionNegocioInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetAccionNegocioInputs());
  };
};

export const saveAccionNegocio = (accionNegocio) => {
  return async (dispatch) => {
    dispatch(creators.saveAccionNegocioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`acciones-o-negocios?token=${token}`, accionNegocio);

      dispatch(creators.saveAccionNegocioDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveAccionNegocioFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const loadAccionNegocioInputs = (accionNegocio) => {
  return (dispatch) => {
    dispatch(creators.loadAccionNegocioInputs(accionNegocio));
  };
};

export const updateAccionNegocio = (accionNegocio) => {
  return async (dispatch) => {
    dispatch(creators.updateAccionNegocioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`acciones-o-negocios/${accionNegocio.id}?token=${token}`, accionNegocio);

      dispatch(creators.updateAccionNegocioDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateAccionNegocioFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteAccionNegocio = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteAccionNegocioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`acciones-o-negocios/${id}?token=${token}`);
      const accionNegocio = await response.data;

      dispatch(creators.deleteAccionNegocioDone(accionNegocio.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteAccionNegocioFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
