import api from '../../api';
import * as creators from '../creators/patrimonio/bienes-valores-sentimentales';
import * as helpers from '../../helpers';

export const getBienesValoresSentimentales= () => {
  return async (dispatch) => {
    dispatch(creators.getBienesValoresSentimentalesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`bienes-con-valor-sentimental?token=${token}`);

      dispatch(creators.getBienesValoresSentimentalesDone(response.data));
    } catch (error) {
      dispatch(creators.getBienesValoresSentimentalesFail(helpers.resolveError(error)));
    }
  };
};

export const changeBienValorSentimentalInputs = (entity, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeBienValorSentimentalInputs(entity, attribute, value));
  };
};

export const resetBienValorSentimentalInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetBienValorSentimentalInputs());
  };
};

export const saveBienValorSentimental = (bienValorSentimental) => {
  return async (dispatch) => {
    dispatch(creators.saveBienValorSentimentalInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`bienes-con-valor-sentimental?token=${token}`, bienValorSentimental);

      dispatch(creators.saveBienValorSentimentalDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveBienValorSentimentalFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const loadBienValorSentimentalInputs = (bienValorSentimental) => {
  return (dispatch) => {
    dispatch(creators.loadBienValorSentimentalInputs(bienValorSentimental));
  };
};

export const updateBienValorSentimental = (bienValorSentimental) => {
  return async (dispatch) => {
    dispatch(creators.updateBienValorSentimentalInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`bienes-con-valor-sentimental/${bienValorSentimental.id}?token=${token}`, bienValorSentimental);

      dispatch(creators.updateBienValorSentimentalDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateBienValorSentimentalFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteBienValorSentimental = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteBienValorSentimentalInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`bienes-con-valor-sentimental/${id}?token=${token}`);
      const bienValorSentimental = await response.data;

      dispatch(creators.deleteBienValorSentimentalDone(bienValorSentimental.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteBienValorSentimentalFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
