import api from '../../api';
import * as creators from '../creators/patrimonio/inversiones';
import * as helpers from '../../helpers';

export const getInversiones = () => {
  return async (dispatch) => {
    dispatch(creators.getInversionesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`inversiones?token=${token}`);

      dispatch(creators.getInversionesDone(response.data));
    } catch (error) {
      dispatch(creators.getInversionesFail(helpers.resolveError(error)));
    }
  };
};

export const changeInversionInputs = (entity, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeInversionInputs(entity, attribute, value));
  };
};

export const addBeneficiario = (beneficiario) => {
  return (dispatch) => {
    dispatch(creators.addBeneficiario(beneficiario));
  };
};

export const resetBeneficiario = () => {
  return (dispatch) => {
    dispatch(creators.resetBeneficiario());
  };
};

export const removeBeneficiario = (index) => {
  return (dispatch) => {
    dispatch(creators.removeBeneficiario(index));
  };
};

export const resetInversionInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetInversionInputs());
  };
};

export const saveInversion = (inversion) => {
  return async (dispatch) => {
    dispatch(creators.saveInversionInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`inversiones?token=${token}`, inversion);

      dispatch(creators.saveInversionDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveInversionFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const loadInversionInputs = (inversion) => {
  return (dispatch) => {
    dispatch(creators.loadInversionInputs(inversion));
  };
};

export const updateInversion = (inversion) => {
  return async (dispatch) => {
    dispatch(creators.updateInversionInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`inversiones/${inversion.id}?token=${token}`, inversion);

      dispatch(creators.updateInversionDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateInversionFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteInversion = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteInversionInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`inversiones/${id}?token=${token}`);
      const inversion = await response.data;

      dispatch(creators.deleteInversionDone(inversion.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteInversionFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
