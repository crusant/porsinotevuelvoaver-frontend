import api from '../../api';
import * as creators from '../creators/patrimonio/vehiculos';
import * as helpers from '../../helpers';

export const getVehiculos= () => {
  return async (dispatch) => {
    dispatch(creators.getVehiculosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`vehiculos?token=${token}`);

      dispatch(creators.getVehiculosDone(response.data));
    } catch (error) {
      dispatch(creators.getVehiculosFail(helpers.resolveError(error)));
    }
  };
};

export const changeVehiculoInputs = (entity, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeVehiculoInputs(entity, attribute, value));
  };
};

export const resetVehiculoInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetVehiculoInputs());
  };
};

export const saveVehiculo = (vehiculo) => {
  return async (dispatch) => {
    dispatch(creators.saveVehiculoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`vehiculos?token=${token}`, vehiculo);

      dispatch(creators.saveVehiculoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveVehiculoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const loadVehiculoInputs = (vehiculo) => {
  return (dispatch) => {
    dispatch(creators.loadVehiculoInputs(vehiculo));
  };
};

export const updateVehiculo = (vehiculo) => {
  return async (dispatch) => {
    dispatch(creators.updateVehiculoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`vehiculos/${vehiculo.id}?token=${token}`, vehiculo);

      dispatch(creators.updateVehiculoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateVehiculoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteVehiculo = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteVehiculoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`vehiculos/${id}?token=${token}`);
      const vehiculo = await response.data;

      dispatch(creators.deleteVehiculoDone(vehiculo.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteVehiculoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
