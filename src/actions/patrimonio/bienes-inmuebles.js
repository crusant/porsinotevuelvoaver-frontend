import api from '../../api';
import * as creators from '../creators/patrimonio/bienes-inmuebles';
import * as helpers from '../../helpers';

export const getBienesInmuebles= () => {
  return async (dispatch) => {
    dispatch(creators.getBienesInmueblesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`bienes-inmuebles?token=${token}`);

      dispatch(creators.getBienesInmueblesDone(response.data));
    } catch (error) {
      dispatch(creators.getBienesInmueblesFail(helpers.resolveError(error)));
    }
  };
};

export const changeBienInmuebleInputs = (entity, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeBienInmuebleInputs(entity, attribute, value));
  };
};

export const resetBienInmuebleInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetBienInmuebleInputs());
  };
};

export const saveBienInmueble = (bienInmueble) => {
  return async (dispatch) => {
    dispatch(creators.saveBienInmuebleInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`bienes-inmuebles?token=${token}`, bienInmueble);

      dispatch(creators.saveBienInmuebleDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveBienInmuebleFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const loadBienInmuebleInputs = (bienInmueble) => {
  return (dispatch) => {
    dispatch(creators.loadBienInmuebleInputs(bienInmueble));
  };
};

export const updateBienInmueble = (bienInmueble) => {
  return async (dispatch) => {
    dispatch(creators.updateBienInmuebleInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`bienes-inmuebles/${bienInmueble.id}?token=${token}`, bienInmueble);

      dispatch(creators.updateBienInmuebleDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateBienInmuebleFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteBienInmueble = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteBienInmuebleInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`bienes-inmuebles/${id}?token=${token}`);
      const bienInmueble = await response.data;

      dispatch(creators.deleteBienInmuebleDone(bienInmueble.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteBienInmuebleFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
