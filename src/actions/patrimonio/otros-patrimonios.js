import api from '../../api';
import * as creators from '../creators/patrimonio/otros-patrimonios';
import * as helpers from '../../helpers';

export const getOtrosPatrimonios = () => {
  return async (dispatch) => {
    dispatch(creators.getOtrosPatrimoniosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`otros-patrimonios?token=${token}`);

      dispatch(creators.getOtrosPatrimoniosDone(response.data));
    } catch (error) {
      dispatch(creators.getOtrosPatrimoniosFail(helpers.resolveError(error)));
    }
  };
};

export const changeOtroPatrimonioInputs = (entity, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeOtroPatrimonioInputs(entity, attribute, value));
  };
};

export const resetOtroPatrimonioInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetOtroPatrimonioInputs());
  };
};

export const saveOtroPatrimonio = (otroPatrimonio) => {
  return async (dispatch) => {
    dispatch(creators.saveOtroPatrimonioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`otros-patrimonios?token=${token}`, otroPatrimonio);

      dispatch(creators.saveOtroPatrimonioDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveOtroPatrimonioFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const loadOtroPatrimonioInputs = (otroPatrimonio) => {
  return (dispatch) => {
    dispatch(creators.loadOtroPatrimonioInputs(otroPatrimonio));
  };
};

export const updateOtroPatrimonio = (otroPatrimonio) => {
  return async (dispatch) => {
    dispatch(creators.updateOtroPatrimonioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`otros-patrimonios/${otroPatrimonio.id}?token=${token}`, otroPatrimonio);

      dispatch(creators.updateOtroPatrimonioDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateOtroPatrimonioFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteOtroPatrimonio = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteOtroPatrimonioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`otros-patrimonios/${id}?token=${token}`);
      const otroPatrimonio = await response.data;

      dispatch(creators.deleteOtroPatrimonioDone(otroPatrimonio.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteOtroPatrimonioFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
