import api from '../../api';
import * as creators from '../creators/patrimonio/adeudos';
import * as helpers from '../../helpers';

export const getAdeudos = () => {
  return async (dispatch) => {
    dispatch(creators.getAdeudosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`adeudos?token=${token}`);

      dispatch(creators.getAdeudosDone(response.data));
    } catch (error) {
      dispatch(creators.getAdeudosFail(helpers.resolveError(error)));
    }
  };
};

export const changeAdeudoInputs = (entity, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeAdeudoInputs(entity, attribute, value));
  };
};

export const resetAdeudoInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetAdeudoInputs());
  };
};

export const saveAdeudo = (adeudo) => {
  return async (dispatch) => {
    dispatch(creators.saveAdeudoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`adeudos?token=${token}`, adeudo);

      dispatch(creators.saveAdeudoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveAdeudoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const loadAdeudoInputs = (adeudo) => {
  return (dispatch) => {
    dispatch(creators.loadAdeudoInputs(adeudo));
  };
};

export const updateAdeudo = (adeudo) => {
  return async (dispatch) => {
    dispatch(creators.updateAdeudoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`adeudos/${adeudo.id}?token=${token}`, adeudo);

      dispatch(creators.updateAdeudoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateAdeudoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteAdeudo = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteAdeudoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`adeudos/${id}?token=${token}`);
      const adeudo = await response.data;

      dispatch(creators.deleteAdeudoDone(adeudo.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteAdeudoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
