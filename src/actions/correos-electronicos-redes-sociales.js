import api from '../api';
import * as creators from './creators/correos-electronicos-redes-sociales';
import * as helpers from '../helpers';

export const getCorreosElectronicosRedesSociales = () => {
  return async (dispatch) => {
    dispatch(creators.getCorreosElectronicosRedesSocialesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`correo-y-redes-sociales?token=${token}`);

      dispatch(creators.getCorreosElectronicosRedesSocialesDone(response.data));
    } catch (error) {
      dispatch(creators.getCorreosElectronicosRedesSocialesFail(helpers.resolveError(error)));
    }
  };
};

export const changeCorreoElectronicoRedSocialInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeCorreoElectronicoRedSocialInput(attribute, value));
  };
};

export const resetCorreoElectronicoRedSocialInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetCorreoElectronicoRedSocialInputs());
  };
};

export const saveCorreoElectronicoRedSocial = (correoElectronicoRedSocial) => {
  return async (dispatch) => {
    dispatch(creators.saveCorreoElectronicoRedSocialInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`correo-y-redes-sociales?token=${token}`, correoElectronicoRedSocial);

      dispatch(creators.saveCorreoElectronicoRedSocialDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveCorreoElectronicoRedSocialFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const editCorreoElectronicoRedSocial = (correoElectronicoRedSocial) => {
  return (dispatch) => {
    dispatch(creators.editCorreoElectronicoRedSocial(correoElectronicoRedSocial));
  };
};

export const updateCorreoElectronicoRedSocial = (correoElectronicoRedSocial) => {
  return async (dispatch) => {
    dispatch(creators.updateCorreoElectronicoRedSocialInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`correo-y-redes-sociales/${correoElectronicoRedSocial.id}?token=${token}`, correoElectronicoRedSocial);

      dispatch(creators.updateCorreoElectronicoRedSocialDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateCorreoElectronicoRedSocialFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteCorreoElectronicoRedSocial = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteCorreoElectronicoRedSocialInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`correo-y-redes-sociales/${id}?token=${token}`);
      const correoElectronicoRedSocial = await response.data;

      dispatch(creators.deleteCorreoElectronicoRedSocialDone(correoElectronicoRedSocial.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteCorreoElectronicoRedSocialFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
