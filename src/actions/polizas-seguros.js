import api from '../api';
import * as creators from './creators/polizas-seguros';
import * as helpers from '../helpers';

export const getPolizasSeguros = () => {
  return async (dispatch) => {
    dispatch(creators.getPolizasSegurosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`polizas-de-seguros?token=${token}`);

      dispatch(creators.getPolizasSegurosDone(response.data));
    } catch (error) {
      dispatch(creators.getPolizasSegurosFail(helpers.resolveError(error)));
    }
  };
};

export const changePolizaSeguroInputs = (entity, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changePolizaSeguroInputs(entity, attribute, value));
  };
};

export const addBeneficiario = (beneficiario) => {
  return (dispatch) => {
    dispatch(creators.addBeneficiario(beneficiario));
  };
};

export const resetBeneficiario = () => {
  return (dispatch) => {
    dispatch(creators.resetBeneficiario());
  };
};

export const removeBeneficiario = (index) => {
  return (dispatch) => {
    dispatch(creators.removeBeneficiario(index));
  };
};

export const changeAsesorInputs = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeAsesorInputs(attribute, value));
  };
};

export const resetPolizaSeguroInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetPolizaSeguroInputs());
  };
};

export const savePolizaSeguro = (polizaSeguro) => {
  return async (dispatch) => {
    dispatch(creators.savePolizaSeguroInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`polizas-de-seguros?token=${token}`, polizaSeguro);

      dispatch(creators.savePolizaSeguroDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.savePolizaSeguroFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const editPolizaSeguro = (polizaSeguro) => {
  return (dispatch) => {
    dispatch(creators.editPolizaSeguro(polizaSeguro));
  };
};

export const updatePolizaSeguro = (polizaSeguro) => {
  return async (dispatch) => {
    dispatch(creators.updatePolizaSeguroInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`polizas-de-seguros/${polizaSeguro.get('id')}?token=${token}`, polizaSeguro);

      dispatch(creators.updatePolizaSeguroDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updatePolizaSeguroFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deletePolizaSeguro = (id) => {
  return async (dispatch) => {
    dispatch(creators.deletePolizaSeguroInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`polizas-de-seguros/${id}?token=${token}`);
      const polizaSeguro = await response.data;

      dispatch(creators.deletePolizaSeguroDone(polizaSeguro.id));
      return true;
    } catch (error) {
      dispatch(creators.deletePolizaSeguroFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const downloadPolizaSeguro = (id, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadPolizaSeguroInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`polizas-de-seguros/${id}/descargar?token=${token}`, {
        responseType: 'blob'
      });

      helpers.fileDownload(response.data, 'poliza-seguro', extension);

      dispatch(creators.downloadPolizaSeguroDone());
    } catch (error) {
      dispatch(creators.downloadPolizaSeguroFail('Ocurrió un error al intentar descargar la póliza de seguro, por favor inténtelo más tarde.'));
    }
  };
};

export const resetPolizasSegurosErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetPolizasSegurosErrors());
  };
};
