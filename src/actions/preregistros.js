import api from '../api';
import * as creators from './creators/preregistros';
import * as helpers from '../helpers';

export const getPreregistrosExpirados = () => {
  return async (dispatch) => {
    dispatch(creators.getPreregistrosExpiradosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`preregistros/expirados?token=${token}`);

      dispatch(creators.getPreregistrosExpiradosDone(response.data));
    } catch (error) {
      dispatch(creators.getPreregistrosExpiradosFail(helpers.resolveError(error)));
    }
  };
};

export const savePreregistro = (preregistro) => {
  return async (dispatch) => {
    dispatch(creators.savePreregistroInit());

    try {
      const response = await api.post('preregistros', preregistro);

      dispatch(creators.savePreregistroDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.savePreregistroFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const getPreregistro = (url) => {
  return async (dispatch) => {
    dispatch(creators.getPreregistroInit());

    try {
      const response = await api.get(`preregistros/${url}`);

      dispatch(creators.getPreregistroDone(response.data));
    } catch (error) {
      dispatch(creators.getPreregistroFail(helpers.resolveError(error)));
    }
  };
};
