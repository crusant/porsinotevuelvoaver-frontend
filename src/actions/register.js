import api from '../api';
import * as creators from './creators/register';
import * as helpers from '../helpers';

export const registerFreePlan = (url) => {
  return async (dispatch) => {
    dispatch(creators.registerFreePlanStart());

    try {
      const response = await api.post(`register/${url}/free-plan`);
      const { token, user, expiresIn } = response.data;

      helpers.setSessionInLocalStorage(token, user, expiresIn);

      dispatch(creators.registerFreePlanDone(token, user));
      return true;
    } catch (error) {
      dispatch(creators.registerFreePlanFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const paypalCreateOrder = (plan_id) => {
  return async (dispatch) => {
    dispatch(creators.paypalCreateOrderStart());

    try {
      return await api.post('register/paypal-create-order', { plan_id });
    } catch (error) {
      return dispatch(creators.paypalCreateOrderFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const payWithPayPalAndRegister = (url, data) => {
  return async (dispatch) => {
    dispatch(creators.payWithPayPalAndRegisterInit());

    try {
      const response = await api.post(`register/${url}/pay-with-paypal`, data);
      const { token, user, expiresIn } = response.data;

      helpers.setSessionInLocalStorage(token, user, expiresIn);

      dispatch(creators.payWithPayPalAndRegisterDone(token, user));
      return true;
    } catch (error) {
      dispatch(creators.payWithPayPalAndRegisterFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const payWithPayPalAndRenew = (data) => {
  return async (dispatch) => {
    dispatch(creators.payWithPayPalAndRenewInit());

    try {
      const oldToken = localStorage.getItem('token');
      const response = await api.post(`register/pay-with-paypal-and-renew?token=${oldToken}`, data);
      const { token, user, expiresIn } = response.data;

      helpers.setSessionInLocalStorage(token, user, expiresIn);

      dispatch(creators.payWithPayPalAndRenewDone(token, user));
      return true;
    } catch (error) {
      dispatch(creators.payWithPayPalAndRenewFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const payWithCreditCardAndRegister = (url, data) => {
  return async (dispatch) => {
    dispatch(creators.payWithCreditCardAndRegisterInit());

    try {
      const response = await api.post(`register/${url}/pay-with-credit-card`, data);
      const { token, user, expiresIn } = response.data;

      helpers.setSessionInLocalStorage(token, user, expiresIn);

      dispatch(creators.payWithCreditCardAndRegisterDone(token, user));
      return true;
    } catch (error) {
      dispatch(creators.payWithCreditCardAndRegisterFail(helpers.resolveErrors(error.response)));
      return false;
    }
  }
};

export const payWithCreditCardAndRenew = (data) => {
  return async (dispatch) => {
    dispatch(creators.payWithCreditCardAndRenewInit());

    try {
      const oldToken = localStorage.getItem('token');
      const response = await api.post(`register/pay-with-credit-card-and-renew?token=${oldToken}`, data);
      const { token, user, expiresIn } = response.data;

      helpers.setSessionInLocalStorage(token, user, expiresIn);

      dispatch(creators.payWithCreditCardAndRenewDone(token, user));
      return true;
    } catch (error) {
      dispatch(creators.payWithCreditCardAndRenewFail(helpers.resolveErrors(error.response)));
      return false;
    }
  }
};

export const payWithOxxoAndRegister = (url, data) => {
  return async (dispatch) => {
    dispatch(creators.payWithOxxoAndRegisterInit());

    try {
      const response = await api.post(`register/${url}/pay-with-oxxo`, data);
      const { transaction_details } = response.data;

      dispatch(creators.payWithOxxoAndRegisterDone(transaction_details.external_resource_url));
      return true;
    } catch (error) {
      dispatch(creators.payWithOxxoAndRegisterFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const payWithOxxoAndRenew = (data) => {
  return async (dispatch) => {
    dispatch(creators.payWithOxxoAndRenewInit());

    try {
      const oldToken = localStorage.getItem('token');
      const response = await api.post(`register/pay-with-oxxo-and-renew?token=${oldToken}`, data);
      const { token, user, expiresIn, mercadoPago: { transaction_details } } = response.data;

      helpers.setSessionInLocalStorage(token, user, expiresIn);

      dispatch(creators.payWithOxxoAndRenewDone(token, user, transaction_details.external_resource_url));
      return true;
    } catch (error) {
      dispatch(creators.payWithOxxoAndRenewFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const passwordReset = (data) => {
  return async (dispatch) => {
    dispatch(creators.passwordResetInit());

    try {
      await api.post('password-reset', data);

      dispatch(creators.passwordResetDone());
      return true;
    } catch (error) {
      dispatch(creators.passwordResetFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};
