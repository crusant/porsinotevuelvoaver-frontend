import api from '../api';
import * as creators from './creators/asesores-contactos';
import * as helpers from '../helpers';

export const getAsesoresContactos = () => {
  return async (dispatch) => {
    dispatch(creators.getAsesoresContactosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`asesores-y-contactos?token=${token}`);

      dispatch(creators.getAsesoresContactosDone(response.data));
    } catch (error) {
      dispatch(creators.getAsesoresContactosFail(helpers.resolveError(error)));
    }
  };
};

export const changeAsesorContactoInputs = (entity, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeAsesorContactoInputs(entity, attribute, value));
  };
};

export const resetAsesorContactoInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetAsesorContactoInputs());
  };
};

export const saveAsesorContacto = (asesorContacto) => {
  return async (dispatch) => {
    dispatch(creators.saveAsesorContactoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`asesores-y-contactos?token=${token}`, asesorContacto);

      dispatch(creators.saveAsesorContactoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveAsesorContactoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const loadAsesorContactoInputs = (asesorContacto) => {
  return (dispatch) => {
    dispatch(creators.loadAsesorContactoInputs(asesorContacto));
  };
};

export const updateAsesorContacto = (asesorContacto) => {
  return async (dispatch) => {
    dispatch(creators.updateAsesorContactoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`asesores-y-contactos/${asesorContacto.id}?token=${token}`, asesorContacto);

      dispatch(creators.updateAsesorContactoDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateAsesorContactoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteAsesorContacto = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteAsesorContactoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`asesores-y-contactos/${id}?token=${token}`);
      const asesorContacto = await response.data;

      dispatch(creators.deleteAsesorContactoDone(asesorContacto.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteAsesorContactoFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
