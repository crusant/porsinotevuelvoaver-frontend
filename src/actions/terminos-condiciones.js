import api from '../api';
import * as creators from './creators/terminos-condiciones';
import * as helpers from '../helpers';

export const getTerminosCondiciones = () => {
  return async (dispatch) => {
    dispatch(creators.getTerminosCondicionesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`terminos-y-condiciones?token=${token}`);

      dispatch(creators.getTerminosCondicionesDone(response.data));
    } catch (error) {
      dispatch(creators.getTerminosCondicionesFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const changeTerminosCondicionesInputs = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeTerminosCondicionesInputs(attribute, value));
  };
};

export const updateTerminosCondiciones = (terminosCondiciones) => {
  return async (dispatch) => {
    dispatch(creators.updateTerminosCondicionesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`terminos-y-condiciones?token=${token}`, terminosCondiciones);

      dispatch(creators.updateTerminosCondicionesDone(response.data));
    } catch (error) {
      dispatch(creators.updateTerminosCondicionesFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const resetTerminosCondicionesErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetTerminosCondicionesErrors());
  };
};

export const resetTerminosCondicionesToStartup = () => {
  return (dispatch) => {
    dispatch(creators.resetTerminosCondicionesToStartup());
  };
};
