import api from '../api';
import * as creators from './creators/aviso-privacidad';
import * as helpers from '../helpers';

export const getAvisoPrivacidad = () => {
  return async (dispatch) => {
    dispatch(creators.getAvisoPrivacidadInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`aviso-de-privacidad?token=${token}`);

      dispatch(creators.getAvisoPrivacidadDone(response.data));
    } catch (error) {
      dispatch(creators.getAvisoPrivacidadFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const changeAvisoPrivacidadInputs = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeAvisoPrivacidadInputs(attribute, value));
  };
};

export const updateAvisoPrivacidad = (avisoPrivacidad) => {
  return async (dispatch) => {
    dispatch(creators.updateAvisoPrivacidadInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`aviso-de-privacidad?token=${token}`, avisoPrivacidad);

      dispatch(creators.updateAvisoPrivacidadDone(response.data));
    } catch (error) {
      dispatch(creators.updateAvisoPrivacidadFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const resetAvisoPrivacidadErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetAvisoPrivacidadErrors());
  };
};

export const resetAvisoPrivacidadToStartup = () => {
  return (dispatch) => {
    dispatch(creators.resetAvisoPrivacidadToStartup());
  };
};
