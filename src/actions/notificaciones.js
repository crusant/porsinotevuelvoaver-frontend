import api from '../api';
import * as creators from './creators/notificaciones';
import * as helpers from '../helpers';

export const getNotificaciones = () => {
  return async (dispatch) => {
    dispatch(creators.getNotificacionesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`notificaciones?token=${token}`);

      dispatch(creators.getNotificacionesDone(response.data));
    } catch (error) {
      dispatch(creators.getNotificacionesFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const confirmarNotificacionPruebaVida = (url) => {
  return async (dispatch) => {
    dispatch(creators.confirmarNotificacionPruebaVidaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`usuario/confirmar-prueba-de-vida/${url}?token=${token}`);

      dispatch(creators.confirmarNotificacionPruebaVidaDone(response.data));
    } catch (error) {
      dispatch(creators.confirmarNotificacionPruebaVidaFail(helpers.resolveErrors(error.response)));
    }
  };
};
