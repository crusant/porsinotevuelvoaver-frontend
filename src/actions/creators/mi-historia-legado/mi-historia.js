import * as types from '../../types/mi-historia-legado/mi-historia';

export const getMiHistoriaInit = () => {
  return {
    type: types.GET_MI_HISTORIA_INIT
  };
};

export const getMiHistoriaDone = (preguntas) => {
  return {
    type: types.GET_MI_HISTORIA_DONE,
    preguntas
  };
};

export const getMiHistoriaFail = (errors) => {
  return {
    type: types.GET_MI_HISTORIA_FAIL,
    errors
  };
};

export const changeMiHistoriaInput = (index, attribute, value) => {
  return {
    type: types.CHANGE_MI_HISTORIA_INPUT,
    index,
    attribute,
    value
  };
};

export const saveMiHistoriaInit = () => {
  return {
    type: types.SAVE_MI_HISTORIA_INIT
  };
};

export const saveMiHistoriaDone = (respuesta) => {
  return {
    type: types.SAVE_MI_HISTORIA_DONE,
    respuesta
  };
};

export const saveMiHistoriaFail = (errors) => {
  return {
    type: types.SAVE_MI_HISTORIA_FAIL,
    errors
  };
};

export const downloadMiHistoriaAudioInit = () => {
  return {
    type: types.DOWNLOAD_MI_HISTORIA_AUDIO_INIT
  };
};

export const downloadMiHistoriaAudioDone = () => {
  return {
    type: types.DOWNLOAD_MI_HISTORIA_AUDIO_DONE
  };
};

export const downloadMiHistoriaAudioFail = (errors) => {
  return {
    type: types.DOWNLOAD_MI_HISTORIA_AUDIO_FAIL,
    errors
  };
};

export const downloadMiHistoriaVideoInit = () => {
  return {
    type: types.DOWNLOAD_MI_HISTORIA_VIDEO_INIT
  };
};

export const downloadMiHistoriaVideoDone = () => {
  return {
    type: types.DOWNLOAD_MI_HISTORIA_VIDEO_DONE
  };
};

export const downloadMiHistoriaVideoFail = (errors) => {
  return {
    type: types.DOWNLOAD_MI_HISTORIA_VIDEO_FAIL,
    errors
  };
};

export const resetMiHistoriaErrors = () => {
  return {
    type: types.RESET_MI_HISTORIA_ERRORS
  };
};

export const resetMiHistoria = () => {
  return {
    type: types.RESET_MI_HISTORIA
  };
};
