import * as types from '../../types/mi-historia-legado/mensajes-postumos';

export const getMensajesPostumosInit = () => {
  return {
    type: types.GET_MENSAJES_POSTUMOS_INIT
  };
};

export const getMensajesPostumosDone = (data) => {
  return {
    type: types.GET_MENSAJES_POSTUMOS_DONE,
    data
  };
};

export const getMensajesPostumosFail = (errors) => {
  return {
    type: types.GET_MENSAJES_POSTUMOS_FAIL,
    errors
  };
};

export const changeMensajePostumoInput = (attribute, value) => {
  return {
    type: types.CHANGE_MENSAJE_POSTUMO_INPUT,
    attribute,
    value
  };
};

export const resetMensajePostumoInputs = () => {
  return {
    type: types.RESET_MENSAJE_POSTUMO_INPUTS
  };
};

export const saveMensajePostumoInit = () => {
  return {
    type: types.SAVE_MENSAJE_POSTUMO_INIT
  };
};

export const saveMensajePostumoDone = (mensajePostumo) => {
  return {
    type: types.SAVE_MENSAJE_POSTUMO_DONE,
    mensajePostumo
  };
};

export const saveMensajePostumoFail = (errors) => {
  return {
    type: types.SAVE_MENSAJE_POSTUMO_FAIL,
    errors
  };
};

export const getMensajePostumoInit = () => {
  return {
    type: types.GET_MENSAJE_POSTUMO_INIT
  };
};

export const getMensajePostumoDone = (mensajePostumo) => {
  return {
    type: types.GET_MENSAJE_POSTUMO_DONE,
    mensajePostumo
  };
};

export const getMensajePostumoFail = (errors) => {
  return {
    type: types.GET_MENSAJE_POSTUMO_FAIL,
    errors
  };
};

export const editMensajePostumo = (mensajePostumo) => {
  return {
    type: types.EDIT_MENSAJE_POSTUMO,
    mensajePostumo
  };
};

export const updateMensajePostumoInit = () => {
  return {
    type: types.UPDATE_MENSAJE_POSTUMO_INIT
  };
};

export const updateMensajePostumoDone = (mensajePostumo) => {
  return {
    type: types.UPDATE_MENSAJE_POSTUMO_DONE,
    mensajePostumo
  };
};

export const updateMensajePostumoFail = (errors) => {
  return {
    type: types.UPDATE_MENSAJE_POSTUMO_FAIL,
    errors
  };
};

export const deleteMensajePostumoInit = () => {
  return {
    type: types.DELETE_MENSAJE_POSTUMO_INIT
  };
};

export const deleteMensajePostumoDone = (id) => {
  return {
    type: types.DELETE_MENSAJE_POSTUMO_DONE,
    id
  };
};

export const deleteMensajePostumoFail = (errors) => {
  return {
    type: types.DELETE_MENSAJE_POSTUMO_FAIL,
    errors
  };
};

export const downloadMensajePostumoAudioInit = () => {
  return {
    type: types.DOWNLOAD_MENSAJE_POSTUMO_AUDIO_INIT
  };
};

export const downloadMensajePostumoAudioDone = () => {
  return {
    type: types.DOWNLOAD_MENSAJE_POSTUMO_AUDIO_DONE
  };
};

export const downloadMensajePostumoAudioFail = (errors) => {
  return {
    type: types.DOWNLOAD_MENSAJE_POSTUMO_AUDIO_FAIL,
    errors
  };
};

export const downloadMensajePostumoVideoInit = () => {
  return {
    type: types.DOWNLOAD_MENSAJE_POSTUMO_VIDEO_INIT
  };
};

export const downloadMensajePostumoVideoDone = () => {
  return {
    type: types.DOWNLOAD_MENSAJE_POSTUMO_VIDEO_DONE
  };
};

export const downloadMensajePostumoVideoFail = (errors) => {
  return {
    type: types.DOWNLOAD_MENSAJE_POSTUMO_VIDEO_FAIL,
    errors
  };
};

export const downloadMensajePostumoFotografiaInit = () => {
  return {
    type: types.DOWNLOAD_MENSAJE_POSTUMO_FOTOGRAFIA_INIT
  };
};

export const downloadMensajePostumoFotografiaDone = () => {
  return {
    type: types.DOWNLOAD_MENSAJE_POSTUMO_FOTOGRAFIA_DONE
  };
};

export const downloadMensajePostumoFotografiaFail = (errors) => {
  return {
    type: types.DOWNLOAD_MENSAJE_POSTUMO_FOTOGRAFIA_FAIL,
    errors
  };
};

export const resetMensajesPostumosErrors = () => {
  return {
    type: types.RESET_MENSAJES_POSTUMOS_ERRORS
  };
};
