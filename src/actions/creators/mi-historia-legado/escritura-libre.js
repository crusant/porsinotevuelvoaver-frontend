import * as types from '../../types/mi-historia-legado/escritura-libre';

export const getEscrituraLibreInit = () => {
  return {
    type: types.GET_ESCRITURA_LIBRE_INIT
  };
};

export const getEscrituraLibreDone = (escrituraLibre) => {
  return {
    type: types.GET_ESCRITURA_LIBRE_DONE,
    escrituraLibre
  };
};

export const getEscrituraLibreFail = (errors) => {
  return {
    type: types.GET_ESCRITURA_LIBRE_FAIL,
    errors
  };
};

export const changeEscrituraLibreInput = (attribute, value) => {
  return {
    type: types.CHANGE_ESCRITURA_LIBRE_INPUT,
    attribute,
    value
  };
};

export const saveEscrituraLibreInit = () => {
  return {
    type: types.SAVE_ESCRITURA_LIBRE_INIT
  };
};

export const saveEscrituraLibreDone = (escrituraLibre) => {
  return {
    type: types.SAVE_ESCRITURA_LIBRE_DONE,
    escrituraLibre
  };
};

export const saveEscrituraLibreFail = (errors) => {
  return {
    type: types.SAVE_ESCRITURA_LIBRE_FAIL,
    errors
  };
};

export const downloadEscrituraLibreAudioInit = () => {
  return {
    type: types.DOWNLOAD_ESCRITURA_LIBRE_AUDIO_INIT
  };
};

export const downloadEscrituraLibreAudioDone = () => {
  return {
    type: types.DOWNLOAD_ESCRITURA_LIBRE_AUDIO_DONE
  };
};

export const downloadEscrituraLibreAudioFail = (errors) => {
  return {
    type: types.DOWNLOAD_ESCRITURA_LIBRE_AUDIO_FAIL,
    errors
  };
};

export const downloadEscrituraLibreVideoInit = () => {
  return {
    type: types.DOWNLOAD_ESCRITURA_LIBRE_VIDEO_INIT
  };
};

export const downloadEscrituraLibreVideoDone = () => {
  return {
    type: types.DOWNLOAD_ESCRITURA_LIBRE_VIDEO_DONE
  };
};

export const downloadEscrituraLibreVideoFail = (errors) => {
  return {
    type: types.DOWNLOAD_ESCRITURA_LIBRE_VIDEO_FAIL,
    errors
  };
};

export const resetEscrituraLibreErrors = () => {
  return {
    type: types.RESET_ESCRITURA_LIBRE_ERRORS
  };
};
