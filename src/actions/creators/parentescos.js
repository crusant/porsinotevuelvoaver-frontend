import * as types from '../types/parentescos';

export const getParentescosInit = () => ({
  type: types.GET_PARENTESCOS_INIT
});

export const getParentescosDone = (parentescos) => ({
  type: types.GET_PARENTESCOS_DONE,
  parentescos
});

export const getParentescosFail = (errors) => ({
  type: types.GET_PARENTESCOS_FAIL,
  errors
});
