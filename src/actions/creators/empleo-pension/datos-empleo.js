import * as types from '../../types/empleo-pension/datos-empleo';

export const getDatosEmpleoInit = () => {
  return {
    type: types.GET_DATOS_EMPLEO_INIT
  };
};

export const getDatosEmpleoDone = (data) => {
  return {
    type: types.GET_DATOS_EMPLEO_DONE,
    data
  };
};

export const getDatosEmpleoFail = (errors) => {
  return {
    type: types.GET_DATOS_EMPLEO_FAIL,
    errors
  };
};

export const changeDatosEmpleoInput = (attribute, value) => {
  return {
    type: types.CHANGE_DATOS_EMPLEO_INPUT,
    attribute,
    value
  };
};

export const changePrestacionInput = (prestacion) => {
  return {
    type: types.CHANGE_PRESTACION_INPUT,
    prestacion
  };
};

export const changeDescripcionInput = (value) => {
  return {
    type: types.CHANGE_DESCRIPCION_INPUT,
    value
  };
};

export const saveDatosEmpleoInit = () => {
  return {
    type: types.SAVE_DATOS_EMPLEO_INIT
  };
};

export const saveDatosEmpleoDone = (empleo) => {
  return {
    type: types.SAVE_DATOS_EMPLEO_DONE,
    empleo
  };
};

export const saveDatosEmpleoFail = (errors) => {
  return {
    type: types.SAVE_DATOS_EMPLEO_FAIL,
    errors
  };
};

export const downloadDatosEmpleoContratoInit = () => {
  return {
    type: types.DOWNLOAD_DATOS_EMPLEO_CONTRATO_INIT
  };
};

export const downloadDatosEmpleoContratoDone = () => {
  return {
    type: types.DOWNLOAD_DATOS_EMPLEO_CONTRATO_DONE
  };
};

export const downloadDatosEmpleoContratoFail = (errors) => {
  return {
    type: types.DOWNLOAD_DATOS_EMPLEO_CONTRATO_FAIL,
    errors
  };
};

export const downloadDatosEmpleoReciboPagoInit = () => {
  return {
    type: types.DOWNLOAD_DATOS_EMPLEO_RECIBO_PAGO_INIT
  };
};

export const downloadDatosEmpleoReciboPagoDone = () => {
  return {
    type: types.DOWNLOAD_DATOS_EMPLEO_RECIBO_PAGO_DONE
  };
};

export const downloadDatosEmpleoReciboPagoFail = (errors) => {
  return {
    type: types.DOWNLOAD_DATOS_EMPLEO_RECIBO_PAGO_FAIL,
    errors
  };
};

export const resetDatosEmpleoErrors = () => {
  return {
    type: types.RESET_DATOS_EMPLEO_ERRORS
  };
};
