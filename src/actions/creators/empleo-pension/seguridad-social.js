import * as types from '../../types/empleo-pension/seguridad-social';

export const getSeguridadesSocialesInit = () => {
  return {
    type: types.GET_SEGURIDADES_SOCIALES_INIT
  };
};

export const getSeguridadesSocialesDone = (seguridadesSociales) => {
  return {
    type: types.GET_SEGURIDADES_SOCIALES_DONE,
    seguridadesSociales
  };
};

export const getSeguridadesSocialesFail = (errors) => {
  return {
    type: types.GET_SEGURIDADES_SOCIALES_FAIL,
    errors
  };
};

export const changeSeguridadSocialInput = (attribute, value) => {
  return {
    type: types.CHANGE_SEGURIDAD_SOCIAL_INPUT,
    attribute,
    value
  };
};

export const resetSeguridadSocialInputs = () => {
  return {
    type: types.RESET_SEGURIDAD_SOCIAL_INPUTS
  };
};

export const saveSeguridadSocialInit = () => {
  return {
    type: types.SAVE_SEGURIDAD_SOCIAL_INIT
  };
};

export const saveSeguridadSocialDone = (seguridadSocial) => {
  return {
    type: types.SAVE_SEGURIDAD_SOCIAL_DONE,
    seguridadSocial
  };
};

export const saveSeguridadSocialFail = (errors) => {
  return {
    type: types.SAVE_SEGURIDAD_SOCIAL_FAIL,
    errors
  };
};

export const editSeguridadSocial = (seguridadSocial) => {
  return {
    type: types.EDIT_SEGURIDAD_SOCIAL,
    seguridadSocial
  };
};

export const updateSeguridadSocialInit = () => {
  return {
    type: types.UPDATE_SEGURIDAD_SOCIAL_INIT
  };
};

export const updateSeguridadSocialDone = (seguridadSocial) => {
  return {
    type: types.UPDATE_SEGURIDAD_SOCIAL_DONE,
    seguridadSocial
  };
};

export const updateSeguridadSocialFail = (errors) => {
  return {
    type: types.UPDATE_SEGURIDAD_SOCIAL_FAIL,
    errors
  };
};

export const deleteSeguridadSocialInit = () => {
  return {
    type: types.DELETE_SEGURIDAD_SOCIAL_INIT
  };
};

export const deleteSeguridadSocialDone = (id) => {
  return {
    type: types.DELETE_SEGURIDAD_SOCIAL_DONE,
    id
  };
};

export const deleteSeguridadSocialFail = (errors) => {
  return {
    type: types.DELETE_SEGURIDAD_SOCIAL_FAIL,
    errors
  };
};

export const resetSeguridadSocialErrors = () => ({
  type: types.RESET_SEGURIDAD_SOCIAL_ERRORS
});
