import * as types from '../../types/empleo-pension/plan-pension';

export const getPlanesPensionesInit = () => {
  return {
    type: types.GET_PLANES_PENSIONES_INIT
  };
};

export const getPlanesPensionesDone = (planesPensiones) => {
  return {
    type: types.GET_PLANES_PENSIONES_DONE,
    planesPensiones
  };
};

export const getPlanesPensionesFail = (errors) => {
  return {
    type: types.GET_PLANES_PENSIONES_FAIL,
    errors
  };
};

export const changePlanPensionInput = (attribute, value) => {
  return {
    type: types.CHANGE_PLAN_PENSION_INPUT,
    attribute,
    value
  };
};

export const changeBeneficiarioInput = (attribute, value) => {
  return {
    type: types.CHANGE_BENEFICIARIO_INPUT,
    attribute,
    value
  };
};

export const addBeneficiario = (beneficiario) => {
  return {
    type: types.ADD_BENEFICIARIO,
    beneficiario
  };
};

export const resetBeneficiario = () => {
  return {
    type: types.RESET_BENEFICIARIO
  };
};

export const removeBeneficiario = (index) => {
  return {
    type: types.REMOVE_BENEFICIARIO,
    index
  };
};

export const changeAsesorInput = (attribute, value) => {
  return {
    type: types.CHANGE_ASESOR_INPUT,
    attribute,
    value
  };
};

export const resetPlanPensionInputs = () => {
  return {
    type: types.RESET_PLAN_PENSION_INPUTS
  };
};

export const savePlanPensionInit = () => {
  return {
    type: types.SAVE_PLAN_PENSION_INIT
  };
};

export const savePlanPensionDone = (planPension) => {
  return {
    type: types.SAVE_PLAN_PENSION_DONE,
    planPension
  };
};

export const savePlanPensionFail = (errors) => {
  return {
    type: types.SAVE_PLAN_PENSION_FAIL,
    errors
  };
};

export const editPlanPension = (planPension) => {
  return {
    type: types.EDIT_PLAN_PENSION,
    planPension
  };
};

export const updatePlanPensionInit = () => {
  return {
    type: types.UPDATE_PLAN_PENSION_INIT
  };
};

export const updatePlanPensionDone = (planPension) => {
  return {
    type: types.UPDATE_PLAN_PENSION_DONE,
    planPension
  };
};

export const updatePlanPensionFail = (errors) => {
  return {
    type: types.UPDATE_PLAN_PENSION_FAIL,
    errors
  };
};

export const deletePlanPensionInit = () => {
  return {
    type: types.DELETE_PLAN_PENSION_INIT
  };
};

export const deletePlanPensionDone = (id) => {
  return {
    type: types.DELETE_PLAN_PENSION_DONE,
    id
  };
};

export const deletePlanPensionFail = (errors) => {
  return {
    type: types.DELETE_PLAN_PENSION_FAIL,
    errors
  };
};

export const resetPlanPensionErrors = () => ({
  type: types.RESET_PLAN_PENSION_ERRORS
});
