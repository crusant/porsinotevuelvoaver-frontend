import * as types from '../../types/ultimos-deseos/datos-testamento';

export const getDatosTestamentoInit = () => {
  return {
    type: types.GET_DATOS_TESTAMENTO_INIT
  };
};

export const getDatosTestamentoDone = (testamento) => {
  return {
    type: types.GET_DATOS_TESTAMENTO_DONE,
    testamento
  };
};

export const getDatosTestamentoFail = (errors) => {
  return {
    type: types.GET_DATOS_TESTAMENTO_FAIL,
    errors
  };
};

export const changeDatosTestamentoInput = (attribute, value) => {
  return {
    type: types.CHANGE_DATOS_TESTAMENTO_INPUT,
    attribute,
    value
  };
};

export const saveDatosTestamentoInit = () => {
  return {
    type: types.SAVE_DATOS_TESTAMENTO_INIT
  };
};

export const saveDatosTestamentoDone = (testamento) => {
  return {
    type: types.SAVE_DATOS_TESTAMENTO_DONE,
    testamento
  };
};

export const saveDatosTestamentoFail = (errors) => {
  return {
    type: types.SAVE_DATOS_TESTAMENTO_FAIL,
    errors
  };
};

export const downloadDatosTestamentoDocumentoInit = () => {
  return {
    type: types.DOWNLOAD_DATOS_TESTAMENTO_DOCUMENTO_INIT
  };
};

export const downloadDatosTestamentoDocumentoDone = () => {
  return {
    type: types.DOWNLOAD_DATOS_TESTAMENTO_DOCUMENTO_DONE
  };
};

export const downloadDatosTestamentoDocumentoFail = (errors) => {
  return {
    type: types.DOWNLOAD_DATOS_TESTAMENTO_DOCUMENTO_FAIL,
    errors
  };
};

export const resetDatosTestamentoErrors = () => {
  return {
    type: types.RESET_DATOS_TESTAMENTO_ERRORS
  };
};
