import * as types from '../../types/ultimos-deseos/secuestro-desaparicion';

export const getSecuestroDesaparicionInit = () => {
  return {
    type: types.GET_SECUESTRO_DESAPARICION_INIT
  };
};

export const getSecuestroDesaparicionDone = (secuestroDesaparicion) => {
  return {
    type: types.GET_SECUESTRO_DESAPARICION_DONE,
    secuestroDesaparicion
  };
};

export const getSecuestroDesaparicionFail = (errors) => {
  return {
    type: types.GET_SECUESTRO_DESAPARICION_FAIL,
    errors
  };
};

export const changeSecuestroDesaparicionInput = (attribute, value) => {
  return {
    type: types.CHANGE_SECUESTRO_DESAPARICION_INPUT,
    attribute,
    value
  };
};

export const saveSecuestroDesaparicionInit = () => {
  return {
    type: types.SAVE_SECUESTRO_DESAPARICION_INIT
  };
};

export const saveSecuestroDesaparicionDone = (secuestroDesaparicion) => {
  return {
    type: types.SAVE_SECUESTRO_DESAPARICION_DONE,
    secuestroDesaparicion
  };
};

export const saveSecuestroDesaparicionFail = (errors) => {
  return {
    type: types.SAVE_SECUESTRO_DESAPARICION_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
