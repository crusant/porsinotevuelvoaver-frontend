import * as types from '../../types/ultimos-deseos/restos';

export const getRestosInit = () => {
  return {
    type: types.GET_RESTOS_INIT
  };
};

export const getRestosDone = (resto) => {
  return {
    type: types.GET_RESTOS_DONE,
    resto
  };
};

export const getRestosFail = (errors) => {
  return {
    type: types.GET_RESTOS_FAIL,
    errors
  };
};

export const changeRestoInput = (attribute, value) => {
  return {
    type: types.CHANGE_RESTO_INPUT,
    attribute,
    value
  };
};

export const changeSepultadoInput = (attribute, value) => {
  return {
    type: types.CHANGE_SEPULTADO_INPUT,
    attribute,
    value
  };
};

export const changeCremadoInput = (attribute, value) => {
  return {
    type: types.CHANGE_CREMADO_INPUT,
    attribute,
    value
  };
};

export const saveRestosInit = () => {
  return {
    type: types.SAVE_RESTOS_INIT
  };
};

export const saveRestosDone = (resto) => {
  return {
    type: types.SAVE_RESTOS_DONE,
    resto
  };
};

export const saveRestosFail = (errors) => {
  return {
    type: types.SAVE_RESTOS_FAIL,
    errors
  };
};

export const downloadRestosDocumentoInit = () => {
  return {
    type: types.DOWNLOAD_RESTOS_DOCUMENTO_INIT
  };
};

export const downloadRestosDocumentoDone = () => {
  return {
    type: types.DOWNLOAD_RESTOS_DOCUMENTO_DONE
  };
};

export const downloadRestosDocumentoFail = (errors) => {
  return {
    type: types.DOWNLOAD_RESTOS_DOCUMENTO_FAIL,
    errors
  };
};

export const resetRestosErrors = () => {
  return {
    type: types.RESET_RESTOS_ERRORS
  };
};
