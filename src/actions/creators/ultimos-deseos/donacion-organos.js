import * as types from '../../types/ultimos-deseos/donacion-organos';

export const getDonacionOrganosInit = () => {
  return {
    type: types.GET_DONACION_ORGANOS_INIT
  };
};

export const getDonacionOrganosDone = (donacionOrgano) => {
  return {
    type: types.GET_DONACION_ORGANOS_DONE,
    donacionOrgano
  };
};

export const getDonacionOrganosFail = (errors) => {
  return {
    type: types.GET_DONACION_ORGANOS_FAIL,
    errors
  };
};

export const changeDonacionOrganosInput = (attribute, value) => {
  return {
    type: types.CHANGE_DONACION_ORGANOS_INPUT,
    attribute,
    value
  };
};

export const saveDonacionOrganosInit = () => {
  return {
    type: types.SAVE_DONACION_ORGANOS_INIT
  };
};

export const saveDonacionOrganosDone = (donacionOrgano) => {
  return {
    type: types.SAVE_DONACION_ORGANOS_DONE,
    donacionOrgano
  };
};

export const saveDonacionOrganosFail = (errors) => {
  return {
    type: types.SAVE_DONACION_ORGANOS_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
