import * as types from '../../types/ultimos-deseos/vida-artificial';

export const getVidaArtificialInit = () => {
  return {
    type: types.GET_VIDA_ARTIFICIAL_INIT
  };
};

export const getVidaArtificialDone = (vidaArtificial) => {
  return {
    type: types.GET_VIDA_ARTIFICIAL_DONE,
    vidaArtificial
  };
};

export const getVidaArtificialFail = (errors) => {
  return {
    type: types.GET_VIDA_ARTIFICIAL_FAIL,
    errors
  };
};

export const changeVidaArtificialInput = (attribute, value) => {
  return {
    type: types.CHANGE_VIDA_ARTIFICIAL_INPUT,
    attribute,
    value
  };
};

export const saveVidaArtificialInit = () => {
  return {
    type: types.SAVE_VIDA_ARTIFICIAL_INIT
  };
};

export const saveVidaArtificialDone = (vidaArtificial) => {
  return {
    type: types.SAVE_VIDA_ARTIFICIAL_DONE,
    vidaArtificial
  };
};

export const saveVidaArtificialFail = (errors) => {
  return {
    type: types.SAVE_VIDA_ARTIFICIAL_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
