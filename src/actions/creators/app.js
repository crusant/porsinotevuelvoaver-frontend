import * as types from '../types/app';

export const downloadFormulariosInit = () => ({
  type: types.DOWNLOAD_FORMULARIOS_INIT
});

export const downloadFormulariosDone = () => ({
  type: types.DOWNLOAD_FORMULARIOS_DONE
});

export const downloadFormulariosFail = (errors) => ({
  type: types.DOWNLOAD_FORMULARIOS_FAIL,
  errors
});
