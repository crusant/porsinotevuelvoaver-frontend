import * as types from '../types/correos-electronicos-redes-sociales';

export const getCorreosElectronicosRedesSocialesInit = () => {
  return {
    type: types.GET_CORREOS_ELECTRONICOS_REDES_SOCIALES_INIT
  };
};

export const getCorreosElectronicosRedesSocialesDone = (correosElectronicosRedesSociales) => {
  return {
    type: types.GET_CORREOS_ELECTRONICOS_REDES_SOCIALES_DONE,
    correosElectronicosRedesSociales
  };
};

export const getCorreosElectronicosRedesSocialesFail = (errors) => {
  return {
    type: types.GET_CORREOS_ELECTRONICOS_REDES_SOCIALES_FAIL,
    errors
  };
};

export const changeCorreoElectronicoRedSocialInput = (attribute, value) => {
  return {
    type: types.CHANGE_CORREO_ELECTRONICO_RED_SOCIAL_INPUT,
    attribute,
    value
  };
};

export const resetCorreoElectronicoRedSocialInputs = () => {
  return {
    type: types.RESET_CORREO_ELECTRONICO_RED_SOCIAL_INPUTS
  };
};

export const saveCorreoElectronicoRedSocialInit = () => {
  return {
    type: types.SAVE_CORREO_ELECTRONICO_RED_SOCIAL_INIT
  };
};

export const saveCorreoElectronicoRedSocialDone = (correoElectronicoRedSocial) => {
  return {
    type: types.SAVE_CORREO_ELECTRONICO_RED_SOCIAL_DONE,
    correoElectronicoRedSocial
  };
};

export const saveCorreoElectronicoRedSocialFail = (errors) => {
  return {
    type: types.SAVE_CORREO_ELECTRONICO_RED_SOCIAL_FAIL,
    errors
  };
};

export const editCorreoElectronicoRedSocial = (correoElectronicoRedSocial) => {
  return {
    type: types.EDIT_CORREO_ELECTRONICO_RED_SOCIAL,
    correoElectronicoRedSocial
  };
};

export const updateCorreoElectronicoRedSocialInit = () => {
  return {
    type: types.UPDATE_CORREO_ELECTRONICO_RED_SOCIAL_INIT
  };
};

export const updateCorreoElectronicoRedSocialDone = (correoElectronicoRedSocial) => {
  return {
    type: types.UPDATE_CORREO_ELECTRONICO_RED_SOCIAL_DONE,
    correoElectronicoRedSocial
  };
};

export const updateCorreoElectronicoRedSocialFail = (errors) => {
  return {
    type: types.UPDATE_CORREO_ELECTRONICO_RED_SOCIAL_FAIL,
    errors
  };
};

export const deleteCorreoElectronicoRedSocialInit = () => {
  return {
    type: types.DELETE_CORREO_ELECTRONICO_RED_SOCIAL_INIT
  };
};

export const deleteCorreoElectronicoRedSocialDone = (id) => {
  return {
    type: types.DELETE_CORREO_ELECTRONICO_RED_SOCIAL_DONE,
    id
  };
};

export const deleteCorreoElectronicoRedSocialFail = (errors) => {
  return {
    type: types.DELETE_CORREO_ELECTRONICO_RED_SOCIAL_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
