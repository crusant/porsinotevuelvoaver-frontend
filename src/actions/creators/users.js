import * as types from '../types/users';

export const changePasswordInit = () => ({
  type: types.CHANGE_PASSWORD_INIT
});

export const changePasswordDone = () => ({
  type: types.CHANGE_PASSWORD_DONE
});

export const changePasswordFail = (errors) => ({
  type: types.CHANGE_PASSWORD_FAIL,
  errors
});

export const resetPasswordErrors = () => ({
  type: types.RESET_PASSWORD_ERRORS
});
