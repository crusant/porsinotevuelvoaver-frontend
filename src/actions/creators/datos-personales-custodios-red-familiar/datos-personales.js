import * as types from '../../types/datos-personales-custodios-red-familiar/datos-personales';

export const getDatosPersonalesInit = () => {
  return {
    type: types.GET_DATOS_PERSONALES_INIT
  };
};

export const getDatosPersonalesDone = (data) => {
  return {
    type: types.GET_DATOS_PERSONALES_DONE,
    data
  };
};

export const getDatosPersonalesFail = (errors) => {
  return {
    type: types.GET_DATOS_PERSONALES_FAIL,
    errors
  };
};

export const changeDatosPersonalesInput = (entity, attribute, value) => {
  return {
    type: types.CHANGE_DATOS_PERSONALES_INPUT,
    entity,
    attribute,
    value
  };
};

export const saveDatosPersonalesInit = () => {
  return {
    type: types.SAVE_DATOS_PERSONALES_INIT
  };
};

export const saveDatosPersonalesDone = (data) => {
  return {
    type: types.SAVE_DATOS_PERSONALES_DONE,
    data
  };
};

export const saveDatosPersonalesFail = (errors) => {
  return {
    type: types.SAVE_DATOS_PERSONALES_FAIL,
    errors
  };
};

export const downloadDatosPersonalesActaNacimientoInit = () => {
  return {
    type: types.DOWNLOAD_DATOS_PERSONALES_ACTA_NACIMIENTO_INIT
  };
};

export const downloadDatosPersonalesActaNacimientoDone = () => {
  return {
    type: types.DOWNLOAD_DATOS_PERSONALES_ACTA_NACIMIENTO_DONE
  };
};

export const downloadDatosPersonalesActaNacimientoFail = (errors) => {
  return {
    type: types.DOWNLOAD_DATOS_PERSONALES_ACTA_NACIMIENTO_FAIL,
    errors
  };
};

export const downloadDatosPersonalesActaMatrimonioInit = () => {
  return {
    type: types.DOWNLOAD_DATOS_PERSONALES_ACTA_MATRIMONIO_INIT
  };
};

export const downloadDatosPersonalesActaMatrimonioDone = () => {
  return {
    type: types.DOWNLOAD_DATOS_PERSONALES_ACTA_MATRIMONIO_DONE
  };
};

export const downloadDatosPersonalesActaMatrimonioFail = (errors) => {
  return {
    type: types.DOWNLOAD_DATOS_PERSONALES_ACTA_NACIMIENTO_FAIL,
    errors
  };
};

export const resetDatosPersonalesErrors = () => {
  return {
    type: types.RESET_DATOS_PERSONALES_ERRORS
  };
};
