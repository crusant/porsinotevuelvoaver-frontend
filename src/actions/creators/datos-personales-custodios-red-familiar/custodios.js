import * as types from '../../types/datos-personales-custodios-red-familiar/custodios';

export const getCustodiosInit = () => {
  return {
    type: types.GET_CUSTODIOS_INIT
  };
};

export const getCustodiosDone = (data) => {
  return {
    type: types.GET_CUSTODIOS_DONE,
    data
  };
};

export const getCustodiosFail = (errors) => {
  return {
    type: types.GET_CUSTODIOS_FAIL,
    errors
  };
};

export const changeCustodioInput = (attribute, value) => {
  return {
    type: types.CHANGE_CUSTODIO_INPUT,
    attribute,
    value
  };
};

export const resetCustodioInputs = () => {
  return {
    type: types.RESET_CUSTODIO_INPUTS
  };
};

export const saveCustodioInit = () => {
  return {
    type: types.SAVE_CUSTODIO_INIT
  };
};

export const saveCustodioDone = (custodio) => {
  return {
    type: types.SAVE_CUSTODIO_DONE,
    custodio
  };
};

export const saveCustodioFail = (errors) => {
  return {
    type: types.SAVE_CUSTODIO_FAIL,
    errors
  };
};

export const editCustodio = (custodio) => {
  return {
    type: types.EDIT_CUSTODIO,
    custodio
  };
};

export const updateCustodioInit = () => {
  return {
    type: types.UPDATE_CUSTODIO_INIT
  };
};

export const updateCustodioDone = (custodio) => {
  return {
    type: types.UPDATE_CUSTODIO_DONE,
    custodio
  };
};

export const updateCustodioFail = (errors) => {
  return {
    type: types.UPDATE_CUSTODIO_FAIL,
    errors
  };
};

export const deleteCustodioInit = () => {
  return {
    type: types.DELETE_CUSTODIO_INIT
  };
};

export const deleteCustodioDone = (id) => {
  return {
    type: types.DELETE_CUSTODIO_DONE,
    id
  };
};

export const deleteCustodioFail = (errors) => {
  return {
    type: types.DELETE_CUSTODIO_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};

export const pruebaVidaCustodioInit = () => ({
  type: types.PRUEBA_VIDA_CUSTODIO_INIT
});

export const pruebaVidaCustodioDone = (pruebaVida) => ({
  type: types.PRUEBA_VIDA_CUSTODIO_DONE,
  pruebaVida
});

export const pruebaVidaCustodioFail = (errors) => ({
  type: types.PRUEBA_VIDA_CUSTODIO_FAIL,
  errors
});

export const confirmarPruebaVidaCustodioInit = () => ({
  type: types.CONFIRMAR_PRUEBA_VIDA_CUSTODIO_INIT
});

export const confirmarPruebaVidaCustodioDone = (pruebaVida) => ({
  type: types.CONFIRMAR_PRUEBA_VIDA_CUSTODIO_DONE,
  pruebaVida
});

export const confirmarPruebaVidaCustodioFail = (errors) => ({
  type: types.CONFIRMAR_PRUEBA_VIDA_CUSTODIO_FAIL,
  errors
});

export const entregarDatosAccesoCustodioInit = () => ({
  type: types.ENTREGAR_DATOS_ACCESO_CUSTODIO_INIT
});

export const entregarDatosAccesoCustodioDone = (pruebaVida) => ({
  type: types.ENTREGAR_DATOS_ACCESO_CUSTODIO_DONE,
  pruebaVida
});

export const entregarDatosAccesoCustodioFail = (errors) => ({
  type: types.ENTREGAR_DATOS_ACCESO_CUSTODIO_FAIL,
  errors
});
