import * as types from '../../types/datos-personales-custodios-red-familiar/red-familiar';

export const getFamiliaresInit = () => {
  return {
    type: types.GET_FAMILIARES_INIT
  };
};

export const getFamiliaresDone = (familiares) => {
  return {
    type: types.GET_FAMILIARES_DONE,
    familiares
  };
};

export const getFamiliaresFail = (errors) => {
  return {
    type: types.GET_FAMILIARES_FAIL,
    errors
  };
};

export const changeFamiliarInput = (attribute, value) => {
  return {
    type: types.CHANGE_FAMILIAR_INPUT,
    attribute,
    value
  };
};

export const resetFamiliarInputs = () => {
  return {
    type: types.RESET_FAMILIAR_INPUTS
  };
};

export const saveFamiliarInit = () => {
  return {
    type: types.SAVE_FAMILIAR_INIT
  };
};

export const saveFamiliarDone = (familiar) => {
  return {
    type: types.SAVE_FAMILIAR_DONE,
    familiar
  };
};

export const saveFamiliarFail = (errors) => {
  return {
    type: types.SAVE_FAMILIAR_FAIL,
    errors
  };
};

export const editFamiliar = (familiar) => {
  return {
    type: types.EDIT_FAMILIAR,
    familiar
  };
};

export const getFamiliarInit = () => {
  return {
    type: types.GET_FAMILIAR_INIT
  };
};

export const getFamiliarDone = (familiar) => {
  return {
    type: types.GET_FAMILIAR_DONE,
    familiar
  };
};

export const getFamiliarFail = (errors) => {
  return {
    type: types.GET_FAMILIAR_FAIL,
    errors
  };
};

export const updateFamiliarInit = () => {
  return {
    type: types.UPDATE_FAMILIAR_INIT
  };
};

export const updateFamiliarDone = (familiar) => {
  return {
    type: types.UPDATE_FAMILIAR_DONE,
    familiar
  };
};

export const updateFamiliarFail = (errors) => {
  return {
    type: types.UPDATE_FAMILIAR_FAIL,
    errors
  };
};

export const deleteFamiliarInit = () => {
  return {
    type: types.DELETE_FAMILIAR_INIT
  };
};

export const deleteFamiliarDone = (id) => {
  return {
    type: types.DELETE_FAMILIAR_DONE,
    id
  };
};

export const deleteFamiliarFail = (errors) => {
  return {
    type: types.DELETE_FAMILIAR_FAIL,
    errors
  };
};

export const downloadFamiliarEscrituraLibreAudioInit = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_ESCRITURA_LIBRE_AUDIO_INIT
  };
};

export const downloadFamiliarEscrituraLibreAudioDone = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_ESCRITURA_LIBRE_AUDIO_DONE
  };
};

export const downloadFamiliarEscrituraLibreAudioFail = (errors) => {
  return {
    type: types.DOWNLOAD_FAMILIAR_ESCRITURA_LIBRE_AUDIO_FAIL,
    errors
  };
};

export const downloadFamiliarEscrituraLibreVideoInit = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_ESCRITURA_LIBRE_VIDEO_INIT
  };
};

export const downloadFamiliarEscrituraLibreVideoDone = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_ESCRITURA_LIBRE_VIDEO_DONE
  };
};

export const downloadFamiliarEscrituraLibreVideoFail = (errors) => {
  return {
    type: types.DOWNLOAD_FAMILIAR_ESCRITURA_LIBRE_VIDEO_FAIL,
    errors
  };
};

export const downloadFamiliarMiHistoriaAudioInit = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MI_HISTORIA_AUDIO_INIT
  };
};

export const downloadFamiliarMiHistoriaAudioDone = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MI_HISTORIA_AUDIO_DONE
  };
};

export const downloadFamiliarMiHistoriaAudioFail = (errors) => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MI_HISTORIA_AUDIO_FAIL,
    errors
  };
};

export const downloadFamiliarMiHistoriaVideoInit = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MI_HISTORIA_VIDEO_INIT
  };
};

export const downloadFamiliarMiHistoriaVideoDone = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MI_HISTORIA_VIDEO_DONE
  };
};

export const downloadFamiliarMiHistoriaVideoFail = (errors) => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MI_HISTORIA_VIDEO_FAIL,
    errors
  };
};

export const downloadFamiliarMensajePostumoAudioInit = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MENSAJE_POSTUMO_AUDIO_INIT
  };
};

export const downloadFamiliarMensajePostumoAudioDone = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MENSAJE_POSTUMO_AUDIO_DONE
  };
};

export const downloadFamiliarMensajePostumoAudioFail = (errors) => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MENSAJE_POSTUMO_AUDIO_FAIL,
    errors
  };
};

export const downloadFamiliarMensajePostumoVideoInit = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MENSAJE_POSTUMO_VIDEO_INIT
  };
};

export const downloadFamiliarMensajePostumoVideoDone = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MENSAJE_POSTUMO_VIDEO_DONE
  };
};

export const downloadFamiliarMensajePostumoVideoFail = (errors) => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MENSAJE_POSTUMO_VIDEO_FAIL,
    errors
  };
};

export const downloadFamiliarMensajePostumoFotografiaInit = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MENSAJE_POSTUMO_FOTOGRAFIA_INIT
  };
};

export const downloadFamiliarMensajePostumoFotografiaDone = () => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MENSAJE_POSTUMO_FOTOGRAFIA_DONE
  };
};

export const downloadFamiliarMensajePostumoFotografiaFail = (errors) => {
  return {
    type: types.DOWNLOAD_FAMILIAR_MENSAJE_POSTUMO_FOTOGRAFIA_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
