import * as types from '../types/contacto';

export const enviarMensajeContactoInit = () => ({
  type: types.ENVIAR_MENSAJE_CONTACTO_INIT
});

export const enviarMensajeContactoDone = () => ({
  type: types.ENVIAR_MENSAJE_CONTACTO_DONE
});

export const enviarMensajeContactoFail = (errors) => ({
  type: types.ENVIAR_MENSAJE_CONTACTO_FAIL,
  errors
});
