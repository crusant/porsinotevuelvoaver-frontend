import * as types from '../types/monedas';

export const getMonedasInit = () => {
  return {
    type: types.GET_MONEDAS_INIT
  };
};

export const getMonedasDone = (monedas) => {
  return {
    type: types.GET_MONEDAS_DONE,
    monedas
  };
};

export const getMonedasFail = (errors) => {
  return {
    type: types.GET_MONEDAS_FAIL,
    errors
  };
};
