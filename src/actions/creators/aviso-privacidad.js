import * as types from '../types/aviso-privacidad';

export const getAvisoPrivacidadInit = () => {
  return {
    type: types.GET_AVISO_PRIVACIDAD_INIT
  };
};

export const getAvisoPrivacidadDone = (avisoPrivacidad) => {
  return {
    type: types.GET_AVISO_PRIVACIDAD_DONE,
    avisoPrivacidad
  };
};

export const getAvisoPrivacidadFail = (errors) => {
  return {
    type: types.GET_AVISO_PRIVACIDAD_FAIL,
    errors
  };
};

export const changeAvisoPrivacidadInputs = (attribute, value) => {
  return {
    type: types.CHANGE_AVISO_PRIVACIDAD_INPUTS,
    attribute,
    value
  };
};

export const updateAvisoPrivacidadInit = () => {
  return {
    type: types.UPDATE_AVISO_PRIVACIDAD_INIT
  };
};

export const updateAvisoPrivacidadDone = (avisoPrivacidad) => {
  return {
    type: types.UPDATE_AVISO_PRIVACIDAD_DONE,
    avisoPrivacidad
  };
};

export const updateAvisoPrivacidadFail = (errors) => {
  return {
    type: types.UPDATE_AVISO_PRIVACIDAD_FAIL,
    errors
  };
};

export const resetAvisoPrivacidadErrors = () => {
  return {
    type: types.RESET_AVISO_PRIVACIDAD_ERRORS
  };
};

export const resetAvisoPrivacidadToStartup = () => {
  return {
    type: types.RESET_AVISO_PRIVACIDAD_TO_STARTUP
  };
};
