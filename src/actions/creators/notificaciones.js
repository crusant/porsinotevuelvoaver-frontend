import * as types from '../types/notificaciones';

export const getNotificacionesInit = () => {
  return {
    type: types.GET_NOTIFICACIONES_INIT
  };
};

export const getNotificacionesDone = (notificaciones) => {
  return {
    type: types.GET_NOTIFICACIONES_DONE,
    notificaciones
  };
};

export const getNotificacionesFail = (errors) => {
  return {
    type: types.GET_NOTIFICACIONES_FAIL,
    errors
  };
};

export const confirmarNotificacionPruebaVidaInit = () => {
  return {
    type: types.CONFIRMAR_NOTIFICACION_PRUEBA_VIDA_INIT
  };
};

export const confirmarNotificacionPruebaVidaDone = (pruebaVida) => {
  return {
    type: types.CONFIRMAR_NOTIFICACION_PRUEBA_VIDA_DONE,
    pruebaVida
  };
};

export const confirmarNotificacionPruebaVidaFail = (errors) => {
  return {
    type: types.CONFIRMAR_NOTIFICACION_PRUEBA_VIDA_FAIL,
    errors
  };
};
