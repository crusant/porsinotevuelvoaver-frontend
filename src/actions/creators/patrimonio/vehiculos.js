import * as types from '../../types/patrimonio/vehiculos';

export const getVehiculosInit = () => {
  return {
    type: types.GET_VEHICULOS_INIT
  };
};

export const getVehiculosDone = (vehiculos) => {
  return {
    type: types.GET_VEHICULOS_DONE,
    vehiculos
  };
};

export const getVehiculosFail = (errors) => {
  return {
    type: types.GET_VEHICULOS_FAIL,
    errors
  };
};

export const changeVehiculoInputs = (entity, attribute, value) => {
  return {
    type: types.CHANGE_VEHICULO_INPUTS,
    entity,
    attribute,
    value
  };
};

export const resetVehiculoInputs = () => {
  return {
    type: types.RESET_VEHICULO_INPUTS
  };
};

export const saveVehiculoInit = () => {
  return {
    type: types.SAVE_VEHICULO_INIT
  };
};

export const saveVehiculoDone = (vehiculo) => {
  return {
    type: types.SAVE_VEHICULO_DONE,
    vehiculo
  };
};

export const saveVehiculoFail = (errors) => {
  return {
    type: types.SAVE_VEHICULO_FAIL,
    errors
  };
};

export const loadVehiculoInputs = (vehiculo) => {
  return {
    type: types.LOAD_VEHICULO_INPUTS,
    vehiculo
  };
};

export const updateVehiculoInit = () => {
  return {
    type: types.UPDATE_VEHICULO_INIT
  };
};

export const updateVehiculoDone = (vehiculo) => {
  return {
    type: types.UPDATE_VEHICULO_DONE,
    vehiculo
  };
};

export const updateVehiculoFail = (errors) => {
  return {
    type: types.UPDATE_VEHICULO_FAIL,
    errors
  };
};

export const deleteVehiculoInit = () => {
  return {
    type: types.DELETE_VEHICULO_INIT
  };
};

export const deleteVehiculoDone = (id) => {
  return {
    type: types.DELETE_VEHICULO_DONE,
    id
  };
};

export const deleteVehiculoFail = (errors) => {
  return {
    type: types.DELETE_VEHICULO_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
