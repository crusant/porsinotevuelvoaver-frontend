import * as types from '../../types/patrimonio/bienes-inmuebles';

export const getBienesInmueblesInit = () => {
  return {
    type: types.GET_BIENES_INMUEBLES_INIT
  };
};

export const getBienesInmueblesDone = (bienesInmuebles) => {
  return {
    type: types.GET_BIENES_INMUEBLES_DONE,
    bienesInmuebles
  };
};

export const getBienesInmueblesFail = (errors) => {
  return {
    type: types.GET_BIENES_INMUEBLES_FAIL,
    errors
  };
};

export const changeBienInmuebleInputs = (entity, attribute, value) => {
  return {
    type: types.CHANGE_BIEN_INMUEBLE_INPUTS,
    entity,
    attribute,
    value
  };
};

export const resetBienInmuebleInputs = () => {
  return {
    type: types.RESET_BIEN_INMUEBLE_INPUTS
  };
};

export const saveBienInmuebleInit = () => {
  return {
    type: types.SAVE_BIEN_INMUEBLE_INIT
  };
};

export const saveBienInmuebleDone = (bienInmueble) => {
  return {
    type: types.SAVE_BIEN_INMUEBLE_DONE,
    bienInmueble
  };
};

export const saveBienInmuebleFail = (errors) => {
  return {
    type: types.SAVE_BIEN_INMUEBLE_FAIL,
    errors
  };
};

export const loadBienInmuebleInputs = (bienInmueble) => {
  return {
    type: types.LOAD_BIEN_INMUEBLE_INPUTS,
    bienInmueble
  };
};

export const updateBienInmuebleInit = () => {
  return {
    type: types.UPDATE_BIEN_INMUEBLE_INIT
  };
};

export const updateBienInmuebleDone = (bienInmueble) => {
  return {
    type: types.UPDATE_BIEN_INMUEBLE_DONE,
    bienInmueble
  };
};

export const updateBienInmuebleFail = (errors) => {
  return {
    type: types.UPDATE_BIEN_INMUEBLE_FAIL,
    errors
  };
};

export const deleteBienInmuebleInit = () => {
  return {
    type: types.DELETE_BIEN_INMUEBLE_INIT
  };
};

export const deleteBienInmuebleDone = (id) => {
  return {
    type: types.DELETE_BIEN_INMUEBLE_DONE,
    id
  };
};

export const deleteBienInmuebleFail = (errors) => {
  return {
    type: types.DELETE_BIEN_INMUEBLE_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
