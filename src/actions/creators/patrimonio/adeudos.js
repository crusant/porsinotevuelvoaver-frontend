import * as types from '../../types/patrimonio/adeudos';

export const getAdeudosInit = () => {
  return {
    type: types.GET_ADEUDOS_INIT
  };
};

export const getAdeudosDone = (adeudos) => {
  return {
    type: types.GET_ADEUDOS_DONE,
    adeudos
  };
};

export const getAdeudosFail = (errors) => {
  return {
    type: types.GET_ADEUDOS_FAIL,
    errors
  };
};

export const changeAdeudoInputs = (entity, attribute, value) => {
  return {
    type: types.CHANGE_ADEUDO_INPUTS,
    entity,
    attribute,
    value
  };
};

export const resetAdeudoInputs = () => {
  return {
    type: types.RESET_ADEUDO_INPUTS
  };
};

export const saveAdeudoInit = () => {
  return {
    type: types.SAVE_ADEUDO_INIT
  };
};

export const saveAdeudoDone = (adeudo) => {
  return {
    type: types.SAVE_ADEUDO_DONE,
    adeudo
  };
};

export const saveAdeudoFail = (errors) => {
  return {
    type: types.SAVE_ADEUDO_FAIL,
    errors
  };
};

export const loadAdeudoInputs = (adeudo) => {
  return {
    type: types.LOAD_ADEUDO_INPUTS,
    adeudo
  };
};

export const updateAdeudoInit = () => {
  return {
    type: types.UPDATE_ADEUDO_INIT
  };
};

export const updateAdeudoDone = (adeudo) => {
  return {
    type: types.UPDATE_ADEUDO_DONE,
    adeudo
  };
};

export const updateAdeudoFail = (errors) => {
  return {
    type: types.UPDATE_ADEUDO_FAIL,
    errors
  };
};

export const deleteAdeudoInit = () => {
  return {
    type: types.DELETE_ADEUDO_INIT
  };
};

export const deleteAdeudoDone = (id) => {
  return {
    type: types.DELETE_ADEUDO_DONE,
    id
  };
};

export const deleteAdeudoFail = (errors) => {
  return {
    type: types.DELETE_ADEUDO_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
