import * as types from '../../types/patrimonio/bienes-valores-sentimentales';

export const getBienesValoresSentimentalesInit = () => {
  return {
    type: types.GET_BIENES_VALORES_SENTIMENTALES_INIT
  };
};

export const getBienesValoresSentimentalesDone = (bienesValoresSentimentales) => {
  return {
    type: types.GET_BIENES_VALORES_SENTIMENTALES_DONE,
    bienesValoresSentimentales
  };
};

export const getBienesValoresSentimentalesFail = (errors) => {
  return {
    type: types.GET_BIENES_VALORES_SENTIMENTALES_FAIL,
    errors
  };
};

export const changeBienValorSentimentalInputs = (entity, attribute, value) => {
  return {
    type: types.CHANGE_BIEN_VALOR_SENTIMENTAL_INPUTS,
    entity,
    attribute,
    value
  };
};

export const resetBienValorSentimentalInputs = () => {
  return {
    type: types.RESET_BIEN_VALOR_SENTIMENTAL_INPUTS
  };
};

export const saveBienValorSentimentalInit = () => {
  return {
    type: types.SAVE_BIEN_VALOR_SENTIMENTAL_INIT
  };
};

export const saveBienValorSentimentalDone = (bienValorSentimental) => {
  return {
    type: types.SAVE_BIEN_VALOR_SENTIMENTAL_DONE,
    bienValorSentimental
  };
};

export const saveBienValorSentimentalFail = (errors) => {
  return {
    type: types.SAVE_BIEN_VALOR_SENTIMENTAL_FAIL,
    errors
  };
};

export const loadBienValorSentimentalInputs = (bienValorSentimental) => {
  return {
    type: types.LOAD_BIEN_VALOR_SENTIMENTAL_INPUTS,
    bienValorSentimental
  };
};

export const updateBienValorSentimentalInit = () => {
  return {
    type: types.UPDATE_BIEN_VALOR_SENTIMENTAL_INIT
  };
};

export const updateBienValorSentimentalDone = (bienValorSentimental) => {
  return {
    type: types.UPDATE_BIEN_VALOR_SENTIMENTAL_DONE,
    bienValorSentimental
  };
};

export const updateBienValorSentimentalFail = (errors) => {
  return {
    type: types.UPDATE_BIEN_VALOR_SENTIMENTAL_FAIL,
    errors
  };
};

export const deleteBienValorSentimentalInit = () => {
  return {
    type: types.DELETE_BIEN_VALOR_SENTIMENTAL_INIT
  };
};

export const deleteBienValorSentimentalDone = (id) => {
  return {
    type: types.DELETE_BIEN_VALOR_SENTIMENTAL_DONE,
    id
  };
};

export const deleteBienValorSentimentalFail = (errors) => {
  return {
    type: types.DELETE_BIEN_VALOR_SENTIMENTAL_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
