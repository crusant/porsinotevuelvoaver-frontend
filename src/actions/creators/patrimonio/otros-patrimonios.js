import * as types from '../../types/patrimonio/otros-patrimonios';

export const getOtrosPatrimoniosInit = () => {
  return {
    type: types.GET_OTROS_PATRIMONIOS_INIT
  };
};

export const getOtrosPatrimoniosDone = (otrosPatrimonios) => {
  return {
    type: types.GET_OTROS_PATRIMONIOS_DONE,
    otrosPatrimonios
  };
};

export const getOtrosPatrimoniosFail = (errors) => {
  return {
    type: types.GET_OTROS_PATRIMONIOS_FAIL,
    errors
  };
};

export const changeOtroPatrimonioInputs = (entity, attribute, value) => {
  return {
    type: types.CHANGE_OTRO_PATRIMONIO_INPUTS,
    entity,
    attribute,
    value
  };
};

export const resetOtroPatrimonioInputs = () => {
  return {
    type: types.RESET_OTRO_PATRIMONIO_INPUTS
  };
};

export const saveOtroPatrimonioInit = () => {
  return {
    type: types.SAVE_OTRO_PATRIMONIO_INIT
  };
};

export const saveOtroPatrimonioDone = (otroPatrimonio) => {
  return {
    type: types.SAVE_OTRO_PATRIMONIO_DONE,
    otroPatrimonio
  };
};

export const saveOtroPatrimonioFail = (errors) => {
  return {
    type: types.SAVE_OTRO_PATRIMONIO_FAIL,
    errors
  };
};

export const loadOtroPatrimonioInputs = (otroPatrimonio) => {
  return {
    type: types.LOAD_OTRO_PATRIMONIO_INPUTS,
    otroPatrimonio
  };
};

export const updateOtroPatrimonioInit = () => {
  return {
    type: types.UPDATE_OTRO_PATRIMONIO_INIT
  };
};

export const updateOtroPatrimonioDone = (otroPatrimonio) => {
  return {
    type: types.UPDATE_OTRO_PATRIMONIO_DONE,
    otroPatrimonio
  };
};

export const updateOtroPatrimonioFail = (errors) => {
  return {
    type: types.UPDATE_OTRO_PATRIMONIO_FAIL,
    errors
  };
};

export const deleteOtroPatrimonioInit = () => {
  return {
    type: types.DELETE_OTRO_PATRIMONIO_INIT
  };
};

export const deleteOtroPatrimonioDone = (id) => {
  return {
    type: types.DELETE_OTRO_PATRIMONIO_DONE,
    id
  };
};

export const deleteOtroPatrimonioFail = (errors) => {
  return {
    type: types.DELETE_OTRO_PATRIMONIO_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
