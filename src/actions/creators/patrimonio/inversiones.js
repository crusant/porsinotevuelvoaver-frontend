import * as types from '../../types/patrimonio/inversiones';

export const getInversionesInit = () => {
  return {
    type: types.GET_INVERSIONES_INIT
  };
};

export const getInversionesDone = (inversiones) => {
  return {
    type: types.GET_INVERSIONES_DONE,
    inversiones
  };
};

export const getInversionesFail = (errors) => {
  return {
    type: types.GET_INVERSIONES_FAIL,
    errors
  };
};

export const changeInversionInputs = (entity, attribute, value) => {
  return {
    type: types.CHANGE_INVERSION_INPUTS,
    entity,
    attribute,
    value
  };
};

export const addBeneficiario = (beneficiario) => {
  return {
    type: types.ADD_BENEFICIARIO,
    beneficiario
  };
};

export const resetBeneficiario = () => {
  return {
    type: types.RESET_BENEFICIARIO
  };
};

export const removeBeneficiario = (index) => {
  return {
    type: types.REMOVE_BENEFICIARIO,
    index
  };
};

export const resetInversionInputs = () => {
  return {
    type: types.RESET_INVERSION_INPUTS
  };
};

export const saveInversionInit = () => {
  return {
    type: types.SAVE_INVERSION_INIT
  };
};

export const saveInversionDone = (inversion) => {
  return {
    type: types.SAVE_INVERSION_DONE,
    inversion
  };
};

export const saveInversionFail = (errors) => {
  return {
    type: types.SAVE_INVERSION_FAIL,
    errors
  };
};

export const loadInversionInputs = (inversion) => {
  return {
    type: types.LOAD_INVERSION_INPUTS,
    inversion
  };
};

export const updateInversionInit = () => {
  return {
    type: types.UPDATE_INVERSION_INIT
  };
};

export const updateInversionDone = (inversion) => {
  return {
    type: types.UPDATE_INVERSION_DONE,
    inversion
  };
};

export const updateInversionFail = (errors) => {
  return {
    type: types.UPDATE_INVERSION_FAIL,
    errors
  };
};

export const deleteInversionInit = () => {
  return {
    type: types.DELETE_INVERSION_INIT
  };
};

export const deleteInversionDone = (id) => {
  return {
    type: types.DELETE_INVERSION_DONE,
    id
  };
};

export const deleteInversionFail = (errors) => {
  return {
    type: types.DELETE_INVERSION_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
