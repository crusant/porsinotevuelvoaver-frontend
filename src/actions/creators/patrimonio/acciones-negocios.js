import * as types from '../../types/patrimonio/acciones-negocios';

export const getAccionesNegociosInit = () => {
  return {
    type: types.GET_ACCIONES_NEGOCIOS_INIT
  };
};

export const getAccionesNegociosDone = (accionesNegocios) => {
  return {
    type: types.GET_ACCIONES_NEGOCIOS_DONE,
    accionesNegocios
  };
};

export const getAccionesNegociosFail = (errors) => {
  return {
    type: types.GET_ACCIONES_NEGOCIOS_FAIL,
    errors
  };
};

export const changeAccionNegocioInputs = (entity, attribute, value) => {
  return {
    type: types.CHANGE_ACCION_NEGOCIO_INPUTS,
    entity,
    attribute,
    value
  };
};

export const addSocio = (socio) => {
  return {
    type: types.ADD_SOCIO,
    socio
  };
};

export const resetSocio = () => {
  return {
    type: types.RESET_SOCIO
  };
};

export const removeSocio = (index) => {
  return {
    type: types.REMOVE_SOCIO,
    index
  };
};

export const resetAccionNegocioInputs = () => {
  return {
    type: types.RESET_ACCION_NEGOCIO_INPUTS
  };
};

export const saveAccionNegocioInit = () => {
  return {
    type: types.SAVE_ACCION_NEGOCIO_INIT
  };
};

export const saveAccionNegocioDone = (accionNegocio) => {
  return {
    type: types.SAVE_ACCION_NEGOCIO_DONE,
    accionNegocio
  };
};

export const saveAccionNegocioFail = (errors) => {
  return {
    type: types.SAVE_ACCION_NEGOCIO_FAIL,
    errors
  };
};

export const loadAccionNegocioInputs = (accionNegocio) => {
  return {
    type: types.LOAD_ACCION_NEGOCIO_INPUTS,
    accionNegocio
  };
};

export const updateAccionNegocioInit = () => {
  return {
    type: types.UPDATE_ACCION_NEGOCIO_INIT
  };
};

export const updateAccionNegocioDone = (accionNegocio) => {
  return {
    type: types.UPDATE_ACCION_NEGOCIO_DONE,
    accionNegocio
  };
};

export const updateAccionNegocioFail = (errors) => {
  return {
    type: types.UPDATE_ACCION_NEGOCIO_FAIL,
    errors
  };
};

export const deleteAccionNegocioInit = () => {
  return {
    type: types.DELETE_ACCION_NEGOCIO_INIT
  };
};

export const deleteAccionNegocioDone = (id) => {
  return {
    type: types.DELETE_ACCION_NEGOCIO_DONE,
    id
  };
};

export const deleteAccionNegocioFail = (errors) => {
  return {
    type: types.DELETE_ACCION_NEGOCIO_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
