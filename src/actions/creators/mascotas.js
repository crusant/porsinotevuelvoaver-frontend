import * as types from '../types/mascotas';

export const getMascotasInit = () => {
  return {
    type: types.GET_MASCOTAS_INIT
  };
};

export const getMascotasDone = (mascotas) => {
  return {
    type: types.GET_MASCOTAS_DONE,
    mascotas
  };
};

export const getMascotasFail = (errors) => {
  return {
    type: types.GET_MASCOTAS_FAIL,
    errors
  };
};

export const changeMascotaInput = (attribute, value) => {
  return {
    type: types.CHANGE_MASCOTA_INPUT,
    attribute,
    value
  };
};

export const resetMascotaInputs = () => {
  return {
    type: types.RESET_MASCOTA_INPUTS
  };
};

export const saveMascotaInit = () => {
  return {
    type: types.SAVE_MASCOTA_INIT
  };
};

export const saveMascotaDone = (mascota) => {
  return {
    type: types.SAVE_MASCOTA_DONE,
    mascota
  };
};

export const saveMascotaFail = (errors) => {
  return {
    type: types.SAVE_MASCOTA_FAIL,
    errors
  };
};

export const editMascota = (mascota) => {
  return {
    type: types.EDIT_MASCOTA,
    mascota
  };
};

export const updateMascotaInit = () => {
  return {
    type: types.UPDATE_MASCOTA_INIT
  };
};

export const updateMascotaDone = (mascota) => {
  return {
    type: types.UPDATE_MASCOTA_DONE,
    mascota
  };
};

export const updateMascotaFail = (errors) => {
  return {
    type: types.UPDATE_MASCOTA_FAIL,
    errors
  };
};

export const deleteMascotaInit = () => {
  return {
    type: types.DELETE_MASCOTA_INIT
  };
};

export const deleteMascotaDone = (id) => {
  return {
    type: types.DELETE_MASCOTA_DONE,
    id
  };
};

export const deleteMascotaFail = (errors) => {
  return {
    type: types.DELETE_MASCOTA_FAIL,
    errors
  };
};

export const downloadMascotaDocumentoInit = () => {
  return {
    type: types.DOWNLOAD_MASCOTA_DOCUMENTO_INIT
  };
};

export const downloadMascotaDocumentoDone = () => {
  return {
    type: types.DOWNLOAD_MASCOTA_DOCUMENTO_DONE
  };
};

export const downloadMascotaDocumentoFail = (errors) => {
  return {
    type: types.DOWNLOAD_MASCOTA_DOCUMENTO_FAIL,
    errors
  };
};

export const resetMascotasErrors = () => {
  return {
    type: types.RESET_MASCOTAS_ERRORS
  };
};
