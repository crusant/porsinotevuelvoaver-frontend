import * as types from '../types/configuracion-envio-pruebas-vida';

export const getConfiguracionEnvioPruebasVidaInit = () => {
  return {
    type: types.GET_CONFIGURACION_ENVIO_PRUEBAS_VIDA_INIT
  };
};

export const getConfiguracionEnvioPruebasVidaDone = (configuracionPruebaVidaUsuario) => {
  return {
    type: types.GET_CONFIGURACION_ENVIO_PRUEBAS_VIDA_DONE,
    configuracionPruebaVidaUsuario
  };
};

export const getConfiguracionEnvioPruebasVidaFail = (errors) => {
  return {
    type: types.GET_CONFIGURACION_ENVIO_PRUEBAS_VIDA_FAIL,
    errors
  };
};

export const changeConfiguracionEnvioPruebasVidaInput = (attribute, value) => {
  return {
    type: types.CHANGE_CONFIGURACION_ENVIO_PRUEBAS_VIDA_INPUT,
    attribute,
    value
  };
};

export const resetConfiguracionEnvioPruebasVidaInputs = () => {
  return {
    type: types.RESET_CONFIGURACION_ENVIO_PRUEBAS_VIDA_INPUTS
  };
};

export const saveConfiguracionEnvioPruebasVidaInit = () => {
  return {
    type: types.SAVE_CONFIGURACION_ENVIO_PRUEBAS_VIDA_INIT
  };
};

export const saveConfiguracionEnvioPruebasVidaDone = (configuracionPruebaVidaUsuario) => {
  return {
    type: types.SAVE_CONFIGURACION_ENVIO_PRUEBAS_VIDA_DONE,
    configuracionPruebaVidaUsuario
  };
};

export const saveConfiguracionEnvioPruebasVidaFail = (errors) => {
  return {
    type: types.SAVE_CONFIGURACION_ENVIO_PRUEBAS_VIDA_FAIL,
    errors
  };
};

export const confirmarPruebaVidaUsuarioInit = () => {
  return {
    type: types.CONFIRMAR_PRUEBA_VIDA_USUARIO_INIT
  };
};

export const confirmarPruebaVidaUsuarioDone = (pruebaVidaUsuario) => {
  return {
    type: types.CONFIRMAR_PRUEBA_VIDA_USUARIO_DONE,
    pruebaVidaUsuario
  };
};

export const confirmarPruebaVidaUsuarioFail = (pruebaVida, errors) => {
  return {
    type: types.CONFIRMAR_PRUEBA_VIDA_USUARIO_FAIL,
    pruebaVida,
    errors
  };
};

export const resetConfiguracionEnvioPruebasVidaErrors = () => {
  return {
    type: types.RESET_CONFIGURACION_ENVIO_PRUEBAS_VIDA_ERRORS
  };
};
