import * as types from '../types/menu-principal';

export const getMenuPrincipalInit = () => {
  return {
    type: types.GET_MENU_PRINCIPAL_INIT
  };
};

export const getMenuPrincipalDone = (data) => {
  return {
    type: types.GET_MENU_PRINCIPAL_DONE,
    data
  };
};

export const getMenuPrincipalFail = (errors) => {
  return {
    type: types.GET_MENU_PRINCIPAL_FAIL,
    errors
  };
};
