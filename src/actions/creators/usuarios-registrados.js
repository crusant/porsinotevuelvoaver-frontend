import * as types from '../types/usuarios-registrados';

export const getUsuariosRegistradosInit = () => {
  return {
    type: types.GET_USUARIOS_REGISTRADOS_INIT
  };
};

export const getUsuariosRegistradosDone = (users) => {
  return {
    type: types.GET_USUARIOS_REGISTRADOS_DONE,
    users
  };
};

export const getUsuariosRegistradosFail = (errors) => {
  return {
    type: types.GET_USUARIOS_REGISTRADOS_FAIL,
    errors
  };
};

export const entregarDatosAccesoInit = () => {
  return {
    type: types.ENTREGAR_DATOS_ACCESO_INIT
  };
};

export const entregarDatosAccesoDone = (user) => {
  return {
    type: types.ENTREGAR_DATOS_ACCESO_DONE,
    user
  };
};

export const entregarDatosAccesoFail = (errors) => {
  return {
    type: types.ENTREGAR_DATOS_ACCESO_FAIL,
    errors
  };
};

export const activarUsuarioRegistradoInit = () => {
  return {
    type: types.ACTIVAR_USUARIO_REGISTRADO_INIT
  };
};

export const activarUsuarioRegistradoDone = (user) => {
  return {
    type: types.ACTIVAR_USUARIO_REGISTRADO_DONE,
    user
  };
};

export const activarUsuarioRegistradoFail = (errors) => {
  return {
    type: types.ACTIVAR_USUARIO_REGISTRADO_FAIL,
    errors
  };
};

export const confirmarPruebaVidaInit = () => {
  return {
    type: types.CONFIRMAR_PRUEBA_VIDA_INIT
  };
};

export const confirmarPruebaVidaDone = (user) => {
  return {
    type: types.CONFIRMAR_PRUEBA_VIDA_DONE,
    user
  };
};

export const confirmarPruebaVidaFail = (errors) => {
  return {
    type: types.CONFIRMAR_PRUEBA_VIDA_FAIL,
    errors
  };
};

export const resetUsuariosRegistrados = () => {
  return {
    type: types.RESET_USUARIOS_REGISTRADOS
  };
};

export const resetUsuariosRegistradosErrors = () => {
  return {
    type: types.RESET_USUARIOS_REGISTRADOS_ERRORS
  };
};
