import * as types from '../types/configuracion-tiempo-respuesta-pruebas-vida';

export const getConfiguracionTiempoRespuestaPruebasVidaInit = () => {
  return {
    type: types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_INIT
  };
};

export const getConfiguracionTiempoRespuestaPruebasVidaDone = (configuracionPruebaVidaUsuario) => {
  return {
    type: types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_DONE,
    configuracionPruebaVidaUsuario
  };
};

export const getConfiguracionTiempoRespuestaPruebasVidaFail = (errors) => {
  return {
    type: types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_FAIL,
    errors
  };
};

export const changeConfiguracionTiempoRespuestaPruebasVidaInput = (attribute, value) => {
  return {
    type: types.CHANGE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_INPUT,
    attribute,
    value
  };
};

export const resetConfiguracionTiempoRespuestaPruebasVidaInputs = () => {
  return {
    type: types.RESET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_INPUTS
  };
};

export const saveConfiguracionTiempoRespuestaPruebasVidaInit = () => {
  return {
    type: types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_INIT
  };
};

export const saveConfiguracionTiempoRespuestaPruebasVidaDone = (configuracionPruebaVidaUsuario) => {
  return {
    type: types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_DONE,
    configuracionPruebaVidaUsuario
  };
};

export const saveConfiguracionTiempoRespuestaPruebasVidaFail = (errors) => {
  return {
    type: types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_FAIL,
    errors
  };
};

export const confirmarPruebaVidaCustodioInit = () => {
  return {
    type: types.CONFIRMAR_PRUEBA_VIDA_CUSTODIO_INIT
  };
};

export const confirmarPruebaVidaCustodioDone = (pruebaVidaCustodio) => {
  return {
    type: types.CONFIRMAR_PRUEBA_VIDA_CUSTODIO_DONE,
    pruebaVidaCustodio
  };
};

export const confirmarPruebaVidaCustodioFail = (pruebaVida, errors) => {
  return {
    type: types.CONFIRMAR_PRUEBA_VIDA_CUSTODIO_FAIL,
    pruebaVida,
    errors
  };
};

export const entregarDatosAccesoCustodioInit = () => {
  return {
    type: types.ENTREGAR_DATOS_ACCESO_CUSTODIO_INIT
  };
};

export const entregarDatosAccesoCustodioDone = (pruebaVida) => {
  return {
    type: types.ENTREGAR_DATOS_ACCESO_CUSTODIO_DONE,
    pruebaVida
  };
};

export const entregarDatosAccesoCustodioFail = (pruebaVida, errors) => {
  return {
    type: types.ENTREGAR_DATOS_ACCESO_CUSTODIO_FAIL,
    pruebaVida,
    errors
  };
};

export const resetConfiguracionTiempoRespuestaPruebasVidaErrors = () => {
  return {
    type: types.RESET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_ERRORS
  };
};
