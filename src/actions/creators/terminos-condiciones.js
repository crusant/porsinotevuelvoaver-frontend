import * as types from '../types/terminos-condiciones';

export const getTerminosCondicionesInit = () => {
  return {
    type: types.GET_TERMINOS_CONDICIONES_INIT
  };
};

export const getTerminosCondicionesDone = (terminosCondiciones) => {
  return {
    type: types.GET_TERMINOS_CONDICIONES_DONE,
    terminosCondiciones
  };
};

export const getTerminosCondicionesFail = (errors) => {
  return {
    type: types.GET_TERMINOS_CONDICIONES_FAIL,
    errors
  };
};

export const changeTerminosCondicionesInputs = (attribute, value) => {
  return {
    type: types.CHANGE_TERMINOS_CONDICIONES_INPUTS,
    attribute,
    value
  };
};

export const updateTerminosCondicionesInit = () => {
  return {
    type: types.UPDATE_TERMINOS_CONDICIONES_INIT
  };
};

export const updateTerminosCondicionesDone = (terminosCondiciones) => {
  return {
    type: types.UPDATE_TERMINOS_CONDICIONES_DONE,
    terminosCondiciones
  };
};

export const updateTerminosCondicionesFail = (errors) => {
  return {
    type: types.UPDATE_TERMINOS_CONDICIONES_FAIL,
    errors
  };
};

export const resetTerminosCondicionesErrors = () => {
  return {
    type: types.RESET_TERMINOS_CONDICIONES_ERRORS
  };
};

export const resetTerminosCondicionesToStartup = () => {
  return {
    type: types.RESET_TERMINOS_CONDICIONES_TO_STARTUP
  };
};
