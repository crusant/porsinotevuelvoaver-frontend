import * as types from '../types/asesores-contactos';

export const getAsesoresContactosInit = () => {
  return {
    type: types.GET_ASESORES_CONTACTOS_INIT
  };
};

export const getAsesoresContactosDone = (asesoresContactos) => {
  return {
    type: types.GET_ASESORES_CONTACTOS_DONE,
    asesoresContactos
  };
};

export const getAsesoresContactosFail = (errors) => {
  return {
    type: types.GET_ASESORES_CONTACTOS_FAIL,
    errors
  };
};

export const changeAsesorContactoInputs = (entity, attribute, value) => {
  return {
    type: types.CHANGE_ASESOR_CONTACTO_INPUTS,
    entity,
    attribute,
    value
  };
};

export const resetAsesorContactoInputs = () => {
  return {
    type: types.RESET_ASESOR_CONTACTO_INPUTS
  };
};

export const saveAsesorContactoInit = () => {
  return {
    type: types.SAVE_ASESOR_CONTACTO_INIT
  };
};

export const saveAsesorContactoDone = (asesorContacto) => {
  return {
    type: types.SAVE_ASESOR_CONTACTO_DONE,
    asesorContacto
  };
};

export const saveAsesorContactoFail = (errors) => {
  return {
    type: types.SAVE_ASESOR_CONTACTO_FAIL,
    errors
  };
};

export const loadAsesorContactoInputs = (asesorContacto) => {
  return {
    type: types.LOAD_ASESOR_CONTACTO_INPUTS,
    asesorContacto
  };
};

export const updateAsesorContactoInit = () => {
  return {
    type: types.UPDATE_ASESOR_CONTACTO_INIT
  };
};

export const updateAsesorContactoDone = (asesorContacto) => {
  return {
    type: types.UPDATE_ASESOR_CONTACTO_DONE,
    asesorContacto
  };
};

export const updateAsesorContactoFail = (errors) => {
  return {
    type: types.UPDATE_ASESOR_CONTACTO_FAIL,
    errors
  };
};

export const deleteAsesorContactoInit = () => {
  return {
    type: types.DELETE_ASESOR_CONTACTO_INIT
  };
};

export const deleteAsesorContactoDone = (id) => {
  return {
    type: types.DELETE_ASESOR_CONTACTO_DONE,
    id
  };
};

export const deleteAsesorContactoFail = (errors) => {
  return {
    type: types.DELETE_ASESOR_CONTACTO_FAIL,
    errors
  };
};

export const resetErrors = () => {
  return {
    type: types.RESET_ERRORS
  };
};
