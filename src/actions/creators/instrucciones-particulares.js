import * as types from '../types/instrucciones-particulares';

export const getInstruccionesParticularesInit = () => {
  return {
    type: types.GET_INSTRUCCIONES_PARTICULARES_INIT
  };
};

export const getInstruccionesParticularesDone = (instruccionesParticulares) => {
  return {
    type: types.GET_INSTRUCCIONES_PARTICULARES_DONE,
    instruccionesParticulares
  };
};

export const getInstruccionesParticularesFail = (errors) => {
  return {
    type: types.GET_INSTRUCCIONES_PARTICULARES_FAIL,
    errors
  };
};

export const changeInstruccionesParticularesInput = (attribute, value) => {
  return {
    type: types.CHANGE_INSTRUCCIONES_PARTICULARES_INPUT,
    attribute,
    value
  };
};

export const saveInstruccionesParticularesInit = () => {
  return {
    type: types.SAVE_INSTRUCCIONES_PARTICULARES_INIT
  };
};

export const saveInstruccionesParticularesDone = (instruccionesParticulares) => {
  return {
    type: types.SAVE_INSTRUCCIONES_PARTICULARES_DONE,
    instruccionesParticulares
  };
};

export const saveInstruccionesParticularesFail = (errors) => {
  return {
    type: types.SAVE_INSTRUCCIONES_PARTICULARES_FAIL,
    errors
  };
};

export const downloadInstruccionesParticularesAudioInit = () => {
  return {
    type: types.DOWNLOAD_INSTRUCCIONES_PARTICULARES_AUDIO_INIT
  };
};

export const downloadInstruccionesParticularesAudioDone = () => {
  return {
    type: types.DOWNLOAD_INSTRUCCIONES_PARTICULARES_AUDIO_DONE
  };
};

export const downloadInstruccionesParticularesAudioFail = (errors) => {
  return {
    type: types.DOWNLOAD_INSTRUCCIONES_PARTICULARES_AUDIO_FAIL,
    errors
  };
};

export const downloadInstruccionesParticularesVideoInit = () => {
  return {
    type: types.DOWNLOAD_INSTRUCCIONES_PARTICULARES_VIDEO_INIT
  };
};

export const downloadInstruccionesParticularesVideoDone = () => {
  return {
    type: types.DOWNLOAD_INSTRUCCIONES_PARTICULARES_VIDEO_DONE
  };
};

export const downloadInstruccionesParticularesVideoFail = (errors) => {
  return {
    type: types.DOWNLOAD_INSTRUCCIONES_PARTICULARES_VIDEO_FAIL,
    errors
  };
};

export const resetInstruccionesParticularesErrors = () => ({
  type: types.RESET_INSTRUCCIONES_PARTICULARES_ERRORS
});
