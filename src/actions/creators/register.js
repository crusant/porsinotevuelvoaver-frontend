import * as types from '../types/auth';

export const registerFreePlanStart = () => {
  return {
    type: types.REGISTER_FREE_PLAN_START
  };
};

export const registerFreePlanDone = (token, user) => {
  return {
    type: types.REGISTER_FREE_PLAN_DONE,
    token,
    user
  };
};

export const registerFreePlanFail = (errors, errorStatus) => {
  return {
    type: types.REGISTER_FREE_PLAN_FAIL,
    errors,
    errorStatus
  };
};

export const paypalCreateOrderStart = () => {
  return {
    type: types.PAYPAL_CREATE_ORDER_START
  };
};

export const paypalCreateOrderFail = (errors) => {
  return {
    type: types.PAYPAL_CREATE_ORDER_FAIL,
    errors
  };
};

export const payWithPayPalAndRegisterInit = () => {
  return {
    type: types.PAY_WITH_PAYPAL_AND_REGISTER_INIT
  };
};

export const payWithPayPalAndRegisterDone = (token, user) => {
  return {
    type: types.PAY_WITH_PAYPAL_AND_REGISTER_DONE,
    token,
    user
  };
};

export const payWithPayPalAndRegisterFail = (errors) => {
  return {
    type: types.PAY_WITH_PAYPAL_AND_REGISTER_INIT,
    errors
  };
};

export const payWithPayPalAndRenewInit = () => {
  return {
    type: types.PAY_WITH_PAYPAL_AND_RENEW_INIT
  };
};

export const payWithPayPalAndRenewDone = (token, user) => {
  return {
    type: types.PAY_WITH_PAYPAL_AND_RENEW_DONE,
    token,
    user
  };
};

export const payWithPayPalAndRenewFail = (errors) => {
  return {
    type: types.PAY_WITH_PAYPAL_AND_RENEW_FAIL,
    errors
  };
};

export const payWithCreditCardAndRegisterInit = () => {
  return {
    type: types.PAY_WITH_CREDIT_CARD_AND_REGISTER_INIT
  };
};

export const payWithCreditCardAndRegisterDone = (token, user) => {
  return {
    type: types.PAY_WITH_CREDIT_CARD_AND_REGISTER_DONE,
    token,
    user
  };
};

export const payWithCreditCardAndRegisterFail = (errors) => {
  return {
    type: types.PAY_WITH_CREDIT_CARD_AND_REGISTER_FAIL,
    errors
  };
};

export const payWithCreditCardAndRenewInit = () => {
  return {
    type: types.PAY_WITH_CREDIT_CARD_AND_RENEW_INIT
  };
};

export const payWithCreditCardAndRenewDone = (token, user) => {
  return {
    type: types.PAY_WITH_CREDIT_CARD_AND_RENEW_DONE,
    token,
    user
  };
};

export const payWithCreditCardAndRenewFail = (errors) => {
  return {
    type: types.PAY_WITH_CREDIT_CARD_AND_RENEW_FAIL,
    errors
  };
};

export const payWithOxxoAndRegisterInit = () => {
  return {
    type: types.PAY_WITH_OXXO_AND_REGISTER_INIT
  };
};

export const payWithOxxoAndRegisterDone = (ticket) => {
  return {
    type: types.PAY_WITH_OXXO_AND_REGISTER_DONE,
    ticket
  };
};

export const payWithOxxoAndRegisterFail = (errors) => {
  return {
    type: types.PAY_WITH_OXXO_AND_REGISTER_FAIL,
    errors
  };
};

export const payWithOxxoAndRenewInit = () => {
  return {
    type: types.PAY_WITH_OXXO_AND_RENEW_INIT
  };
};

export const payWithOxxoAndRenewDone = (token, user, ticket) => {
  return {
    type: types.PAY_WITH_OXXO_AND_RENEW_DONE,
    token,
    user,
    ticket
  };
};

export const payWithOxxoAndRenewFail = (errors) => {
  return {
    type: types.PAY_WITH_OXXO_AND_RENEW_FAIL,
    errors
  };
};

export const passwordResetInit = () => {
  return {
    type: types.PASSWORD_RESET_INIT
  };
};

export const passwordResetDone = () => {
  return {
    type: types.PASSWORD_RESET_DONE
  };
};

export const passwordResetFail = (errors) => {
  return {
    type: types.PASSWORD_RESET_FAIL,
    errors
  };
};
