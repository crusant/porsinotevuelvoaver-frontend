import * as types from '../types/polizas-seguros';

export const getPolizasSegurosInit = () => {
  return {
    type: types.GET_POLIZAS_SEGUROS_INIT
  };
};

export const getPolizasSegurosDone = (polizasSeguros) => {
  return {
    type: types.GET_POLIZAS_SEGUROS_DONE,
    polizasSeguros
  };
};

export const getPolizasSegurosFail = (errors) => {
  return {
    type: types.GET_POLIZAS_SEGUROS_FAIL,
    errors
  };
};

export const changePolizaSeguroInputs = (entity, attribute, value) => {
  return {
    type: types.CHANGE_POLIZA_SEGURO_INPUTS,
    entity,
    attribute,
    value
  };
};

export const addBeneficiario = (beneficiario) => {
  return {
    type: types.ADD_BENEFICIARIO,
    beneficiario
  };
};

export const resetBeneficiario = () => {
  return {
    type: types.RESET_BENEFICIARIO
  };
};

export const removeBeneficiario = (index) => {
  return {
    type: types.REMOVE_BENEFICIARIO,
    index
  };
};

export const changeAsesorInputs = (attribute, value) => {
  return {
    type: types.CHANGE_ASESOR_INPUTS,
    attribute,
    value
  };
};

export const resetPolizaSeguroInputs = () => {
  return {
    type: types.RESET_POLIZA_SEGURO_INPUTS
  };
};

export const savePolizaSeguroInit = () => {
  return {
    type: types.SAVE_POLIZA_SEGURO_INIT
  };
};

export const savePolizaSeguroDone = (polizaSeguro) => {
  return {
    type: types.SAVE_POLIZA_SEGURO_DONE,
    polizaSeguro
  };
};

export const savePolizaSeguroFail = (errors) => {
  return {
    type: types.SAVE_POLIZA_SEGURO_FAIL,
    errors
  };
};

export const editPolizaSeguro = (polizaSeguro) => {
  return {
    type: types.EDIT_POLIZA_SEGURO,
    polizaSeguro
  };
};

export const updatePolizaSeguroInit = () => {
  return {
    type: types.UPDATE_POLIZA_SEGURO_INIT
  };
};

export const updatePolizaSeguroDone = (polizaSeguro) => {
  return {
    type: types.UPDATE_POLIZA_SEGURO_DONE,
    polizaSeguro
  };
};

export const updatePolizaSeguroFail = (errors) => {
  return {
    type: types.UPDATE_POLIZA_SEGURO_FAIL,
    errors
  };
};

export const deletePolizaSeguroInit = () => {
  return {
    type: types.DELETE_POLIZA_SEGURO_INIT
  };
};

export const deletePolizaSeguroDone = (id) => {
  return {
    type: types.DELETE_POLIZA_SEGURO_DONE,
    id
  };
};

export const deletePolizaSeguroFail = (errors) => {
  return {
    type: types.DELETE_POLIZA_SEGURO_FAIL,
    errors
  };
};

export const downloadPolizaSeguroInit = () => {
  return {
    type: types.DOWNLOAD_POLIZA_SEGURO_INIT
  };
};

export const downloadPolizaSeguroDone = () => {
  return {
    type: types.DOWNLOAD_POLIZA_SEGURO_DONE
  };
};

export const downloadPolizaSeguroFail = (errors) => {
  return {
    type: types.DOWNLOAD_POLIZA_SEGURO_FAIL,
    errors
  };
};

export const resetPolizasSegurosErrors = () => {
  return {
    type: types.RESET_POLIZAS_SEGUROS_ERRORS
  };
};
