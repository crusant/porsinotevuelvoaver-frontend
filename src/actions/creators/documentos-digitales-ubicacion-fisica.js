import * as types from '../types/documentos-digitales-ubicacion-fisica';

export const getDocumentosInit = () => {
  return {
    type: types.GET_DOCUMENTOS_INIT
  };
};

export const getDocumentosDone = (documentos) => {
  return {
    type: types.GET_DOCUMENTOS_DONE,
    documentos
  };
};

export const getDocumentosFail = (errors) => {
  return {
    type: types.GET_DOCUMENTOS_FAIL,
    errors
  };
};

export const changeDocumentoInput = (attribute, value) => {
  return {
    type: types.CHANGE_DOCUMENTO_INPUT,
    attribute,
    value
  };
};

export const resetDocumentoInputs = () => {
  return {
    type: types.RESET_DOCUMENTO_INPUTS
  };
};

export const saveDocumentoInit = () => {
  return {
    type: types.SAVE_DOCUMENTO_INIT
  };
};

export const saveDocumentoDone = (documento) => {
  return {
    type: types.SAVE_DOCUMENTO_DONE,
    documento
  };
};

export const saveDocumentoFail = (errors) => {
  return {
    type: types.SAVE_DOCUMENTO_FAIL,
    errors
  };
};

export const editDocumento = (documento) => {
  return {
    type: types.EDIT_DOCUMENTO,
    documento
  };
};

export const updateDocumentoInit = () => {
  return {
    type: types.UPDATE_DOCUMENTO_INIT
  };
};

export const updateDocumentoDone = (documento) => {
  return {
    type: types.UPDATE_DOCUMENTO_DONE,
    documento
  };
};

export const updateDocumentoFail = (errors) => {
  return {
    type: types.UPDATE_DOCUMENTO_FAIL,
    errors
  };
};

export const deleteDocumentoInit = () => {
  return {
    type: types.DELETE_DOCUMENTO_INIT
  };
};

export const deleteDocumentoDone = (id) => {
  return {
    type: types.DELETE_DOCUMENTO_DONE,
    id
  };
};

export const deleteDocumentoFail = (errors) => {
  return {
    type: types.DELETE_DOCUMENTO_FAIL,
    errors
  };
};

export const downloadDocumentoInit = () => {
  return {
    type: types.DOWNLOAD_DOCUMENTO_INIT
  };
};

export const downloadDocumentoDone = () => {
  return {
    type: types.DOWNLOAD_DOCUMENTO_DONE
  };
};

export const downloadDocumentoFail = (errors) => {
  return {
    type: types.DOWNLOAD_DOCUMENTO_FAIL,
    errors
  };
};

export const resetDocumentosErrors = () => {
  return {
    type: types.RESET_DOCUMENTOS_ERRORS
  };
};
