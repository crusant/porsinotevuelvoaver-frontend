import * as types from '../types/preregistros';

export const getPreregistrosExpiradosInit = () => ({
  type: types.GET_PREREGISTROS_EXPIRADOS_INIT
});

export const getPreregistrosExpiradosDone = (preregistros) => ({
  type: types.GET_PREREGISTROS_EXPIRADOS_DONE,
  preregistros
});

export const getPreregistrosExpiradosFail = (errors) => ({
  type: types.GET_PREREGISTROS_EXPIRADOS_FAIL,
  errors
});

export const savePreregistroInit = () => {
  return {
    type: types.SAVE_PREREGISTRO_INIT
  };
};

export const savePreregistroDone = () => {
  return {
    type: types.SAVE_PREREGISTRO_DONE
  };
};

export const savePreregistroFail = (errors) => {
  return {
    type: types.SAVE_PREREGISTRO_FAIL,
    errors
  };
};

export const getPreregistroInit = () => {
  return {
    type: types.GET_PREREGISTRO_INIT
  };
};

export const getPreregistroDone = (preregistro) => {
  return {
    type: types.GET_PREREGISTRO_DONE,
    preregistro
  };
};

export const getPreregistroFail = (errors) => {
  return {
    type: types.GET_PREREGISTRO_FAIL,
    errors
  };
};
