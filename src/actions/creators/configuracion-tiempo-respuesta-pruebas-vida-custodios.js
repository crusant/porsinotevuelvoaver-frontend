import * as types from '../types/configuracion-tiempo-respuesta-pruebas-vida-custodios';

export const getConfiguracionTiempoRespuestaPruebasVidaCustodiosInit = () => {
  return {
    type: types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_INIT
  };
};

export const getConfiguracionTiempoRespuestaPruebasVidaCustodiosDone = (configuracionPruebaVidaCustodio) => {
  return {
    type: types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_DONE,
    configuracionPruebaVidaCustodio
  };
};

export const getConfiguracionTiempoRespuestaPruebasVidaCustodiosFail = (errors) => {
  return {
    type: types.GET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_FAIL,
    errors
  };
};

export const changeConfiguracionTiempoRespuestaPruebasVidaCustodiosInput = (attribute, value) => {
  return {
    type: types.CHANGE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_INPUT,
    attribute,
    value
  };
};

export const resetConfiguracionTiempoRespuestaPruebasVidaCustodiosInputs = () => {
  return {
    type: types.RESET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_INPUTS
  };
};

export const saveConfiguracionTiempoRespuestaPruebasVidaCustodiosInit = () => {
  return {
    type: types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_INIT
  };
};

export const saveConfiguracionTiempoRespuestaPruebasVidaCustodiosDone = (configuracionPruebaVidaCustodio) => {
  return {
    type: types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_DONE,
    configuracionPruebaVidaCustodio
  };
};

export const saveConfiguracionTiempoRespuestaPruebasVidaCustodiosFail = (errors) => {
  return {
    type: types.SAVE_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_FAIL,
    errors
  };
};

export const resetConfiguracionTiempoRespuestaPruebasVidaCustodiosErrors = () => {
  return {
    type: types.RESET_CONFIGURACION_TIEMPO_RESPUESTA_PRUEBAS_VIDA_CUSTODIOS_ERRORS
  };
};
