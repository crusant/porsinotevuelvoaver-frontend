import api from '../../api';
import downloader from '../../helpers/downloader';
import * as creators from '../creators/datos-personales-custodios-red-familiar/red-familiar';
import * as helpers from '../../helpers';

export const getFamiliares = () => {
  return async (dispatch) => {
    dispatch(creators.getFamiliaresInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`red-familiar?token=${token}`);

      dispatch(creators.getFamiliaresDone(response.data));
    } catch (error) {
      dispatch(creators.getFamiliaresFail(helpers.resolveError(error)));
    }
  };
};

export const changeFamiliarInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeFamiliarInput(attribute, value));
  };
};

export const resetFamiliarInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetFamiliarInputs());
  };
};

export const saveFamiliar = (familiar) => {
  return async (dispatch) => {
    dispatch(creators.saveFamiliarInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`red-familiar?token=${token}`, familiar);

      dispatch(creators.saveFamiliarDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveFamiliarFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const editFamiliar = (familiar) => {
  return (dispatch) => {
    dispatch(creators.editFamiliar(familiar));
  };
};

export const getFamiliar = (url) => {
  return async (dispatch) => {
    dispatch(creators.getFamiliarInit());

    try {
      const response = await api.get(`red-familiar/${url}`);

      dispatch(creators.getFamiliarDone(response.data));
    } catch (error) {
      dispatch(creators.getFamiliarFail(helpers.resolveError(error)));
    }
  };
};

export const updateFamiliar = (familiar) => {
  return async (dispatch) => {
    dispatch(creators.updateFamiliarInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`red-familiar/${familiar.id}?token=${token}`, familiar);

      dispatch(creators.updateFamiliarDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateFamiliarFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteFamiliar = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteFamiliarInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`red-familiar/${id}?token=${token}`);
      const familiar = await response.data;

      dispatch(creators.deleteFamiliarDone(familiar.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteFamiliarFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const downloadFamiliarEscrituraLibreAudio = (url, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadFamiliarEscrituraLibreAudioInit());

    try {
      const response = await api.get(`red-familiar/${url}/escritura-libre/audio`, {
        responseType: 'blob'
      });

      if (extension === 'mp3') {
        downloader(response.data, 'audio', 'mp3', { type: 'audio/mpeg-3' });
      } else {
        downloader(response.data, 'audio', extension);
      }

      dispatch(creators.downloadFamiliarEscrituraLibreAudioDone());
    } catch (error) {
      dispatch(creators.downloadFamiliarEscrituraLibreAudioFail(helpers.resolveError(error)));
    }
  };
};

export const downloadFamiliarEscrituraLibreVideo = (url, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadFamiliarEscrituraLibreVideoInit());

    try {
      const response = await api.get(`red-familiar/${url}/escritura-libre/video`, {
        responseType: 'blob'
      });

      downloader(response.data, 'video', extension);

      dispatch(creators.downloadFamiliarEscrituraLibreVideoDone());
    } catch (error) {
      dispatch(creators.downloadFamiliarEscrituraLibreVideoFail(helpers.resolveError(error)));
    }
  };
};

export const downloadFamiliarMiHistoriaAudio = (url, id, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadFamiliarMiHistoriaAudioInit());

    try {
      const response = await api.get(`red-familiar/${url}/mi-historia/${id}/audio`, {
        responseType: 'blob'
      });

      if (extension === 'mp3') {
        downloader(response.data, 'audio', 'mp3', { type: 'audio/mpeg-3' });
      } else {
        downloader(response.data, 'audio', extension);
      }

      dispatch(creators.downloadFamiliarMiHistoriaAudioDone());
    } catch (error) {
      dispatch(creators.downloadFamiliarMiHistoriaAudioFail(helpers.resolveError(error)));
    }
  };
};

export const downloadFamiliarMiHistoriaVideo = (url, id, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadFamiliarMiHistoriaVideoInit());

    try {
      const response = await api.get(`red-familiar/${url}/mi-historia/${id}/video`, {
        responseType: 'blob'
      });

      downloader(response.data, 'video', extension);

      dispatch(creators.downloadFamiliarMiHistoriaVideoDone());
    } catch (error) {
      dispatch(creators.downloadFamiliarMiHistoriaVideoFail(helpers.resolveError(error)));
    }
  };
};

export const downloadFamiliarMensajePostumoAudio = (url, id, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadFamiliarMensajePostumoAudioInit());

    try {
      const response = await api.get(`red-familiar/${url}/mensaje-postumo/${id}/audio`, {
        responseType: 'blob'
      });

      if (extension === 'mp3') {
        downloader(response.data, 'audio', 'mp3', { type: 'audio/mpeg-3' });
      } else {
        downloader(response.data, 'audio', extension);
      }

      dispatch(creators.downloadFamiliarMensajePostumoAudioDone());
    } catch (error) {
      dispatch(creators.downloadFamiliarMensajePostumoAudioFail(helpers.resolveError(error)));
    }
  };
};

export const downloadFamiliarMensajePostumoVideo = (url, id, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadFamiliarMensajePostumoVideoInit());

    try {
      const response = await api.get(`red-familiar/${url}/mensaje-postumo/${id}/video`, {
        responseType: 'blob'
      });

      downloader(response.data, 'video', extension);

      dispatch(creators.downloadFamiliarMensajePostumoVideoDone());
    } catch (error) {
      dispatch(creators.downloadFamiliarMensajePostumoVideoFail(helpers.resolveError(error)));
    }
  };
};

export const downloadFamiliarMensajePostumoFotografia = (url, mensajePostumoId, extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadFamiliarMensajePostumoFotografiaInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`red-familiar/${url}/mensaje-postumo/${mensajePostumoId}/fotografia?token=${token}`, {
        responseType: 'blob'
      });

      helpers.fileDownload(response.data, 'fotografia', extension);

      dispatch(creators.downloadFamiliarMensajePostumoFotografiaDone());
    } catch (error) {
      dispatch(creators.downloadFamiliarMensajePostumoFotografiaFail(helpers.resolveError(error)));
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
