import api from '../../api';
import * as creators from '../creators/datos-personales-custodios-red-familiar/custodios';
import * as helpers from '../../helpers';

export const getCustodios = () => {
  return async (dispatch) => {
    dispatch(creators.getCustodiosInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`custodios?token=${token}`);

      dispatch(creators.getCustodiosDone(response.data));
    } catch (error) {
      dispatch(creators.getCustodiosFail(helpers.resolveError(error)));
    }
  };
};

export const changeCustodioInput = (attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeCustodioInput(attribute, value));
  };
};

export const resetCustodioInputs = () => {
  return (dispatch) => {
    dispatch(creators.resetCustodioInputs());
  };
};

export const saveCustodio = (custodio) => {
  return async (dispatch) => {
    dispatch(creators.saveCustodioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`custodios?token=${token}`, custodio);

      dispatch(creators.saveCustodioDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveCustodioFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const editCustodio = (custodio) => {
  return (dispatch) => {
    dispatch(creators.editCustodio(custodio));
  };
};

export const updateCustodio = (custodio) => {
  return async (dispatch) => {
    dispatch(creators.updateCustodioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.put(`custodios/${custodio.id}?token=${token}`, custodio);

      dispatch(creators.updateCustodioDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.updateCustodioFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const deleteCustodio = (id) => {
  return async (dispatch) => {
    dispatch(creators.deleteCustodioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.delete(`custodios/${id}?token=${token}`);
      const custodio = await response.data;

      dispatch(creators.deleteCustodioDone(custodio.id));
      return true;
    } catch (error) {
      dispatch(creators.deleteCustodioFail(helpers.resolveError(error)));
      return false;
    }
  };
};

export const pruebaVidaCustodio = (id, url) => {
  return async (dispatch) => {
    dispatch(creators.pruebaVidaCustodioInit());

    try {
      const response = await api.get(`custodios/${id}/prueba-vida/${url}`);

      dispatch(creators.pruebaVidaCustodioDone(response.data));
    } catch (error) {
      dispatch(creators.pruebaVidaCustodioFail(helpers.resolveError(error)));
    }
  };
};

export const confirmarPruebaVidaCustodio = (url) => {
  return async (dispatch) => {
    dispatch(creators.confirmarPruebaVidaCustodioInit());

    try {
      const response = await api.post(`custodios/prueba-vida/${url}/confirmar`);

      dispatch(creators.confirmarPruebaVidaCustodioDone(response.data));
    } catch (error) {
      dispatch(creators.confirmarPruebaVidaCustodioFail(helpers.resolveError(error)));
    }
  };
};

export const entregarDatosAccesoCustodio = (url) => {
  return async (dispatch) => {
    dispatch(creators.entregarDatosAccesoCustodioInit());

    try {
      const response = await api.post(`custodios/datos-acceso/${url}/entregar`);

      dispatch(creators.entregarDatosAccesoCustodioDone(response.data));
    } catch (error) {
      dispatch(creators.entregarDatosAccesoCustodioFail(helpers.resolveError(error)));
    }
  };
};

export const resetErrors = () => {
  return (dispatch) => {
    dispatch(creators.resetErrors());
  };
};
