import api from '../../api';
import * as creators from '../creators/datos-personales-custodios-red-familiar/datos-personales';
import * as helpers from '../../helpers';

export const getDatosPersonales = () => {
  return async (dispatch) => {
    dispatch(creators.getDatosPersonalesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`datos-personales?token=${token}`);

      dispatch(creators.getDatosPersonalesDone(response.data));
    } catch (error) {
      dispatch(creators.getDatosPersonalesFail(helpers.resolveErrors(error.response)));
    }
  };
};

export const changeDatosPersonalesInput = (entity, attribute, value) => {
  return (dispatch) => {
    return dispatch(creators.changeDatosPersonalesInput(entity, attribute, value));
  };
};

export const saveDatosPersonales = (data) => {
  return async (dispatch) => {
    dispatch(creators.saveDatosPersonalesInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.post(`datos-personales?token=${token}`, data);

      dispatch(creators.saveDatosPersonalesDone(response.data));
      return true;
    } catch (error) {
      dispatch(creators.saveDatosPersonalesFail(helpers.resolveErrors(error.response)));
      return false;
    }
  };
};

export const downloadDatosPersonalesActaNacimiento = (extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadDatosPersonalesActaNacimientoInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`datos-personales/descargar?token=${token}`, {
        params: {
          tipo: 1
        },
        responseType: 'blob'
      });

      helpers.fileDownload(response.data, 'acta-nacimiento', extension);

      dispatch(creators.downloadDatosPersonalesActaNacimientoDone());
    } catch (error) {
      dispatch(creators.downloadDatosPersonalesActaNacimientoFail('Ocurrió un error al intentar descargar el documento, por favor inténtelo más tarde.'));
    }
  };
};

export const downloadDatosPersonalesActaMatrimonio = (extension) => {
  return async (dispatch) => {
    dispatch(creators.downloadDatosPersonalesActaMatrimonioInit());

    try {
      const token = localStorage.getItem('token');
      const response = await api.get(`datos-personales/descargar?token=${token}`, {
        params: {
          tipo: 2
        },
        responseType: 'blob'
      });

      helpers.fileDownload(response.data, 'acta-matrimonio', extension);

      dispatch(creators.downloadDatosPersonalesActaMatrimonioDone());
    } catch (error) {
      dispatch(creators.downloadDatosPersonalesActaMatrimonioFail('Ocurrió un error al intentar descargar el documento, por favor inténtelo más tarde.'));
    }
  };
};

export const resetDatosPersonalesErrors = () => {
  return (dispatch) => {
    return dispatch(creators.resetDatosPersonalesErrors());
  };
};
