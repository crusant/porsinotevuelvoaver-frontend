import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useBeforeunload } from 'react-beforeunload';
import Landing from './pages/Landing/Landing';
import Login from './pages/Login/Login';
import PasswordReset from './pages/PasswordReset/PasswordReset';
import Loader from './components/UI/Loader/Loader';
import FamiliarMiHistoriaLegado from './pages/Familiar/MiHistoriaLegado/MiHistoriaLegado';
import OtrosDestinatarios from './pages/OtrosDestinatarios/OtrosDestinatarios';
import ConfirmarPruebaVida from './pages/ConfirmarPruebaVida/ConfirmarPruebaVida';
import ConfirmandoPruebaVida from './pages/ConfirmandoPruebaVida/ConfirmandoPruebaVida';
import EntregandoDatosAcceso from './pages/EntregandoDatosAcceso/EntregandoDatosAcceso';
import ConfirmarPruebaVidaUsuario from './pages/ConfirmarPruebaVidaUsuario/ConfirmarPruebaVidaUsuario';
import RouteGuard from './utilities/RouteGuard';
import * as actions from './actions';

const MenuPrincipal = React.lazy(() => import('./pages/MenuPrincipal/MenuPrincipal'));
const ConfiguracionEnvioPruebasVida = React.lazy(() => import('./pages/ConfiguracionEnvioPruebasVida/ConfiguracionEnvioPruebasVida'));
const ConfiguracionTiempoRespuestaPruebasVida = React.lazy(() => import('./pages/ConfiguracionTiempoRespuestaPruebasVida/ConfiguracionTiempoRespuestaPruebasVida'));
const ConfiguracionTiempoRespuestaPruebasVidaCustodios = React.lazy(() => import('./pages/ConfiguracionTiempoRespuestaPruebasVidaCustodios/ConfiguracionTiempoRespuestaPruebasVidaCustodios'));
const UsuariosRegistrados = React.lazy(() => import('./pages/UsuariosRegistrados/UsuariosRegistrados'));
const EnlacesExpirados = React.lazy(() => import('./pages/EnlacesExpirados/EnlacesExpirados'));
const ActualizarAvisoPrivacidad = React.lazy(() => import('./pages/ActualizarAvisoPrivacidad/ActualizarAvisoPrivacidad'));
const ActualizarTerminosCondiciones = React.lazy(() => import('./pages/ActualizarTerminosCondiciones/ActualizarTerminosCondiciones'));

const DatosPersonalesCustodiosRedFamiliar = React.lazy(() => import('./pages/DatosPersonalesCustodiosRedFamiliar/DatosPersonalesCustodiosRedFamiliar'));
const AsesoresContactos = React.lazy(() => import('./pages/AsesoresContactos/AsesoresContactos'));
const PolizasSeguros = React.lazy(() => import('./pages/PolizasSeguros/PolizasSeguros'));
const Patrimonio = React.lazy(() => import('./pages/Patrimonio/Patrimonio'));
const EmpleoPension = React.lazy(() => import('./pages/EmpleoPension/EmpleoPension'));
const UltimosDeseos = React.lazy(() => import('./pages/UltimosDeseos/UltimosDeseos'));
const Mascotas = React.lazy(() => import('./pages/Mascotas/Mascotas'));
const InstruccionesParticulares = React.lazy(() => import('./pages/InstruccionesParticulares/InstruccionesParticulares'));
const CorreoRedesSociales = React.lazy(() => import('./pages/CorreoRedesSociales/CorreoRedesSociales'));
const MensajesPostumos = React.lazy(() => import('./pages/MensajesPostumos/MensajesPostumos'));
const MiHistoriaLegado = React.lazy(() => import('./pages/MiHistoriaLegado/MiHistoriaLegado'));
const DocumentosDigitalesUbicacionFisica = React.lazy(() => import('./pages/DocumentosDigitalesUbicacionFisica/DocumentosDigitalesUbicacionFisica'));
const ChangePassword = React.lazy(() => import('./pages/ChangePassword/ChangePassword'));
const UpdatePlan = React.lazy(() => import('./pages/UpdatePlan/UpdatePlan'));

function App() {
  const timeout = React.useRef(null);
  const user = useSelector((state) => state.auth.user);
  const isAuthenticated = useSelector((state) => state.auth.token !== null);
  const isChecking = useSelector((state) => state.auth.isChecking);
  const dispatch = useDispatch();

  useBeforeunload(async () => {
    await dispatch(actions.logout());
  });

  const isAdministrador = () => {
    return user?.is_administrador;
  };

  const isPlanPremium = () => {
    return user?.suscripcion?.is_premium
        && user?.suscripcion?.is_activo;
  };

  React.useEffect(() => {
    if (isAuthenticated) {
      const expirationDate = new Date(localStorage.getItem('expirationDate'));
      const timeRemaining = (expirationDate - new Date());

      function checkIfSessionExpired() {
        clearTimeout(timeout.current);

        timeout.current = setTimeout(() => {
          dispatch(actions.logout());
          clearTimeout(timeout.current);
        }, timeRemaining);
      }

      checkIfSessionExpired();
    }

    // eslint-disable-next-line
  }, [isAuthenticated]);

  React.useEffect(() => {
    dispatch(actions.checkIfUserIsAuthenticated());

    return () => {
      clearTimeout(timeout.current);
    };

    // eslint-disable-next-line
  }, []);

  if (isChecking) {
    return <Loader title="Cargando el legado de amor y seguridad para tus seres queridos" />;
  }

  return (
    <Router>
      {isAuthenticated ? (
        <React.Suspense fallback={<Loader title="Cargando el legado de amor y seguridad para tus seres queridos" />}>
          <Switch>
            <Route exact path="/usuario/confirmar-prueba-de-vida/:url" component={ConfirmarPruebaVidaUsuario} />
            <Route exact path="/datos-de-acceso/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})" component={EntregandoDatosAcceso} />
            <Route exact path="/confirmar/:id/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})" component={ConfirmarPruebaVida} />
            <Route exact path="/custodio/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})">
              <ConfirmandoPruebaVida custodio />
            </Route>
            <Route exact path="/mi-historia-y-legado/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})" component={FamiliarMiHistoriaLegado} />
            <Route exact path="/mensaje-postumo/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})" component={OtrosDestinatarios} />
            <Route exact path="/usuario/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})" component={ConfirmarPruebaVidaUsuario} />
            <RouteGuard exact path="/configuracion-de-tiempo-de-respuesta-de-pruebas-de-vida-a-custodios" passes={isAdministrador()}>
              <ConfiguracionTiempoRespuestaPruebasVidaCustodios />
            </RouteGuard>
            <RouteGuard exact path="/configuracion-de-tiempo-de-respuesta-a-pruebas-de-vida" passes={isAdministrador()}>
              <ConfiguracionTiempoRespuestaPruebasVida />
            </RouteGuard>
            <RouteGuard exact path="/configuracion-de-envio-de-pruebas-de-vida" passes={isAdministrador()}>
              <ConfiguracionEnvioPruebasVida />
            </RouteGuard>
            <RouteGuard exact path="/enlaces-expirados" passes={isAdministrador()}>
              <EnlacesExpirados />
            </RouteGuard>
            <RouteGuard exact path="/usuarios-registrados" passes={isAdministrador()}>
              <UsuariosRegistrados />
            </RouteGuard>
            <RouteGuard exact path="/actualizar-aviso-de-privacidad" passes={isAdministrador()}>
              <ActualizarAvisoPrivacidad />
            </RouteGuard>
            <RouteGuard exact path="/actualizar-terminos-y-condiciones" passes={isAdministrador()}>
              <ActualizarTerminosCondiciones />
            </RouteGuard>
            <RouteGuard exact path="/cambiar-contrasena" passes={isPlanPremium()}>
              <ChangePassword />
            </RouteGuard>
            <RouteGuard exact path="/planes" passes={true}>
              <UpdatePlan />
            </RouteGuard>
            <RouteGuard exact path="/documentos-digitales-y-ubicacion-fisica" passes={isPlanPremium()}>
              <DocumentosDigitalesUbicacionFisica />
            </RouteGuard>
            <RouteGuard exact path="/mi-historia-y-legado" passes={isPlanPremium()}>
              <MiHistoriaLegado />
            </RouteGuard>
            <RouteGuard exact path="/mensajes-postumos" passes={isPlanPremium()}>
              <MensajesPostumos />
            </RouteGuard>
            <RouteGuard exact path="/correo-y-redes-sociales" passes={isPlanPremium()}>
              <CorreoRedesSociales />
            </RouteGuard>
            <RouteGuard exact path="/instrucciones-particulares" passes={true}>
              <InstruccionesParticulares />
            </RouteGuard>
            <RouteGuard exact path="/mascotas" passes={isPlanPremium()}>
              <Mascotas />
            </RouteGuard>
            <RouteGuard exact path="/deseos-postumos" passes={isPlanPremium()}>
              <UltimosDeseos />
            </RouteGuard>
            <RouteGuard exact path="/empleo-y-pension" passes={isPlanPremium()}>
              <EmpleoPension />
            </RouteGuard>
            <RouteGuard exact path="/patrimonio" passes={isPlanPremium()}>
              <Patrimonio />
            </RouteGuard>
            <RouteGuard exact path="/polizas-de-seguros" passes={isPlanPremium()}>
              <PolizasSeguros />
            </RouteGuard>
            <RouteGuard exact path="/asesores-y-contactos" passes={true}>
              <AsesoresContactos />
            </RouteGuard>
            <RouteGuard exact path="/datos-personales-custodios-y-red-familiar" passes={true}>
              <DatosPersonalesCustodiosRedFamiliar />
            </RouteGuard>
            <RouteGuard exact path="/" passes={true}>
              <MenuPrincipal />
            </RouteGuard>
            <Redirect to="/" />
          </Switch>
        </React.Suspense>
      ) : (
        <Switch>
          <Route exact path="/datos-de-acceso/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})" component={EntregandoDatosAcceso} />
          <Route exact path="/confirmar/:id/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})" component={ConfirmarPruebaVida} />
          <Route exact path="/custodio/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})">
            <ConfirmandoPruebaVida custodio />
          </Route>
          <Route exact path="/usuario/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})" component={ConfirmarPruebaVidaUsuario} />
          <Route exact path="/restablecer-contrasena" component={PasswordReset} />
          <Route exact path="/iniciar-sesion" component={Login} />
          <Route exact path="/mi-historia-y-legado/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})" component={FamiliarMiHistoriaLegado} />
          <Route exact path="/mensaje-postumo/:url([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})" component={OtrosDestinatarios} />
          <Route exact path="/" component={Landing} />
          <Redirect to="/" />
        </Switch>
      )}
    </Router>
  );
}

export default App;
