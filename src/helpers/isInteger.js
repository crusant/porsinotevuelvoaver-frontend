export const isInteger = (value) => {
  return /^[0-9\b]+$/.test(value);
};
