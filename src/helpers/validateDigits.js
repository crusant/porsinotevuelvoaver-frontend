export const validateDigits = (value, digits) => {
  const constraint = new RegExp(`\\b\\d{${digits}}\\b`, 'g');

  return constraint.test(value);
};
