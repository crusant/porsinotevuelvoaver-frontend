export const arrayReplace = (array, index, value) => {
  const newArray = [...array];

  newArray[index] = value;

  return newArray;
};
