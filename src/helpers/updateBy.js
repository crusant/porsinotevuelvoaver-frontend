import _ from 'lodash';

export const updateBy = (array, updater, predicate) => {
  const newArray = [...array];
  const index = _.findIndex(newArray, predicate);

  newArray.splice(index, 1, updater);

  return newArray;
};
