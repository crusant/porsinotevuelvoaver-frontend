export const validateDecimal = (value, precision = 15, scale = 2) => {
  const regExp = new RegExp(`^\\d{0,${precision}}(\\.\\d{1,${scale}})?$`, 'g');

  return value === '' || regExp.test(value);
};
