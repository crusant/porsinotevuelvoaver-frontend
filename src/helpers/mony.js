import currency from 'currency.js';

export const money = (value) => {
  return currency(value, { formatWithSymbol: true }).format();
};
