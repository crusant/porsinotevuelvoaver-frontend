import React from 'react';
import _ from 'lodash';

export const textToParagraph = (text, className) => {
  return _.flow(
    (text) => _.split(text, /\n/),
    (text) => _.map(text, (paragraph, index) => {
      return <p key={index} className={className}>{paragraph}</p>;
    })
  )(text);
};
