export const isFileOrBlob = (value) => {
  return (value instanceof File)
      || (value instanceof Blob)
      ;
};
