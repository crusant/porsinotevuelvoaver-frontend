export const setSessionInLocalStorage = (token, user, expiresIn) => {
  const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);

  localStorage.setItem('token', token);
  localStorage.setItem('user', JSON.stringify(user));
  localStorage.setItem('expirationDate', expirationDate.toString());
};
