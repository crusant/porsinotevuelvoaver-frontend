export const isBlob = (value) => {
  return value instanceof Blob;
};
