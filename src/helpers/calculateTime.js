import { padStart } from './padStart';

export const calculateTime = (seconds) => {
  const hour = Math.floor(seconds / 3600);
  const minute = Math.floor((seconds - (hour * 3600)) / 60);
  const second = Math.floor(seconds - (hour * 3600) - (minute * 60));

  return `${padStart(hour, 2, 0)}:${padStart(minute, 2, 0)}:${padStart(second, 2, 0)}`;
};
