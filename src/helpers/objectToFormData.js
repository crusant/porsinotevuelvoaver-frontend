import isNull from 'lodash/isNull';

const buildFormData = (formData, data, parentKey) => {
  const isObject = (typeof data === 'object');
  const isDate = (data instanceof Date);
  const isFile = (data instanceof File);
  const isBlob = (data instanceof Blob);

  if (data && isObject && !isDate && !isFile && !isBlob) {
    Object.keys(data).forEach((key) => {
      buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
    });
  } else {
    const value = isNull(data) ? '' : data;

    formData.append(parentKey, value);
  }
};

export const objectToFormData = (data) => {
  const formData = new FormData();

  buildFormData(formData, data);

  return formData;
};
