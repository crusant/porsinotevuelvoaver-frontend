export const padStart = (value, size, fill) => {
  let s = String(value);

  while (s.length < (size || 2)) {
      s = `${fill}${s}`;
  }

  return s;
};
