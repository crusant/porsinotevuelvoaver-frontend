export const getDecrementNumbers = (start, finish = 0) => {
  const numbers = [];
  
  for (let index = start; index >= finish; index--) {
    numbers.push(index);
  }

  return numbers;
};
