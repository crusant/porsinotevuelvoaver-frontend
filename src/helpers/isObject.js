export const isObject = (value) => {
  const type = typeof value;

  // eslint-disable-next-line
  return type === 'function' || type === 'object' && !!value;
};
