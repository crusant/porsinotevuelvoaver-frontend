import get from 'lodash/get';
import isNull from 'lodash/isNull';
import isPlainObject from 'lodash/isPlainObject';

export const resolveError = (error) => {
  const message = get(error, 'response.data.message', null);
  const errors = get(error, 'response.data.errors', null);

  if (isPlainObject(errors)) {
    return Object.keys(errors)
      .reduce((obj, key) => {
        obj[key] = errors[key][0];

        return obj;
      }, {});
  }

  if (!isNull(message) && message !== '') {
    return message;
  }

  if (!isNull(message) && message === '') {
    return get(error, 'response.statusText');
  }

  if (errors) {
    return errors;
  }

  return error;
};
