const downloader = (file, name, extension, options = {}) => {
  const blob = new Blob([file], options);

  const url = (window.URL && window.URL.createObjectURL)
    ? window.URL.createObjectURL(blob)
    : window.webkitURL.createObjectURL(blob);

  const link = document.createElement('a');

  link.setAttribute('href', url);
  link.setAttribute('download', `${name}.${extension}`);

  if (!link.download) {
    link.setAttribute('target', '_blank');
  }

  link.style.display = 'none';
  document.body.appendChild(link);
  link.click();

  setTimeout(() => {
    document.body.removeChild(link);
    window.URL.revokeObjectURL(url);
  }, 200);
};

export default downloader;
