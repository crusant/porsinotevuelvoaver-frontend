import { isInteger } from './isInteger';

export const validateInteger = (val) => {
  return val === '' || isInteger(val);
};
