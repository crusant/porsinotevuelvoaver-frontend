export const isFile = (value) => {
  return value instanceof File;
};
