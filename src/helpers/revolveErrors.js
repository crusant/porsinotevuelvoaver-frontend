import _ from 'lodash';
import { httpStatus } from '../constants';

export const resolveErrors = (response, toList = false) => {
  const errors = _.get(response, 'data.errors', '');
  const status = _.get(response, 'status', '');

  if (status === httpStatus.UNPROCESSABLE_ENTITY) {
    if (toList) {
      return Object.keys(errors)
        .reduce((obj, key) => {
          obj.push(errors[key][0]);

          return obj;
        }, []);
    }

    return Object.keys(errors)
      .reduce((obj, key) => {
        obj[key] = errors[key][0];

        return obj;
      }, {});
  }

  if (status === httpStatus.INTERNAL_SERVER_ERROR) {
    return errors;
  }

  return 'Ocurrió un error inesperado, verifica por favor si la acción que realizaste se completó con éxito.';
};
