export const validateMax = (value, max) => {
  return value === '' || (value <= max && String(value).length <= String(max).length);
};
