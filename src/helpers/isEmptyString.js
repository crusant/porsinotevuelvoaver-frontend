export const isEmptyString = (value) => {
  return value === '';
};
