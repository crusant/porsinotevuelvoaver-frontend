export const truncate = (n, length) => {
  const extension = n.substring(n.lastIndexOf('.') + 1, n.length).toLowerCase();
  let filename = n.replace(`.${extension}`, '');

  if(filename.length <= length) {
      return n;
  }

  filename = filename.substr(0, length) + (n.length > length ? '[...]' : '');
  return `${filename}.${extension}`;
};
