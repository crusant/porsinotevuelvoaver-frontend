export const fileDownload = (file, name, extension) => {
  const blob = new Blob([file]);
  const url = (window.URL && window.URL.createObjectURL) ? window.URL.createObjectURL(blob) : window.webkitURL.createObjectURL(blob);;
  const link = document.createElement('a');
  link.href = url;
  link.setAttribute('download', `${name}.${extension}`);

  if (typeof link.download === 'undefined') {
    link.setAttribute('target', '_blank');
  }

  link.style.display = 'none';
  document.body.appendChild(link);
  link.click();

  setTimeout(() => {
    document.body.removeChild(link);
    window.URL.revokeObjectURL(url);
  }, 200);
};
